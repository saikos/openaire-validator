package eu.dnetlib.validator.service.impls.listeners;


import org.apache.log4j.Logger;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

public class RegistrationListener extends AbstractValidatorListener {

	private String datasourceId;
	private String interfaceId;
	private String providerUrl = null;
	private String userMail;

	private static Logger logger = Logger.getLogger(RegistrationListener.class);

	public RegistrationListener() {
		logger.error("Creating new registration listener");
	}



	public synchronized void jobSuccess(int jobId,  Map<String, Object> jobContext) {
		logger.info("Preregistration job "+jobId+" finished");
		try {
			logger.debug("Sending results to provide @ " + providerUrl +"/validator/complete");
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
			map.add("interfaceId", interfaceId);
			map.add("repoId",datasourceId);
			map.add("jobId",Integer.toString(jobId));
			map.add("issuerEmail",userMail);
			map.add("isUpdate",Boolean.toString(updateExisting));

			// isUsccess: the job finished succesfully, regardless of validation scores.

			map.add("isSuccess","true");
			map.add("scoreUsage", Integer.toString(score_usage));
			map.add("scoreContent",Integer.toString(score_content));

			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
			RestTemplate restTemplate = new RestTemplate();

			ResponseEntity<String> response = restTemplate.postForEntity( providerUrl + "/validator/complete", request , String.class );

		} catch (Exception e) {
			logger.error("", e);
		}

	}

	public synchronized void jobFailure(int jobId,  Map<String, Object> jobContext, Throwable t) {
		logger.info("Pregistration job "+jobId+" failed with exception: "+t.getMessage());
		try {
			logger.debug("Sending results to provide @ " + providerUrl + "/validator/complete");
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			map.add("interfaceId", this.interfaceId);
			map.add("repoId", this.datasourceId);
			map.add("jobId", this.jobId + "");
			map.add("issuerEmail", this.userMail);
			map.add("isUpdate", this.updateExisting + "");

			// The job failed to complete. Validator error or Repository is completely dead and cannot be validated.
			map.add("isSuccess", "false");
			map.add("scoreUsage", score_usage + "");
			map.add("scoreContent", score_content + "");

			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
			RestTemplate restTemplate = new RestTemplate();

			ResponseEntity<String> response = restTemplate.postForEntity(providerUrl + "/validator/complete", request, String.class);

		} catch (Exception e) {
			logger.error("", e);
		}
	}

	public String getProviderUrl() {
		return providerUrl;
	}

	public void setProviderUrl(String providerUrl) {
		this.providerUrl = providerUrl;
	}


	public int getJobId() {
		return jobId;
	}

	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	public String getUserMail() {
		return userMail;
	}

	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}

	public String getDatasourceId() {
		return datasourceId;
	}

	public void setDatasourceId(String datasourceId) {
		this.datasourceId = datasourceId;
	}

	public String getInterfaceId() {
		return interfaceId;
	}

	public void setInterfaceId(String interfaceId) {
		this.interfaceId = interfaceId;
	}

	public boolean isUpdateExisting() {
		return updateExisting;
	}

	public void setUpdateExisting(boolean updateExisting) {
		this.updateExisting = updateExisting;
	}

}
