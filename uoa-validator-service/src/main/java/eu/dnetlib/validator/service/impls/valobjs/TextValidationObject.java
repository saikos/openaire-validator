package eu.dnetlib.validator.service.impls.valobjs;

import eu.dnetlib.validator.engine.execution.ValidationObject;

/**
 * Indicates that a Validatiob Object is actually plain text.
 * @author Manos Karvounis
 */
public interface TextValidationObject extends ValidationObject {

	public String getContentAsText();
	
}
