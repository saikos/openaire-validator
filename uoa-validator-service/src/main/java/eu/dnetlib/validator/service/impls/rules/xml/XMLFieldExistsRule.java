package eu.dnetlib.validator.service.impls.rules.xml;

import java.util.Properties;

import org.w3c.dom.NodeList;

import eu.dnetlib.validator.engine.data.DataException;
import eu.dnetlib.validator.engine.data.Rule;
import eu.dnetlib.validator.engine.data.RuleException;
import eu.dnetlib.validator.engine.execution.ValidationObject;
import eu.dnetlib.validator.service.impls.valobjs.XMLTextValidationObject;

public class XMLFieldExistsRule extends Rule implements XMLRule {

	private static final long serialVersionUID = -1532674779961679673L;

	public XMLFieldExistsRule(Properties pros, int id) {
		super(pros, id);
	}

	@Override
	public boolean apply(ValidationObject obj) throws RuleException {
		XMLTextValidationObject tobj = (XMLTextValidationObject) obj;
		NodeList nodes;
		try {
			nodes = tobj.getNodes(this.pros.getProperty(XPATH));
		} catch (DataException e) {
			log.error("", e);
			return false;
		}
		
		int success = 0, all = nodes.getLength();
		if(nodes.getLength() == 0)
			return false;
		for(int i=0; i<nodes.getLength(); i++)
			if(!nodes.item(i).getNodeValue().trim().equals(""))
				success++;
		
		String successConditions = this.pros.getProperty(SUCCESS);
		return Utils.success(successConditions, success, all);
	}
}
