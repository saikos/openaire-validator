package eu.dnetlib.validator.service.impls.rules;

import java.util.Properties;

import eu.dnetlib.validator.engine.data.Rule;

/**
 * Represents a rule that checks if an object has a value matching the Regular Expression {@link RegularExpressionRule#REGEXP}
 * @author Manos Karvounis
 *
 */
public abstract class RegularExpressionRule extends Rule {

	public static final String REGEXP = "regexp";
	
	private static final long serialVersionUID = 2159775453207438509L;

	public RegularExpressionRule(Properties pros, int id) {
		super(pros, id);
	}
	
}
