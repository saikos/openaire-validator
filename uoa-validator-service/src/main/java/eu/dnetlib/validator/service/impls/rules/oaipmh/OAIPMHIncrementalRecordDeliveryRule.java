package eu.dnetlib.validator.service.impls.rules.oaipmh;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import org.w3c.dom.NodeList;

import eu.dnetlib.validator.engine.data.DataException;
import eu.dnetlib.validator.engine.data.Rule;
import eu.dnetlib.validator.engine.data.RuleException;
import eu.dnetlib.validator.engine.execution.ValidationObject;
import eu.dnetlib.validator.service.impls.providers.OAIPMHSinglePageVerbProvider;
import eu.dnetlib.validator.service.impls.valobjs.XMLTextValidationObject;

/**
 * Checks if an OAI-PMH repositories handles Incremental Record Delivery (from,
 * until) correctly, i.e., the records retrieved using from, until have
 * datestamps in that period. The Validation Object used in
 * {@link OAIPMHDateGranularityRule#apply(ValidationObject)} must be provided
 * from a {@link OAIPMHSinglePageVerbProvider} that issues a simple
 * 'ListIdentifiers&metadataPrefix=oai_dc&from=...&until=...' verb (no need for
 * full harvesting)
 * 
 * @author Manos Karvounis
 */
public class OAIPMHIncrementalRecordDeliveryRule extends Rule {

	public OAIPMHIncrementalRecordDeliveryRule(Properties pros, int id) {
		super(pros, id);
	}

	private static final long serialVersionUID = -5193298020858675978L;

	/**
	 * The from used to retrieve the Validation Object.
	 */
	public static final String FROM = "from";
	/**
	 * The until used to retrieve the Validation Object.
	 */
	public static final String UNTIL = "until";

	@Override
	public boolean apply(ValidationObject obj) throws RuleException {
		XMLTextValidationObject tobj = (XMLTextValidationObject) obj;
		NodeList nodes;
		try {
			nodes = tobj.getNodes("/OAI-PMH/ListIdentifiers/header/datestamp/text()");
		} catch (DataException e) {
			log.error("", e);
			return false;
		}

		try {
			String from = this.pros.getProperty(FROM);
			String until = this.pros.getProperty(UNTIL);
			for (int i = 0; i < nodes.getLength(); i++) {
				String datestamp = nodes.item(i).getNodeValue();

				log.debug("Incremental Record Delivery Rule. Datestamp: " + datestamp);

				SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				Date dateFrom = null, dateUntil = null, dateCurrent = null;
				dateFrom = df2.parse(from);
				dateUntil = df2.parse(until);
				try {
					dateCurrent = df1.parse(datestamp);
				} catch (Exception ex) {
					try {
						dateCurrent = df2.parse(datestamp);
					} catch (Exception e) {
						return false;
					}
				}
				Calendar fromCal = Calendar.getInstance();
				Calendar untilCal = Calendar.getInstance();
				Calendar currentCal = Calendar.getInstance();
				fromCal.setTime(dateFrom);
				untilCal.setTime(dateUntil);
				currentCal.setTime(dateCurrent);
				long millsLeast = fromCal.getTimeInMillis();
				long millsMost = untilCal.getTimeInMillis();
				long millsCurrent = currentCal.getTimeInMillis();
				if (millsCurrent < millsLeast || millsCurrent > millsMost)
					return false;
			}
			return true;
		} catch (Exception e) {
			log.error("", e);
			throw new RuleException(e.getMessage());
		}
	}

}
