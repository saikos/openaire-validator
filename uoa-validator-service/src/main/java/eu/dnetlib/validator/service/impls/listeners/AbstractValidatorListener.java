package eu.dnetlib.validator.service.impls.listeners;

import eu.dnetlib.validator.commons.dao.jobs.JobsDAO;
import eu.dnetlib.validator.engine.execution.CompletedTask;
import eu.dnetlib.validator.engine.execution.JobListener;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

public abstract class AbstractValidatorListener implements JobListener {

    private static Logger logger = Logger.getLogger(AbstractValidatorListener.class);
    private JobsDAO jobsDao;
    protected int jobId;
    protected boolean updateExisting;
    private int totalJobs;
    private int jobsFinished = 0;

    protected int score_content = 0, score_usage = 0;


    public AbstractValidatorListener() {
        logger.error("Creating new validator listener");
    }

    protected abstract void jobSuccess(int jobId,  Map<String, Object> jobContext);

    protected abstract void jobFailure(int jobId,  Map<String, Object> jobContext, Throwable t);


    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public synchronized void finished(int jobId, Map<String, Object> jobContext) {

        try{
            jobsFinished++;
            if (jobContext.containsKey("score_content")) {
                score_content = (Integer) jobContext.get("score_content");
            } else if (jobContext.containsKey("score_usage")) {
                score_usage = (Integer) jobContext.get("score_usage");
            }
            if (jobsFinished == totalJobs) {
                logger.debug("all jobs for registration finished");
                jobsDao.setTotalJobFinished(jobId, null, false);
                logger.debug("id:"+jobId+ "c: " + score_content + " u:" + score_usage);
                jobSuccess((Integer) jobContext.get("jobSubmittedId"), jobContext);
            } else {
                logger.debug("not all jobs finished yet. Waiting "+ (totalJobs-jobsFinished) + " job(s) to finish" );
            }
        } catch (Exception e) {
            logger.error("Error while finalizing successfull registration job");
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public synchronized void failed(int jobId, Map<String, Object> jobContext, Throwable t) {

        try{
            jobsDao.setTotalJobFinished(jobId, t.getMessage(), true);
            this.jobFailure((Integer) jobContext.get("jobSubmittedId"), jobContext,t);
        } catch (Exception e) {
            logger.error("Error while finalizing failed registration job");
        }
    }

    @Override
    public synchronized void currentResults(List<CompletedTask> tasks, int jobId,
                                            Object object, Map<String, Object> recordContext, Throwable t) {
        // We don't care about partial results.
    }

    @Override
    public synchronized void currentResults(List<CompletedTask> tasks, int jobId,
                                            Object object, Map<String, Object> recordContext) {
        // We don't care about partial results.
    }

    public JobsDAO getJobsDao() {
        return jobsDao;
    }

    public void setJobsDao(JobsDAO jobsDao) {
        this.jobsDao = jobsDao;
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public boolean isUpdateExisting() {
        return updateExisting;
    }

    public void setUpdateExisting(boolean updateExisting) {
        this.updateExisting = updateExisting;
    }

    public int getTotalJobs() {
        return totalJobs;
    }

    public void setTotalJobs(int totalJobs) {
        this.totalJobs = totalJobs;
    }

    public int getJobsFinished() {
        return jobsFinished;
    }

    public void setJobsFinished(int jobsFinished) {
        this.jobsFinished = jobsFinished;
    }

    public int getScore_content() {
        return score_content;
    }

    public void setScore_content(int score_content) {
        this.score_content = score_content;
    }

    public int getScore_usage() {
        return score_usage;
    }

    public void setScore_usage(int score_usage) {
        this.score_usage = score_usage;
    }
}
