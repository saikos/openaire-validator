package eu.dnetlib.validator.service.impls.valobjs;

import java.io.StringWriter;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import eu.dnetlib.validator.engine.data.DataException;
/**
 * Indicates that a Validation Object is actually an xml text.
 * @author Manos Karvounis
 */
public class XMLTextValidationObject implements TextValidationObject {

	private transient Logger log = Logger.getLogger(XMLTextValidationObject.class);
	
	private String id = "";
	private String status;
	private final Document doc;
	private XPathFactory factory = XPathFactory.newInstance();

	public XMLTextValidationObject(Document doc) {
		super();
		this.doc = doc;
	}

	@Override
	public String getId() {
		return this.id;
	}
	
	@Override
	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public Object getContentAsObject() {
		return this.doc;
	}

	@Override
	public String getContentAsText() {
		StringWriter sw = new StringWriter();
	    Transformer transformer = null;
		try {
			transformer = TransformerFactory.newInstance().newTransformer();
		    Source source = new DOMSource(doc);
		    Result output = new StreamResult(sw);
			transformer.transform(source, output);
		} catch (TransformerConfigurationException e) {
			log.error("", e);
		} catch (TransformerFactoryConfigurationError e) {
			log.error("", e);
		} catch (TransformerException e) {
			log.error("", e);
		}
	    return sw.toString();
	}
	
	public Document getDocument() {
		return this.doc;
	}

	/**
	 * Get the nodes retrieved by issuing the given XPATH query.
	 * @param xpath
	 * @return
	 * @throws DataException
	 */
	public NodeList getNodes(String xpath) throws DataException {
		log.debug("xpath: "+xpath);
		try {
			XPath xxpath = factory.newXPath();
			XPathExpression expr = xxpath.compile(xpath);
			return (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			throw new DataException();
		}
	}

}
