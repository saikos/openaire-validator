package eu.dnetlib.validator.service.impls.rules.xml;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Properties;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import eu.dnetlib.validator.engine.data.DataException;
import eu.dnetlib.validator.engine.data.RuleException;
import eu.dnetlib.validator.engine.execution.ValidationObject;
import eu.dnetlib.validator.service.impls.rules.ValidUrlRule;
import eu.dnetlib.validator.service.impls.valobjs.XMLTextValidationObject;

public class XMLValidURL extends ValidUrlRule implements XMLRule {

	private static final long serialVersionUID = -4236498857969746873L;

	public XMLValidURL(Properties pros, int id) {
		super(pros, id);
	}
	
	@Override
	public boolean apply(ValidationObject obj) throws RuleException {
		XMLTextValidationObject tobj = (XMLTextValidationObject) obj;
		NodeList nodes;
		try {
			nodes = tobj.getNodes(this.pros.getProperty(XPATH));
		} catch (DataException e) {
			log.error("", e);
			return false;
		}
		
		int success = 0, all = nodes.getLength();
		for(int i=0; i<nodes.getLength(); i++) {
			Node node = nodes.item(i);
			String val = node.getNodeValue();
			
			log.debug("XML Valid url. Node: "+val);
			
			URLConnection connection = null;
			HttpURLConnection httpConnection = null;
			try {
				URL url = new URL(val);
				connection = url.openConnection();
				if (connection instanceof HttpURLConnection) {
					httpConnection = (HttpURLConnection) connection;
					httpConnection.connect();
					InputStream is = httpConnection.getInputStream();

					byte[] buffer = new byte[128];
					int numberOfBytes = is.read(buffer);
					if (numberOfBytes != -1) {
						is.close();
						success++;
					}
				}
			} catch (Exception e) {
				continue;
			} finally {
				if (connection != null)
					httpConnection.disconnect();
			}
		}
		
		String successConditions = this.pros.getProperty(SUCCESS);
		return Utils.success(successConditions, success, all);
	}

}
