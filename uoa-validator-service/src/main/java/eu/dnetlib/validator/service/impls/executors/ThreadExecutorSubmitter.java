package eu.dnetlib.validator.service.impls.executors;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import eu.dnetlib.validator.engine.data.Provider;
import eu.dnetlib.validator.engine.data.Provider.ProviderException;
import eu.dnetlib.validator.engine.data.ResultSet;
import eu.dnetlib.validator.engine.data.Rule;
import eu.dnetlib.validator.engine.execution.Executor;
import eu.dnetlib.validator.engine.execution.Job;
import eu.dnetlib.validator.engine.execution.JobListener;
import eu.dnetlib.validator.engine.execution.Task;
import eu.dnetlib.validator.engine.execution.TaskList;
import eu.dnetlib.validator.engine.execution.ValidationObject;
import eu.dnetlib.validator.service.impls.providers.DnetProvider;
import eu.dnetlib.validator.service.impls.providers.OAIPMHRecordProvider;

/**
 * Retrieves the Validation Objects of a {@link Job} using the contained Provider.
 * A new Task in created for each Validation Object and all the rules contained in the Job.
 * Each task is submitted for execution at an {@link Executor}
 * @author Manos Karvounis
 * @author Nikon Gasparis
 *
 */
@Deprecated
public class ThreadExecutorSubmitter extends ExecutorSubmitter {

	private transient Logger log = Logger.getLogger(ThreadExecutorSubmitter.class);
	
	public final Executor executor;
	public final long timeout, generalTimeout;
	public final String set;
	
	private Map<String,Object> recordContext;
	
	public ThreadExecutorSubmitter(int jobId, Set<Rule> rules, String set, Provider provider, JobListener validatorListener, Executor executor, long timeout, long generalTimeout, JobListener... listeners) {
		super(jobId, rules, set, provider, new HashMap<String, Object>() , validatorListener, listeners);
		log.debug("Creating a new executor submitter with timeout "+timeout+" and general timeout "+generalTimeout);
		this.executor = executor;
		this.timeout = timeout;
		this.generalTimeout = generalTimeout;
		this.set = set;

	}

	@Override
	public void submit() throws ExecutionException {
		long elapsed = 0;
		int count = 0;
		int record_limit = -1;
		log.debug("Submitting job");
		if (provider.getConfiguration().getProperty(OAIPMHRecordProvider.RECORDS) != null) {
			if (!provider.getConfiguration().getProperty(OAIPMHRecordProvider.RECORDS).trim().equals("-1")) {
				record_limit = Integer.parseInt(provider.getConfiguration().getProperty(OAIPMHRecordProvider.RECORDS).trim());
			}
		}
		if (provider.getConfiguration().getProperty(DnetProvider.RECORDS) != null) {
			if (!provider.getConfiguration().getProperty(DnetProvider.RECORDS).trim().equals("-1")) {
				record_limit = Integer.parseInt(provider.getConfiguration().getProperty(DnetProvider.RECORDS).trim());
			}
		}		
		log.debug("Number of records to validate: " + record_limit+ " (-1 for all)");

		Boolean crisFlag = false;
		if (set != null && set.contains("_cris_"))
			crisFlag = true;
			
		boolean success = false;
		List<Task> tasks = new ArrayList<Task>();
		ResultSet<ValidationObject> ivobjs = null;
		try {
			if (!crisFlag) {
				log.debug("request to get validation objects without set..");
				ivobjs = provider.getValidationObjects();
			} else {
				log.debug("request to get validation objects with set: " + set);
				ivobjs = provider.getValidationObjects(set);
			}
		} catch (ProviderException e1) {
			log.error("Error while getting validation objects from provider", e1);
			for(JobListener listener : listeners){
				listener.failed(jobId, this.jobContext, e1);				
			}
		}
		try {
			while((count<record_limit || record_limit == -1) && ivobjs.next()) {
				this.recordContext = new HashMap<String, Object>();
				tasks.clear();
				ValidationObject vobj = ivobjs.get();
				
				log.debug("Checking if rules will be applied on object " + vobj.getId());
				if (vobj.getStatus() != null && vobj.getStatus().equalsIgnoreCase("deleted")) {
		            	log.debug("Object is deleted and will be ignored");
		            	continue;
				}
				
				if (crisFlag && ( vobj.getId().contains("cfEAddr") || vobj.getId().contains("cfEquip"))) {
					log.debug("ignoring cfEaddr and cfEquip records from persons and datasets");
					continue;
				}
				
				log.debug("Applying rules on object " + vobj.getId());
				count++;
				for(Rule rule : rules) {
					if (rule != null) {
						rule.setValObjId(vobj.getId());
						rule.setProvider(provider);
						Task task = new Task(vobj, rule);
						tasks.add(task);
					}
				}
				
				TaskList ltasks = new TaskList(tasks);
				try {
					long time1 = Calendar.getInstance().getTimeInMillis() / 1000;
					executor.execute(ltasks).get(timeout, TimeUnit.SECONDS);
					long time2 = Calendar.getInstance().getTimeInMillis() / 1000;
					log.debug("Task execution took "+((time2 - time1))+" seconds");
					elapsed += time2 - time1;
					log.debug("Elapsed time till now is "+elapsed+" seconds");
					if(elapsed > this.generalTimeout)
						throw new ExecutionException("Job timed out");
					for(JobListener listener : listeners){
						listener.currentResults(ltasks.getCtasks(), jobId, vobj.getContentAsObject(), this.recordContext );
					}
					success = true;
				} catch(Exception e) {
					log.error("an error occured while executing tasks: ", e);
					for(JobListener listener : listeners){
						listener.currentResults(ltasks.getCtasks(), jobId, vobj.getContentAsObject(), this.recordContext, e);
					}
				}
					
			}
		} catch (Exception e) {
			log.error("data error", e);
			for(JobListener listener : listeners){
				listener.failed(jobId, this.jobContext, e);
			}
		}
		if(success) {
			for(JobListener listener : listeners){
				listener.finished(jobId, this.jobContext);
			}
		} else {
			log.error("an error occured");
			for(JobListener listener : listeners){
				if (ivobjs.getError() == null)
					listener.failed(jobId, this.jobContext, new ExecutionException("All tasks failed"));
				else {
					listener.failed(jobId, this.jobContext, new ExecutionException(ivobjs.getError()));
					log.error("an error occured.", new ExecutionException(ivobjs.getError()));
				}
			}
		}
	}

}
