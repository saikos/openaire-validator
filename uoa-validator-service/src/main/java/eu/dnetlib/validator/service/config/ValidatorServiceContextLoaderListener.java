package eu.dnetlib.validator.service.config;

import java.util.Properties;

import javax.servlet.ServletContext;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;

public class ValidatorServiceContextLoaderListener extends ContextLoaderListener {
	private static Logger logger = Logger.getLogger(ValidatorServiceContextLoaderListener.class);


	public ValidatorServiceContextLoaderListener() {
		super();
	}

	public ValidatorServiceContextLoaderListener(WebApplicationContext context) {
		super(context);
	}

	@Override
	protected WebApplicationContext createWebApplicationContext(
			ServletContext servletContext)
			throws BeansException {
		logger.debug("Creating web application context");
		Properties props = this.loadProperties();
		String repoMode = props.getProperty("services.validator.mode.repo");
		String userMode = props.getProperty("services.validator.mode.user");
		String dnetWorkflow = props.getProperty("services.validator.dnetWorkflow");
		Boolean standaloneMode = Boolean.parseBoolean(props.getProperty("services.validator.mode.standalone"));
		
//		logger.debug("User mode: " + userMode);
//		logger.debug("Repo mode: " + repoMode);
		logger.debug("Dnet workflow enabled: " + dnetWorkflow);
		String serviceMode= "";
		if (standaloneMode) 
			serviceMode = "standalone";
		else
			serviceMode = "IS";
		logger.debug("Service mode: " + serviceMode);
		XmlWebApplicationContext ctx = new XmlWebApplicationContext();
//		AbstractApplicationContext ctxP = 
//			    new ClassPathXmlApplicationContext("classpath:WEB-INF/applicationContext.xml");
//		
//		ctx.setParent(ctxP);

//		<import	resource="classpath*:/gr/uoa/di/driver/app/springContext-registrator.xml" />

		ctx.setServletContext(servletContext);
		
		String[] springContextCore = new String[] {
				
				"classpath*:/eu/dnetlib/validator/service/config/springContext-validator-config.xml",
				"classpath:META-INF/cxf/cxf.xml",
				//"classpath:META-INF/cxf/cxf-extension-soap.xml",
				"classpath:META-INF/cxf/cxf-extension-jaxws.xml",
				"classpath:META-INF/cxf/cxf-servlet.xml",
				"classpath*:/eu/dnetlib/soap/cxf/applicationContext-eprbuilders.xml",
				"classpath*:/eu/dnetlib/validator/commons/dao/springContext-validator-dao-datasource.xml",
				"classpath*:/eu/dnetlib/validator/commons/dao/springContext-validator-dao.xml",
				"classpath*:/eu/dnetlib/validator/commons/email/springContext-validator-emailer.xml",
				"classpath*:/eu/dnetlib/validator/service/listeners/springContext-validator-listeners.xml",
				"classpath*:/eu/dnetlib/validator/service/providers/springContext-validator-providers.xml",
				"classpath*:/eu/dnetlib/validator/service/springContext-validator-library.xml",
				"classpath*:/eu/dnetlib/validator/service/springContext-validator-manager.xml",
				"classpath*:/eu/dnetlib/validator/service/springContext-validator-service-"+serviceMode+".xml"
		};
		
		String[] springContextForIS = new String[] {
				"classpath*:/gr/uoa/di/driver/util/springContext-locators.xml",
				"classpath*:/eu/dnetlib/clients/ws/springContext-locatorFactory.xml",
				"classpath*:/gr/uoa/di/driver/app/springContext-commons.xml",
				"classpath*:/gr/uoa/di/driver/app/springContext-lookupFactory.xml",
				"classpath*:/gr/uoa/di/driver/app/springContext-lookupClients.xml",
				"classpath*:/gr/uoa/di/driver/app/springContext-registrator.xml",
				"classpath*:/eu/dnetlib/enabling/hcm/springContext-hcmService.xml",
				"classpath*:/eu/dnetlib/validator/service/springContext-validator-blackboard.xml"
//				"classpath*:/eu/dnetlib/validator/service/springContext-validator-service-IS.xml"
		};
		
		if (standaloneMode) {
			logger.debug("Loading contexts for standalone mode");
			ctx.setConfigLocations(springContextCore);
		} else {
			logger.debug("Loading contexts for dnet");
			ctx.setConfigLocations((String[])
					ArrayUtils.addAll(springContextCore,springContextForIS));
		}
		
		ctx.refresh();
		
		logger.debug("done");
		
		return ctx;
	}	
	
	private Properties loadProperties() {
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(new String[] {
				"classpath*:/eu/dnetlib/validator/service/config/springContext-validator-config.xml"
		});
		
		CascadingPropertyLoader pLoader = (CascadingPropertyLoader) ctx.getBean("propertyLoader1");
		Properties props = pLoader.getProperties();
		
		ctx.destroy();
		ctx.close();
		return props;
	}

}
