package eu.dnetlib.validator.service.impls.rules.xml;

import java.util.Properties;

import org.w3c.dom.NodeList;

import eu.dnetlib.validator.engine.data.DataException;
import eu.dnetlib.validator.engine.data.RuleException;
import eu.dnetlib.validator.engine.execution.ValidationObject;
import eu.dnetlib.validator.service.impls.rules.CardinalityRule;
import eu.dnetlib.validator.service.impls.valobjs.XMLTextValidationObject;

public class XMLCardinalityRule extends CardinalityRule implements XMLRule {

	private static final long serialVersionUID = -3293960407395273309L;

	public XMLCardinalityRule(Properties pros, int id) {
		super(pros, id);
	}

	@Override
	public boolean apply(ValidationObject obj) throws RuleException {
		XMLTextValidationObject tobj = (XMLTextValidationObject) obj;
		NodeList nodes;
		try {
			nodes = tobj.getNodes(this.pros.getProperty(XPATH));
		} catch (DataException e) {
			log.error("", e);
			return false;
		}
		
		int number = nodes.getLength();
		
		log.debug("XML Cardinality Rule. Count: "+number);
		
		if (number < Integer.parseInt(pros.getProperty(LESS_THAN)) && number > Integer.parseInt(pros.getProperty(GREATER_THAN)))
			return true;
		else
			return false;
	}

}
