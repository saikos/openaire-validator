package eu.dnetlib.validator.service.impls.rules.xml;

import java.util.Properties;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import eu.dnetlib.validator.engine.data.DataException;
import eu.dnetlib.validator.engine.data.RuleException;
import eu.dnetlib.validator.engine.execution.ValidationObject;
import eu.dnetlib.validator.service.impls.rules.VocabularyRule;
import eu.dnetlib.validator.service.impls.valobjs.XMLTextValidationObject;

public class XMLVocabularyRule extends VocabularyRule implements XMLRule {

	private static final long serialVersionUID = 5138104621562420041L;

	public XMLVocabularyRule(Properties pros, int id) {
		super(pros, id);
	}

	@Override
	public boolean apply(ValidationObject obj) throws RuleException {
		String[] aterms = this.pros.getProperty(TERMS).split(",");
		XMLTextValidationObject tobj = (XMLTextValidationObject) obj;
		NodeList nodes;
		try {
			nodes = tobj.getNodes(this.pros.getProperty(XPATH));
		} catch (DataException e) {
			log.error("", e);
			return false;
		}
		int success = 0, all = nodes.getLength();
		for(int i=0; i<nodes.getLength();i++) {
			Node node = nodes.item(i);
			log.debug("XML Vocabulary Rule. Node: "+node.getNodeValue());
			for(String term : aterms)
				if(term.trim().equals(node.getNodeValue().trim())) {
					log.debug("XML Vocabulary Rule. Node: "+node.getNodeValue().trim()+" matches with "+term);
					success++;
					break;
				}
		}
		
		String successConditions = this.pros.getProperty(SUCCESS);
		Boolean result = Utils.success(successConditions, success, all);
		if (!this.pros.containsKey(TERMS_TYPE))
			this.pros.setProperty(TERMS_TYPE, "blacklist");
		if (this.pros.getProperty(TERMS_TYPE).toLowerCase().equals("blacklist"))
			return !result;
		else if (this.pros.getProperty(TERMS_TYPE).toLowerCase().equals("whitelist"))
			return result;
		else
			return result;
	}

}
