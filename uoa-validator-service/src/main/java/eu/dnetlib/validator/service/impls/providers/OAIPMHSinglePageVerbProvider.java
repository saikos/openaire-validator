package eu.dnetlib.validator.service.impls.providers;

import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import eu.dnetlib.validator.engine.data.DataException;
import eu.dnetlib.validator.engine.data.Provider;
import eu.dnetlib.validator.engine.data.ResultSet;
import eu.dnetlib.validator.engine.execution.ValidationObject;
import eu.dnetlib.validator.service.impls.valobjs.XMLTextValidationObject;

/**
 * A provider that retrieves the XML response created by sending a verb request to an OAI-PMH repository.
 * 
 * @author Manos Karvounis
 * 
 */
public class OAIPMHSinglePageVerbProvider extends Provider {
	
	private static final long serialVersionUID = -8398496197308270467L;

	private int timeout;
	private int delay;
	private int retryDelay;
	private int retryEfforts;
	
	public static final String BASEURL = "BASEURL";
	
	/**
	 * The verb request to be issued.
	 * Note that you should not include the "?verb=" part, but if you need additional parameters, you should include them as part of the verb.
	 * E.g., "GetRecord&metadataPrefix=oai_dc&identifier=..."
	 */
	public static final String VERB="VERB";
	
	/**
	 * The maximum time to wait for a response from the repository (in millis)
	 */
//	public static final String TIMEOUT = "TIMEOUT";
	/**
	 * How much time to wait between consecutive HTTP requests to the repository
	 * (in millis).
	 */
//	public static final String DELAY = "DELAY";
	/**
	 * How much to wait if an HTTP request fails before trying again by
	 * resending the request.
	 */
//	public static final String RETRY_DELAY = "RETRY_DELAY";
	/**
	 * If an HTTP requests fails, how many times to try to resend the request.
	 */
//	public static final String RETRY_EFFORTS = "RETRY_EFFORTS";
	
	public OAIPMHSinglePageVerbProvider() {
		super(2);
	}

	@Override
	public ResultSet<ValidationObject> getValidationObjects() throws ProviderException {
		return new OAIPMHVerbResultSet();
	}

	@Override
	public ResultSet<String> getValidationObjectIds() throws ProviderException {
		throw new UnsupportedOperationException();
	}

	@Override
	public ValidationObject getValidationObject(String valObjId) throws ProviderException {
		throw new UnsupportedOperationException();
	}
	
	private class OAIPMHVerbResultSet implements ResultSet<ValidationObject> {

		protected DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		protected DocumentBuilder builder;
		private URLStreamer streamer = new URLStreamer();
		
		private Document doc = null;
		boolean finished = false;
		
		public OAIPMHVerbResultSet() {
			super();
			try {
				builder = factory.newDocumentBuilder();
			} catch (ParserConfigurationException e) {
				log.error("", e);
			}
		}
		
		@Override
		public boolean next() throws DataException {
			if(!finished) {
				String surl = pros.getProperty(BASEURL) + "?verb="+pros.getProperty(VERB);
				String response = null;
				
				log.debug("Issuing request: "+surl);
				
				try {
					response = streamer.getResponse(new URL(surl), timeout, delay, retryDelay, retryEfforts);
				} catch (NumberFormatException e) {
					log.error("", e);
					throw new DataException();
				} catch (MalformedURLException e) {
					log.error("", e);
					throw new DataException();
				} catch (IOException e) {
					log.error("", e);
					throw new DataException();
				}
				
				InputSource is = new InputSource(new StringReader(response));
				try {
					doc = builder.parse(is);
				} catch (SAXException e) {
					log.error("", e);
					throw new DataException();
				} catch (IOException e) {
					log.error("", e);
					throw new DataException();
				}
				finished = true;
				return true;
			}
			return false;
		}

		@Override
		public ValidationObject get() throws DataException {
			XMLTextValidationObject obj = new XMLTextValidationObject(doc);
			obj.setId(pros.getProperty(VERB)+"@"+pros.getProperty(BASEURL));
			
			log.debug("Getting object with id "+obj.getId());
			
			return obj;
		}

		@Override
		public String getError() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}

	@Override
	public ResultSet<ValidationObject> getValidationObjects(String entity)
			throws ProviderException {
		// TODO Auto-generated method stub
		return null;
	}

	public Integer getTimeout() {
		return timeout;
	}

	public void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}

	public Integer getDelay() {
		return delay;
	}

	public void setDelay(Integer delay) {
		this.delay = delay;
	}

	public Integer getRetryDelay() {
		return retryDelay;
	}

	public void setRetryDelay(Integer retryDelay) {
		this.retryDelay = retryDelay;
	}

	public Integer getRetryEfforts() {
		return retryEfforts;
	}

	public void setRetryEfforts(Integer retryEfforts) {
		this.retryEfforts = retryEfforts;
	}

}
