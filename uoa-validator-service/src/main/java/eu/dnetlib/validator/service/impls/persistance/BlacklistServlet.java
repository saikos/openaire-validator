package eu.dnetlib.validator.service.impls.persistance;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class BlacklistServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1788389574262137242L;
	private static Logger logger = Logger.getLogger(BlacklistServlet.class);
	
	private static final String REQUEST_GETBLACKLIST = "GetBlacklistedRecords";
//	private TasksDAO tasksDao;
	private String path;
	
	@Override
	public void init() throws ServletException {
		ApplicationContext context = WebApplicationContextUtils
                .getWebApplicationContext(getServletContext());
		
//		this.tasksDao = (TasksDAO) context.getBean("tasksDao");
//		this.path = "/tmp/validator-wf/";
		this.path = "/var/lib/dnet/validator/workflow_blacklists/";
//		this.path = context.getEnvironment().getProperty("blacklistedRecordsPath");
	}
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.handleRequest(req, resp);
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.handleRequest(req, resp);
	}

	public void handleRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		InputStream is = null;
		
		
		String dataSourceId = req.getParameter("datasourceId");
		String requestType = req.getParameter("request");
		try {
			if (requestType.equals(REQUEST_GETBLACKLIST)) {
				logger.debug("parameter: " + dataSourceId);
				File file = new File(path + dataSourceId);
				if (file.isFile()) {
					logger.debug("file exists");
					is = new BufferedInputStream(new FileInputStream(file));
					resp.setStatus(200);
					resp.setContentType("text/plain");
					resp.setContentLength((int) file.length());
					
//					InputStream fileStream = new FileInputStream(file);
//					InputStream gzipStream = new GZIPInputStream(fileStream);
//					Reader decoder = new InputStreamReader(gzipStream, "UTF-8");
//					BufferedReader buffered = new BufferedReader(decoder);
//					IOUtils.copyLarge(buffered, resp.getWriter());
					
					IOUtils.copyLarge(is, resp.getOutputStream());
//					tasksDao.get(4335);
				} else {
					PrintWriter out = resp.getWriter();
					resp.setStatus(400);
					resp.setContentType("text/plain");
					out.write("No file for this id: " + dataSourceId);
					out.close();
				}
			} else {
				PrintWriter out = resp.getWriter();
				resp.setStatus(500);
				resp.setContentType("text/plain");
				out.write("Request: " + requestType + " is not supported");
				out.close();
			}
		} catch (Exception e) {
			resp.setStatus(500);
			e.printStackTrace(resp.getWriter());
		} finally {
			if (is != null)
				is.close();
		}
		
	}
}
