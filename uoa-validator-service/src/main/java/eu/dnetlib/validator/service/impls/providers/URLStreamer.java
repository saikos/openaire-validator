package eu.dnetlib.validator.service.impls.providers;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

public class URLStreamer {
	
	private transient Logger logger = Logger.getLogger(URLStreamer.class);

	public String getResponse(URL url, int timeout, int delay, int retryDelay, int retryEfforts) throws IOException {
    	try {
			Thread.sleep(delay);
		} catch (InterruptedException e1) {
			logger.error("", e1);
			throw new IOException();
		}
    	
		InputStream inStream = null;
		String ret = null;
		for(int i=1; i<=retryEfforts; i++) {
			try {
				URLConnection urlConn = url.openConnection();
		    	urlConn.setConnectTimeout(timeout);
		    	urlConn.setReadTimeout(timeout);
		    	urlConn.setAllowUserInteraction(false);         
		    	urlConn.setDoOutput(true);
		    	
		    	if(urlConn instanceof HttpURLConnection) {
		    		HttpURLConnection huc = (HttpURLConnection) urlConn;
		    		int code = huc.getResponseCode();
		    		if(code < 200 || code >=300) {
		    			logger.error("Status code: "+code+". Retry effort: "+i);
						try {
							logger.debug("Sleeping for "+retryDelay+" millis before retrying");
							Thread.sleep(retryDelay);
						} catch (InterruptedException ex) {
							logger.error("", ex);
							throw new IOException();
						}
		    			continue;
		    		}
		    	}
		    	
		    	inStream = urlConn.getInputStream();
		    	
		    	ret = IOUtils.toString(inStream, "UTF-8");
			} catch(Exception e) {
				logger.error("Exception raised. Retry effort: "+i, e);
				try {
					logger.debug("Sleeping for "+retryDelay+" millis before retrying");
					Thread.sleep(retryDelay);
				} catch (InterruptedException ex) {
					logger.error("", ex);
					throw new IOException();
				}
				continue;
			} finally {
				if(inStream != null)
					inStream.close();
			}
			return ret;
		}
		return null;
	}
}
