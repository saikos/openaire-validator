package eu.dnetlib.validator.service.impls.rules.oaipmh;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import eu.dnetlib.validator.engine.data.Rule;
import eu.dnetlib.validator.engine.data.RuleException;
import eu.dnetlib.validator.engine.execution.ValidationObject;
import eu.dnetlib.validator.service.impls.providers.OAIPMHRecordProvider;
import eu.dnetlib.validator.service.impls.rules.ChainRule;
import eu.dnetlib.validator.service.impls.rules.xml.XMLRegularExpressionRule;
import eu.dnetlib.validator.service.impls.rules.xml.XMLVocabularyRule;

/**
 * <p>
 * Checks if an oai_dc record handles embargo dates as specified by the OpenAIRE
 * Guidelines.
 * </p>
 * <p>
 * The Validation Object given to
 * {@link OAIPMHEmbargoDateRule#apply(ValidationObject)} must be an oai_dc
 * record retrieved by an {@link OAIPMHRecordProvider}.
 * </p>
 * 
 * @author Manos Karvounis
 * 
 */
public class OAIPMHEmbargoDateRule extends Rule {

	public OAIPMHEmbargoDateRule(Properties pros, int id) {
		super(pros, id);
	}

	private static final long serialVersionUID = 770275867973085409L;

	
	@Override
	public boolean apply(ValidationObject obj) throws RuleException {
		Properties pros1 = new Properties();
		pros1.setProperty(XMLVocabularyRule.XPATH, "/OAI-PMH/GetRecord/record/metadata//*[name()='dc:rights']/text()");
		pros1.setProperty(XMLVocabularyRule.TERMS, "info:eu-repo/semantics/embargoedAccess");
		pros1.setProperty(XMLVocabularyRule.SUCCESS, ">0");
		XMLVocabularyRule rights = new XMLVocabularyRule(pros1, 1);

		Properties pros2 = new Properties();
		pros2.setProperty(XMLRegularExpressionRule.REGEXP, "info:eu-repo/date/embargoEnd/[0123456789]{4}-[01][0123456789]-[0123][0123456789]");
		pros2.setProperty(XMLRegularExpressionRule.XPATH, "/OAI-PMH/GetRecord/record/metadata//*[name()='dc:date']/text()");
		pros2.setProperty(XMLRegularExpressionRule.SUCCESS, ">0");
		XMLRegularExpressionRule date = new XMLRegularExpressionRule(pros2, 2);

		List<Rule> rules = new ArrayList<Rule>();
		rules.add(rights);
		rules.add(date);

		Properties pros3 = new Properties();
		pros3.setProperty(ChainRule.TYPE, "horn");
		ChainRule<Rule> chain = new ChainRule<Rule>(pros3, 3, rules);

		return chain.apply(obj);
	}

}
