package eu.dnetlib.validator.service.impls.rules.text;

import java.util.Properties;

import eu.dnetlib.validator.engine.data.RuleException;
import eu.dnetlib.validator.engine.execution.ValidationObject;
import eu.dnetlib.validator.service.impls.rules.VocabularyRule;
import eu.dnetlib.validator.service.impls.valobjs.TextValidationObject;

/**
 * Checks if the {@link TextValidationObject} containes a value contained in the {@link VocabularyRule#TERMS}.
 * @author Manos Karvounis
 *
 */
public class TextVocabularyRule extends VocabularyRule implements TextRule {

	private static final long serialVersionUID = 7959834222669049476L;

	public TextVocabularyRule(Properties pros, int id) {
		super(pros, id);
	}

	@Override
	public boolean apply(ValidationObject obj) throws RuleException {
		TextValidationObject tobj = (TextValidationObject) obj;
		String[] aterms = this.pros.getProperty(TERMS).split(",");
		for(String term : aterms)
			if(term.trim().equals(tobj.getContentAsText()))
				return true;
		return false;
	}
}
