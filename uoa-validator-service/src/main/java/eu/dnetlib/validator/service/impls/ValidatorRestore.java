package eu.dnetlib.validator.service.impls;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import eu.dnetlib.domain.functionality.validator.RuleSet;
import eu.dnetlib.domain.functionality.validator.StoredJob;
import eu.dnetlib.validator.commons.dao.jobs.JobsDAO;
import eu.dnetlib.validator.commons.dao.rules.RulesetsDAO;
import eu.dnetlib.validator.engine.ValidatorException;
import eu.dnetlib.validator.service.impl.ValidatorManager;

@Transactional(propagation = Propagation.REQUIRED)
public class ValidatorRestore {
	
	private JobsDAO jobsDao;
	private RulesetsDAO rulesetsDao;
	private ValidatorManager valManager;
	private boolean autoRestoreOnStartup;
	
	private static Logger logger = Logger.getLogger(ValidatorRestore.class);
	
	public List<StoredJob> deleteUncompleted() {
		List<StoredJob> unCompletedJobs = new ArrayList<StoredJob>(); 
		try {
			logger.info("Checking for uncompleted jobs");
			unCompletedJobs = jobsDao.getUncompletedJobs();
			logger.info("Uncompleted jobs found: "+unCompletedJobs.size());
			if(unCompletedJobs.size() > 0 ) {
				logger.info("deleting uncompleted jobs..");
				jobsDao.deleteUncompletedJobs();
			}
		} catch (Exception e) {
			logger.error("Error deleting uncompleted jobs", e);
		}
		return unCompletedJobs;
	}
	
	public void restartJobs(List<StoredJob> jobs) {
		try {
			for (StoredJob job : jobs) {
				try {
					if (job.getUserEmail().equalsIgnoreCase("workflow service"))
						continue;
					logger.debug("Restarting job: "+job.getId());
					Set<Integer> contentRules = new HashSet<Integer>();
					Set<Integer> usageRules = new HashSet<Integer>();
					for (RuleSet ruleset : rulesetsDao.getRuleSets()) {
						if (ruleset.getGuidelinesAcronym().equals(job.getDesiredCompatibilityLevel())) {
							for (int ruleId : job.getRules()) {
								if (ruleset.getContentRulesIds().contains(ruleId))
									contentRules.add(ruleId);
								else if (ruleset.getUsageRulesIds().contains(ruleId))
									usageRules.add(ruleId);
							}
						}	
					}	
					if (!contentRules.isEmpty())
						job.setSelectedContentRules(contentRules);
					if (!usageRules.isEmpty())
						job.setSelectedUsageRules(usageRules);
					valManager.submitJob(job);
				} catch (Exception e) {
					logger.error("error Re-Submitting validation job", e);
					throw new ValidatorException(e);
				}
			}
		} catch (Exception e) {
			logger.error("error while restarting uncompleted jobs", e);
		}
	}
	
	public ValidatorManager getValManager() {
		return valManager;
	}

	public void setValManager(ValidatorManager valManager) {
		this.valManager = valManager;
	}

	public JobsDAO getJobsDao() {
		return jobsDao;
	}

	public void setJobsDao(JobsDAO jobsDao) {
		this.jobsDao = jobsDao;
	}

	public RulesetsDAO getRulesetsDao() {
		return rulesetsDao;
	}
	public void setRulesetsDao(RulesetsDAO rulesetsDao) {
		this.rulesetsDao = rulesetsDao;
	}
	public boolean isAutoRestoreOnStartup() {
		return autoRestoreOnStartup;
	}

	public void setAutoRestoreOnStartup(boolean autoRestoreOnStartup) {
		this.autoRestoreOnStartup = autoRestoreOnStartup;
	}

	
}
