package eu.dnetlib.validator.service.impls.executors;

import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import eu.dnetlib.validator.engine.ValidatorException;
import eu.dnetlib.validator.engine.data.Provider;
import eu.dnetlib.validator.engine.data.Rule;
import eu.dnetlib.validator.engine.execution.JobListener;

/**
 * Respresents a module that receives job submissions.
 * It retrieves the Validation Objects using the {@link ExecutorSubmitter#provider} and applies the {@link ExecutorSubmitter#rules} on them.
 * @author Manos Karvounis
 */
@Deprecated
public abstract class ExecutorSubmitter implements Runnable {

	public final int jobId;
	public final Set<Rule> rules;
	public final Provider provider;
	public final String set;
	
	public final JobListener validatorListener;
	public final JobListener[] listeners;
	
	protected Map<String,Object> jobContext;
	
	
	private transient Logger log = Logger.getLogger(ExecutorSubmitter.class);

	public ExecutorSubmitter(int jobId, Set<Rule> rules, String set, Provider provider, Map<String,Object> jobContext, JobListener validatorListener, JobListener... listeners) {
		super();
		this.jobId = jobId;
		this.rules = rules;
		this.set = set;
		this.provider = provider;
		this.jobContext = jobContext;
		this.listeners = listeners;
		this.validatorListener = validatorListener;
		
	}

	public abstract void submit() throws ExecutionException;
	
	@Override
	public void run() {
		try {
			submit();
		} catch (ExecutionException e) {
			log.error("", e);
			for(JobListener listener : listeners){
				listener.failed(jobId, this.jobContext, e);
			}
			validatorListener.failed(jobId, this.jobContext, e);
			return;
		}
		validatorListener.finished(jobId, this.jobContext);
	}
	
	public class ExecutionException extends ValidatorException {
		
		private static final long serialVersionUID = -1748951703892618739L;

		public ExecutionException() {
			super();
		}
		
		public ExecutionException(String msg) {
			super(msg);
		}
	}
}
