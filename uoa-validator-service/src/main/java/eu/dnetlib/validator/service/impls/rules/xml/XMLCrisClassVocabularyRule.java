package eu.dnetlib.validator.service.impls.rules.xml;

import java.util.Properties;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;

import eu.dnetlib.validator.engine.data.DataException;
import eu.dnetlib.validator.engine.data.RuleException;
import eu.dnetlib.validator.engine.execution.ValidationObject;
import eu.dnetlib.validator.service.impls.rules.VocabularyRule;
import eu.dnetlib.validator.service.impls.valobjs.XMLTextValidationObject;

public class XMLCrisClassVocabularyRule extends VocabularyRule implements XMLRule {

	
	private static final long serialVersionUID = 6697956781771512326L;
	
	public static final String SCHEME_ID = "scheme_id";

	public XMLCrisClassVocabularyRule(Properties pros, int id) {
		super(pros, id);
	}

	@Override
	public boolean apply(ValidationObject obj) throws RuleException {
		String[] aterms = this.pros.getProperty(TERMS).split(",");
		XMLTextValidationObject tobj = (XMLTextValidationObject) obj;
		NodeList nodes;
		try {
			nodes = tobj.getNodes(this.pros.getProperty(XPATH));
		} catch (DataException e) {
			log.error("", e);
			return false;
		}
		log.debug("XML CRIS Class Vocabulary Rule. Record ID: " + this.getValObjId());
		log.debug("XML CRIS Class Vocabulary Rule. Class Nodes Found: " + nodes.getLength());
		log.debug("XML CRIS Class Vocabulary Rule. Scheme_id to match: " + this.pros.getProperty(SCHEME_ID));
		int success = 0, all = 0;

		for(int i=0; i<nodes.getLength();i++) {
				try {
					CrisClass crisClass  = this.getCrisClass(nodes.item(i));
					log.debug("XML CRIS Class Vocabulary Rule. Internal Node Class ID Value: "+ crisClass.getClassId());
					log.debug("XML CRIS Class Vocabulary Rule. Internal Node Class Scheme ID Value: "+ crisClass.getClassSchemeId());
					if (crisClass.getClassSchemeId().equalsIgnoreCase(pros.getProperty(SCHEME_ID))) {
						all++;
						for(String term : aterms) {
							if(term.trim().equals(crisClass.getClassId().trim())) {
								log.debug("XML CRIS Class Vocabulary Rule. Node: "+crisClass.getClassId().trim()+" matches with "+term);
								success++;
								break;
							}
						}
					}
				} catch (DataException e) {
					log.error("error getting cris class"+ e);
				}

		}
		
		String successConditions = this.pros.getProperty(SUCCESS);
		return Utils.success(successConditions, success, all);
	}
	
	private CrisClass getCrisClass(Node node) throws DataException {
		
		CrisClass retClass = new CrisClass();
		Document newXmlDocument;
		try {
			newXmlDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
			Element root = newXmlDocument.createElement("root");
			newXmlDocument.appendChild(root);
            Node copyNode = newXmlDocument.importNode(node, true);
            root.appendChild(copyNode);
            this.printXmlDocument(newXmlDocument);
            XPathFactory factory = XPathFactory.newInstance();
            XPath xPath = factory.newXPath();
            retClass.setClassId((String) xPath.evaluate("//cfClassId/text()",newXmlDocument ,XPathConstants.STRING));
            retClass.setClassSchemeId((String) xPath.evaluate("//cfClassSchemeId/text()",newXmlDocument ,XPathConstants.STRING));
            retClass.setStartDate((String) xPath.evaluate("//cfStartDate/text()",newXmlDocument ,XPathConstants.STRING));
            retClass.setEndDate((String) xPath.evaluate("//cfEndDate/text()",newXmlDocument ,XPathConstants.STRING));
			
		} catch (ParserConfigurationException e) {
			log.error("error getting cris class"+ e);
		} catch (XPathExpressionException e) {
			log.error("error getting cris class"+ e);
		}			
		return retClass;
	}

	private void printXmlDocument(Document document) {
		DOMImplementationLS domImplementationLS = 
				(DOMImplementationLS) document.getImplementation();
		LSSerializer lsSerializer = 
				domImplementationLS.createLSSerializer();
		String string = lsSerializer.writeToString(document);
		log.debug(string);
	}

}

class CrisClass {
	String classId;
	String ClassSchemeId;
	String startDate;
	String endDate;
	
	
	public CrisClass() {
	}
	
	public String getClassId() {
		return classId;
	}
	public void setClassId(String classId) {
		this.classId = classId;
	}
	public String getClassSchemeId() {
		return ClassSchemeId;
	}
	public void setClassSchemeId(String classSchemeId) {
		ClassSchemeId = classSchemeId;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
}