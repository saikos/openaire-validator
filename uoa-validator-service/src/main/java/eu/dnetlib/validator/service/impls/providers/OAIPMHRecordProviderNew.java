package eu.dnetlib.validator.service.impls.providers;

import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.springframework.beans.factory.annotation.Value;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import se.kb.oai.pmh.OaiPmhServer;
import se.kb.oai.pmh.ResumptionToken;
import se.kb.oai.pmh.Set;
import se.kb.oai.pmh.SetsList;
import eu.dnetlib.validator.engine.data.DataException;
import eu.dnetlib.validator.engine.data.Provider;
import eu.dnetlib.validator.engine.data.ResultSet;
import eu.dnetlib.validator.engine.execution.ValidationObject;
import eu.dnetlib.validator.service.impls.valobjs.XMLTextValidationObject;

/**
 * A provider that retrieves records from an OAI-PMH repository. Resumption
 * tokens are handled transparently.
 * 
 * @author Manos Karvounis
 * 
 */


public class OAIPMHRecordProviderNew extends Provider {
	
	private static final long serialVersionUID = 3386029339653670731L;

	@Value("${services.validator.provider.timeout}")
	private int timeout;
	private int delay;
	private int retryDelay;
	private int retryEfforts;
	
	public static final String BASEURL = "BASEURL";
	public static final String METADATA_PREFIX = "metadataPrefix";
	/**
	 * optional
	 */
	public static final String FROM = "from";
	/**
	 * optional
	 */
	public static final String UNTIL = "until";
	/**
	 * optional
	 */
	public static final String SET = "set";

	/**
	 * The maximum time to wait for a response from the repository (in millis)
	 */
//	public static final String TIMEOUT = "TIMEOUT";
	/**
	 * How much time to wait between consecutive HTTP requests to the repository
	 * (in millis).
	 */
//	public static final String DELAY = "DELAY";
	/**
	 * How much to wait if an HTTP request fails before trying again by
	 * resending the request.
	 */
//	public static final String RETRY_DELAY = "RETRY_DELAY";
	/**
	 * If an HTTP requests fails, how many times to try to resend the request.
	 */
//	public static final String RETRY_EFFORTS = "RETRY_EFFORTS";
	/**
	 * How many records to test.
	 */
	public static final String RECORDS = "records";
	
	public OAIPMHRecordProviderNew() {
		super(1);
	}

	@Override
	public ResultSet<ValidationObject> getValidationObjects() throws ProviderException {
		return new OAIPMHRecordResultSet(this);
	}

	@Override
	public ResultSet<String> getValidationObjectIds() throws ProviderException {
		return new OAIPMHRecordIdentifierResultSet(this);
	}

	@Override
	public ValidationObject getValidationObject(String valObjId) throws ProviderException {
		OAIPMHResultSet oai = new OAIPMHResultSet(this);
		try {
			return new XMLTextValidationObject(oai.getRecord(valObjId));
		} catch (DataException e) {
			log.error("", e);
			throw new ProviderException();
		}
	}

	private class OAIPMHResultSet {
		protected DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		protected DocumentBuilder builder;
		protected XPathFactory xfactory = XPathFactory.newInstance();
		protected NodeList recordIds = null;
		protected NodeList records = null;
		protected int index = -1;
		protected String resumptionToken = null;
		protected String error = null;
		private URLStreamer streamer = new URLStreamer();
		

		public OAIPMHResultSet(OAIPMHRecordProviderNew prv) {
			super();
			try {
				builder = factory.newDocumentBuilder();
			} catch (ParserConfigurationException e) {
				log.error("", e);
			}
		}

		protected NodeList getIds() throws DataException {
			NodeList recordIds = null;
			String surl = pros.getProperty(BASEURL) + "?verb=ListIdentifiers";
			if (resumptionToken == null || resumptionToken.trim().length() == 0) {
				surl += "&metadataPrefix=" + pros.getProperty(METADATA_PREFIX);
				if (pros.getProperty(FROM) != null)
					surl += "&from=" + pros.getProperty(FROM);
				if (pros.getProperty(UNTIL) != null)
					surl += "&until=" + pros.getProperty(UNTIL);
				if (!pros.getProperty(SET).equals("none"))
					surl += "&set=" + pros.getProperty(SET);
			} else {
				surl += "&resumptionToken=" + resumptionToken;
			}
			
			log.debug("Issuing request "+surl);
			
			String response = null;
			try {
				response = streamer.getResponse(new URL(surl), timeout, delay, retryDelay, retryEfforts);
			} catch (NumberFormatException e) {
				log.error("", e);
				throw new DataException();
			} catch (MalformedURLException e) {
				log.error("", e);
				throw new DataException();
			} catch (IOException e) {
				log.error("", e);
				throw new DataException();
			}
			try {
				InputSource is = new InputSource(new StringReader(response));
				Document doc = builder.parse(is);
				XPath xpath = xfactory.newXPath();
				XPathExpression expr = xpath.compile("OAI-PMH/ListIdentifiers/header/identifier/text()");
				recordIds = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
				XPath xpath2 = xfactory.newXPath();
				XPathExpression expr2 = xpath2.compile("OAI-PMH/ListIdentifiers/resumptionToken/text()");
				NodeList rtl = (NodeList) expr2.evaluate(doc, XPathConstants.NODESET);
				if (rtl == null || rtl.getLength() == 0) {
					log.debug("There seems to be no resumption token present");
					resumptionToken = null;
				}
				else {
					resumptionToken = rtl.item(0).getNodeValue();
					resumptionToken = URLEncoder.encode(resumptionToken, "UTF-8");
					log.debug("Found resumption token: "+resumptionToken);
				}
			} catch (Exception e) {
				log.error("", e);
				throw new DataException();
			}
			return recordIds;
		}

		protected NodeList getRecords() throws DataException {
			NodeList records = null;
			String surl = pros.getProperty(BASEURL) + "?verb=ListRecords";
			if (resumptionToken == null || resumptionToken.trim().length() == 0) {
				surl += "&metadataPrefix=" + pros.getProperty(METADATA_PREFIX);
				if (pros.getProperty(FROM) != null)
					surl += "&from=" + pros.getProperty(FROM);
				if (pros.getProperty(UNTIL) != null)
					surl += "&until=" + pros.getProperty(UNTIL);
				if (!pros.getProperty(SET).equals("none"))
					surl += "&set=" + pros.getProperty(SET);
			} else {
				surl += "&resumptionToken=" + resumptionToken;
			}
			String setError=null;
			if (!pros.getProperty(SET).equals("none")) {
				OaiPmhServer harvester = new OaiPmhServer(pros.getProperty(BASEURL));
				try {
					SetsList setList = harvester.listSets();
					ResumptionToken token = setList.getResumptionToken();
					List<Set> sets = new ArrayList<Set>();
					sets.addAll(setList.asList());
					while (token != null) {
						setList = harvester.listSets(token);
						token = setList.getResumptionToken();
						sets.addAll(setList.asList());
					}
					List<String> ret = new ArrayList<String>();
					for (Set set : sets) {
						ret.add(set.getSpec().trim());
					}
					if (!ret.contains(pros.getProperty(SET))){
						error =  "Set: <b>'" + pros.getProperty(SET) + "'</b> is not exposed by the repository. \n Please make sure that 'ListSets' verb is configured correctly on your server as well as that the exposed sets list includes this set.";
						setError = error;
					}
				} catch (Exception e) {
					log.error("error getting sets from url: " + pros.getProperty(BASEURL), e);
				}
			}
			log.debug("Issuing request "+surl);
			
			String response = null;
			try {
				response = streamer.getResponse(new URL(surl), timeout, delay, retryDelay, retryEfforts);
			} catch (NumberFormatException e) {
				log.error("", e);
				throw new DataException();
			} catch (MalformedURLException e) {
				log.error("", e);
				throw new DataException();
			} catch (IOException e) {
				log.error("", e);
				throw new DataException();
			}
			try {
				InputSource is = new InputSource(new StringReader(response));
				Document doc = builder.parse(is);
				XPath xpath = xfactory.newXPath();
//				xpath.setNamespaceContext(new NamespaceResolver(doc));
				XPathExpression expr = xpath.compile("OAI-PMH/ListRecords/record");
				records = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
				XPath xpath2 = xfactory.newXPath();
				XPathExpression expr2 = xpath2.compile("OAI-PMH/ListRecords/resumptionToken/text()");
				NodeList rtl = (NodeList) expr2.evaluate(doc, XPathConstants.NODESET);
				log.debug("Check number of records: "+ records.getLength());
				if (records.getLength() == 0) {
					log.debug("There are no records: "+ records.getLength());
					XPath xpath3 = xfactory.newXPath();
					XPathExpression expr3 = xpath3.compile("OAI-PMH/error/text()");
					error = "The response on request: <a href=\""+ surl + "\">"+surl+"</a> was the following: ";
					error += "<br><b>" + (String) expr3.evaluate(doc, XPathConstants.STRING) + "</b>";
					if (setError != null) {
						error += "<br>" + setError;
					}
					log.debug("Error: "+ error);
				}
				
				if (rtl == null || rtl.getLength() == 0) {
					log.debug("There seems to be no resumption token present");
					resumptionToken = null;
				}
				else {
					resumptionToken = rtl.item(0).getNodeValue();
					resumptionToken = URLEncoder.encode(resumptionToken, "UTF-8");
					log.debug("Found resumption token: "+resumptionToken);
				}
			} catch (Exception e) {
				log.error("", e);
				throw new DataException();
			}
			return records;
		}
		
		protected Document getRecord(String id) throws DataException {
			String surl = pros.getProperty(BASEURL) + "?verb=GetRecord";
			surl += "&metadataPrefix=" + pros.getProperty(METADATA_PREFIX);
			surl += "&identifier=" + id;
			
			log.debug("Issuing request: "+surl);
			
			String response = null;
			try {
				response = streamer.getResponse(new URL(surl), timeout, delay, retryDelay, retryEfforts);
			} catch (NumberFormatException e) {
				log.error("", e);
				throw new DataException();
			} catch (MalformedURLException e) {
				log.error("", e);
				throw new DataException();
			} catch (IOException e) {
				log.error("", e);
				throw new DataException();
			}

			InputSource is = new InputSource(new StringReader(response));
			Document doc;
			try {
				doc = builder.parse(is);
			} catch (SAXException e) {
				log.error("", e);
				throw new DataException();
			} catch (IOException e) {
				log.error("", e);
				throw new DataException();
			}

			return doc;
		}
	}

	private class OAIPMHRecordResultSet extends OAIPMHResultSet implements ResultSet<ValidationObject> {

		public OAIPMHRecordResultSet(OAIPMHRecordProviderNew prv) {
			super(prv);
			// TODO Auto-generated constructor stub
		}

		@Override
		public String getError() {
			if (error != null)
				log.debug("An error occured "+ this.error);
			else
				log.debug("No errors on request");
			return this.error;
		}
		
		@Override
		public boolean next() throws DataException {
			index++;

			log.debug("Moving cursor to result "+index);
			if (records == null || index >= records.getLength()) {
				// if we have previously received some results and there no more to take
				if (records != null && (resumptionToken == null || resumptionToken.trim().length() == 0))
					return false;
				index = -1;
				records = getRecords();
				return next();
			}
			return true;
		}

		@Override
		public ValidationObject get() throws DataException {
			XMLTextValidationObject ret = null;
			
			Document newXmlDocument;
			try {
				newXmlDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

	        Element root = newXmlDocument.createElement("root");
	        newXmlDocument.appendChild(root);
	            Node node = records.item(index);
	            Node copyNode = newXmlDocument.importNode(node, true);
	            root.appendChild(copyNode);
//	            printXmlDocument(newXmlDocument);
	            ret = new XMLTextValidationObject(newXmlDocument);
	            XPathFactory factory = XPathFactory.newInstance();
	            XPath xPath = factory.newXPath();
	            ret.setId(xPath.evaluate("header/identifier", records.item(index)));
	            ret.setStatus(xPath.evaluate("header/@status", records.item(index)));
	           
			} catch (ParserConfigurationException e) {
				log.error("error getting object"+ e);
			} catch (XPathExpressionException e) {
				log.error("error getting object"+ e);
			}			
			return ret;
		}

	}
	
	public static void printXmlDocument(Document document) {
	    DOMImplementationLS domImplementationLS = 
	        (DOMImplementationLS) document.getImplementation();
	    LSSerializer lsSerializer = 
	        domImplementationLS.createLSSerializer();
	    String string = lsSerializer.writeToString(document);
	    System.out.println(string);
	}

	private class OAIPMHRecordIdentifierResultSet extends OAIPMHResultSet implements ResultSet<String> {

		public OAIPMHRecordIdentifierResultSet(OAIPMHRecordProviderNew prv) {
			super(prv);
			// TODO Auto-generated constructor stub
		}

		@Override
		public boolean next() throws DataException {
			index++;
			log.debug("Moving cursor to result "+index);
			if (recordIds == null || index >= recordIds.getLength()) {
				// if we have previously received some results and there no more to take
				if (recordIds != null && (resumptionToken == null || resumptionToken.trim().length() == 0))
					return false;
				index = -1;
				recordIds = getIds();
				return next();
			}
			return true;
		}

		@Override
		public String get() throws DataException {
			String id = recordIds.item(index).getNodeValue();

			log.debug("Returing object with id "+id);
			
			return id;
		}

		@Override
		public String getError() {
			// TODO Auto-generated method stub
			return null;
		}

	}

	@Override
	public ResultSet<ValidationObject> getValidationObjects(String entity)
			throws ProviderException {
		// TODO Auto-generated method stub
		return null;
	}

	public Integer getTimeout() {
		return timeout;
	}

	public void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}

	public Integer getDelay() {
		return delay;
	}

	public void setDelay(Integer delay) {
		this.delay = delay;
	}

	public Integer getRetryDelay() {
		return retryDelay;
	}

	public void setRetryDelay(Integer retryDelay) {
		this.retryDelay = retryDelay;
	}

	public Integer getRetryEfforts() {
		return retryEfforts;
	}

	public void setRetryEfforts(Integer retryEfforts) {
		this.retryEfforts = retryEfforts;
	}

	/*
	class NamespaceResolver implements NamespaceContext {
		
		private static final String OAI_NS = "http://www.openarchives.org/OAI/2.0/";
		private static final String OAI_DC_NS = "http://www.openarchives.org/OAI/2.0/oai_dc/";
		private static final String DC_NS = "http://purl.org/dc/elements/1.1/";
		
		private final Document document;
		
		public NamespaceResolver(Document document) {
			this.document = document;
		}
		
		public String getNamespaceURI(String prefix) {
			log.debug("prefix: " + prefix);
			if ("".equals(prefix) || "oai".equals(prefix)){
				return OAI_NS;                     
			}else if ("oai_dc".equals(prefix)){
				return OAI_DC_NS;
			}else if ("dc".equals(prefix)){
				return DC_NS;
			}
			return "";
		}
		
		public String getPrefix(String namespaceURI) {
			log.debug("prefix: " + namespaceURI);
			return "";
		}
		
		@SuppressWarnings("rawtypes")
		public Iterator getPrefixes(String namespaceURI) {
			// not implemented
			return null;
		}
		
	}	*/
	
}
