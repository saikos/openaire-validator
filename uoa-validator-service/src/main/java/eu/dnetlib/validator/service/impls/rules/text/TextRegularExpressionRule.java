package eu.dnetlib.validator.service.impls.rules.text;

import java.util.Properties;

import eu.dnetlib.validator.engine.execution.ValidationObject;
import eu.dnetlib.validator.service.impls.rules.RegularExpressionRule;
import eu.dnetlib.validator.service.impls.valobjs.TextValidationObject;

/**
 * Checks if the {@link TextValidationObject} matches a Regular Expression.
 * @author Manos Karvounis
 *
 */
public class TextRegularExpressionRule extends RegularExpressionRule implements TextRule {

	private static final long serialVersionUID = -261263584724373647L;

	public TextRegularExpressionRule(Properties pros, int id) {
		super(pros, id);
	}

	@Override
	public boolean apply(ValidationObject obj) {
		TextValidationObject tobj = (TextValidationObject) obj;
		String regexp = this.pros.getProperty(REGEXP);
		return tobj.getContentAsText().matches(regexp);
	}
}
