package eu.dnetlib.validator.service.impls.rules.xml;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.w3c.dom.NodeList;

import eu.dnetlib.validator.engine.data.DataException;
import eu.dnetlib.validator.engine.data.Rule;
import eu.dnetlib.validator.engine.data.RuleException;
import eu.dnetlib.validator.engine.execution.ValidationObject;
import eu.dnetlib.validator.service.impls.valobjs.XMLTextValidationObject;

/**
 * Checks that the XML fields specified by {@link XMLNotConfusedFieldsRule#FIELDS} have no intersecting values.
 * @author Manos Karvounis
 */
public class XMLNotConfusedFieldsRule extends Rule implements XMLRule {

	private static final long serialVersionUID = -6253009649310760559L;

	// comma seperated xpaths
	public static final String FIELDS = "fields";

	public XMLNotConfusedFieldsRule(Properties pros, int id) {
		super(pros, id);
	}

	@Override
	public boolean apply(ValidationObject obj) throws RuleException {

		XMLTextValidationObject tobj = (XMLTextValidationObject) obj;
		List<NodeList> lnodelist = new ArrayList<NodeList>();
		try {
			String[] xpaths = this.pros.getProperty(FIELDS).split(",");
			for (String xpath : xpaths) {
				lnodelist.add(tobj.getNodes(xpath.trim()));
			}
		} catch (DataException e) {
			log.error("", e);
			return false;
		}

		for (int i = 0; i < lnodelist.size(); i++) {
			for (int k = 0; k < lnodelist.get(i).getLength(); k++) {
				String val1 = lnodelist.get(i).item(k).getNodeValue();
				log.debug("XML Not Confused Fields Rule. Val1: "+val1);
				for (int j = i + 1; j < lnodelist.size(); j++) {
					for (int l = 0; l < lnodelist.get(j).getLength(); l++) {
						String val2 = lnodelist.get(j).item(l).getNodeValue();
						log.debug("XML Not Confused Fields Rule. Val1: "+val2);
						if(val1.trim().equalsIgnoreCase(val2.trim()))
							return false;
					}
				}
			}
		}
		
		return true;
	}

}
