package eu.dnetlib.validator.service.impls.executors;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import eu.dnetlib.validator.engine.execution.Executor;
import eu.dnetlib.validator.engine.execution.TaskList;

/**
 * An executor that executes a {@link TaskList} using an {@link ExecutorService}
 * @author Manos Karvounis
 *
 */
@Deprecated
public class ThreadExecutor implements Executor {

	private final ExecutorService executor;

	public ThreadExecutor(ExecutorService executor) {
		super();
		this.executor = executor;
	}

	@Override
	public Future<?> execute(TaskList ltasks) {
		return executor.submit(ltasks);
	}
}
