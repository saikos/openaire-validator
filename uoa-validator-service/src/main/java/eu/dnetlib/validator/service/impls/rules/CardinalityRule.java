package eu.dnetlib.validator.service.impls.rules;

import java.util.Properties;

import eu.dnetlib.validator.engine.data.Rule;

/**
 * Represents a rule that checks if an object has a value between {@link CardinalityRule#GREATER_THAN} and {CardinalityRule#LESS_THAN}.
 * @author Manos Karvounis
 *
 */
public abstract class CardinalityRule extends Rule {
	
	private static final long serialVersionUID = 9028598583906357328L;
	
	public static final String GREATER_THAN = "gt";
	public static final String LESS_THAN = "lt";
	
	public CardinalityRule(Properties pros, int id) {
		super(pros, id);
	}

}
