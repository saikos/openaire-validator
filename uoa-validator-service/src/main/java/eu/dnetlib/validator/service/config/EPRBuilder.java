package eu.dnetlib.validator.service.config;

import org.apache.cxf.endpoint.Endpoint;
import org.apache.log4j.Logger;

import eu.dnetlib.soap.cxf.StandaloneCxfEndpointReferenceBuilder;

public class EPRBuilder extends StandaloneCxfEndpointReferenceBuilder {
	private Logger logger = Logger.getLogger(this.getClass());
	
	@Override
	public String getAddress(Endpoint endpoint) {
		String address = super.getAddress(endpoint);
		
		if (!address.contains("validator/services")) {
			logger.info("Address doesn't contain cxf servlet path part. Hacking");
			
			String baseAddress = this.getBaseAddress();
			
			if (!baseAddress.endsWith("/"))
				baseAddress += "/";
			
			address = baseAddress + "services/" + address.substring(baseAddress.length());
		}
		
		logger.info("new Address: " + address);
		return address;
	}
}