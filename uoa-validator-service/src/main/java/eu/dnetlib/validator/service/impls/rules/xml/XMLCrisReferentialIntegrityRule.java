package eu.dnetlib.validator.service.impls.rules.xml;

import java.util.List;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;

import eu.dnetlib.validator.engine.data.DataException;
import eu.dnetlib.validator.engine.data.Provider.ProviderException;
import eu.dnetlib.validator.engine.data.RuleException;
import eu.dnetlib.validator.engine.execution.ValidationObject;
import eu.dnetlib.validator.service.impls.rules.VocabularyRule;
import eu.dnetlib.validator.service.impls.valobjs.XMLTextValidationObject;

public class XMLCrisReferentialIntegrityRule extends VocabularyRule implements
		XMLRule {

	private static final long serialVersionUID = 6697956781771512326L;

	public static final String SCHEME_ID = "scheme_id";
	public static final String ENTITY_ACRONYM = "entity_acronym";
	public static final String RELATED_ENTITY_ACRONYM = "related_entity_acronym";
	public static final String RELATED_ENTITY_XPATH = "related_entity_xpath";

	public XMLCrisReferentialIntegrityRule(Properties pros, int id) {
		super(pros, id);
	}

	@Override
	public boolean apply(ValidationObject obj) throws RuleException {
		String[] aterms = this.pros.getProperty(TERMS).split(",");
		XMLTextValidationObject tobj = (XMLTextValidationObject) obj;
		NodeList nodes;
		try {
			nodes = tobj.getNodes(this.pros.getProperty(XPATH));
		} catch (DataException e) {
			log.error("", e);
			return false;
		}
		log.debug("XML CRIS Class Vocabulary Rule. Class Nodes Found: "
				+ nodes.getLength());
		log.debug("XML CRIS Class Vocabulary Rule. Scheme_id to match: "
				+ this.pros.getProperty(SCHEME_ID));
		int success = 0, all = nodes.getLength();

		boolean firstCheck = false, secondCheck = false;
		for (int i = 0; i < nodes.getLength(); i++) {
			firstCheck = true;
			secondCheck = true;
			try {
				CrisRefClass crisRefClass = this.getCrisRefClass(nodes.item(i),pros.getProperty(RELATED_ENTITY_ACRONYM));
				log.debug("XML CRIS Class Vocabulary Rule. Internal Node Class ID Value: "
						+ crisRefClass.getClassId());
				log.debug("XML CRIS Class Vocabulary Rule. Internal Node Class Related ID Value: "
						+ crisRefClass.getRelClassId());
				log.debug("XML CRIS Class Vocabulary Rule. Internal Node Class Scheme ID Value: "
						+ crisRefClass.getClassSchemeId());
				log.debug("XML CRIS Class Vocabulary Rule. Internal Node Class Related  full ID Value: "
						+ crisRefClass.getRelClassFullId());
				if (crisRefClass.getClassSchemeId().equalsIgnoreCase(
						pros.getProperty(SCHEME_ID))) {
					Boolean termSuccess = false;
					for (String term : aterms) {
						if (term.trim()
								.equals(crisRefClass.getClassId().trim())) {
							log.debug("XML CRIS Class Vocabulary Rule. Node: "
									+ crisRefClass.getClassId().trim()
									+ " matches with " + term);
							success++;
							termSuccess = true;
							break;
						}
					}
					if (!termSuccess){
						log.debug("Wrong Value ID");
						this.getErrors().add("Wrong Value ID");
						firstCheck = false;
					}
				} else {
					log.debug("Wrong Scheme ID");
					this.getErrors().add("Wrong Scheme ID");
					firstCheck = false;
				}
				secondCheck = checkRefIntegrity(crisRefClass,this.getErrors());
//				secondCheck = true;
			} catch (DataException e) {
				log.error("error getting cris class" + e);
			}
			log.debug("FirstCheck: " + firstCheck);
			log.debug("SecondCheck: " + secondCheck);
			if (firstCheck && !secondCheck)
				success--;
		}

		String successConditions = this.pros.getProperty(SUCCESS);
		return (Utils.success(successConditions, success, all));
	}

	private Boolean checkRefIntegrity (CrisRefClass internalNode, List<String> errors) {
		try {
			String error = "";
			log.debug("Checking ref integrity with relClass: " + internalNode.getRelClassFullId());
			ValidationObject relObj = this.getProvider().getValidationObject(internalNode.getRelClassFullId());
			XMLTextValidationObject tobj = (XMLTextValidationObject) relObj;
			NodeList nodes;
			try {
				nodes = tobj.getNodes(this.pros.getProperty(RELATED_ENTITY_XPATH));
			} catch (DataException e) {
				log.error("", e);
				return false;
			}
			log.debug("XML CRIS Class Vocabulary Rule. Related Class Nodes Found: "
					+ nodes.getLength());
			if (nodes.getLength() == 0) {
				error = "The referenced element doesn't exist";
				errors.add(error);
				return false;
			}

			int passed = 0;
			for (int i = 0; i < nodes.getLength(); i++) {
				passed = 5;
				error = "none";
				try {
					CrisRefClass crisRefClass = this.getCrisRefClass(nodes.item(i),pros.getProperty(ENTITY_ACRONYM));
					
					log.debug("XML CRIS Class Vocabulary Rule. Related Node Class ID Value: " + crisRefClass.getClassId());
					log.debug("must match with: " + internalNode.getClassId());
					if (!crisRefClass.getClassId().equals(internalNode.getClassId())) {
						error = "ClassId values do not match!";
						errors.add(error);
						log.debug("error: " + error);
						passed--;
					}
					log.debug("XML CRIS Class Vocabulary Rule. Related Node Class Scheme ID Value: " + crisRefClass.getClassSchemeId());
					log.debug("must match with: " + internalNode.getClassSchemeId());
					if (!crisRefClass.getClassSchemeId().equals(internalNode.getClassSchemeId())) {
						error = "ClassSchemeId values do not match!";
						errors.add(error);
						log.debug("error: " + error);
						passed--;
					}
					log.debug("XML CRIS Class Vocabulary Rule. Related Node Class Related  full ID Value: " + crisRefClass.getRelClassFullId());
					log.debug("must match with: " + this.getValObjId());
					if (!crisRefClass.getRelClassFullId().equals(this.getValObjId())) {
						error = "Related ID value does not match the record's one!";
						errors.add(error);
						log.debug("error: " + error);
						passed--;
					}
					log.debug("XML CRIS Class Vocabulary Rule. Related Node Class Start Date: " + crisRefClass.getStartDate());
					log.debug("must match with: " + internalNode.getStartDate());
					if (!crisRefClass.getStartDate().equals(internalNode.getStartDate())) {
						error = "Start Date values do not match!";
						errors.add(error);
						log.debug("error: " + error);
						passed--;
					}
					log.debug("XML CRIS Class Vocabulary Rule. Related Node Class End Date: " + crisRefClass.getStartDate());
					log.debug("must match with: " + internalNode.getEndDate());
					if (!crisRefClass.getEndDate().equals(internalNode.getEndDate())) {
						error = "End Date values do not match!";
						errors.add(error);
						log.debug("error: " + error);
						passed--;
					}
					
					
				} catch (DataException e) {
					log.error("error getting cris class" + e);
				}
				if (passed == 5)
					break;
			}
			if (passed != 5) {
				return false;
			}
				
		} catch (ProviderException e) {
			log.error("error getting record" + e);
		} catch (UnsupportedOperationException e) {
			log.error("error getting record" + e);
		}
		return true;
	}
	private CrisRefClass getCrisRefClass(Node node, String entity_acronym) throws DataException {

		CrisRefClass retClass = new CrisRefClass();
		Document newXmlDocument;
		try {
			log.debug("XML CRIS Class Vocabulary Rule. ref Record ID: "
					+ this.getValObjId()
							.substring(0, this.getValObjId().indexOf(":cf") + 1)
							.concat(entity_acronym.substring(0, entity_acronym.indexOf("Id"))));
			newXmlDocument = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder().newDocument();
			Element root = newXmlDocument.createElement("root");
			newXmlDocument.appendChild(root);
			Node copyNode = newXmlDocument.importNode(node, true);
			root.appendChild(copyNode);
			this.printXmlDocument(newXmlDocument);
			XPathFactory factory = XPathFactory.newInstance();
			XPath xPath = factory.newXPath();
			retClass.setRelClassId((String) xPath.evaluate("//"
					+ entity_acronym + "/text()", newXmlDocument,
					XPathConstants.STRING));
			retClass.setClassId((String) xPath.evaluate("//cfClassId/text()",
					newXmlDocument, XPathConstants.STRING));
			retClass.setClassSchemeId((String) xPath.evaluate(
					"//cfClassSchemeId/text()", newXmlDocument,
					XPathConstants.STRING));
			retClass.setStartDate((String) xPath.evaluate(
					"//cfStartDate/text()", newXmlDocument,
					XPathConstants.STRING));
			retClass.setEndDate((String) xPath.evaluate("//cfEndDate/text()",
					newXmlDocument, XPathConstants.STRING));
			retClass.setRelClassFullId(this.getValObjId()
					.substring(0, this.getValObjId().indexOf(":cf") + 1)
					.concat(entity_acronym.substring(0, entity_acronym.indexOf("Id")))
					.concat("/")
					.concat(retClass.getRelClassId()));
		} catch (ParserConfigurationException e) {
			log.error("error getting cris class" + e);
		} catch (XPathExpressionException e) {
			log.error("error getting cris class" + e);
		}
		return retClass;
	}

	private void printXmlDocument(Document document) {
		DOMImplementationLS domImplementationLS = (DOMImplementationLS) document
				.getImplementation();
		LSSerializer lsSerializer = domImplementationLS.createLSSerializer();
		String string = lsSerializer.writeToString(document);
		log.debug(string);
	}

}

class CrisRefClass {
	String relClassId;
	String relClassFullId;
	String classId;
	String classSchemeId;
	String startDate;
	String endDate;

	public CrisRefClass() {
	}

	public String getRelClassId() {
		return relClassId;
	}

	public void setRelClassId(String relClassId) {
		this.relClassId = relClassId;
	}

	public String getRelClassFullId() {
		return relClassFullId;
	}

	public void setRelClassFullId(String relClassFullId) {
		this.relClassFullId = relClassFullId;
	}

	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public String getClassSchemeId() {
		return classSchemeId;
	}

	public void setClassSchemeId(String classSchemeId) {
		this.classSchemeId = classSchemeId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

}