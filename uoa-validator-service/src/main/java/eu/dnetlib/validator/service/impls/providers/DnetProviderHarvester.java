package eu.dnetlib.validator.service.impls.providers;

import gr.uoa.di.driver.enabling.resultset.ResultSet;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;

public class DnetProviderHarvester implements Runnable {

	private transient Logger log = Logger
			.getLogger(DnetProviderHarvester.class);

	private BlockingQueue<String> queue;
	private ResultSet<String> rs;
	private int beginRecord;
	private int endRecord;
	private int batchSize;
	private int workerId;
	private long elapsed;
	
	public DnetProviderHarvester(BlockingQueue<String> q, ResultSet<String> rs,
			int beginRecord, int endRecord, int batchSize, int workerId) {
		log.debug("Harvester for worker " + workerId + " created");
		this.queue = q;
		this.rs = rs;
		this.beginRecord = beginRecord;
		this.endRecord = endRecord;
		this.batchSize = batchSize;
		this.workerId = workerId;
	}

	@Override
	public void run() {
		// produce messages

		int recordsFetched = 0;
		log.debug("Harvester for worker " + workerId + " started");
		int pointer = beginRecord;

		long timeBegin = Calendar.getInstance().getTimeInMillis();
		while (pointer <= endRecord) {
			int to = pointer + batchSize - 1;
			if ((pointer + batchSize - 1) < endRecord) {
				to = pointer + batchSize - 1;
			} else {
				if (pointer <= endRecord)
					to = endRecord;
			}
			log.error("to : " + to + " and limit: " + endRecord);
			log.error("PH" + workerId + "# Issuing request for records. From : "
					+ pointer + " to : " + to);
			long time1 = Calendar.getInstance().getTimeInMillis();
			List<String> tempRecords = rs.get(pointer, to);
			recordsFetched += tempRecords.size();
			long time2 = Calendar.getInstance().getTimeInMillis();
			try {
				for (String record : tempRecords) {
					queue.put(record);
				}
			} catch (InterruptedException e) {
				log.error("Error fetching records", e);
			}
			log.debug("PH" + workerId + "#records fetching from is took "
					+ ((time2 - time1)) + " milliseconds");
			elapsed += time2 - time1;
			log.debug("PH" + workerId
					+ "#Elapsed time till now for rules fetching is " + elapsed
					/ 1000 + " seconds");

			pointer += batchSize;
		}
		long timeEnd = Calendar.getInstance().getTimeInMillis();
		try {
			queue.put("finished");
			log.debug("PH" + workerId + "# Fetching records finished.");
			log.debug("PH" + workerId + "# Records fetched: " + recordsFetched);
			log.debug("PH" + workerId + "# Total time in seconds: " + (timeEnd - timeBegin)/1000);
		} catch (InterruptedException e) {
			log.error("PH" + workerId
					+ "# Error finalizing queue", e);
		}
	}
}
