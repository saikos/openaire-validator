package eu.dnetlib.validator.service.impls.providers;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;

import eu.dnetlib.api.data.MDStoreService;
import eu.dnetlib.domain.EPR;
import eu.dnetlib.validator.engine.data.DataException;
import eu.dnetlib.validator.engine.data.Provider;
import eu.dnetlib.validator.engine.data.ResultSet;
import eu.dnetlib.validator.engine.execution.ValidationObject;
import eu.dnetlib.validator.service.impls.valobjs.XMLTextValidationObject;
import gr.uoa.di.driver.enabling.resultset.ResultSetFactory;
import gr.uoa.di.driver.util.ServiceLocator;

public class DnetProvider extends Provider{

	private static final long serialVersionUID = -4280319954962194170L;

	private static ServiceLocator<MDStoreService> mdStoreServiceLocator;

	private static ResultSetFactory rsFactory;
	
	public static final String DATASOURCE = "DATASOURCE";
	
	public static final String BATCH_SIZE = "BATCH_SIZE";

	public static final String RECORDS = "RECORDS";
	
	public static final String MDSTORE_ID = "MDSTORE_ID";
	
	public static final String FROM = "FROM";
	
	public static final String BEGIN_RECORD = "BEGIN_RECORD";
	
	public static final String UNTIL = "UNTIL";
	
	public static final String RECORD_FILTER = "RECORD_FILTER";

	public static final String WORKER_ID = "WORKER_ID";

	public static final String WORKERS = "WORKERS";
	
	

	public DnetProvider() {
		super(3);
		
	}

	@Override
	public ResultSet<ValidationObject> getValidationObjects() throws ProviderException {
		return new DnetRecordResultSet();
	}

	@Override
	public ResultSet<String> getValidationObjectIds() throws ProviderException,
			UnsupportedOperationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ValidationObject getValidationObject(String valObjId)
			throws ProviderException, UnsupportedOperationException {
		// TODO Auto-generated method stub
		return null;
	}
	
	public static void printXmlDocument(Document document) {
	    DOMImplementationLS domImplementationLS = 
	        (DOMImplementationLS) document.getImplementation();
	    LSSerializer lsSerializer = 
	        domImplementationLS.createLSSerializer();
	    String string = lsSerializer.writeToString(document);
	    System.out.println(string);
	}

	private class DnetResultSet {
				
		gr.uoa.di.driver.enabling.resultset.ResultSet<String> rs = null;

		protected DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		protected DocumentBuilder builder;
		protected XPathFactory xfactory = XPathFactory.newInstance();
		protected List<Node> records = null;
		protected int records_sum = 0;
		protected int index = -1;
		protected int pointer = 0;
		protected int recordsNum = 0;
		protected int workers = 0;
		protected int workerId = 0;
		protected int beginRecord = 0;
		protected int endRecord = 0;
		protected String error = null;
		private long elapsed = 0;

		public DnetResultSet() {

			try {
				builder = factory.newDocumentBuilder();
				log.debug("Retrieving the datasource..");

				log.debug("RECORDS " + pros.getProperty(RECORDS));
				log.debug("MDSTORE_ID " + pros.getProperty(MDSTORE_ID));
				log.debug("DATASOURCE: " + pros.getProperty(DATASOURCE));
				log.debug("WORKER_ID: " + pros.getProperty(WORKER_ID));
				workerId = Integer.parseInt(pros.getProperty(WORKER_ID));
				workers = Integer.parseInt(pros.getProperty(WORKERS));
//				log.debug("BEGIN RECORD: " + pros.getProperty(BEGIN_RECORD));
//				EPR epr = mdStoreServiceServiceLocator.getService().deliverMDRecords(pros.getProperty(MD_ID), pros.getProperty(FROM), pros.getProperty(UNTIL), pros.getProperty(RECORD_FILTER));
				log.debug("Issuing request on mdstore: " + pros.getProperty(MDSTORE_ID));
				EPR epr = mdStoreServiceLocator.getService().deliverMDRecords(pros.getProperty(MDSTORE_ID), null, null, null);

				rs = rsFactory.createResultSet(epr);

				log.debug("rs created");
				records_sum = rs.size();
				log.debug("Number of records in ResultSet: " + records_sum);
				if (pros.getProperty(RECORDS).equalsIgnoreCase("-1") ) {
					pros.setProperty(RECORDS,Integer.toString(records_sum));
				} else if (Integer.parseInt(pros.getProperty(RECORDS)) > records_sum) {
					pros.setProperty(RECORDS,Integer.toString(records_sum));
				}
				recordsNum = Integer.parseInt(pros.getProperty(RECORDS));
				log.info("W"+ workerId + "# RECORDS TO TEST: " + recordsNum);
				log.info("W"+ workerId + "# WORKERS: " + workers);
//				pros.setProperty(DnetProvider.BEGIN_RECORD, Integer.toString(Integer.parseInt(pros.getProperty(WORKER_ID)) * (Integer.parseInt(pros.getProperty(RECORDS))/Integer.parseInt(pros.getProperty(WORKERS)))));
				beginRecord = workerId * (recordsNum/workers) + 1;
				endRecord = (recordsNum/workers) + beginRecord -1;
//				endRecord = Integer.parseInt(pros.getProperty(RECORDS))/Integer.parseInt(pros.getProperty(WORKERS)) + Integer.parseInt(pros.getProperty(BEGIN_RECORD));
				if (workerId == workers-1)
					endRecord += recordsNum % workers;
				log.info("W"+ workerId + "# BEGIN RECORD: " + beginRecord);
				log.info("W"+ workerId + "# END RECORD: " + endRecord);
				pointer = beginRecord;
			} catch (ParserConfigurationException e) {
				log.error("", e);
			} catch (Exception e) {
				log.error("", e);
			}
		}

		protected List<Node> getRecords() throws DataException {
			List<Node> records = new ArrayList<Node>();
			
			try {
				
				int to = pointer + Integer.parseInt(pros.getProperty(BATCH_SIZE)) -1;
				if ((pointer + Integer.parseInt(pros.getProperty(BATCH_SIZE)) -1) < endRecord) {
					to = pointer + Integer.parseInt(pros.getProperty(BATCH_SIZE)) -1;
				} else {
					if (pointer <= endRecord)
						to = endRecord ;
				}
				log.error("to : " + to + " and limit: " + endRecord); 
				if (to <= endRecord) {
					log.error("W"+ workerId + "# Issuing request for records. From : " + pointer + " to : " + to); 
					long time1 = Calendar.getInstance().getTimeInMillis();
					List<String> tempRecords = rs.get(pointer, to);
					long time2 = Calendar.getInstance().getTimeInMillis();
					log.debug("W"+ workerId + "#Rule fetching took " + ((time2 - time1))
							+ " milliseconds");
					elapsed += time2 - time1;
					log.error("W"+ workerId + "#records fetched : " + tempRecords.size());
					log.debug("W"+ workerId + "#Elapsed time till now is for rules fetching" + elapsed/1000
							+ " seconds");

					pointer += Integer.parseInt(pros.getProperty(BATCH_SIZE));
					for (String record : tempRecords) {
	//					log.debug("record from resultSet is : " + record);
						InputSource is = new InputSource(new StringReader(record));
						Document doc = builder.parse(is);
						XPath xpath = xfactory.newXPath();
						XPathExpression expr = xpath.compile("//*[local-name()='record']");
						records.add((Node) expr.evaluate(doc, XPathConstants.NODE));
					}
					log.error("W"+ workerId + "records to return : " + records.size());
				}
				if (records.size() == 0) {
					log.debug("There are no records. ");
					error = "There are no records";
					log.debug("Error: "+ error);
				}
				
			} catch (Exception e) {
				log.error("", e);
				throw new DataException();
			}
			return records;
		}		
	}

	private class DnetRecordResultSet extends DnetResultSet implements ResultSet<ValidationObject> {

		@Override
		public String getError() {
			if (error != null)
				log.debug("An error occured "+ this.error);
			else
				log.debug("No errors on request");
			return this.error;
		}
		
		@Override
		public boolean next() throws DataException {
			index++;
			log.debug("Moving cursor to result "+index);
			if (records == null || index >= records.size()) {
				if (records != null && (records.size() == 0))
					return false;
				index = -1;
				records = getRecords();
				return next();
			}
			return true;
		}

		@Override
		public ValidationObject get() throws DataException {
			XMLTextValidationObject ret = null;
			
			Document newXmlDocument;
			try {
				newXmlDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

		        Element root = newXmlDocument.createElement("root");
		        newXmlDocument.appendChild(root);
	            Node node = records.get(index);
	            Node copyNode = newXmlDocument.importNode(node, true);
	            root.appendChild(copyNode);
	            printXmlDocument(newXmlDocument);
	            ret = new XMLTextValidationObject(newXmlDocument);
	            XPathFactory factory = XPathFactory.newInstance();
	            XPath xPath = factory.newXPath();
	            String id = xPath.evaluate("//*[local-name()='header']/*[name()='dri:objIdentifier']/text()", records.get(index));
	            if (id.isEmpty())
	            	id = xPath.evaluate("//*[local-name()='header']/*[name()='dri:recordIdentifier']/text()", records.get(index));
	            if (id.isEmpty())
	            	id = xPath.evaluate("//*[local-name()='header']/*[name()='identifier']/text()", records.get(index));
	            ret.setId(id);
	            ret.setStatus(xPath.evaluate("//*[local-name()='header']/@status", records.get(index)));
	            
	            log.debug("record id: " + ret.getId());
	            log.debug("record status: " + ret.getStatus());
	            
			} catch (ParserConfigurationException e) {
				log.error("error getting object"+ e);
			} catch (XPathExpressionException e) {
				log.error("error getting object"+ e);
			}			
			return ret;
		}


	}

	
	public static ResultSetFactory getRsFactory() {
		return rsFactory;
	}

	public static void setRsFactory(ResultSetFactory rsFactory) {
		DnetProvider.rsFactory = rsFactory;
	}

	public static ServiceLocator<MDStoreService> getMdStoreServiceLocator() {
		return mdStoreServiceLocator;
	}

	public static void setMdStoreServiceLocator(
			ServiceLocator<MDStoreService> mdStoreServiceLocator) {
		DnetProvider.mdStoreServiceLocator = mdStoreServiceLocator;
	}

	@Override
	public ResultSet<ValidationObject> getValidationObjects(String entity)
			throws ProviderException {
		// TODO Auto-generated method stub
		return null;
	}


}
