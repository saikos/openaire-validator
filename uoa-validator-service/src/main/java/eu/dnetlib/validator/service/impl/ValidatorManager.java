package eu.dnetlib.validator.service.impl;

import eu.dnetlib.domain.functionality.validator.JobForValidation;
import eu.dnetlib.domain.functionality.validator.JobResultEntry;
import eu.dnetlib.domain.functionality.validator.RuleSet;
import eu.dnetlib.domain.functionality.validator.StoredJob;
import eu.dnetlib.enabling.tools.blackboard.BlackboardJob;
import eu.dnetlib.enabling.tools.blackboard.BlackboardNotificationHandler;
import eu.dnetlib.enabling.tools.blackboard.BlackboardServerHandler;
import eu.dnetlib.validator.commons.dao.DaoException;
import eu.dnetlib.validator.engine.ValidatorException;

import java.util.List;

public interface ValidatorManager {

    StoredJob beginDataJobForWorkflow(String datasource, String guidelines, String groupBy, int records, int workers, BlackboardJob bJob, BlackboardNotificationHandler<BlackboardServerHandler> blackboardHandler, int jobStatusUpdateInterval, boolean outputEpr, boolean blacklistedRecords, String blacklistGuidelines) throws ValidatorException;

    StoredJob getStoredJob(int jobId, String groupBy) throws ValidatorException;

    List<StoredJob> getStoredJobs(String userMail, String jobType,
                                         Integer offset, Integer limit, String dateFrom, String dateTo) throws ValidatorException;

    List<StoredJob> getStoredJobs(String userMail, String jobType,
                                  Integer offset, Integer limit, String dateFrom, String dateTo, String jobStatus) throws ValidatorException;

    List<RuleSet> getRuleSets() throws ValidatorException;

    void submitJob(JobForValidation job) throws ValidatorException;

    int getStoredJobsTotalNumber(String userMail, String jobType) throws ValidatorException;

    int getStoredJobsTotalNumber(String userMail, String jobType, String jobStatus) throws ValidatorException;

    List<StoredJob> getJobSummary(List<String> baseUrl, int limit) throws DaoException;


}
