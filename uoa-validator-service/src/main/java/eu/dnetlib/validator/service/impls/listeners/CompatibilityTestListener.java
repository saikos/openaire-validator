package eu.dnetlib.validator.service.impls.listeners;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import eu.dnetlib.validator.commons.dao.jobs.JobsDAO;
import eu.dnetlib.validator.commons.email.Emailer;
import eu.dnetlib.validator.engine.execution.CompletedTask;
import eu.dnetlib.validator.engine.execution.JobListener;

public class CompatibilityTestListener extends AbstractValidatorListener{
	private static Logger logger = Logger.getLogger(CompatibilityTestListener.class);
	
	private Emailer emailer = null;
	private String valBaseUrl = null;
	private String validationSet = null; 
	private String guidelines = null;

	@Override
	protected void jobSuccess(int jobId, Map<String, Object> jobContext) {
		sendMail(jobId, (String) jobContext.get("jobSubmittedUser"));
	}

	@Override
	protected void jobFailure(int jobId, Map<String, Object> jobContext, Throwable t) {
		sendMail( jobId, (String) jobContext.get("jobSubmittedUser"));
	}

	private void sendMail(int jobSubmittedId, String jobSubmittedUser) {
		logger.debug("JOBID:"+jobSubmittedId+"# Sending email for finished job");
		String mailTo = jobSubmittedUser;
		List<String> recipients = new ArrayList<String>();
		recipients.add(mailTo);
		String msgUpgrade = "";
//		if (this.validationSet != null && !this.validationSet.equalsIgnoreCase("openaire"))
		if (this.guidelines.equalsIgnoreCase("openaire2.0") || this.guidelines.equalsIgnoreCase("driver"))
			msgUpgrade = "\n\n Please consider to upgrade to OpenAIRE Guidelines v3. Link: https://guidelines.openaire.eu/wiki/OpenAIRE_Guidelines:_For_Literature_repositories";
		String message = "The compatibility test you have submitted has finished. You can retrieve the results by following this url: " + valBaseUrl + "/compatibility/browseHistory/" + jobSubmittedId  + msgUpgrade;

		String subject = "OpenAIRE compatibility Test Results";
		try {
				emailer.sendMail(recipients, subject, message, false, null);
		} catch (Exception e) {
			logger.error("JOBID:"+jobSubmittedId+"# Error sending email for finished job", e);
		}
	}

	public String getValBaseUrl() {
		return valBaseUrl;
	}

	public void setValBaseUrl(String valBaseUrl) {
		this.valBaseUrl = valBaseUrl;
	}

	public String getValidationSet() {
		return validationSet;
	}

	public void setValidationSet(String validationSet) {
		this.validationSet = validationSet;
	}

	public Emailer getEmailer() {
		return emailer;
	}

	public void setEmailer(Emailer emailer) {
		this.emailer = emailer;
	}

	public String getGuidelines() {
		return guidelines;
	}

	public void setGuidelines(String guidelines) {
		this.guidelines = guidelines;
	}

}
