package eu.dnetlib.validator.service.impls.providers;


import java.util.Iterator;

import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.dom4j.io.DOMWriter;
import org.w3c.dom.Document;

import se.kb.oai.OAIException;
import se.kb.oai.pmh.Identification;
import se.kb.oai.pmh.OaiPmhServer;

public class RepositoryChecks {

	private static Logger logger = Logger.getLogger(RepositoryChecks.class);

	public static boolean identifyRepository(String baseUrl) {
		logger.debug("sending identify request to repo " + baseUrl);
	
		OaiPmhServer harvester = new OaiPmhServer(baseUrl);
	
		if (baseUrl.trim().isEmpty()) {
			return false;
		}
	
		try {
			Identification identification = harvester.identify();
			DOMWriter d4Writer = new DOMWriter();
			Document d = d4Writer.write(identification.getResponse());
	
			return verifyIdentify(d);
		} catch (OAIException e) {
			logger.debug("Could not get response form " + baseUrl);
			return false;
		} catch (XPathExpressionException e) {
			logger.debug("Error verifying identify response", e);
			return false;
		} catch (DocumentException e) {
			logger.debug("Error verifying identify response", e);
			return false;
		}
	}
	
	private static boolean verifyIdentify(Document doc) throws XPathExpressionException {
		NamespaceContext ctx = new NamespaceContext() {
			public String getNamespaceURI(String prefix) {
				String uri;
				if (prefix.equals("oai"))
					uri = "http://www.openarchives.org/OAI/2.0/";
				else
					uri = null;
				return uri;
			}
	
			// Dummy implementation - not used!
			public Iterator<String> getPrefixes(String val) {
				return null;
			}
	
			// Dummy implemenation - not used!
			public String getPrefix(String uri) {
				return null;
			}
		};
	
		// Now the XPath expression
	
		String xpathStr = "//oai:OAI-PMH/oai:Identify";
		XPathFactory xpathFact = XPathFactory.newInstance();
		XPath xpath = xpathFact.newXPath();
		xpath.setNamespaceContext(ctx);
		String result = xpath.evaluate(xpathStr, doc);
	
		return (result != null && !result.equals(""));
	}
}