package eu.dnetlib.validator.service.impls.listeners;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import eu.dnetlib.api.enabling.ResultSetService;

/**
 * A Rule that is applied on a Validation Object.
 * 
 * @author Nikon Gasparis
 *
 */
public class RSTask implements Runnable {

	private final ResultSetService resultSetService;
	private final String outputResultSetID;
	private final BlockingQueue<String> queue;
	
	private final AtomicInteger activeThreads;
	private final Object allThreadsFinished;
	
	private boolean success;
	private List<String> errors;
	private Exception exception = null;;

	
public RSTask(ResultSetService resultSetService, String outputResultSetID,
		BlockingQueue<String> queue, AtomicInteger activeThreads, Object allThreadsFinished) {
		super();
		this.resultSetService = resultSetService;
		this.outputResultSetID = outputResultSetID;
		this.queue = queue;
		this.activeThreads = activeThreads;
		this.allThreadsFinished = allThreadsFinished;
	}

	@Override
	public void run() {
		Logger log = Logger.getLogger(RSTask.class);
		long time1 = Calendar.getInstance().getTimeInMillis();
		try {
			List <String> outputRSBuffer = new ArrayList<String>();
			for (int i=0 ; i < 50 ; i++) {
				String record = queue.take();
				if (record.equalsIgnoreCase("finished")) {
//					log.debug("closing result set");
//					resultSetService.closeRS(outputResultSetID);
					break;
				}
				else 
					outputRSBuffer.add(record);
			}
			resultSetService.populateRS(outputResultSetID, outputRSBuffer);
			if (activeThreads.decrementAndGet() == 0) {
				synchronized (allThreadsFinished) {
					allThreadsFinished.notify();
				}
			}
		} catch (Exception e) {
			log.error("Error populating ResultSetService.", e);
		}
		long time2 = Calendar.getInstance().getTimeInMillis();
		log.debug("Populating RS took " + ((time2 - time1))
				+ " milli seconds");
	}

	public boolean isSuccess() {
		return success;
	}

	public Exception getException() {
		return exception;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

}
