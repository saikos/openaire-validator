package eu.dnetlib.validator.service.impls.persistance;

public interface ThreadSafeRegistry {

	public void lock();
	public void unlock();
	public void removeObject(int id);
}
