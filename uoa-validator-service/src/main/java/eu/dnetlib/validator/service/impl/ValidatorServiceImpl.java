package eu.dnetlib.validator.service.impl;

import eu.dnetlib.api.functionality.ValidatorService;
import eu.dnetlib.api.functionality.ValidatorServiceException;
import eu.dnetlib.domain.ActionType;
import eu.dnetlib.domain.ResourceType;
import eu.dnetlib.domain.enabling.Notification;
import eu.dnetlib.domain.functionality.validator.JobForValidation;
import eu.dnetlib.domain.functionality.validator.JobResultEntry;
import eu.dnetlib.domain.functionality.validator.RuleSet;
import eu.dnetlib.domain.functionality.validator.StoredJob;
import eu.dnetlib.enabling.tools.blackboard.BlackboardNotificationHandler;
import eu.dnetlib.enabling.tools.blackboard.BlackboardServerHandler;
import eu.dnetlib.validator.commons.dao.DaoException;
import eu.dnetlib.validator.engine.ValidatorException;
import gr.uoa.di.driver.app.DriverServiceImpl;
import gr.uoa.di.driver.enabling.issn.NotificationListener;

import java.util.List;

import org.apache.log4j.Logger;

public class ValidatorServiceImpl extends DriverServiceImpl implements
		ValidatorService {

	private BlackboardNotificationHandler<BlackboardServerHandler> blackboardNotificationHandler = null;
	private Logger logger = Logger.getLogger(ValidatorServiceImpl.class);

	private ValidatorManager valManager;

	// do NOT call directly. It will be called by the
	// InitializingServiceRegistrationManager
	// after it sets the service EPR and id.
	@Override
	public void init() {
		super.init();
		if (this.blackboardNotificationHandler != null) { 
			this.subscribe(ActionType.UPDATE,
				ResourceType.VALIDATORSERVICERESOURCETYPE, this.getServiceEPR()
						.getParameter("serviceId"),
				"RESOURCE_PROFILE/BODY/BLACKBOARD/LAST_REQUEST",
				new NotificationListener() {
					@Override
					public void processNotification(Notification notification) {
						blackboardNotificationHandler.notified(
								notification.getSubscriptionId(),
								notification.getTopic(),
								notification.getIsId(),
								notification.getMessage());
						logger.debug(notification.getSubscriptionId());
						logger.debug(notification.getTopic());
						logger.debug(notification.getIsId());
						logger.debug(notification.getMessage());
					}
				});
		}
	}

	@Override
	public StoredJob getStoredJob(int jobId, String groupBy)
			throws ValidatorServiceException {
		try {
			logger.info("received request for job " + jobId);
			return valManager.getStoredJob(jobId, groupBy);
		} catch (ValidatorException e) {
			throw new ValidatorServiceException(e);
		}

	}

	@Override
	public List<StoredJob> getJobSummary(List<String> baseUrl, int size) throws ValidatorServiceException {
		try {
			return valManager.getJobSummary(baseUrl,size);
		} catch (DaoException e) {
			throw new ValidatorServiceException(e);
		}
	}


	@Override
	public List<StoredJob> getStoredJobs(String userMail, String jobType,
										 Integer offset, Integer limit, String dateFrom, String dateTo)
			throws ValidatorServiceException {
		try {
			logger.debug("received request for jobs of user " + userMail);
			return valManager.getStoredJobs(userMail, jobType, offset, limit, dateFrom, dateTo);
		} catch (ValidatorException e) {
			throw new ValidatorServiceException(e);
		}
	}

	@Override
	public List<StoredJob> getStoredJobsNew(String userMail, String jobType,
										 Integer offset, Integer limit, String dateFrom, String dateTo, String jobStatus)
			throws ValidatorServiceException {
		try {
			logger.debug("received request for jobs of user " + userMail);
			return valManager.getStoredJobs(userMail, jobType, offset, limit, dateFrom, dateTo, jobStatus);
		} catch (ValidatorException e) {
			throw new ValidatorServiceException(e);
		}
	}

	@Override
	public int getStoredJobsTotalNumber(String userMail, String jobType)
			throws ValidatorServiceException {
		try {
			logger.debug("received request for total number of jobs of user " + userMail);
			return valManager.getStoredJobsTotalNumber(userMail, jobType);
		} catch (ValidatorException e) {
			throw new ValidatorServiceException(e);
		}
	}

	@Override
	public int getStoredJobsTotalNumberNew(String userMail, String jobType, String jobStatus)
			throws ValidatorServiceException {
		try {
			logger.debug("received request for total number of jobs of user " + userMail);
			return valManager.getStoredJobsTotalNumber(userMail, jobType, jobStatus);
		} catch (ValidatorException e) {
			throw new ValidatorServiceException(e);
		}
	}

	@Override
	public List<RuleSet> getRuleSets() throws ValidatorServiceException {
		try {
			logger.info("received request for rulesets ");
			return valManager.getRuleSets();
		} catch (ValidatorException e) {
			throw new ValidatorServiceException(e);
		}
	}

	@Override
	public void submitValidationJob(JobForValidation job)
			throws ValidatorServiceException {
		try {
			logger.info("received request to submit job");
			this.valManager.submitJob(job);
		} catch (ValidatorException e) {
			throw new ValidatorServiceException(e);
		}
	}

	public BlackboardNotificationHandler<BlackboardServerHandler> getBlackboardNotificationHandler() {
		return blackboardNotificationHandler;
	}

	public ValidatorManager getValManager() {
		return valManager;
	}

	public void setValManager(ValidatorManager valManager) {
		this.valManager = valManager;
	}

	public void setBlackboardNotificationHandler(
			BlackboardNotificationHandler<BlackboardServerHandler> blackboardNotificationHandler) {
		this.blackboardNotificationHandler = blackboardNotificationHandler;
	}

}
