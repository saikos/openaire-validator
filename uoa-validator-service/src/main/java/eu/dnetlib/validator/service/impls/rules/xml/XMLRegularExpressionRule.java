package eu.dnetlib.validator.service.impls.rules.xml;

import java.util.Properties;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import eu.dnetlib.validator.engine.data.DataException;
import eu.dnetlib.validator.engine.execution.ValidationObject;
import eu.dnetlib.validator.service.impls.rules.RegularExpressionRule;
import eu.dnetlib.validator.service.impls.valobjs.XMLTextValidationObject;

public class XMLRegularExpressionRule extends RegularExpressionRule implements XMLRule {

	private static final long serialVersionUID = -7791692519583028162L;
	public XMLRegularExpressionRule(Properties pros, int id) {
		super(pros, id);
	}

	
	@Override
	public boolean apply(ValidationObject obj) {
		XMLTextValidationObject tobj = (XMLTextValidationObject) obj;
		NodeList nodes;
		try {
			nodes = tobj.getNodes(this.pros.getProperty(XPATH));
		} catch (DataException e) {
			log.error("", e);
			return false;
		}
		int success = 0, all = nodes.getLength();
		for(int i=0; i<nodes.getLength();i++) {
			Node node = nodes.item(i);
			log.debug("XML RegExp Rule. Field Value: "+node.getNodeValue());
			if(node.getNodeValue().matches(this.pros.getProperty(REGEXP)))
				success++;
		}
		
		String successConditions = this.pros.getProperty(SUCCESS);
		return Utils.success(successConditions, success, all);
	}
}
