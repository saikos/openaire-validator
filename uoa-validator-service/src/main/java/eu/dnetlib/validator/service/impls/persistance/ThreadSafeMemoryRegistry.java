package eu.dnetlib.validator.service.impls.persistance;

import java.io.Serializable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * A {@link MemoryRegistry} that may be accesses by multiple threads silmutaneously.
 * @author Manos Karvounis
 * @param <T>
 */
public class ThreadSafeMemoryRegistry<T extends Serializable> extends MemoryRegistry<T> implements ThreadSafeRegistry {
	
	private final Lock lock = new ReentrantLock();
	
	public ThreadSafeMemoryRegistry(String name) {
		super(name);
	}

	@Override
	public void lock() {
		lock.lock();
	}

	@Override
	public void unlock() {
		lock.unlock();
	}

	@Override
	public void removeObject(int id) {
		registry.remove(id);
	}

}
