package eu.dnetlib.validator.service.config;

import java.util.Properties;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

/**
 * CascadingPropertyLoader loads a number of property files and mergers them together, so that the last properties
 * override the previous. It also supports property expansion like:
 * 
 * <code>
 *   something = 1
 *   somethingelse = 2
 *   test = ${something}/${somethingelse}
 *  </code>
 * 
 * <p>
 * And if you override something to XX, then test will become XX/2
 * </p>
 * 
 * 
 * @author marko
 * 
 */
public class CascadingPropertyLoader extends PropertyPlaceholderConfigurer implements InitializingBean {

	private Properties properties;

	public void afterPropertiesSet() throws Exception {
		this.properties = mergeProperties();

		// Convert the merged properties, if necessary.
		convertProperties(this.properties);
		
		logger.debug("Properties: " + properties);
	}

	@Override
	protected void processProperties(final ConfigurableListableBeanFactory beanFactoryToProcess, final Properties props) throws BeansException {
		super.processProperties(beanFactoryToProcess, props);
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(final Properties properties) {
		super.setProperties(properties);
		
		this.properties = properties;
	}
}