package eu.dnetlib.validator.service.impls.rules.oaipmh;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import org.w3c.dom.NodeList;

import eu.dnetlib.validator.engine.data.DataException;
import eu.dnetlib.validator.engine.data.Rule;
import eu.dnetlib.validator.engine.data.RuleException;
import eu.dnetlib.validator.engine.execution.ValidationObject;
import eu.dnetlib.validator.service.impls.providers.OAIPMHSinglePageVerbProvider;
import eu.dnetlib.validator.service.impls.valobjs.XMLTextValidationObject;

/**
 * Represents a rule that checks if the resumptions tokens used by an OAI-PMH
 * repository have at least the duration specified by the
 * {@link OAIPMHResumptionTokenDurationRule#DURATION} parameter. The Validation
 * Object used in
 * {@link OAIPMHResumptionTokenDurationRule#apply(ValidationObject)} must be
 * provided from a {@link OAIPMHSinglePageVerbProvider} that issues a simple
 * 'ListRecords&metadataPrefix=oai_dc' verb (no need for full harvesting)
 * 
 * @author Manos Karvounis
 */
public class OAIPMHResumptionTokenDurationRule extends Rule {

	public OAIPMHResumptionTokenDurationRule(Properties pros, int id) {
		super(pros, id);
	}

	private static final long serialVersionUID = 1409872914282475394L;

	/**
	 * In millis
	 */
	public static final String DURATION = "duration";

	@Override
	public boolean apply(ValidationObject obj) throws RuleException {
		XMLTextValidationObject tobj = (XMLTextValidationObject) obj;
		NodeList nodes, nodes1;
		try {
			nodes = tobj.getNodes("/OAI-PMH/ListRecords/resumptionToken/@expirationDate");
			nodes1 = tobj.getNodes("/OAI-PMH/responseDate/text()");
		} catch (DataException e) {
			log.error("", e);
			return false;
		}

		try {
			if (nodes.getLength() == 0)
				return true;
			String expirationDateS = nodes.item(0).getNodeValue();
			String responseDateS = nodes1.item(0).getNodeValue();

			SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
			Date expirationDate = null, responseDate = null;
			try {
				expirationDate = df1.parse(expirationDateS);
			} catch (Exception ex) {
				try {
					expirationDate = df2.parse(expirationDateS);
				} catch (Exception e) {
					return false;
				}
			}
			try {
				responseDate = df1.parse(responseDateS);
			} catch (Exception ex) {
				try {
					responseDate = df2.parse(responseDateS);
				} catch (Exception e) {
					return false;
				}
			}
			
			log.debug("Resumption Token Duration. Response date: "+responseDate+", expiration date: "+expirationDate);
			
			Calendar responseCal = Calendar.getInstance();
			Calendar expiryCal = Calendar.getInstance();
			responseCal.setTime(responseDate);
			expiryCal.setTime(expirationDate);
			long response = responseCal.getTimeInMillis();
			long expiry = expiryCal.getTimeInMillis();

			long duration = Long.parseLong(this.pros.getProperty(DURATION));

			return (expiry - response >= duration);
		} catch (Exception e) {
			log.error("", e);
			throw new RuleException(e.getMessage());
		}
	}

}
