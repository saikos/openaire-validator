package eu.dnetlib.validator.service.impls.rules.xml;

import org.apache.log4j.Logger;

import eu.dnetlib.validator.service.impls.valobjs.XMLTextValidationObject;

/**
 * Indicates that a rule is applied on xml text objects.
 * @author Manos Karvounis
 * @author Nikon Gasparis
 * @see XMLTextValidationObject
 */
public interface XMLRule {
	
	/**
	 * The xpath that will be used to retrieve specific nodes on which the rule will be applied.
	 */
	public static final String XPATH = "xpath";
	// 'all', '>0', '0'
	/**
	 * Accepted values:
	 * 'all': The rule is successful if all the nodes retrieved by the XPATH have the rule applied successfully on them
	 * '>0': If at least one of the nodes is successful
	 * '0': If none of the nodes is successful
	 * '1': If total nodes number is 1 and that node succeeds
	 */
	public static final String SUCCESS = "success";
	
	public class Utils {
		public static boolean success(String condition, int successes, int all) {
			Logger log = Logger.getLogger(Utils.class);
			log.debug("condition: "+condition+", "+"successes: "+successes+" all: "+all);
			if(condition.equals("all")) {
				return (successes == all && all != 0);
			} else if(condition.equals(">0")) {
				return successes > 0;
			} else if(condition.equals("0")) {
				return successes == 0;
			} else if(condition.equals("1")) {
				return (successes == 1 && all == 1);
			} else
				return false;
		}
	}
}
