package eu.dnetlib.validator.service.impls.rules.oaipmh;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.w3c.dom.NodeList;

import eu.dnetlib.validator.engine.data.DataException;
import eu.dnetlib.validator.engine.data.ResultSet;
import eu.dnetlib.validator.engine.data.Rule;
import eu.dnetlib.validator.engine.data.RuleException;
import eu.dnetlib.validator.engine.execution.ValidationObject;
import eu.dnetlib.validator.service.impls.providers.OAIPMHSinglePageVerbProvider;
import eu.dnetlib.validator.service.impls.valobjs.XMLTextValidationObject;

/**
 * <p>
 * Checks if an OAI-PMH repository handles Date Granularities correctly, i.e.,
 * the granularity reported in Identify is the same as the one used in the
 * records' datestamps.
 * </p>
 * <p>
 * The Validation Object used in
 * {@link OAIPMHDateGranularityRule#apply(ValidationObject)} must be provided
 * from a {@link OAIPMHSinglePageVerbProvider} that issues a simple
 * 'ListIdentifiers&metadataPrefix=oai_dc' verb (no need for full harvesting)
 * </p>
 * 
 * @author Manos Karvounis
 * 
 */
public class OAIPMHDateGranularityRule extends Rule {

	public OAIPMHDateGranularityRule(Properties pros, int id) {
		super(pros, id);
	}

	private static final long serialVersionUID = -3213680069275644769L;

	public static final String BASEURL = "base_url";

	@Override
	public boolean apply(ValidationObject obj) throws RuleException {
		XMLTextValidationObject tobj = (XMLTextValidationObject) obj;
		NodeList nodes;
		try {
			nodes = tobj.getNodes("/OAI-PMH/ListIdentifiers/header/datestamp/text()");
		} catch (DataException e) {
			log.error("", e);
			return false;
		}

		Properties pros = new Properties();
		pros.setProperty(OAIPMHSinglePageVerbProvider.BASEURL, this.pros.getProperty(BASEURL));
//		pros.setProperty(OAIPMHSinglePageVerbProvider.DELAY, "1000");
//		pros.setProperty(OAIPMHSinglePageVerbProvider.RETRY_DELAY, "60000");
//		pros.setProperty(OAIPMHSinglePageVerbProvider.TIMEOUT, "60000");
//		pros.setProperty(OAIPMHSinglePageVerbProvider.RETRY_EFFORTS, "5");
		pros.setProperty(OAIPMHSinglePageVerbProvider.VERB, "Identify");
		OAIPMHSinglePageVerbProvider prv = new OAIPMHSinglePageVerbProvider();
		prv.setConfiguration(pros);

		try {
			ResultSet<ValidationObject> identify = prv.getValidationObjects();
			identify.next();
			XMLTextValidationObject xiden = (XMLTextValidationObject) identify.get();
			NodeList grns = xiden.getNodes("/OAI-PMH/Identify/granularity/text()");
			String granularity = grns.item(0).getNodeValue();

			log.debug("Date Granularity Rule. Granularity: " + granularity);

			List<String> regularExpressionValues = new ArrayList<String>();
			if (granularity.equals("YYYY-MM-DDThh:mm:ssZ")) {
				regularExpressionValues.add("\\d\\d\\d\\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])T([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]Z");
			} else if (granularity.equals("YYYY-MM-DD")) {
				regularExpressionValues.add("\\d\\d\\d\\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])");
				regularExpressionValues.add("\\d\\d\\d\\d");
				regularExpressionValues.add("\\d\\d\\d\\d[- /.](0[1-9]|1[012])");
			} else {
				return false;
			}

			Matcher matcher;
			Pattern pattern;
			for (int i = 0; i < nodes.getLength(); i++) {
				String datestamp = nodes.item(i).getNodeValue();
				log.debug("Date Granularity Rule. Datestamp: " + datestamp);
				boolean matched = false;
				for (String regExpr : regularExpressionValues) {
					pattern = Pattern.compile(regExpr);
					matcher = pattern.matcher(datestamp);
					if (matcher.matches()) {
						log.debug("Date Granularity Rule. Datestamp: " + datestamp + " (matched)");
						matched = true;
						break;
					}
				}
				if (!matched)
					return false;
			}
			return true;
		} catch (Exception e) {
			log.error("", e);
			throw new RuleException(e.getMessage());
		}
	}

}
