package eu.dnetlib.validator.service.impls.rules;

import java.util.List;
import java.util.Properties;

import eu.dnetlib.validator.engine.data.Rule;
import eu.dnetlib.validator.engine.data.RuleException;
import eu.dnetlib.validator.engine.execution.ValidationObject;

/**
 * Applies a chain of rules on a single Validation Object.
 * The behavior may be changedusing the {@link ChainRule#TYPE} property.
 * @author manos
 *
 * @param <T>
 */
public class ChainRule<T extends Rule> extends Rule {

	private static final long serialVersionUID = -6422953546341833949L;
	protected final List<T> rules;
	
	/**
	 * Permitted values:
	 * 'or': true if any of the rules is successful
	 * 'and': true if all the rules are successful
	 * 'horn': rule[0] => rule[1] /\ rule[2] /\ ... /\ rule[n-1]
	 */
	public static final String TYPE = "type";
	
	public ChainRule(Properties pros, int id, List<T> rules) {
		super(pros, id);
		this.rules = rules;
		log.debug("Chain Rule. Rules: "+rules);
	}

	@Override
	public boolean apply(ValidationObject obj) throws RuleException {
		if(rules.size() == 0)
			return true;
		String condition = pros.getProperty(TYPE).trim();
		if(condition.equalsIgnoreCase("AND")) {
			for(T rule : rules) {
				if(!rule.apply(obj))
					return false;
			}
			return true;
		} else if(condition.equalsIgnoreCase("OR")) {
			for(T rule : rules) {
				if(rule.apply(obj))
					return true;
			}
			return false;
		} else if(condition.equalsIgnoreCase("HORN")) {
			if(!rules.get(0).apply(obj))
				return true;
			for(int i=1; i<rules.size(); i++) {
				T rule = rules.get(i);
				if(!rule.apply(obj))
					return false;
			}
			return true;
		} else {
			return false;
		}
	}

}
