package eu.dnetlib.validator.service.impls.listeners;

public abstract class ListenersManager {

	public abstract ValidatorJobListener createListener();

	public abstract CompatibilityTestListener createCompTestListener();

	public abstract RegistrationListener createRegListener();

	public abstract DnetListener createDnetListener();

	public abstract CrisListener createCrisListener();

}
