package eu.dnetlib.validator.service.impl;

import eu.dnetlib.enabling.tools.blackboard.BlackboardJob;
import eu.dnetlib.enabling.tools.blackboard.BlackboardNotificationHandler;
import eu.dnetlib.enabling.tools.blackboard.BlackboardServerHandler;
import org.apache.log4j.Logger;

public class NotificationListener extends
        BlackboardNotificationHandler<BlackboardServerHandler> {

    private static Logger logger = Logger.getLogger(NotificationListener.class);
    private ValidatorManager valManager;

    @Override
    protected void processJob(BlackboardJob job) {
        super.processJob(job);

        logger.debug("New Job!");
//		String validationSet = job.getParameters().get("validationSet");
//		String validationType = job.getParameters().get("validationType");
        String type = job.getParameters().get("type"); //OAI
        String mdstoreId = job.getParameters().get("mdstoreId");
        String guidelines = job.getParameters().get("guidelines");
        String groupBy = job.getParameters().get("groupBy");
        String records = job.getParameters().get("records");
        String outputEpr = job.getParameters().get("outputEpr");
        if (outputEpr == null) {
            outputEpr = "false";
        }

        String blacklistGuidelines = job.getParameters().get("blacklistGuidelines");
        if (blacklistGuidelines == null)
            blacklistGuidelines = guidelines;

        String blacklistedRecords = job.getParameters().get("blacklistedRecords");
        if (blacklistedRecords == null) {
            blacklistedRecords = "false";
        }

        String submissionDate = job.getParameters().get("submissionDate");

        String jobStatusUpdateInterval = job.getParameters().get("jobStatusUpdateInterval");

        //the datasource id
        String datasourceId = job.getParameters().get("datasourceId");

        //the datasource baseUrl, optional for type=OAI, mandatory for type=DNET
        String baseUrl = job.getParameters().get("baseUrl");

        //the namespace prefix of the datasource, optional
        String datasourceNamespacePrefix = job.getParameters().get("datasourceNamespacePrefix");

        //the datasource name, optional
        String datasourceName = job.getParameters().get("datasourceName");

        //json map of other parameters that may be of interest, optional
        String extra_param = job.getParameters().get("extra_param");

        try {
            int workers = 1;
            int recordsInt = 0;
            int jobStatusUpdateIntervalInt = 100;
            if (records == null || records.equalsIgnoreCase("all"))
                recordsInt = -1;
            else
                recordsInt = Integer.parseInt(records);
            if (recordsInt > -1 && recordsInt < 100)
                workers = 1;
            else
                workers = 4;

            if (job.getParameters().get("workers") != null)
                workers = Integer.parseInt(job.getParameters().get("workers"));

            if (jobStatusUpdateInterval != null)
                jobStatusUpdateIntervalInt = Integer.parseInt(jobStatusUpdateInterval);

            if (recordsInt != 0) {
                logger.debug("New Job of type: " + type + " - workers: " + workers + " - recordsInt: " + recordsInt);
                logger.debug("workers: " + workers);
                logger.debug("recordsInt: " + recordsInt);
                logger.debug("jobStatusUpdateInterval: " + jobStatusUpdateIntervalInt);
                if (job.getAction().equalsIgnoreCase("VALIDATE")) {
                    if (type.equalsIgnoreCase("OAI")) {
                        logger.debug("type is oai");

                    } else if (type.equalsIgnoreCase("DNET")) {
                        logger.debug("type is dnet");
                        logger.debug("groupBy: " + groupBy);
//						this.getBlackboardHandler().ongoing(job);
                        valManager.beginDataJobForWorkflow(mdstoreId, guidelines, groupBy, recordsInt, workers, job, this, jobStatusUpdateIntervalInt, Boolean.parseBoolean(outputEpr), Boolean.parseBoolean(blacklistedRecords), blacklistGuidelines);
                    }
                } else {
                    throw new Exception("Unknown action");
                }
            } else {
                throw new Exception("There are no records in the MDStore.");
            }
        } catch (Exception e) {
            logger.error("Failed to begin the workflow for the job received", e);
            getBlackboardHandler().failed(job, e);
        }
    }

    public ValidatorManager getValManager() {
        return valManager;
    }

    public void setValManager(ValidatorManager valManager) {
        this.valManager = valManager;
    }
}