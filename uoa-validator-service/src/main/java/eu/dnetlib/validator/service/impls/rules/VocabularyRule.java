package eu.dnetlib.validator.service.impls.rules;

import java.util.Properties;

import eu.dnetlib.validator.engine.data.Rule;

/**
 * Represents a rule that checks if an object has a value matching on of the terms contained in {@link VocabularyRule#TERMS}
 * @author Manos Karvounis
 * @author Nikon Gasparis
 *
 */
public abstract class VocabularyRule extends Rule {

	private static final long serialVersionUID = 6618578915649889004L;

	/**
	 * comma-separated list of terms
	 */
	public static final String TERMS = "terms";
	
	public static final String TERMS_TYPE = "terms_type";
	
	public VocabularyRule(Properties pros, int id) {
		super(pros, id);
	}

}
