package eu.dnetlib.validator.service.impls.persistance;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import eu.dnetlib.validator.engine.persistance.Registry;

/**
 * A Registry that stores the objects in an in-memory {@link Map}
 * @author Manos Karvounis
 * @param <T>
 */
public class MemoryRegistry<T extends Serializable> extends Registry<T>  {

	protected Map<Integer, T> registry = null;

	public MemoryRegistry(String name) {
		super(name);
		this.registry = new HashMap<Integer, T>();
	}

	@Override
	public T getObject(int id) {
		return registry.get(id);
	}

	@Override
	public void addObject(int id, T obj) {
		registry.put(id, obj);
	}

	public Map<Integer, T> getAllObjects() {
		return this.registry;
	}
}
