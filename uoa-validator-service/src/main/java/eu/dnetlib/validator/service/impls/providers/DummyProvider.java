package eu.dnetlib.validator.service.impls.providers;

import eu.dnetlib.validator.engine.data.DataException;
import eu.dnetlib.validator.engine.data.Provider;
import eu.dnetlib.validator.engine.data.ResultSet;
import eu.dnetlib.validator.engine.execution.ValidationObject;

/**
 * A dummy provider that returns a single, dummy Validation Object.
 * This provider is used along with Rules that do not need an external validation object to be provided to them.
 * @author Manos Karvounis
 */
public class DummyProvider extends Provider {

	private static final long serialVersionUID = -8644756507323283600L;

	public DummyProvider(Integer id) {
		super(id);
	}

	@Override
	public ResultSet<ValidationObject> getValidationObjects() throws ProviderException {
		return new DummyResultSet();
	}

	@Override
	public ResultSet<String> getValidationObjectIds() throws ProviderException {
		throw new UnsupportedOperationException();
	}

	@Override
	public ValidationObject getValidationObject(String valObjId) throws ProviderException {
		throw new UnsupportedOperationException();
	}
	
	private class DummyValidationObject implements ValidationObject {
		
		@Override
		public String getId() {
			return this.hashCode()+"";
		}

		@Override
		public void setId(String id) {
		}

		
		@Override
		public Object getContentAsObject() {
			return null;
		}

		@Override
		public String getStatus() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
	
	private class DummyResultSet implements ResultSet<ValidationObject> {

		private boolean done = false;
		
		@Override
		public boolean next() throws DataException {
			if(!done) {
				done = true;
				return true;
			}
			return false;
		}

		@Override
		public ValidationObject get() throws DataException {
			return new DummyValidationObject();
		}

		@Override
		public String getError() {
			// TODO Auto-generated method stub
			return null;
		}
	}

	@Override
	public ResultSet<ValidationObject> getValidationObjects(String entity)
			throws ProviderException {
		// TODO Auto-generated method stub
		return null;
	}

}
