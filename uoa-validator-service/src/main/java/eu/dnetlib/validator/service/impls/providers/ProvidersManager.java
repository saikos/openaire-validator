package eu.dnetlib.validator.service.impls.providers;

public abstract class ProvidersManager {

	public abstract OAIPMHRecordProvider createOaipmhRecordProvider();

	public abstract DnetProviderNew createDnetProvider();

	public abstract OAIPMHSinglePageVerbProvider createOaipmhSinglePageVerbProvider();

	public abstract CrisProvider createCrisProvider();

}
