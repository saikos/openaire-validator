package eu.dnetlib.validator.service.impls.executors;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.core.task.SyncTaskExecutor;

import eu.dnetlib.validator.engine.ValidatorException;
import eu.dnetlib.validator.engine.data.Provider;
import eu.dnetlib.validator.engine.data.Provider.ProviderException;
import eu.dnetlib.validator.engine.data.ResultSet;
import eu.dnetlib.validator.engine.data.Rule;
import eu.dnetlib.validator.engine.execution.Executor;
import eu.dnetlib.validator.engine.execution.Job;
import eu.dnetlib.validator.engine.execution.JobListener;
import eu.dnetlib.validator.engine.execution.Task;
import eu.dnetlib.validator.engine.execution.TaskList;
import eu.dnetlib.validator.engine.execution.ValidationObject;
import eu.dnetlib.validator.service.impls.providers.DnetProvider;
import eu.dnetlib.validator.service.impls.providers.OAIPMHRecordProvider;

/**
 * Retrieves the Validation Objects of a {@link Job} using the contained
 * Provider. A new Task in created for each Validation Object and all the rules
 * contained in the Job. Each task is submitted for execution at an
 * {@link Executor}
 * 
 * @author Nikon Gasparis
 *
 */
public class JobWorker implements Runnable {

	private transient Logger log = Logger.getLogger(JobWorker.class);

	private final SyncTaskExecutor taskExecutor;
	public final long generalTimeout;
	public final String set;
	public final int jobId;
	public final Set<Rule> rules;
	public final Provider provider;
	public final JobListener validatorListener;
	public final JobListener[] listeners;

	protected Map<String, Object> jobContext;

	private Map<String, Object> recordContext;
	private int deletedRecords = 0;

	public JobWorker(int jobId, Set<Rule> rules, String set, Provider provider,
			JobListener validatorListener, SyncTaskExecutor taskExecutor,
			long generalTimeout, JobListener... listeners) {
		super();
		log.debug("Creating a new job Worker with generalTimeout " + generalTimeout);
		this.taskExecutor = taskExecutor;
		this.generalTimeout = generalTimeout;
		this.set = set;
		this.jobId = jobId;
		this.rules = rules;
		this.provider = provider;
		this.jobContext = new HashMap<String, Object>();
		this.listeners = listeners;
		this.validatorListener = validatorListener;

	}

	public void submit() throws JobWorkerException {
		long elapsed = 0;
		int count = 0;
		int record_limit = -1;
		log.debug("Submitting job");
		if (provider.getConfiguration().getProperty(
				OAIPMHRecordProvider.RECORDS) != null) {
			if (!provider.getConfiguration()
					.getProperty(OAIPMHRecordProvider.RECORDS).trim()
					.equals("-1")) {
				record_limit = Integer.parseInt(provider.getConfiguration()
						.getProperty(OAIPMHRecordProvider.RECORDS).trim());
			}
		}
		if (provider.getConfiguration().getProperty(DnetProvider.RECORDS) != null) {
			if (!provider.getConfiguration().getProperty(DnetProvider.RECORDS)
					.trim().equals("-1")) {
				record_limit = Integer.parseInt(provider.getConfiguration()
						.getProperty(DnetProvider.RECORDS).trim());
			}
		}
		
//		int batchSize = -1;
//		if (provider.getConfiguration().getProperty(DnetProvider.BATCH_SIZE) != null) {
//			 batchSize = Integer.parseInt(provider.getConfiguration()
//						.getProperty(DnetProvider.BATCH_SIZE));
//		}
		log.debug("Number of records to validate: " + record_limit
				+ " (-1 for all)");

		Boolean crisFlag = false;
		if (set != null && set.contains("_cris_"))
			crisFlag = true;

		boolean success = false;
		List<Task> tasks = new ArrayList<Task>();
		ResultSet<ValidationObject> ivobjs = null;
		try {
			if (!crisFlag) {
				log.debug("request to get validation objects without set..");
				ivobjs = provider.getValidationObjects();
			} else {
				log.debug("request to get validation objects with set: " + set);
				ivobjs = provider.getValidationObjects(set);
			}
		} catch (ProviderException e1) {
			log.error("Error while getting validation objects from provider",
					e1);
			for (JobListener listener : listeners) {
				listener.failed(jobId, this.jobContext, e1);
			}
		}
		try {
			long recordsTime = 0;
			while ((count < record_limit || record_limit == -1)
					&& ivobjs.next()) {
				this.recordContext = new HashMap<String, Object>();
				tasks.clear();
				ValidationObject vobj = ivobjs.get();

				log.debug("Checking if rules will be applied on object "
						+ vobj.getId());
				if (vobj.getStatus() != null
						&& vobj.getStatus().equalsIgnoreCase("deleted")) {
					log.debug("Object is deleted and will be ignored");
					deletedRecords++;
					continue;
				}

				
				if (crisFlag
						&& (vobj.getId().contains("cfEAddr") || vobj.getId()
								.contains("cfEquip"))) {
					log.debug("ignoring cfEaddr and cfEquip records from persons and datasets");
					continue;
				}

				log.debug("Applying rules on object " + vobj.getId());
				count++;
				for (Rule rule : rules) {
					if (rule != null) {
						rule.setValObjId(vobj.getId());
						rule.setProvider(provider);
						Task task = new Task(vobj, rule);
						tasks.add(task);
					}
				}

				TaskList ltasks = new TaskList(tasks);
				try {
					long time1 = Calendar.getInstance().getTimeInMillis();
//					Object lock = new Object();
					taskExecutor.execute(ltasks);
					long time2 = Calendar.getInstance().getTimeInMillis();
					log.debug("Task execution took " + ((time2 - time1))
							+ " milli seconds");
					elapsed += time2 - time1;
					log.debug("Elapsed time till now is " + elapsed/1000
							+ " seconds");
					recordsTime += time2 - time1;
//				    if (count % batchSize == 0) {	
//						log.debug("Elapsed time for " + batchSize + " records is  " + recordsTime
//								+ " milli seconds");
//						recordsTime = 0;
//					}
//					if (elapsed > this.generalTimeout)
//						throw new JobWorkerException("Job timed out");
					long time3 = Calendar.getInstance().getTimeInMillis();
					for (JobListener listener : listeners) {
						listener.currentResults(ltasks.getCtasks(), jobId,
								vobj.getContentAsObject(), this.recordContext);
					}
					long time4 = Calendar.getInstance().getTimeInMillis();
					log.debug("Informing listeners took " + ((time4 - time3))
							+ " milli seconds");
					success = true;
				} catch (Exception e) {
					log.error("an error occured while executing tasks: ", e);
					for (JobListener listener : listeners) {
						listener.currentResults(ltasks.getCtasks(), jobId,
								vobj.getContentAsObject(), this.recordContext,
								e);
					}
				}

			}
		} catch (Exception e) {
			log.error("data error", e);
			for (JobListener listener : listeners) {
				listener.failed(jobId, this.jobContext, e);
			}
		}
		if (success) {
			jobContext.put("deletedRecords", deletedRecords);
			for (JobListener listener : listeners) {
				listener.finished(jobId, this.jobContext);
			}
		} else {
			log.error("an error occured");
			for (JobListener listener : listeners) {
				if (ivobjs.getError() == null)
					listener.failed(jobId, this.jobContext,
							new JobWorkerException("All tasks failed"));
				else {
					listener.failed(jobId, this.jobContext,
							new JobWorkerException(ivobjs.getError()));
					log.error("an error occured.", new JobWorkerException(
							ivobjs.getError()));
				}
			}
		}
	}

	@Override
	public void run() {
		try {
			submit();
		} catch (JobWorkerException e) {
			log.error("", e);
			for(JobListener listener : listeners){
				listener.failed(jobId, this.jobContext, e);
			}
			validatorListener.failed(jobId, this.jobContext, e);
			return;
		}
		validatorListener.finished(jobId, this.jobContext);
	}

	public class JobWorkerException extends ValidatorException {

		private static final long serialVersionUID = -1748951703892618739L;

		public JobWorkerException() {
			super();
		}

		public JobWorkerException(String msg) {
			super(msg);
		}
	}

}
