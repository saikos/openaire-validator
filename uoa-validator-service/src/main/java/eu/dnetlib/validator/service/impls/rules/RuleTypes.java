package eu.dnetlib.validator.service.impls.rules;

import java.util.HashMap;

import org.apache.log4j.Logger;

import eu.dnetlib.validator.service.impls.rules.oaipmh.OAIPMHDateGranularityRule;
import eu.dnetlib.validator.service.impls.rules.oaipmh.OAIPMHEmbargoDateRule;
import eu.dnetlib.validator.service.impls.rules.oaipmh.OAIPMHIncrementalRecordDeliveryRule;
import eu.dnetlib.validator.service.impls.rules.oaipmh.OAIPMHResumptionTokenDurationRule;
import eu.dnetlib.validator.service.impls.rules.xml.XMLCardinalityRule;
import eu.dnetlib.validator.service.impls.rules.xml.XMLCrisClassVocabularyRule;
import eu.dnetlib.validator.service.impls.rules.xml.XMLCrisReferentialIntegrityRule;
import eu.dnetlib.validator.service.impls.rules.xml.XMLFieldExistsRule;
import eu.dnetlib.validator.service.impls.rules.xml.XMLNotConfusedFieldsRule;
import eu.dnetlib.validator.service.impls.rules.xml.XMLRegularExpressionRule;
import eu.dnetlib.validator.service.impls.rules.xml.XMLValidURL;
import eu.dnetlib.validator.service.impls.rules.xml.XMLVocabularyRule;


public class RuleTypes {
	
	static private Logger logger = Logger.getLogger(RuleTypes.class);

	static private HashMap<String, String> types;
	static private HashMap<String, String> entities;
	
	static {
		logger.debug("initializing the RuleTypes map");
		types = new HashMap<String, String>();
		//rules
//		types.put("Cardinality", CardinalityRule.class.getName());
		types.put("ChainRule", ChainRule.class.getName());
//		types.put("Regular Expression", RegularExpressionRule.class.getName());
//		types.put("Valid Url", ValidUrlRule.class.getName());
//		types.put("Vocabulary", VocabularyRule.class.getName());
		//rules OAIPMH
		types.put("OAIPMH Incremental Record Delivery", OAIPMHIncrementalRecordDeliveryRule.class.getName());
		types.put("OAIPMH Date Granularity", OAIPMHDateGranularityRule.class.getName());
		types.put("OAIPMH Resumption Token Duration Check", OAIPMHResumptionTokenDurationRule.class.getName());
		types.put("OAIPMH Embargo Date Check", OAIPMHEmbargoDateRule.class.getName());
		//rules TEXT
//		types.put("TEXT Regular Expression", TextRegularExpressionRule.class.getName());
//		types.put("TEXT", TextRule.class.getName());
//		types.put("TEXT Vocabulary", TextVocabularyRule.class.getName());		
		//rules XML
		types.put("XML Cardinality", XMLCardinalityRule.class.getName());
		types.put("XML Field Exists", XMLFieldExistsRule.class.getName());
		types.put("XML Not Confused Fields", XMLNotConfusedFieldsRule.class.getName());
		types.put("XML Regular Expression", XMLRegularExpressionRule.class.getName());
//		types.put("XML", XMLRule.class.getName());
		types.put("XML Valid Url", XMLValidURL.class.getName());
		types.put("XML Vocabulary", XMLVocabularyRule.class.getName());
		types.put("XML Cris Class Vocabulary", XMLCrisClassVocabularyRule.class.getName());
		types.put("XML CRIS Referential Integrity Rule", XMLCrisReferentialIntegrityRule.class.getName());

		//unsupported
//		types.put("Retrievable Resource", RetrievableResourceRule.class.getName());
//		types.put("Schema Validity Check", SchemaValidityRule.class.getName());

		logger.debug("initializing the RuleEntities map");
		entities = new HashMap<String, String>();
		entities.put("Publication", "openaire_cris_publications");
		entities.put("Person", "openaire_cris_persons");
		entities.put("Organisation", "openaire_cris_orgunits");
		entities.put("Project", "openaire_cris_projects" );
		entities.put("Funding", "openaire_cris_funding" );
		entities.put("Service", "openaire_cris_services");
		entities.put("Product", "openaire_cris_datasets");		
	}

	public static HashMap<String, String> getEntities() {
		return entities;
	}

	public static void setEntities(HashMap<String, String> entities) {
		RuleTypes.entities = entities;
	}
	
	static public String getSetOfEntity(String entity) {
		return entities.get(entity);
	}
		
	static public String[] getTypes() {
		logger.debug("getting all types of rules");
		String[] dummy = new String[0];
		return types.keySet().toArray(dummy);
	}
	
	static public String getClassOfType(String type) {
		logger.debug("getting class of rule type "+type);
		return types.get(type);
	}
	
}
