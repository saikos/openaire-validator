package eu.dnetlib.validator.service.impls.listeners;

import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class RecordXMLBuilder {

	private static Logger logger = Logger.getLogger(RecordXMLBuilder.class);
	
	private VelocityEngine ve = null;
	
	public void init() throws Exception {
		this.ve = new VelocityEngine();
		this.ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		this.ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
		this.ve.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.NullLogSystem");
		this.ve.init();	
	}
	
	public String buildXml(List<Map<String, String>> veloList, Object record, Map<String, String> recordValidationResult) {
		logger.debug("Building XML file for record");
		String xmlRetString = null;
		
		try {
        
			VelocityContext context = new VelocityContext();
			NodeList list = ((Node) record).getChildNodes();
	        Node recordNode = null;
	        for(int i = 0; i < list.getLength(); i++)
	        {
	            Node node = list.item(i);
	            if(node.getNodeName().equals("root"))
	            {
	            	logger.debug("root found");
	            	list = node.getChildNodes();
	            	for(i = 0; i < list.getLength(); i++)
			        {
			            node = list.item(i);
			            logger.debug("node name: " + node.getNodeName());
			            logger.debug("node local name: " + node.getLocalName());
			            if(node.getNodeName().contains("record"))
			            {
			            	recordNode = node;
		                	logger.debug("record found");
		                	break;
			            }
			        }
	            }
	        } 
	        Transformer transformer;
			transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
//			transformer.setOutputProperty(OutputKeys.VERSION, "no");
			
			
			StreamResult result = new StreamResult(new StringWriter());
			DOMSource source = new DOMSource(recordNode);

			transformer.transform(source, result);
						
			String xmlString = result.getWriter().toString();
//			logger.debug("xmlstring: " + xmlString);
			context.put("recordFull", xmlString.substring(xmlString.indexOf("?>")+3));
			context.put("ruleList", veloList);
			context.put("record", recordValidationResult);
			Template t = this.ve.getTemplate("/eu/dnetlib/validator/service/listeners/xml.vm");
			
			StringWriter writer = new StringWriter();
			
			t.merge(context, writer);
			xmlRetString = writer.toString();
			
		} catch (Exception e) {
			logger.error("Error while building XML file", e);
		}
		
        return xmlRetString;
	}
}
