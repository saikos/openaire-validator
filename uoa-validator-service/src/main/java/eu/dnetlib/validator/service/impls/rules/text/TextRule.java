package eu.dnetlib.validator.service.impls.rules.text;

import eu.dnetlib.validator.service.impls.valobjs.TextValidationObject;

/**
 * Indicates a rule that may be applied on a Validation Object that is plain text.
 * @author Manos Karvounis
 * @see TextValidationObject
 */
public interface TextRule {

}
