package eu.dnetlib.validator.service.impls.rules;

import java.util.Properties;

import eu.dnetlib.validator.engine.data.Rule;

/**
 * Represents a rule that checks if an object has a value that is a valid URL
 * @author Manos Karvounis
 *
 */
public abstract class ValidUrlRule extends Rule {

	private static final long serialVersionUID = 1943143552438932457L;
	
	public ValidUrlRule(Properties pros, int id) {
		super(pros, id);
	}

}
