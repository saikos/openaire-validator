package eu.dnetlib.validator.service.impls.providers;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.springframework.core.task.TaskExecutor;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;

import eu.dnetlib.api.data.MDStoreService;
import eu.dnetlib.domain.EPR;
import eu.dnetlib.validator.engine.data.DataException;
import eu.dnetlib.validator.engine.data.Provider;
import eu.dnetlib.validator.engine.data.ResultSet;
import eu.dnetlib.validator.engine.execution.ValidationObject;
import eu.dnetlib.validator.service.impls.valobjs.XMLTextValidationObject;
import gr.uoa.di.driver.enabling.resultset.ResultSetFactory;
import gr.uoa.di.driver.util.ServiceLocator;

public class DnetProviderNew extends Provider {

	private static final long serialVersionUID = -4280319954962194170L;

	private static ServiceLocator<MDStoreService> mdStoreServiceLocator;

	private static ResultSetFactory rsFactory;

	private TaskExecutor harvesterExecutor;
	
	public static final String DATASOURCE = "DATASOURCE";
	
	public static final String BATCH_SIZE = "BATCH_SIZE";

	public static final String RECORDS = "RECORDS";
	
	public static final String MDSTORE_ID = "MDSTORE_ID";
	
	public static final String FROM = "FROM";
	
	public static final String BEGIN_RECORD = "BEGIN_RECORD";
	
	public static final String UNTIL = "UNTIL";
	
	public static final String RECORD_FILTER = "RECORD_FILTER";

	public static final String WORKER_ID = "WORKER_ID";

	public static final String WORKERS = "WORKERS";
	

	public DnetProviderNew() {
		super(3);
	}

	@Override
	public ResultSet<ValidationObject> getValidationObjects() throws ProviderException {
		return new DnetRecordResultSet();
	}
	
	private class DnetResultSet {
				
		gr.uoa.di.driver.enabling.resultset.ResultSet<String> rs = null;

		protected DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		protected DocumentBuilder builder;
		protected XPathFactory xfactory = XPathFactory.newInstance();
		protected List<Node> records = null;
		protected int index = -1;
		protected int globalIndex = -1;
		protected int workerId = 0;
		protected String error = null;
		private long elapsed = 0;
		protected BlockingQueue<String> queue;
		protected boolean allRecordsFetched = false;

		public DnetResultSet() {

			try {
				
				builder = factory.newDocumentBuilder();
	
				log.debug("Retrieving the datasource..");
				log.debug("WORKER_ID: " + pros.getProperty(WORKER_ID));
				
				workerId = Integer.parseInt(pros.getProperty(WORKER_ID));
				int workers = Integer.parseInt(pros.getProperty(WORKERS));
				log.debug("Issuing request on mdstore: " + pros.getProperty(MDSTORE_ID));
				EPR epr = mdStoreServiceLocator.getService().deliverMDRecords(pros.getProperty(MDSTORE_ID), null, null, null);

				rs = rsFactory.createResultSet(epr);

				log.debug("rs created");
				int recordsInRS = rs.size();
				
				log.debug("W"+ workerId + "# Number of records in ResultSet: " + recordsInRS);

				int recordsToTest = Integer.parseInt(pros.getProperty(RECORDS));
				
				if (recordsToTest == -1 || recordsToTest > recordsInRS)
					recordsToTest = recordsInRS;
				
				log.info("W"+ workerId + "# RECORDS TO TEST: " + recordsToTest);
				int beginRecord = workerId * (recordsToTest/workers) + 1;
				int endRecord = (recordsToTest/workers) + beginRecord -1;
				if (workerId == workers-1)
					endRecord += recordsToTest % workers;

				log.info("W"+ workerId + "# BEGIN RECORD: " + beginRecord);
				log.info("W"+ workerId + "# END RECORD: " + endRecord);
				
//				queue = new LinkedBlockingQueue<String>();
				queue = new ArrayBlockingQueue<String>(50000);
				DnetProviderHarvester harvester = new DnetProviderHarvester(queue, rs, beginRecord, endRecord, Integer.parseInt(pros.getProperty(BATCH_SIZE)), workerId);
				log.debug("W"+ workerId + "# Started harvester" );
				harvesterExecutor.execute(harvester);
				log.debug("W"+ workerId + "# Started harvester OK" );
			} catch (Exception e) {
				log.error("Error while initializing result Set", e);
			}
		}

		protected List<Node> getRecords() throws DataException {
			List<Node> records = null;
			
			try {
				if (!allRecordsFetched) {
					records = new ArrayList<Node>();
					List<String> tempRecords = new ArrayList<String>();
					long time1 = Calendar.getInstance().getTimeInMillis();
					for (int i=0 ; i < Integer.parseInt(pros.getProperty(BATCH_SIZE)) ; i++) {
						String record = queue.take();
						if (record.equalsIgnoreCase("finished")) {
							allRecordsFetched = true;
							break;
						}
						else 
							tempRecords.add(record);
					}
					long time2 = Calendar.getInstance().getTimeInMillis();
					log.debug("W"+ workerId + "#records fetching from queue took " + ((time2 - time1))
							+ " milliseconds");
					elapsed += time2 - time1;
					log.error("W"+ workerId + "#records fetched : " + tempRecords.size());
					log.debug("W"+ workerId + "#Elapsed time till now for records fetching " + elapsed/1000
							+ " seconds");
	
					long time3 = Calendar.getInstance().getTimeInMillis();
					for (String record : tempRecords) {
						InputSource is = new InputSource(new StringReader(record));
						Document doc = builder.parse(is);
						XPath xpath = xfactory.newXPath();
						XPathExpression expr = xpath.compile("//*[local-name()='record']");
						records.add((Node) expr.evaluate(doc, XPathConstants.NODE));
					}
					long time4 = Calendar.getInstance().getTimeInMillis();
		            log.debug("W"+ workerId + "# records conversion took " + ((time4 - time3))
							+ " milliseconds");
					log.error("W"+ workerId + "# records to return : " + records.size());
				}
				
			} catch (Exception e) {
				log.error("Error while getting records", e);
				throw new DataException();
			}
			return records;
		}		
	}

	private class DnetRecordResultSet extends DnetResultSet implements ResultSet<ValidationObject> {

		@Override
		public String getError() {
			if (error != null)
				log.debug("An error occured "+ this.error);
			else
				log.debug("No errors on request");
			return this.error;
		}
		
		@Override
		public boolean next() throws DataException {
			index++;
			globalIndex++;
			log.debug("Moving cursor to result " + index);
			log.debug("GlobalIndex " + globalIndex);
			if (records == null || index >= records.size()) {
				if (allRecordsFetched)
					return false;
				else {
					index = -1;
					records = getRecords();
					return next();
				}
			}
			return true;
		}

		@Override
		public ValidationObject get() throws DataException {
			XMLTextValidationObject ret = null;
			
			Document newXmlDocument;
			try {
				long time1 = Calendar.getInstance().getTimeInMillis();
				newXmlDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

		        Element root = newXmlDocument.createElement("root");
		        newXmlDocument.appendChild(root);
	            Node node = records.get(index);
	            Node copyNode = newXmlDocument.importNode(node, true);
	            root.appendChild(copyNode);
//	            log.debug("record: " +  printXmlDocument(newXmlDocument));
	            ret = new XMLTextValidationObject(newXmlDocument);
	            XPathFactory factory = XPathFactory.newInstance();
	            XPath xPath = factory.newXPath();
	            String id = xPath.evaluate("//*[local-name()='header']/*[name()='dri:objIdentifier']/text()", records.get(index));
	            if (id.isEmpty())
	            	id = xPath.evaluate("//*[local-name()='header']/*[name()='dri:recordIdentifier']/text()", records.get(index));
	            if (id.isEmpty())
	            	id = xPath.evaluate("//*[local-name()='header']/*[name()='identifier']/text()", records.get(index));
	            ret.setId(id);
	            ret.setStatus(xPath.evaluate("//*[local-name()='header']/@status", records.get(index)));
	            
	            log.debug("record id: " + ret.getId());
	            log.debug("record status: " + ret.getStatus());
	            long time2 = Calendar.getInstance().getTimeInMillis();
	            log.debug("W"+ workerId + "#record conversion in ValidationObject took " + ((time2 - time1))
						+ " milliseconds");
	            
			} catch (Exception e) {
				log.error("error getting object"+ e);
			}			
			return ret;
		}
	}

	public static ResultSetFactory getRsFactory() {
		return rsFactory;
	}

	public static void setRsFactory(ResultSetFactory rsFactory) {
		DnetProviderNew.rsFactory = rsFactory;
	}

	public static ServiceLocator<MDStoreService> getMdStoreServiceLocator() {
		return mdStoreServiceLocator;
	}

	public static void setMdStoreServiceLocator(
			ServiceLocator<MDStoreService> mdStoreServiceLocator) {
		DnetProviderNew.mdStoreServiceLocator = mdStoreServiceLocator;
	}

	public TaskExecutor getHarvesterExecutor() {
		return harvesterExecutor;
	}

	public void setHarvesterExecutor(TaskExecutor harvesterExecutor) {
		this.harvesterExecutor = harvesterExecutor;
	}

	@Override
	public ResultSet<ValidationObject> getValidationObjects(String entity)
			throws ProviderException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResultSet<String> getValidationObjectIds() throws ProviderException,
			UnsupportedOperationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ValidationObject getValidationObject(String valObjId)
			throws ProviderException, UnsupportedOperationException {
		// TODO Auto-generated method stub
		return null;
	}
	
	public static String printXmlDocument(Document document) {
	    DOMImplementationLS domImplementationLS = 
	        (DOMImplementationLS) document.getImplementation();
	    LSSerializer lsSerializer = 
	        domImplementationLS.createLSSerializer();
	    return lsSerializer.writeToString(document);
	}

}
