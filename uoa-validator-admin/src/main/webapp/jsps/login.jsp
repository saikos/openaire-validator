<%@ page contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><s:text name="login.title"/></title>
<link href="jsps/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="jsps/style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<div id="wrapper">
	<jsp:include page="menu.jsp"/>
	<div id="page">
        <div id="page-bgtop">
             <div id="middle">
                <div class="middle-b">
                    <div class="background">
                        <div id="main">
                            <div id="main_container" class="clearfix">
                                <div id="mainmiddle" class="floatbox">
                                <h1>Sign In </h1>
	                                <div class="notes">
    		                            <s:text name="login.overview"/>
   		                            </div>
                                    <s:actionerror/>
                                    <div class="post width45 separator float-left">
                                        <h2><s:text name="login.title"/></h2>
                                        <s:text name="login.description"/>
                                        <%--<p class="meta"><a href='<s:url value="/jsps/help.jsp#login"/>' target="_blank"><s:text name="help"/></a></p>--%>
                                        <div class="entry">
                                            <s:form action="Login">
                                            	
                                            	<s:hidden name="url" value="%{#request['javax.servlet.forward.servlet_path']}"/>
            									<s:hidden name="params" value="%{#request['javax.servlet.forward.query_string']}"/>  
    
                                                <s:textfield name="email_username" key="registration.username_email"/>
                                                <s:password name="password_login" key="registration.password"/>

                                                <%-- <s:checkbox name="rememberme" key="registration.rememberme"/> --%>
                                                <s:submit  cssClass="button-default" value="Sign In"/>
                                            </s:form>
                                            <a class="smallfont" href='<s:url action="forgotPassword"/>'><s:text name="login.forgotpassword"/></a>
                                        </div>
                                    </div>
                                    
                                    <div class="post width45 float-left">
                                        <h2><s:text name="menu.account.registration.title"/></h2>
                                        <div class="entry">
                                            <!-- NMI take the following out if everything works fine
                                            <a href='<s:url action="go2registration" />'><s:text name="menu.account.registration" /></a>
                                            -->
                                        <s:text name="registration.description"/>
                                        <%--<p class="meta"><a href='<s:url value="/jsps/help.jsp#registration"/>' target="_blank"><s:text name="help"/></a></p>--%>
                                        
                                            <%-- <s:if test="%{@gr.uoa.di.validatorweb.configs.Constants@isModeLdap()}">
                                                <s:text name="registration.ldap.message"/>
                                            </s:if> --%>
                                            <s:form action="Register">
                                                <s:textfield name="username" key="registration.username" requiredLabel="true"/>
                                                <s:textfield name="firstName" key="registration.firstName" requiredLabel="true"/>
                                                <s:textfield name="lastName" key="registration.lastName" requiredLabel="true"/>
                                                <s:textfield name="email" key="registration.email" requiredLabel="true"/>
                                                <s:textfield name="institution" key="registration.institution" />
                                                <s:password name="password" key="registration.password" requiredLabel="true"/>
                                                <s:password name="repassword" key="registration.repassword" requiredLabel="true"/>
                                                <s:submit cssClass="button-default" value="Register"/>
                                            </s:form>
                                        </div>
                                </div>
                                 </div>
                             </div>
                         </div>
                    </div>
                </div>
            </div>        
        </div>
        <!-- NMI This needs to be the same in all pages and the text is some properties file -->
        <div id="footer"> 
		<jsp:include page="footer.jsp"/>
	</div>
	</div>
</div>
</body>
</html>
