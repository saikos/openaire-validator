<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="jsps/style.css" rel="stylesheet" type="text/css" media="screen" />
<link href="jsps/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<title><s:text name="info.title"/></title>
</head>
<body>
<div id="wrapper">
	<jsp:include page="menu.jsp"/>
	<div id="page">
	<div id="page-bgtop">
		<div id="content">
			<s:actionerror escape="false"/> 
			<s:actionmessage escape="false"/> 
		</div>
		<div style="clear: both;">&nbsp;</div>
	</div>
	</div>
</div>
</body>
</html>