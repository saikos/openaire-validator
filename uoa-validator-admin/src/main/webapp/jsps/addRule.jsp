<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<link href="jsps/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="jsps/style.css" rel="stylesheet" type="text/css" media="screen" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><s:text name="addRule.title"/></title>
</head>
<body class="left">
<div id="wrapper">
	<jsp:include page="menu.jsp"/>
	<div id="page">
	<div id="page-bgtop">
	<div id="middle">
            <div class="middle-b">
                <div class="background">

			 <div id="left">
                              <div id="left_container" >
                                                <h2 class="helpHdr">Info</h2>
                                                <s:text name="help.admin"/>
                              </div>
                         </div>
                                <div id="main">
                        <div id="main_container" class="clearfix">
		<div id="content">
				<h2 class="title"><s:text name="addRule.title"/></h2>
				<p class="meta"><a href='<s:url value="/jsps/help.jsp#addRule"/>' target="_blank"><s:text name="help"/></a></p>
				<div class="entry">
					<s:form action="addRule">
<%-- 						<s:hidden name="type"/> --%>
						<s:hidden name="jobType"/>
						<s:hidden name="forCris"/>
						<s:iterator value="labels" var="label">
							<s:set name="dscr" value="%{#label+\".description\"}"/>
							<s:set name="tltp" value="%{#label+\".name\"}"/>
							<s:if test="%{#label.equals(\"xmlvocabulary_rule.terms\") || #label.equals(\"rules.description\")}">
								<s:textarea name="inputs" title="%{getText(#dscr)}" label="%{getText(#tltp)}" cols="50" rows="5"/>
							</s:if>
							<s:elseif test="%{#label.equals(\"xmlcrisclassvocabulary_rule.terms\") || #label.equals(\"xmlcrisreferentialintegrity_rule.terms\")}">
								<s:textarea name="inputs" title="%{getText(#dscr)}" label="%{getText(#tltp)}" cols="50" rows="5"/>
							</s:elseif>
                            <s:elseif test="%{#label.equals(\"xmlvocabulary_rule.terms_type\")}">
                                <s:select label="%{getText(#tltp)}" name="inputs" list="#{'whitelist':'whitelist', 'blacklist':'blacklist'}" title="%{getText(#dscr)}"/>
							</s:elseif>
							<s:elseif test="%{#label.equals(\"rules.mandatory\")}">
								<s:select label="%{getText(#tltp)}" name="inputs" list="#{'true':'true', 'false':'false'}" title="%{getText(#dscr)}"/>
							</s:elseif>
							<s:elseif test="%{#label.equals(\"rules.cris_entity\")}">
                                <s:select label="%{getText(#tltp)}" name="inputs" list="entityTypes" value="%{rule.entity_type}" title="%{getText(#dscr)}"/>
                            </s:elseif>
<%-- 							<s:elseif test="%{#label.equals(\"rules.provider_information\")}"> --%>
<%-- 								<s:if test="%{jobType.equals(\"usage\")}"> --%>
<%-- 									<s:textfield name="inputs" title="%{getText(#dscr)}" label="%{getText(#tltp)}"/> --%>
<%-- 								</s:if> --%>
<%-- 							</s:elseif> --%>
							<s:elseif test="%{#label.matches('.*success')}">
								<s:select label="%{getText(#tltp)}" name="inputs" list="#{'>0':'>0', 'all':'all', '0':'0', '1':'1'}"  title="%{getText(#dscr)}"/>
							</s:elseif>
							<s:elseif test="%{#label.equals(\"chain_rule.type\")}">
								<s:select label="%{getText(#tltp)}" name="inputs" list="#{'or':'or', 'and':'and', 'horn':'horn'}" title="%{getText(#dscr)}"/>
							</s:elseif>							
							<s:elseif test="%{#label.equals(\"chain_rule.rule_1\")}">
								<s:select name="inputs" list="rules" listValue="ruleName" listKey="ruleId" label="%{getText(#tltp)}" title="%{getText(#dscr)}"/>
							</s:elseif>							
							<s:elseif test="%{#label.equals(\"chain_rule.rule_2\")}">
								<s:select name="inputs" list="rules" listValue="ruleName" listKey="ruleId" label="%{getText(#tltp)}" title="%{getText(#dscr)}"/>
							</s:elseif>		
							<s:else>
								<s:textfield name="inputs" title="%{getText(#dscr)}" label="%{getText(#tltp)}"/>
							</s:else>
						</s:iterator>
<%-- 						<s:if test="%{forCris == true}"> --%>
<%-- 							<s:select name="entityType" list="entityTypes" label="%{getText(\"jobs.entity_type.name\")}" title="%{getText(\"jobs.entity_type.description\")}" /> --%>
<%-- 						</s:if> --%>
<%-- 						<s:textfield name="jobType" value="%{#jobType}" label="%{getText(\"jobs.job_type.name\")}" title="%{getText(\"jobs.job_type.description\")}" readonly='true'/> --%>
						<s:textfield name="type" label="%{getText(\"rules.type.name\")}" title="%{getText(\"rules.type.description\")}" readonly='true'/>
						<s:submit value="Insert" cssClass="button-default"/>
					</s:form>
				</div>
			</div>
		</div>
	</div>
	</div>
	</div>
	</div>
	</div>
    <div id="footer">
                <jsp:include page="footer.jsp"/>
        </div>
        </div>
        </div>
</body>
</html>
