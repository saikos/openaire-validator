<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<link href="jsps/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="jsps/style.css" rel="stylesheet" type="text/css" media="screen" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><s:text name="deleteRule.title"/></title>
</head>
<body class="left">
<div id="wrapper">
	<jsp:include page="menu.jsp"/>
	<div id="page">
	<div id="page-bgtop">
	<div id="middle">
            <div class="middle-b">
                <div class="background">
			 <div id="left">
                              <div id="left_container" >
                                                <h2 class="helpHdr">Info</h2>
                                                <s:text name="help.admin"/>
                              </div>
                         </div>
                          <div id="main">
                        <div id="main_container" class="clearfix">
			<div id="content">
			<h2 class="title"><s:text name="deleteRule.title"/></h2>
                                <div class="entry">
                                        <s:form action="deleteRule">
                                                <s:select name="ruleId" list="rules" listValue="name" listKey="id"  key="deleteRule.label"/>
						<s:submit cssClass="button-default"/>
                                        </s:form>
                                </div>
			</div>
		</div>
	</div>
	</div>
	</div>
	</div>
	</div>

    <div id="footer">
                <jsp:include page="footer.jsp"/>
        </div>
</div>
</div>
</body>
</html>
