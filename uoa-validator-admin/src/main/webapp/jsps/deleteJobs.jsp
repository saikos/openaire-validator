<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<html>
<head>
<link href="jsps/style.css" rel="stylesheet" type="text/css" media="screen" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!--<meta http-equiv="refresh" content="13" >-->
<title><s:text name="manageJobs.deleteJobs.title"/></title>
<link href="jsps/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<sj:head />
</head>
<body class="left">

<div id="wrapper">
	<jsp:include page="menu.jsp"/>
	<div id="page">
		<div id="page-bgtop">
			<div id="middle">
                <div class="middle-b">
                    <div class="background">
                    	<div id="left">
                        	<div id="left_container">
	                             <h2 class="helpHdr">Info</h2>
                                 <s:text name="help.browsevalidations"/>
                            </div>
                        </div>
                        <div id="main">
                            <div id="main_container" class="clearfix">
                                <div id="mainmiddle" class="floatbox">
                                    <h1 class="title"><s:text name="manageJobs.deleteJobs.title"/></h1>
<%--                                     <s:if test="%{enableCuration}"> --%>
	                                    <s:form action="curateDB">
	                                   		 <s:select name="period" 
	                                    		key="manageJobs.deleteJobs.deleteOld"
 	                          					list="#{'older':'older than', 'newer':'newer than', 'exact':'of'}"
                                            	required="true"
	  					                    />
     										<sj:datepicker name="inDate" changeMonth="true" changeYear="true" displayFormat="yy-mm-dd"/>
<%-- 	                                    	<s:textfield name="inDate" value="YYYY-MM-DD"/> --%>
	                                    	<s:select name="mode" 
	                                    		key="manageJobs.deleteJobs.whatToDelete"
 	                          					list="#{'all':'All jobs', 'compTest_only':'Compatibility Tests only', 'uncompleted_only':'Uncompleted jobs'}"
                                            	required="true"
	  					                    />
<%--  	                          					list="#{'all':'All jobs', 'compTest_only':'Compatibility Tests only', 'uncompleted_only':'Uncompleted jobs','import':'Import jobs'}" --%>
	                                    	<s:submit cssClass="button-default"/>
	                                    </s:form>
<%-- 	                               </s:if> --%>
                                    
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
		<!-- NMI This needs to be the same in all pages and the text is some properties file -->
        <div id="footer"> 
           <jsp:include page="footer.jsp"/>
        </div>
	</div>
</div>
</body>
</html>
