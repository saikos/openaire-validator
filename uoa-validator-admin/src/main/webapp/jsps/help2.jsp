<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="jsps/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="jsps/style.css" rel="stylesheet" type="text/css" media="screen" />
<title><s:text name="help"/></title>
</head>
<body>
<div id="wrapper">
	<jsp:include page="menu.jsp"/>
	<div id="page">
	<div id="page-bgtop">
		<div id="content">
			<div class="post">
				<h2 class="title"><a name="addOaiContent"><s:text name="help.addOaiContent.title"/></a></h2>
				<div class="entry">
					<s:text name="help.addOaiContent.message"/>
				</div>
			</div>
			<div class="post">
				<h2 class="title"><a name="addOaiUsage"><s:text name="help.addOaiUsage.title"/></a></h2>
				<div class="entry">
					<s:text name="help.addOaiUsage.message"/>
				</div>
			</div>
			<%--<div class="post">
				<h2 class="title"><a name="addRule"><s:text name="help.addRule.title"/></a></h2>
				<div class="entry">
					<s:text name="help.addRule.message"/>
				</div>
			</div>--%>
			<div class="post">
				<h2 class="title"><a name="browseJobs"><s:text name="help.browseJobs.title"/></a></h2>
				<div class="entry">
					<s:text name="help.browseJobs.message"/>
				</div>
			</div>
			<%--<div class="post">
				<h2 class="title"><a name="chooseJobCategory"><s:text name="help.chooseJobCategory.title"/></a></h2>
				<div class="entry">
					<s:text name="help.chooseJobCategory.message"/>
				</div>
			</div>
			<div class="post">
				<h2 class="title"><a name="chooseRuleCategory"><s:text name="help.chooseRuleCategory.title"/></a></h2>
				<div class="entry">
					<s:text name="help.chooseRuleCategory.message"/>
				</div>
			</div>
			<div class="post">
				<h2 class="title"><a name="chooseRuleForEdit"><s:text name="help.chooseRuleForEdit.title"/></a></h2>
				<div class="entry">
					<s:text name="help.chooseRuleForEdit.message"/>
				</div>
			</div>
			<div class="post">
				<h2 class="title"><a name="chooseRuleSet"><s:text name="help.chooseRuleSet.title"/></a></h2>
				<div class="entry">
					<s:text name="help.chooseRuleSet.message"/>
				</div>
			</div>
			<div class="post">
				<h2 class="title"><a name="chooseRuleSetForDeletion"><s:text name="help.chooseRuleSetForDeletion.title"/></a></h2>
				<div class="entry">
					<s:text name="help.chooseRuleSetForDeletion.message"/>
				</div>
			</div>--%>
			<div class="post">
				<h2 class="title"><a name="createRuleSet"><s:text name="help.createRuleSet.title"/></a></h2>
				<div class="entry">
					<s:text name="help.createRuleSet.message"/>
				</div>
			</div>
			<%--<div class="post">
				<h2 class="title"><a name="deleteRule"><s:text name="help.deleteRule.title"/></a></h2>
				<div class="entry">
					<s:text name="help.deleteRule.message"/>
				</div>
			</div>--%>
			<div class="post">
				<h2 class="title"><a name="editRule"><s:text name="help.editRule.title"/></a></h2>
				<div class="entry">
					<s:text name="help.editRule.message"/>
				</div>
			</div>
			<div class="post">
				<h2 class="title"><a name="jobDetails"><s:text name="help.jobDetails.title"/></a></h2>
				<div class="entry">
					<s:text name="help.jobDetails.message"/>
				</div>
			</div>
			<div class="post">
				<h2 class="title"><a name="jobSummary"><s:text name="help.jobSummary.title"/></a></h2>
				<div class="entry">
					<s:text name="help.jobSummary.message"/>
				</div>
			</div>
			<%--<div class="post">
				<h2 class="title"><a name="login"><s:text name="help.login.title"/></a></h2>
				<div class="entry">
					<s:text name="help.login.message"/>
				</div>
			</div>
			<div class="post">
				<h2 class="title"><a name="registration"><s:text name="help.registration.title"/></a></h2>
				<div class="entry">
					<s:text name="help.registration.message"/>
				</div>
			</div>--%>
			<div class="post">
				<h2 class="title"><a name="repoRegistration"><s:text name="help.repoRegistration.title"/></a></h2>
				<div class="entry">
					<s:text name="help.repoRegistration.message"/>
				</div>
			</div>
			<%--<div class="post">
				<h2 class="title"><a name="resetPassword"><s:text name="help.resetPassword.title"/></a></h2>
				<div class="entry">
					<s:text name="help.resetPassword.message"/>
				</div>
			</div>--%>
		</div>
		<div style="clear: both;">&nbsp;</div>
	</div>
	</div>
</div>
</body>
</html>
