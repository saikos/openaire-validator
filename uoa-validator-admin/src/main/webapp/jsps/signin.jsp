<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><s:text name="login.title"/></title>
<link href="jsps/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="jsps/style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<div id="wrapper">
	<jsp:include page="menu.jsp"/>
	<div id="page">
		<div id="page-bgtop">
			<div id="middle">
                <div class="middle-b">
                    <div class="background">
                        <div class="main">
                        <div id="main_container" class="clearfix">
                                <div id="mainmiddle" class="floatbox">
                                    <s:actionerror/>
                                    <div class="post">
                                        <h2 class="title"><s:text name="login.title"/></h2>
                                        <s:text name="login.description"/>
                                        <%--<p class="meta"><a href='<s:url value="/jsps/help.jsp#login"/>' target="_blank"><s:text name="help"/></a></p>--%>
                                        <div class="entry">
                                            <s:form action="Login">
                                                <s:if test="%{repoMode.trim().toLowerCase().equals(\"dnet\")}">
                                                    <s:textfield name="email" key="registration.username"/>
                                                </s:if>
                                                <s:else>
                                                    <s:textfield name="email" key="registration.email"/>
                                                </s:else>
                                                <s:password name="password" key="registration.password"/>
                                                <%-- <s:checkbox name="rememberme" key="registration.rememberme"/> --%>
                                                <s:submit/>
                                            </s:form>
                                            <a href='<s:url value="forgotPassword" />'><s:text name="login.forgotpassword"/></a>
                                        </div>
                                    </div>
                                    <div class="post">
                                        <h2 class="title"><s:text name="registration.title"/></h2>
                                        <s:text name="registration.description"/>
                                        <%--<p class="meta"><a href='<s:url value="/jsps/help.jsp#registration"/>' target="_blank"><s:text name="help"/></a></p>--%>
                                        <div class="entry">
                                            <%-- <s:if test="%{@gr.uoa.di.validatorweb.configs.Constants@isModeLdap()}">
                                                <s:text name="registration.ldap.message"/>
                                            </s:if> --%>
                                            <s:form action="Register">
                                                <s:if test="%{repoMode.trim().toLowerCase().equals(\"dnet\")}">
                                                    <s:textfield name="username" key="registration.username"/>
                                                    <s:textfield name="firstName" key="registration.firstName"/>
                                                    <s:textfield name="lastName" key="registration.lastName"/>
                                                </s:if>
                                                <s:textfield name="email" key="registration.email"/>
                                                <s:password name="password" key="registration.password"/>
                                                <s:password name="repassword" key="registration.repassword"/>
                                                <s:submit/>
                                            </s:form>
                                        </div>
                                    </div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
		  <div id="footer"> 
           <jsp:include page="footer.jsp"/>
        </div>
	</div>
</div>
</body>
</html>