<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><s:text name="forgotPassword.title"/></title>
<link href="jsps/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="jsps/style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<div id="wrapper">
	<jsp:include page="menu.jsp"/>
	<div id="page">
		<div id="page-bgtop">
			<div id="middle">
                <div class="middle-b">
                    <div class="background">
                        <div class="main">
                        <div id="main_container" class="clearfix">
                                <div id="mainmiddle" class="floatbox">
                                <h2 class="title"><s:text name="forgotPassword.title"/></h2>
                                <%--<p class="meta"><a href='<s:url value="/jsps/help.jsp#forgotPassword"/>' target="_blank"><s:text name="help"/></a></p>--%>
                                <div class="entry">
                                    <s:form action="resetPassword">
                                    	<s:text name="resetPassword.securityCode.message" />
                                        <s:textfield name="securityCode" key="resetPassword.securityCode"/>
                                        <s:password name="password" key="registration.password"/>
                                        <s:password name="repassword" key="registration.repassword"/>
                                        <s:submit/>
                                    </s:form>
                                </div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
		  <div id="footer"> 
           <jsp:include page="footer.jsp"/>
        </div>
	</div>
</div>
</body>
</html>