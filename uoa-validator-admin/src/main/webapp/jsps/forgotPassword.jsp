<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><s:text name="forgotPassword.title"/></title>
<link href="jsps/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="jsps/style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<div id="wrapper">
	<jsp:include page="menu.jsp"/>
	<div id="page">
        <div id="page-bgtop">
             <div id="middle">
                <div class="middle-b">
                    <div class="background">
                        <div id="main">
                            <div id="main_container" class="clearfix">
                                <div id="mainmiddle" class="floatbox">
                                    <h1>Reset your password</h1>
                                    <s:actionerror/>
                                    <s:form action="sendPassword">
                                        <s:textfield name="email" key="registration.email"/>
                                        <s:submit/>
                                    </s:form>
                                    <s:text name="forgotPassword.message"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- NMI This needs to be the same in all pages and the text is some properties file -->
        <div id="footer"> 
           <jsp:include page="footer.jsp"/>
        </div>
	</div>
</div>
</body>
</html>