<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<link href="jsps/ddmenu.css" rel="stylesheet" type="text/css" media="screen" />
<script src="jsps/js/ddmenu.js" type="text/javascript"></script>


	<div id="header">
	<div id="topmenu">
		<ul>
				<s:if test="%{#session.email == null}">
					<li   style="float: right;">
						<a href='<s:url action="go2login" />'><s:text name="menu.account.login" /></a>
					</li>
				</s:if>
				
				<s:else>
					<li  style="float: right;">
						<a href='<s:url action="logout" />'><s:text name="menu.account.logout" />(<s:property value="#session.email" />)</a>
					</li>
					<li  style="float: right;">
						<a href='<s:url action="userDetails" />'><s:text name="menu.account.edit" /></a>
					</li>
				</s:else>
		</ul>
	</div>
			<s:if test="%{deployEnvironment.trim().toLowerCase().equals(\"lareferencia\")}">
				<div id="logoLareferencia"></div>
			</s:if>
			<s:else>
				<div id="logo">
					<a id="logo" href="https://www.openaire.eu">
					<img src="jsps/images/openairepluslogo.png" alt="openairepluslogo" width="171" height="120">
					</a>
				</div>
			</s:else>
		<nav id="ddmenu">
			<ul>
				<li><a href='<s:url action="welcome" />'><s:text name="menu.home" /></a></li>
				
<%-- 						<s:if test="%{repoMode.trim().toLowerCase().equals(\"dnet\")}"> --%>
				<li>
					<s:text name="home.manageRules" />
					<div>
					<div class="column">
						<s:if test="%{#session.isAdmin != null && #session.isAdmin.equals(\"true\")}">
						
							<a href='<s:url action="populateRuleCategories" />'>
								<s:text name="home.manageRules.addRule" />
							</a>
							<a href='<s:url action="populateRuleListForEdit" />'>
								<s:text name="home.manageRules.editRule" />
							</a>
							<a href='<s:url action="populateRuleListForClone" />'>
								<s:text name="home.manageRules.cloneRule" />
							</a>
							<a href='<s:url action="populateRuleList" />'>
								<s:text name="home.manageRules.deleteRule" />
							</a>
						</s:if>
						<s:else>
							<a href='<s:url action="populateRuleListForEdit" />'>
								<s:text name="home.manageRules.viewRule" />
							</a>
						</s:else>
					</div>
					</div>					
				</li>
				
				<li>
					<s:text name="home.manageRulesets" />
					<div>
					<div class="column">
						<s:if test="%{#session.isAdmin != null && #session.isAdmin.equals(\"true\")}">
							<a href='<s:url action="getRulesForSet" />'>
								<s:text name="home.manageRulesets.createRuleSet" />
							</a>
							<a href='<s:url action="getRuleSets" />'>
								<s:text name="home.manageRulesets.editRuleSet" />
							</a>
							<a href='<s:url action="getRuleSetsForDeletion" />'>
								<s:text name="home.manageRulesets.deleteRuleSet" />
							</a>
						</s:if>
						<s:else>
							<a href='<s:url action="getRuleSets" />'>
								<s:text name="home.manageRulesets.viewRuleSet" />
							</a>
						</s:else>
					</div>
					</div>					
				</li>
				
				
				<li>
					<s:text name="home.manageJobs" />
					<div>
					<div class="column">
					  <s:if test="%{#session.isAdmin != null && #session.isAdmin.equals(\"true\")}">
						<a href='<s:url action="deleteJobs" />'>
							<s:text name="home.manageJobs.deleteJobs" />
						</a>
						
							<s:if test="%{!deployEnvironment.trim().toLowerCase().equals(\"lareferencia\")}">
							<a href='<s:url action="registrationsReport" />'>
								<s:text name="home.manageJobs.sendReport" />
							</a>
							</s:if>
					  </s:if>
<%-- 					  		<s:if test="%{!deployEnvironment.trim().toLowerCase().equals(\"lareferencia\")}"> --%>
<%-- 								<a href='<s:url action="getRuleSetsForDeletion" />'> --%>
<%-- 									<s:text name="home.manageJobs.viewStats" /> --%>
<!-- 								</a> -->
<%-- 							</s:if> --%>
					</div>
					</div>					
				</li>
				
				<s:if test="%{!deployEnvironment.trim().toLowerCase().equals(\"lareferencia\")}">
					<s:if test="%{#session.isAdmin != null && #session.isAdmin.equals(\"true\")}">
<!-- 						<li> -->
<%-- 							<s:text name="home.manageUsers" /> --%>
<!-- 							<div> -->
<!-- 							<div class="column"> -->
<%-- 								<a href='<s:url action="getRulesForSet" />'> --%>
<%-- 									<s:text name="home.manageUsers.addUser" /> --%>
<!-- 								</a> -->
<%-- 								<a href='<s:url action="getRuleSets" />'> --%>
<%-- 									<s:text name="home.manageUsers.editUser" /> --%>
<!-- 								</a> -->
<%-- 								<a href='<s:url action="getRuleSetsForDeletion" />'> --%>
<%-- 									<s:text name="home.manageUsers.makeAdmin" /> --%>
<!-- 								</a> -->
<!-- 							</div> -->
<!-- 							</div>					 -->
<!-- 						</li> -->
					</s:if>
				</s:if>
				<li>
					<s:text name="menu.about" />
					<div>
					<div class="column">
						<a href="http://www.openaire.eu//support/faq/repman" target="_blank">
							<s:text name="menu.about.faq" />
						</a>
						<a href='<s:url action="help" />'>
							<s:text name="menu.about.help" />
						</a>
						<a href='<s:url action="feedback" />'>
							<s:text name="menu.about.feedback" />
						</a>
					</div>
					</div>
				</li>

			</ul>
			</nav>
			
			<div style="clear:both"></div>
		</div>
