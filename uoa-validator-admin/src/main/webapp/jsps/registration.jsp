<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><s:text name="registration.title"/></title>
<link href="jsps/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="jsps/style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<div id="wrapper">
	<jsp:include page="menu.jsp"/>
	<div id="page">
	<div id="page-bgtop">
		<div class="content">
			<div class="main">
				<h2 class="title"><s:text name="registration.title"/></h2>
				<s:text name="registration.description"/>
				<%--<p class="meta"><a href='<s:url value="/jsps/help.jsp#registration"/>' target="_blank"><s:text name="help"/></a></p>--%>
				<div class="entry">
					<%-- <s:if test="%{@gr.uoa.di.validatorweb.configs.Constants@isModeLdap()}">
						<s:text name="registration.ldap.message"/>
					</s:if> --%>
					<s:form action="Register">
						<s:if test="%{repoMode.trim().toLowerCase().equals(\"dnet\")}">
							<s:textfield name="username" key="registration.username"/>
							<s:textfield name="firstName" key="registration.firstName"/>
							<s:textfield name="lastName" key="registration.lastName"/>
						</s:if>
						<s:textfield name="email" key="registration.email"/>
						<s:password name="password" key="registration.password"/>
						<s:password name="repassword" key="registration.repassword"/>
						<s:submit value="Register" cssClass="button-default"/>
					</s:form>
				</div>
			</div>
		</div>
		
	</div>
	</div>
</div>
</body>
</html>