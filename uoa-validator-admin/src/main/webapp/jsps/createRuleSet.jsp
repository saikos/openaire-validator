<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<link href="jsps/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="jsps/style.css" rel="stylesheet" type="text/css" media="screen" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<s:if test="%{#session.isAdmin != null && #session.isAdmin.equals(\"true\")}">
        <s:set name="isAdmin" value="true"/>
</s:if>
<s:else>
        <s:set name="isAdmin" value="false"/>
</s:else>
<title>
        <s:if test="%{#isAdmin}">
                <s:text name="createRuleSet.title"/>
        </s:if>
        <s:else>
                <s:text name="createRuleSet.title2"/>
        </s:else>
</title>
</head>
<body class="left">
<div id="wrapper">
	<jsp:include page="menu.jsp"/>
	<div id="page">
	<div id="page-bgtop">
	<div id="middle">
            <div class="middle-b">
                <div class="background">
			 <div id="left">
                              <div id="left_container" >
                                   <h2 class="helpHdr">Info</h2>
                                   <s:text name="help.admin"/>
                              </div>
                         </div>
                        <div id="main">
                        <div id="main_container" class="clearfix">
			<div id="content">
				<s:if test="%{#isAdmin}">
                                        <h2 class="title"><s:text name="createRuleSet.title"/></h2>
                                </s:if>
                                <s:else>
                                        <h2 class="title"><s:text name="createRuleSet.title2"/></h2>
                                </s:else>
                                <div class="entry">
                                        <s:form action="createRuleSet">
                                                <s:hidden name="funct"/>
                                                <s:hidden name="ruleSet.id"/>
                                                <s:textfield name="ruleSet.name" key="createRuleSet.name"/>
                                                <s:textfield name="ruleSet.shortName" key="createRuleSet.shortName"/>
                                                <s:textfield name="ruleSet.description" key="createRuleSet.description"/>
                                                <s:textfield name="ruleSet.guidelinesAcronym" key="createRuleSet.guidelinesAcronym"/>
<%--                                                 <s:textfield name="ruleSet.visibility" key="createRuleSet.visibility"/> --%>
                                                 
                                                <s:select key="createRuleSet.visibility"
                                                       name="ruleSet.visibility"
                                                       list="deployEnvironments"
                                                       multiple="true"
                                                       size="%{#ruleSet.visibility.size()}"
                                                       value="%{ruleSet.visibility}"
                                                       disabled='%{!#isAdmin}'
                                                /> 
                                                 
                                                <s:select key="createRuleSet.rulesContent"
                                                       name="chosenContentRules"
                                                       list="rulesContent"
                                                       listKey="id"
                                                       listValue="name"
                                                       multiple="true"
                                                       size="%{#rulesContent.size()}"
                                                       value="%{ruleIdsContent}"
                                                       disabled='%{!#isAdmin}'
                                                />
                                                <s:select key="createRuleSet.rulesUsage"
                                                       name="chosenUsageRules"
                                                       list="rulesUsage"
                                                       listKey="id"
                                                       listValue="name"
                                                       multiple="true"
                                                       size="%{#rulesUsage.size()}"
                                                       value="%{ruleIdsUsage}"
                                                       disabled='%{!#isAdmin}'
                                                />

                                                <s:if test="%{#isAdmin}">
                                                	<s:submit cssClass="button-default"/>
                                                </s:if>
                                        </s:form>
                                </div>

			</div>
			</div>
			</div>
		</div>
	</div>
	</div>
	</div>
    <div id="footer">
                <jsp:include page="footer.jsp"/>
        </div>
	</div>
</div>
</body>
</html>
