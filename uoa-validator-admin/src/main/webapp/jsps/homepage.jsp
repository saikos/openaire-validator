<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><s:text name="home.title"/></title>
<link href="jsps/style.css" rel="stylesheet" type="text/css" media="screen" />

 <link href="jsps/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>	
<body>
<div id="wrapper">
	<jsp:include page="menu.jsp"/>
	<div id="page">
	<div id="page-bgtop">
		<div id="middle">
            <div class="middle-b">
                <div class="background">
        			<div id="main">
                        <div id="main_container" class="clearfix">
                            <div id="mainmiddle" class="floatbox">
                                <h1 class="title"><s:text name="homepage.welcome"/></h1>
                                <div class="entry">
                                    <s:if test="%{#session.isAdmin != null && #session.isAdmin.equals(\"true\")}">
                                        <s:text name="homepage.admin.message"/>
                                    </s:if>
                                    <s:else>
                                        <s:text name="homepage.message"/>
                                    </s:else>
                                </div>
                             </div>
                        </div>
                    </div>
                 </div>
			</div>
		</div>
		<div style="clear: both;">&nbsp;</div>
	</div>
    <!-- NMI This needs to be the same in all pages and the text is some properties file -->
    <div id="footer"> 
		<jsp:include page="footer.jsp"/>
	</div>
</div>
</div>
</body>
</html>