<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<link href="jsps/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="jsps/style.css" rel="stylesheet" type="text/css" media="screen" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<s:if test="%{#session.isAdmin != null && #session.isAdmin.equals(\"true\")}">
        <s:set name="isAdmin" value="true"/>
</s:if>
<s:else>
        <s:set name="isAdmin" value="false"/>
</s:else>
<title>
        <s:if test="%{#isAdmin}">
                <s:text name="editRule.title"/>
        </s:if>
        <s:else>
                <s:text name="editRule.title2"/>
        </s:else>
</title>

</head>
<body class="left">
<div id="wrapper">
	<jsp:include page="menu.jsp"/>
	<div id="page">
	<div id="page-bgtop">
	<div id="middle">
            <div class="middle-b">
                <div class="background">
			 <div id="left">
                              <div id="left_container" >
                                   <h2 class="helpHdr">Info</h2>
                                   <s:text name="help.admin"/>
                              </div>
                         </div>
                        <div id="main">
                        <div id="main_container" class="clearfix">
			<div id="content">
				<s:if test="%{#isAdmin}">
                                        <h2 class="title"><s:text name="editRule.title"/></h2>
                                </s:if>
                                <s:else>
                                        <h2 class="title"><s:text name="editRule.title2"/></h2>
                                </s:else>
                                <div class="entry">
                                        <s:form action="editRule">
                                        		<s:hidden name="funct"/>
                                                <s:hidden name="rule.id" value="%{rule.id}"/>
                                                <s:hidden name="rule.for_cris"/>
                                                <s:iterator value="ruleValues" var="ruleValue">
                                                        <s:set name="label" value="%{#ruleValue.fieldName}"/>
                                                        <s:set name="dscr" value="%{#ruleValue.fieldName+\".description\"}"/>
                                                        <s:set name="tltp" value="%{#ruleValue.fieldName+\".name\"}"/>
                                                        <s:if test="%{#label.equals(\"xmlvocabulary_rule.terms\") || #label.equals(\"rules.description\")}">
                                                                <s:textarea name="inputs" value="%{#ruleValue.fieldValue}" title="%{getText(#dscr)}" label="%{getText(#tltp)}" cols="50" rows="10" disabled='%{!#isAdmin}'/>
                                                        </s:if>
                                                        <s:elseif test="%{#label.equals(\"xmlcrisclassvocabulary_rule.terms\") || #label.equals(\"xmlcrisreferentialintegrity_rule.terms\")}">
                                                                <s:textarea name="inputs" value="%{#ruleValue.fieldValue}" title="%{getText(#dscr)}" label="%{getText(#tltp)}" cols="50" rows="10" disabled='%{!#isAdmin}'/>
                                                        </s:elseif>
                                                        <s:elseif test="%{#label.equals(\"xmlvocabulary_rule.terms_type\")}">
                                                                <s:select label="%{getText(#tltp)}" name="inputs" value="%{#ruleValue.fieldValue}" list="#{'whitelist':'whitelist', 'blacklist':'blacklist'}" title="%{getText(#dscr)}"/>
                                                        </s:elseif>
                                                        <s:elseif test="%{#label.equals(\"rules.mandatory\")}">
                                                                <s:select label="%{getText(#tltp)}" name="inputs" value="%{rule.mandatory}" list="#{'true':'true', 'false':'false'}" title="%{getText(#dscr)}"/>
                                                        </s:elseif>
                                                        <s:elseif test="%{#label.equals(\"rules.cris_entity\")}">
                                                                <s:select label="%{getText(#tltp)}" name="inputs" list="entityTypes" value="%{rule.entity_type}" title="%{getText(#dscr)}"/>
                                                        </s:elseif>
														<s:elseif test="%{#label.matches('.*success')}">
															<s:select label="%{getText(#tltp)}" name="inputs" value="%{#ruleValue.fieldValue}" list="#{'>0':'>0', 'all':'all', '0':'0', '1':'1'}" title="%{getText(#dscr)}"/>
														</s:elseif>
                                                        <s:elseif test="%{#label.equals(\"chain_rule.type\")}">
                                                                <s:select label="%{getText(#tltp)}" name="inputs" value="%{#ruleValue.fieldValue}" list="#{'or':'or', 'and':'and', 'horn':'horn'}" title="%{getText(#dscr)}"/>
                                                        </s:elseif>
                                                        <s:elseif test="%{#label.equals(\"chain_rule.rule_1\")}">
                                                                <s:select name="inputs" list="rules" listValue="name" listKey="id"  value="%{#ruleValue.fieldValue}" label="%{getText(#tltp)}" title="%{getText(#dscr)}"/>
                                                        </s:elseif>
                                                        <s:elseif test="%{#label.equals(\"chain_rule.rule_2\")}">
                                                                <s:select name="inputs" list="rules" listValue="name" listKey="id"  value="%{#ruleValue.fieldValue}" label="%{getText(#tltp)}" title="%{getText(#dscr)}"/>
                                                        </s:elseif>
                                                        <s:elseif test="%{#label.equals(\"rules.provider_information\")}">
<%-- 															<s:if test="%{rule.job_type.equals(\"OAI Usage\")}"> --%>
															<s:textfield name="inputs" value="%{#ruleValue.fieldValue}" title="%{getText(#dscr)}" label="%{getText(#tltp)}" disabled='%{!#isAdmin}'/>
<%--                                                         	</s:if> --%>
														</s:elseif>
                                                        <s:else>
                                                                <s:textfield name="inputs" value="%{#ruleValue.fieldValue}" title="%{getText(#dscr)}" label="%{getText(#tltp)}" disabled='%{!#isAdmin}'/>
                                                        </s:else>
                                                </s:iterator>
<%--                                                 <s:textfield name="rule.entity_type" label="%{getText(\"jobs.entity_type.name\")}" title="%{getText(\"jobs.entity_type.description\")}" /> --%>
                                                <s:textfield name="rule.job_type" label="%{getText(\"jobs.job_type.name\")}" title="%{getText(\"jobs.job_type.description\")}" readonly='true'/>
                                                <s:textfield name="rule.type" label="%{getText(\"rules.type.name\")}" title="%{getText(\"rules.type.description\")}" readonly='true'/>
                                                <s:if test="%{#isAdmin}">
                                                 	<s:if test="%{funct.equals(\"edit\")}">
                                                        <s:submit value="Update" cssClass="button-default"/>
                                                    </s:if>
                                                    <s:elseif test="%{funct.equals(\"clone\")}">
                                                    	<s:submit value="Clone" cssClass="button-default"/>
                                                    </s:elseif>
                                                </s:if>
                                        </s:form>
                                </div>

			</div>
			</div>
			</div>
		</div>
	</div>
	</div>
	</div>

    <div id="footer">
                <jsp:include page="footer.jsp"/>
        </div>
</div>
</div>
</body>
</html>
