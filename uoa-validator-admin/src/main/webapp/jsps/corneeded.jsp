<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<link href="jsps/style.css" rel="stylesheet" type="text/css" media="screen" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><s:text name="corneeded.title"/></title>
</head>
<body>
<div id="wrapper">
	<jsp:include page="menu.jsp"/>
	<div id="page">
	<div id="page-bgtop">
		<div id="content">
			<s:actionerror/>
			<br>
			<s:text name="corneeded.back"/>
		</div>
		<div style="clear: both;">&nbsp;</div>
	</div>
	</div>
</div>
</body>
</html>