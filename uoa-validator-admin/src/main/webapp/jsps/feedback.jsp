<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="jsps/style.css" rel="stylesheet" type="text/css" media="screen" />
<title><s:text name="feedback.title"/></title>
<link href="jsps/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>
<body>
<div id="wrapper">
	<jsp:include page="menu.jsp"/>
	<div id="page">
		<div id="page-bgtop">
			<div id="middle">
                <div class="middle-b">
                    <div class="background">
                        <div class="main">
                        <div id="main_container" class="clearfix">
                                <div id="mainmiddle" class="floatbox">
                                    <h2 class="title"><s:text name="feedback.title"/></h2>
                                    <s:actionerror/>
                                    <s:text name="feedback.loggedin"/>
                                    <div class="entry">
                                        <s:form action="sendFeedback">
                                            <s:select name="reason" list="#{'validator form: feedback':'Feedback', 'validator form: bug':'Report a Bug', 'validator form: other':'Other'}" key="feedback.reason"/>
                                            <s:textarea name="message" cols="50" rows="10" key="feedback.message"/>
                                            <s:submit/>
                                        </s:form>
                                    </div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
		  <div id="footer"> 
           <jsp:include page="footer.jsp"/>
        </div>
	</div>
</div>
</body>
</html>