package eu.dnetlib.validator.admin.actions.rulesets;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Action;

import eu.dnetlib.domain.functionality.validator.Rule;
import eu.dnetlib.domain.functionality.validator.RuleSet;
import eu.dnetlib.validator.admin.actions.BaseValidatorAction;
import eu.dnetlib.validator.admin.constants.TypesAndTableNames;

public class GetRules extends BaseValidatorAction {

	private static final long serialVersionUID = 3336444896590055688L;
	private int setId = -1;
	private List<Rule> rulesContent;
	private List<Rule> rulesUsage;
	private Set<Integer> ruleIdsContent;
	private Set<Integer> ruleIdsUsage;
	private List<String> deployEnvironments;
	private String funct = null;
	private RuleSet ruleSet;
	
	private static final Logger logger = Logger.getLogger(GetRules.class);

	public String execute() {
		this.clearErrorsAndMessages();
		try {
			logger.debug("getting rules in rule set "+setId);
			ruleIdsContent = new HashSet<Integer>();
			ruleIdsUsage = new HashSet<Integer>();
			rulesContent = new ArrayList<Rule>();
			rulesUsage = new ArrayList<Rule>();
			this.splitRules(this.getValidatorAdminAPI().getAllRules(),rulesContent, rulesUsage);
			this.deployEnvironments = TypesAndTableNames.deployEnvironments;
			if (setId == -1) {
				ruleSet = new RuleSet();
				ruleSet.setId(-1);
				funct = "create";
			} else {
				funct = "edit";
				ruleSet = this.getValidatorAdminAPI().getRuleSet(setId);
				ruleIdsContent=ruleSet.getContentRulesIds();
				ruleIdsUsage=ruleSet.getUsageRulesIds();
//				visibility = Utilities.convertListToString(set.getVisibility());
			}
			return Action.SUCCESS;
		}
		catch(Exception e) {
			logger.error("Error getting rules", e);
			this.addActionError(this.getText("generic.error"));
			reportException(e);
			return "exception";
		}
	}

	private void splitRules(List<Rule> totalRules, List<Rule> contentRules, List<Rule> usageRules) {
		logger.debug("splitting rules..");
		for (Rule rule : totalRules) {
			if (rule.getJob_type().equals("content")) {
				contentRules.add(rule);
			} else if (rule.getJob_type().equals("usage")) {
				usageRules.add(rule);
			}
		}
	}

	public String getFunct() {
		return funct;
	}

	public void setFunct(String funct) {
		this.funct = funct;
	}
		

	public RuleSet getRuleSet() {
		return ruleSet;
	}

	public void setRuleSet(RuleSet ruleSet) {
		this.ruleSet = ruleSet;
	}

	public int getSetId() {
		return setId;
	}

	public void setSetId(int setId) {
		this.setId = setId;
	}


	public List<Rule> getRulesContent() {
		return rulesContent;
	}


	public void setRulesContent(List<Rule> rulesContent) {
		this.rulesContent = rulesContent;
	}


	public List<Rule> getRulesUsage() {
		return rulesUsage;
	}


	public void setRulesUsage(List<Rule> rulesUsage) {
		this.rulesUsage = rulesUsage;
	}

	public Set<Integer> getRuleIdsContent() {
		return ruleIdsContent;
	}

	public void setRuleIdsContent(Set<Integer> ruleIdsContent) {
		this.ruleIdsContent = ruleIdsContent;
	}

	public Set<Integer> getRuleIdsUsage() {
		return ruleIdsUsage;
	}

	public void setRuleIdsUsage(Set<Integer> ruleIdsUsage) {
		this.ruleIdsUsage = ruleIdsUsage;
	}

	public List<String> getDeployEnvironments() {
		return deployEnvironments;
	}

	public void setDeployEnvironments(List<String> deployEnvironments) {
		this.deployEnvironments = deployEnvironments;
	}


}

	

