package eu.dnetlib.validator.admin.actions.login;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import eu.dnetlib.validator.admin.actions.BaseValidatorAction;

@SuppressWarnings("serial")
public class LoginPage extends BaseValidatorAction implements SessionAware{

	@Override
	public String execute() throws Exception {
		this.clearErrorsAndMessages();
		String email = (String) session.get("email");
		
		if ( email!=null && !email.trim().isEmpty() )
			return INPUT;
		
		return SUCCESS;
	}
	
	private Map<String, Object> session;
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;		
	}

}
