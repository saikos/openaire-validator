package eu.dnetlib.validator.admin.actions.login;

import java.util.Map;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

import eu.dnetlib.validator.admin.api.UserApi;
import eu.dnetlib.validator.admin.api.ValidatorAdminAPI;
import eu.dnetlib.validator.admin.config.Constants;

public class AdminInterceptor implements Interceptor {

	private static final long serialVersionUID = -172948943490485824L;
	private UserApi userAPI = null;
	private ValidatorAdminAPI validatorAdminAPI = null;

//	private Logger logger = Logger.getLogger(AdminInterceptor.class);
	
	@Override
	public void destroy() {

	}

	@Override
	public void init() {

	}

	@Override
	public String intercept(ActionInvocation actionInvocation) throws Exception {
		Map<String, Object> session = actionInvocation.getInvocationContext().getSession();
		String auth = (String) session.get(Constants.loggedInField);

		if (this.getUserAPI().isAdmin(auth) || this.getValidatorAdminAPI().userIsMasterAdmin(auth) || this.getValidatorAdminAPI().userIsSecondaryAdmin(auth))
			return actionInvocation.invoke();
		else
			return Action.LOGIN;
	}

	public UserApi getUserAPI() {
		return userAPI;
	}

	public void setUserAPI(UserApi userAPI) {
		this.userAPI = userAPI;
	}

	public ValidatorAdminAPI getValidatorAdminAPI() {
		return validatorAdminAPI;
	}

	public void setValidatorAdminAPI(ValidatorAdminAPI validatorAdminAPI) {
		this.validatorAdminAPI = validatorAdminAPI;
	}
	
}
