package eu.dnetlib.validator.admin.actions.login;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Action;

import eu.dnetlib.validator.admin.actions.BaseValidatorAction;

public class ResetPassword extends BaseValidatorAction {

	private static final long serialVersionUID = -4565098546731325693L;
	private Logger logger = Logger.getLogger(ResetPassword.class);
	
	private String password, repassword, securityCode;

	public String execute() {
		this.clearErrorsAndMessages();
		try {
			logger.debug("reseting password with security code " + securityCode);
			this.getUserAPI().resetPassword(this.getSecurityCode(), this.getPassword());
			this.addActionMessage(this.getText("resetPassword.success"));
			return Action.SUCCESS;
		} catch (Exception e) {
			logger.error("error reseting password with security code " + securityCode, e);
			this.addActionError(this.getText("generic.error"));
			return "exception";
		}
	}

	public void validate() {
		this.clearErrors();
		try {
			if (this.getSecurityCode() == null || this.getSecurityCode().length() == 0) {
				this.addFieldError("securityCode", this.getText("resetPassword.wrongSecurityCode"));
				return;
			}
			if (!this.getPassword().equals(this.getRepassword())) {
				this.addFieldError("password", this.getText("identicalPasswords"));
				return;
			}
		} catch (Exception e) {
			this.addActionError("");
		}
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRepassword() {
		return repassword;
	}

	public void setRepassword(String repassword) {
		this.repassword = repassword;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

}
