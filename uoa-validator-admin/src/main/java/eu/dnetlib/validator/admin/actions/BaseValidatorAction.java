package eu.dnetlib.validator.admin.actions;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.ActionSupport;

import eu.dnetlib.validator.admin.api.UserApi;
import eu.dnetlib.validator.admin.api.ValidatorAdminAPI;
import eu.dnetlib.validator.admin.config.Constants;
import eu.dnetlib.validator.commons.email.Emailer;

public class BaseValidatorAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -622158192564945303L;
	private UserApi userAPI = null;
	private ValidatorAdminAPI validatorAdminAPI = null;
	private Emailer emailer = null;

	public static String loggedInField = "LOGGED_IN_FIELD";
	
	private String valBaseUrl = null;
	private String valAdminEmail = null;
	private String userMode = null;
	private String repoMode = null;
	private String deployEnvironment = null;

	public void reportException(Exception exception) {
		Writer writer = new StringWriter();
		PrintWriter printWriter = new PrintWriter(writer);
		exception.printStackTrace(printWriter);
		
		List<String> recipients = new ArrayList<String>();
		
		try {
			recipients.add(this.getValAdminEmail());
			String message = "An exception has occurred:\n"+writer.toString();
			String subject = "Automatic Bug Report";
			this.getEmailer().sendMail(recipients, subject, message, false, null);
		} catch (Exception e) {
			Logger logger = Logger.getLogger(BaseValidatorAction.class);
			logger.error("error sending error report", e);
		}
	}
	
	public boolean isModeLdap() {
		return userMode.trim().toLowerCase().equals(Constants.MODE_LDAP);
	}

	public boolean isModeOpenAIRE() {
		return repoMode.trim().toLowerCase().equals(Constants.MODE_DNET);
	}

	public UserApi getUserAPI() {
		return userAPI;
	}

	public void setUserAPI(UserApi userAPI) {
		this.userAPI = userAPI;
	}

	public ValidatorAdminAPI getValidatorAdminAPI() {
		return validatorAdminAPI;
	}

	public void setValidatorAdminAPI(ValidatorAdminAPI validatorAdminAPI) {
		this.validatorAdminAPI = validatorAdminAPI;
	}

	public Emailer getEmailer() {
		return emailer;
	}

	public void setEmailer(Emailer emailer) {
		this.emailer = emailer;
	}

	public String getUserMode() {
		return userMode;
	}

	public void setUserMode(String userMode) {
		this.userMode = userMode;
	}

	public String getRepoMode() {
		return repoMode;
	}

	public void setRepoMode(String repoMode) {
		this.repoMode = repoMode;
	}

	public void setValBaseUrl(String valBaseUrl) {
		this.valBaseUrl = valBaseUrl;
	}

	public String getValBaseUrl() {
		return valBaseUrl;
	}

	public void setValAdminEmail(String valAdminEmail) {
		this.valAdminEmail = valAdminEmail;
	}

	public String getValAdminEmail() {
		return valAdminEmail;
	}

	public String getDeployEnvironment() {
		return deployEnvironment;
	}

	public void setDeployEnvironment(String deployEnvironment) {
		this.deployEnvironment = deployEnvironment;
	}

}