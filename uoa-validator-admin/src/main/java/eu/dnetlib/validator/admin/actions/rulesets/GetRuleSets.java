package eu.dnetlib.validator.admin.actions.rulesets;

import java.util.List;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Action;

import eu.dnetlib.domain.functionality.validator.RuleSet;
import eu.dnetlib.validator.admin.actions.BaseValidatorAction;

public class GetRuleSets extends BaseValidatorAction {

	private static final long serialVersionUID = -5220242100308666597L;
	private List<RuleSet> sets;
	private static final Logger logger = Logger.getLogger(GetRuleSets.class);

	public List<RuleSet> getSets() {
		return sets;
	}

	public void setSets(List<RuleSet> sets) {
		this.sets = sets;
	}

	public String execute() throws Exception {
		this.clearErrorsAndMessages();
		try {
			logger.debug("getting all rule sets");
			sets = this.getValidatorAdminAPI().getRuleSets();
			return Action.SUCCESS;
		} catch (Exception e) {
			logger.error("Error getting rule sets", e);
			this.addActionError(this.getText("generic.error"));
			reportException(e);
			return "exception";
		}
	}
}