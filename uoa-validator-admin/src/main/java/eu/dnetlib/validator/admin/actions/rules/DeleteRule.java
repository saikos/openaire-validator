package eu.dnetlib.validator.admin.actions.rules;


import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Action;

import eu.dnetlib.validator.admin.actions.BaseValidatorAction;
import eu.dnetlib.validator.admin.api.ValidatorAdminException;

public class DeleteRule extends BaseValidatorAction {

	private static final long serialVersionUID = -2888943921398211370L;
	private static final Logger logger = Logger.getLogger(DeleteRule.class);
	private int ruleId;
	
	public String execute() {
		this.clearErrorsAndMessages();
		logger.debug("deleting rule");
		String ret = null;
		try {
			logger.debug("deleting rule with id "+ruleId);
			ret = this.getValidatorAdminAPI().deleteRule(ruleId);
		}
		catch (ValidatorAdminException e) {
			logger.error("Error deleting rule with id "+ruleId, e);
			this.addActionError(this.getText("generic.error"));
			reportException(e);
			return "exception";
		}
		
		if (ret != null) {
			this.addActionError(this.getText(ret));
			return "input-problem";
		}
		this.addActionMessage(this.getText("deleteRule.success"));
		return Action.SUCCESS;
	}

	public int getRuleId() {
		return ruleId;
	}

	public void setRuleId(int ruleId) {
		this.ruleId = ruleId;
	}
	
}
