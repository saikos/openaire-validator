package eu.dnetlib.validator.admin.actions.rules;


import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Action;

import eu.dnetlib.domain.functionality.validator.Rule;
import eu.dnetlib.validator.admin.actions.BaseValidatorAction;

public class AddRule extends BaseValidatorAction {

	private static final long serialVersionUID = -4636737264204104816L;
	private String[] inputs;
	private String type, jobType;
	private boolean forCris;
	private static final Logger logger = Logger.getLogger(AddRule.class);

	public String execute() {
		this.clearErrorsAndMessages();
		try {
			logger.debug("adding rule of type "+type+" using user inputs");
			logger.debug("for cris: "+forCris);
			logger.debug("jobType: "+ jobType);
			Rule rule = new Rule();
			rule.setType(type);
			rule.setJob_type(jobType);
			rule.setFor_cris(forCris);
//			rule.setEntity_type(entityType);
			String ret = this.getValidatorAdminAPI().storeRule(rule, inputs, true);
			if (ret != null) {
				this.addActionError(this.getText(ret));
				return "input-problem";
			}
			
			this.addActionMessage(this.getText("addRule.success"));
			
			return Action.SUCCESS;
		} catch(Exception e) {
			logger.error("Error adding rule", e);
			this.addActionError(this.getText("generic.error"));
			reportException(e);
			return "exception";
		}
	}
	
	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public boolean isForCris() {
		return forCris;
	}

	public void setForCris(boolean forCris) {
		this.forCris = forCris;
	}

//	public String getEntityType() {
//		return entityType;
//	}
//
//	public void setEntityType(String entityType) {
//		this.entityType = entityType;
//	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String[] getInputs() {
		return inputs;
	}

	public void setInputs(String[] inputs) {
		this.inputs = inputs;
	}

}