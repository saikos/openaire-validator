package eu.dnetlib.validator.admin.actions.jobs;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Action;

import eu.dnetlib.validator.admin.actions.BaseValidatorAction;
import eu.dnetlib.validator.admin.api.ValidatorAdminException;

public class DatabaseCurator extends BaseValidatorAction {

	private static final long serialVersionUID = -1868055729316139461L;
	private static final Logger logger = Logger.getLogger(DatabaseCurator.class);
	private String inDate = null;
	private String mode, period;

	public String execute() {
		this.clearErrorsAndMessages();
		int jobsDeleted = 0;
		try {
			jobsDeleted = this.getValidatorAdminAPI().deleteOldJobs(inDate, period, mode);
		} catch (ValidatorAdminException e) {
			logger.error("Error deleting old jobs", e);
			this.addActionError(this.getText("generic.error"));
			reportException(e);
			return "input-problem";
		}
		
		this.addActionMessage(jobsDeleted + " " + this.getText("manageJobs.deleteJobs.success"));
		return Action.SUCCESS;
	}
	

	public void validate() {
		this.clearErrors();
		if (!mode.equalsIgnoreCase("uncompleted_only")){
			if (inDate == null) {
				this.addActionError(this.getText("wrongFieldValue"));
				return;
			}
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			if (inDate.trim().length() != dateFormat.toPattern().length()) {
				this.addActionError(this.getText("wrongFieldValue"));
				return;
			}
			dateFormat.setLenient(false);
			try {
				dateFormat.parse(inDate.trim());
			} catch (ParseException pe) {
				this.addActionError(this.getText("wrongFieldValue"));
				return;
			}
		}
	}

	public String getInDate() {
		return inDate;
	}

	public void setInDate(String inDate) {
		this.inDate = inDate;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

}
