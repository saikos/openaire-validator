package eu.dnetlib.validator.admin.constants;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

public class TypesAndTableNames {

	static private Logger logger = Logger.getLogger(TypesAndTableNames.class);
	
	static HashMap<String, String> ruleTypeToTableNames;
	static HashMap<String, String> crisEntities;
	static public List<String> deployEnvironments;
	static public HashMap<String, String> jobTypes;
	
	static {
		logger.debug("initializing the TableNames map");
		ruleTypeToTableNames = new HashMap<String, String>();
		
		ruleTypeToTableNames.put("ChainRule", "chain_rule");
		
		//rules OAIPMH
		ruleTypeToTableNames.put("OAIPMH Date Granularity", "oaipmhdategranularity_rule");
		ruleTypeToTableNames.put("OAIPMH Embargo Date Check", "oaipmhembargodate_rule");
		ruleTypeToTableNames.put("OAIPMH Incremental Record Delivery", "oaipmhincrementalrecorddelivery_rule");
		ruleTypeToTableNames.put("OAIPMH Resumption Token Duration Check", "oaipmhresumptiontokenduration_rule");

		//rules TEXT
//		ruleTypeToTableNames.put("TEXT Regular Expression", "textreguralexpresion_rule");
//		ruleTypeToTableNames.put("TEXT", "text_rule");
//		ruleTypeToTableNames.put("TEXT Vocabulary","textvocabulary_rule");		

		//rules XML
//		names.put(XMLRule.class.getName(),"xml_rule");
		ruleTypeToTableNames.put("XML Cardinality", "xmlcardinality_rule");
		ruleTypeToTableNames.put("XML Field Exists", "xmlfieldexist_rule");
		ruleTypeToTableNames.put("XML Not Confused Fields", "xmlnotconfusedfields_rule");
		ruleTypeToTableNames.put("XML Regular Expression", "xmlregularexpression_rule");
		ruleTypeToTableNames.put("XML Valid Url", "xmlvalidurl_rule");
		ruleTypeToTableNames.put("XML Vocabulary", "xmlvocabulary_rule");
		ruleTypeToTableNames.put("XML Cris Class Vocabulary", "xmlcrisclassvocabulary_rule");
		ruleTypeToTableNames.put("XML CRIS Referential Integrity Rule", "xmlcrisreferentialintegrity_rule");
		
		crisEntities = new HashMap<String, String>();
		crisEntities.put("Publication", "openaire_cris_publications");
		crisEntities.put("Person", "openaire_cris_persons");
		crisEntities.put("Organisation", "openaire_cris_orgunits");
		crisEntities.put("Project", "openaire_cris_projects" );
		crisEntities.put("Funding", "openaire_cris_funding" );
		crisEntities.put("Service", "openaire_cris_services");
		crisEntities.put("Product", "openaire_cris_datasets");		
		
		jobTypes = new HashMap<String, String>();
		jobTypes.put("usage", "OAI Usage");
		jobTypes.put("content", "OAI Content");
		
		deployEnvironments = new ArrayList<String>();
		deployEnvironments.add("openaire-production");
		deployEnvironments.add("openaire-beta");
		deployEnvironments.add("development");
		deployEnvironments.add("lareferencia");
		deployEnvironments.add("mincyt");
		
//		names.put(JobInfo.class.getName(), "jobs");
//		names.put(OAIContentJob.class.getName(), "oai_content_job");
//		names.put(OAIUsageJob.class.getName(), "oai_usage_job");

	}
	
	static public String getSetOfEntity(String entity) {
		return crisEntities.get(entity);
	}
	
	public static HashMap<String, String> getCrisEntities() {
		return crisEntities;
	}
	
	

	public static Set<String> getJobTypes() {
		logger.debug("getting all types of jobs");
		return new HashSet<String>(jobTypes.values());
	}

	public static void setJobTypes(HashMap<String, String> jobTypes) {
		TypesAndTableNames.jobTypes = jobTypes;
	}

	static public Set<String> getRuleTypes() {
		logger.debug("getting all types of rules");
		return ruleTypeToTableNames.keySet();
	}
	
	static public String getTableNameOfType(String type) {
		logger.debug("getting tableName of rule type "+type);
		return ruleTypeToTableNames.get(type);
	}
}
