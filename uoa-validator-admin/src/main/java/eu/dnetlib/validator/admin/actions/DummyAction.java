package eu.dnetlib.validator.admin.actions;

import com.opensymphony.xwork2.Action;

public class DummyAction extends BaseValidatorAction {

	private static final long serialVersionUID = 4105253794510714980L;

	public String execute() {
		return Action.SUCCESS;
		
	}
}
