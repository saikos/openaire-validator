package eu.dnetlib.validator.admin.actions.login;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.Action;

import eu.dnetlib.validator.admin.actions.BaseValidatorAction;
import eu.dnetlib.validator.admin.config.Constants;

public class Logout extends BaseValidatorAction implements SessionAware {

	private static final long serialVersionUID = -5304052800228900726L;
	private Map<String, Object> session;

	
	public String execute() {
		this.clearErrorsAndMessages();
		session.remove(Constants.loggedInField);
		session.remove("email");
		session.remove("isAdmin");
		this.addActionMessage("Logged-out successfully");
		
		return Action.SUCCESS;
	}
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}
}