package eu.dnetlib.validator.admin.actions.rules;

import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Action;

import eu.dnetlib.domain.functionality.validator.Rule;
import eu.dnetlib.validator.admin.actions.BaseValidatorAction;
import eu.dnetlib.validator.admin.api.ValidatorAdminException;
import eu.dnetlib.validator.admin.constants.TypesAndTableNames;

public class PrepareAddRule extends BaseValidatorAction {

	private static final long serialVersionUID = -8414397033694574297L;
	private Logger logger = Logger.getLogger(PrepareAddRule.class);

	private String type;
	private Boolean forCris, forUsage = false;
	private List<String> labels;
	private String jobType;
	private Set<String> entityTypes;
	private List<Rule> rules;


	public String execute() {
		this.clearErrorsAndMessages();
		logger.debug("preparing to add rule of type " + type );
		logger.debug("cris:  " + forCris);
		entityTypes = TypesAndTableNames.getCrisEntities().keySet();
		
		if (jobType.equals("usage"))
			forUsage = true; 
		try {
			labels = getValidatorAdminAPI().getRuleLabels(type, forUsage, forCris);
		} catch (ValidatorAdminException e) {
			return Action.ERROR;
		}
		if (type.equals("ChainRule")) {
			try {
				rules = this.getValidatorAdminAPI().getAllRules();
			} catch (ValidatorAdminException e) {
				return Action.ERROR;
			}
		}

		return Action.SUCCESS;
	}
	

	public List<String> getLabels() {
		return labels;
	}

	public void setLabels(List<String> labels) {
		this.labels = labels;
	}

	public String getType() {
		return type;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}


	public void setType(String type) {
		this.type = type;
	}

	public List<Rule> getRules() {
		return rules;
	}

	public void setRules(List<Rule> rules) {
		this.rules = rules;
	}

	public Set<String> getEntityTypes() {
		return entityTypes;
	}

	public void setEntityTypes(Set<String> entityTypes) {
		this.entityTypes = entityTypes;
	}

	public Boolean getForCris() {
		return forCris;
	}

	public void setForCris(Boolean forCris) {
		this.forCris = forCris;
	}
}
