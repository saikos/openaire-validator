package eu.dnetlib.validator.admin.actions.login;

import java.util.Map;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

import eu.dnetlib.validator.admin.config.Constants;

public class AuthenticationInterceptor implements Interceptor {

	private static final long serialVersionUID = 977098762082596481L;

	@Override
	public void destroy() {

	}

	@Override
	public void init() {

	}

	@Override
	public String intercept(ActionInvocation actionInvocation) throws Exception {
		Map<String, Object> session = actionInvocation.getInvocationContext().getSession();
		String auth = (String) session.get(Constants.loggedInField);

		if (auth == null) {
			return Action.LOGIN;
		}
		return actionInvocation.invoke();
	}
}