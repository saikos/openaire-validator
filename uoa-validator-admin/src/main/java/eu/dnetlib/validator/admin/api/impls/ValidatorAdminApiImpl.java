package eu.dnetlib.validator.admin.api.impls;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import eu.dnetlib.domain.functionality.validator.CustomProperties;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import eu.dnetlib.domain.functionality.validator.Rule;
import eu.dnetlib.domain.functionality.validator.RuleSet;
import eu.dnetlib.domain.functionality.validator.StoredJob;
import eu.dnetlib.validator.admin.actions.rules.FieldPair;
import eu.dnetlib.validator.admin.api.ValidatorAdminAPI;
import eu.dnetlib.validator.admin.api.ValidatorAdminException;
import eu.dnetlib.validator.admin.constants.FieldNames;
import eu.dnetlib.validator.admin.constants.FieldsAndDetails;
import eu.dnetlib.validator.admin.constants.TypesAndTableNames;
import eu.dnetlib.validator.admin.constants.fielddetails.FieldDetails;
import eu.dnetlib.validator.commons.dao.jobs.JobsDAO;
import eu.dnetlib.validator.commons.dao.rules.RulesDAO;
import eu.dnetlib.validator.commons.dao.rules.RulesetsDAO;
import eu.dnetlib.validator.commons.dao.users.UsersDAO;
import eu.dnetlib.validator.commons.email.Emailer;


/**
 * 
 * @author Nikon Gasparis
 *
 */

@Transactional(propagation = Propagation.REQUIRED)
public class ValidatorAdminApiImpl implements ValidatorAdminAPI {
	
	private JobsDAO jobsDao;
	private RulesetsDAO rulesetsDao;
	private RulesDAO rulesDao;
	private UsersDAO usersDao;

	private EmailBuilder emailBuilder;
	
	private Emailer emailer = null;
	private List<String> masterAdmins = new ArrayList<String>();
	private List<String> secondaryAdmins = new ArrayList<String>();
	private String registrationsReportEmail;
	private static Logger logger = Logger.getLogger(ValidatorAdminApiImpl.class);

	public void start(){
	}

	@Override
	public Rule getRule(int ruleId) throws ValidatorAdminException {
		try {
			logger.debug("getting rule with id: "+ruleId);
			return rulesDao.get(ruleId);
		} catch (Exception e) {
			logger.error("Error getting rule ", e);
			throw new ValidatorAdminException(e);
		}
		
	}
		
	@Override
	public List<FieldPair> getRuleValuePairs(Rule rule) throws ValidatorAdminException {
		try {
			logger.debug("getting rule value pairs");
			boolean forUsage = false;
			if( rule.getJob_type().equals("usage"))
				forUsage = true;
			List<String> labels = this.getRuleLabels(rule.getType(), forUsage, rule.isFor_cris());

			List<FieldPair> ruleValues = new ArrayList<FieldPair>();
			for (String fieldName : labels) {
				FieldPair pair = new FieldPair();
//				String fieldValue=null;
				pair.setFieldName(fieldName);
				if (fieldName.split("\\.")[1].equals(FieldNames.RULE_NAME)) {
					pair.setFieldValue(rule.getName());
				}
				else if (fieldName.split("\\.")[1].equals(FieldNames.RULE_DESCRIPTION)) {
					pair.setFieldValue(rule.getDescription());
				}
				else if (fieldName.split("\\.")[1].equals(FieldNames.RULE_MANDATORY)) {
					pair.setFieldValue(Boolean.toString(rule.isMandatory()));
				}				
				else if (fieldName.split("\\.")[1].equals(FieldNames.RULE_WEIGHT)) {
					pair.setFieldValue(Integer.toString(rule.getWeight()));
				}
				else if (fieldName.split("\\.")[1].equals(FieldNames.RULE_PROVIDER_INFORMATION)) {
					pair.setFieldValue(rule.getProvider_information());
				}
				else{
					pair.setFieldValue(rule.getConfiguration().getProperty(fieldName.split("\\.")[1]));
				}				
				logger.debug("getting rule value pairs-addpair: "+fieldName+"-"+rule.getConfiguration().getProperty(fieldName.split("\\.")[1]));
				ruleValues.add(pair);
			}
			return ruleValues;
		} catch (Exception e) {
			logger.error("Error getting ruleValue pairs", e);
			throw new ValidatorAdminException(e);
		}

	}	

	
	@Override
	public List<Rule> getAllRules() throws ValidatorAdminException {
		try {
			logger.debug("getting all rules ");
			return rulesDao.getAllRules();
		} catch (Exception e) {
			logger.error("Error getting all rules  to rule pair", e);
			throw new ValidatorAdminException(e);
		}	
	}	
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public String storeRule(Rule rule, String[] inputs, Boolean updateExisting) throws ValidatorAdminException {
		try {
			logger.debug("edit rule using the inputs of the user");
			List<String> fields = new ArrayList<String>();
			List<FieldDetails> generalFields = FieldsAndDetails.getFieldsAndDetails("Rules");
			List<FieldDetails> usageFields = FieldsAndDetails.getFieldsAndDetails("usage_rules");
			List<FieldDetails> crisFields = FieldsAndDetails.getFieldsAndDetails("cris_rules");
			List<FieldDetails> subFields = FieldsAndDetails.getFieldsAndDetails(TypesAndTableNames.getTableNameOfType(rule.getType()));
			for (FieldDetails field : generalFields) {
				if (field.isForDet() && !field.getFieldName().equals(FieldNames.RULE_TYPE) && !field.getFieldName().equals(FieldNames.RULE_JOBTYPE))
					fields.add(field.getFieldName());
			}
			if (subFields != null) {
				for (FieldDetails field : subFields) {
					if (field.isForDet())
						fields.add(field.getFieldName());
				}
			}
			if (rule.getJob_type().toLowerCase().contains("usage")) {
				for (FieldDetails field : usageFields) {
					if (field.isForDet())
						fields.add(field.getFieldName());
				}
			}
			if (rule.isFor_cris()) {
				for (FieldDetails field : crisFields) {
					if (field.isForDet())
						fields.add(field.getFieldName());
				}
			}
//			Properties pros = new Properties();
			CustomProperties pros = new CustomProperties();

			logger.debug("Fields: " + fields);
			for (int i = 0; i < inputs.length; i++) {
				logger.debug("input-" + i + " : "+ inputs[i]);
			}
			for (int i = 0; i < inputs.length; i++) {
				logger.debug("Field: " + fields.get(i) + " - input: " + inputs[i]);
				if (fields.get(i).equals(FieldNames.RULE_NAME)) {
					rule.setName(inputs[i]);
				}
				else if (fields.get(i).equals(FieldNames.RULE_DESCRIPTION)) {
					rule.setDescription(inputs[i]);
				}
				else if (fields.get(i).equals(FieldNames.RULE_MANDATORY)) {
					rule.setMandatory(Boolean.parseBoolean(inputs[i]));
				}				
				else if (fields.get(i).equals(FieldNames.RULE_WEIGHT)) {
					rule.setWeight(Integer.parseInt(inputs[i]));
				}
				else if (fields.get(i).equals(FieldNames.RULE_PROVIDER_INFORMATION)) {
					rule.setProvider_information(inputs[i]);
				}
				else if (fields.get(i).equals(FieldNames.RULE_CRIS_ENTITY)) {
					rule.setEntity_type(inputs[i]);
				}
				else{
					if (fields.get(i).equals(FieldNames.RULE_VOCABULARY_WORDS)) {
						String words = "";
						BufferedReader bf = new BufferedReader(new StringReader(inputs[i]));
						String line;
						while ((line = bf.readLine()) != null) {
							words += ",";
							words += line;
						}
						words = words.substring(1);
						pros.setProperty(fields.get(i), words);
					} else if (fields.get(i).equals(FieldNames.RULE_NOTCONNFUSEDFIELDS)) {
						String ncfields = "";
						BufferedReader bf = new BufferedReader(new StringReader(inputs[i]));
						String line;
						while ((line = bf.readLine()) != null)
							ncfields += line + "\n";
						ncfields = ncfields.substring(0, ncfields.lastIndexOf('\n'));
						pros.setProperty(fields.get(i), ncfields);
					
					}else  
						pros.setProperty(fields.get(i), inputs[i]);
				}
			}
			rule.setConfiguration(pros);
			
			if (!updateExisting)
				rule.setId(-1);
			rulesDao.save(rule);
			return null;

		} catch (Exception e) {
			logger.error("Error storing rule", e);
			throw new ValidatorAdminException(e);
		}
	}
		
	@Override
	public List<String> getRuleLabels(String ruleType, Boolean forUsage, Boolean forCris) throws ValidatorAdminException {
		try {
			logger.debug("getting rule labels for type " + ruleType);
			List<String> labels = new ArrayList<String>();
			List<FieldDetails> generalFields = FieldsAndDetails.getFieldsAndDetails("Rules");
			List<FieldDetails> usageFields = FieldsAndDetails.getFieldsAndDetails("usage_rules");
			List<FieldDetails> crisFields = FieldsAndDetails.getFieldsAndDetails("cris_rules");
			List<FieldDetails> subFields = FieldsAndDetails.getFieldsAndDetails(TypesAndTableNames.getTableNameOfType(ruleType));
			for (FieldDetails field : generalFields) {
				if (field.isForDet() && !field.getFieldName().equals(FieldNames.RULE_TYPE) && !field.getFieldName().equals(FieldNames.RULE_JOBTYPE))
					labels.add(field.getTableName() + "." + field.getFieldName());
			}
			if (subFields != null) {
				for (FieldDetails field : subFields) {
					if (field.isForDet())
						labels.add(field.getTableName() + "." + field.getFieldName());
				}
			}
			if (forUsage) {
				for (FieldDetails field : usageFields) {
					if (field.isForDet())
						labels.add(field.getTableName().split("_")[1] + "." + field.getFieldName());
				}
			}
			if (forCris) {
				for (FieldDetails field :crisFields) {
					if (field.isForDet())
						labels.add(field.getTableName().split("_")[1] + "." + field.getFieldName());
				}
			}
			return labels;
		}catch (Exception e) {
			logger.error("getting rule labels for type " + ruleType, e);
			throw new ValidatorAdminException(e);
		}			
	}
	
	@Override
	public List<RuleSet> getRuleSets() throws ValidatorAdminException {
		try {
			logger.debug("Getting all rule sets");
			return rulesetsDao.getRuleSets();
		} catch (Exception e) {
			logger.error("Error getting all rule sets", e);
			throw new ValidatorAdminException(e);
		}
	}
	
	@Override
	public String deleteRule(int ruleId) throws ValidatorAdminException {
		try {
			logger.debug("deleting rule with id "+ruleId);
			return rulesDao.delete(ruleId);
		}
		catch (Exception e) {
			logger.error("Error deleting rule with id "+ruleId, e);
			throw new ValidatorAdminException(e);
		}
	}
	
	@Override
	public RuleSet getRuleSet(int setId) throws ValidatorAdminException {
		try {
			logger.debug("getting ruleSet with id: " + setId);
			return rulesetsDao.get(setId);
		} catch (Exception e) {
			logger.error("Error getting ruleSet with id: " + setId, e);
			throw new ValidatorAdminException(e);
		}
	}
	
	@Override
	public void storeRuleSet(RuleSet ruleSet, boolean updateExisting) throws ValidatorAdminException {
		try {
			if (updateExisting)
				logger.debug("editing rule set " + ruleSet.getId());
			else
				logger.debug("creating rule set " + ruleSet.getName());
			
			rulesetsDao.save(ruleSet);
			
			} catch (Exception e) {
				logger.error("Error storing ruleSet " + ruleSet.getName(), e);
			throw new ValidatorAdminException(e);
		}
		
	}
	
	@Override
	public void deleteRuleSet(int setId) throws ValidatorAdminException {
		try {
			logger.debug("deleting rule set " + setId);
			rulesetsDao.delete(setId);
		} catch (Exception e) {
			logger.error("Error deleting rule set " + setId, e);
			throw new ValidatorAdminException(e);
		}
	}	
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public int deleteOldJobs(String date, String period, String mode) throws ValidatorAdminException {
		int jobsDeleted = 0;
		try {
			logger.debug("Deleting jobs " + period + " than "+date);
			if (mode.equalsIgnoreCase("uncompleted_only")) {
				jobsDeleted = jobsDao.deleteUncompletedJobs();
			}
			else if (mode.equalsIgnoreCase("compTest_only"))
				jobsDeleted = jobsDao.deleteOld(date, period, "Compatibility Test");
			else if (mode.equalsIgnoreCase("all"))
				jobsDeleted = jobsDao.deleteOld(date, period, null);
			else if (mode.equalsIgnoreCase("import"))
				jobsDao.importOldJobs();;
		} catch (Exception e) {
			logger.error("Error deleting old jobs", e);
			throw new ValidatorAdminException(e);
		}
		return jobsDeleted;
	}

	public Emailer getEmailer() {
		return emailer;
	}
	public JobsDAO getJobsDao() {
		return jobsDao;
	}

	public void setJobsDao(JobsDAO jobsDao) {
		this.jobsDao = jobsDao;
	}

	public RulesetsDAO getRulesetsDao() {
		return rulesetsDao;
	}

	public void setRulesetsDao(RulesetsDAO rulesetsDao) {
		this.rulesetsDao = rulesetsDao;
	}

	public RulesDAO getRulesDao() {
		return rulesDao;
	}

	public void setRulesDao(RulesDAO rulesDao) {
		this.rulesDao = rulesDao;
	}

	public UsersDAO getUsersDao() {
		return usersDao;
	}

	public void setUsersDao(UsersDAO usersDao) {
		this.usersDao = usersDao;
	}

	public void setEmailer(Emailer emailer) {
		this.emailer = emailer;
	}

	
	public String getRegistrationsReportEmail() {
		return registrationsReportEmail;
	}

	public void setRegistrationsReportEmail(String registrationsReportEmail) {
		this.registrationsReportEmail = registrationsReportEmail;
	}

	public EmailBuilder getEmailBuilder() {
		return emailBuilder;
	}

	public void setEmailBuilder(EmailBuilder emailBuilder) {
		this.emailBuilder = emailBuilder;
	}

	@Override
	public boolean userIsMasterAdmin(String user)
			throws ValidatorAdminException {
		logger.debug("checking if user " + user + " is master admin");
		if (this.masterAdmins.contains(user)) {
			logger.debug("is master admin");
			return true;
		} else {
			logger.debug("isn't master admin");
			return false;
		}
	}
	
	@Override
	public boolean userIsSecondaryAdmin(String user)
			throws ValidatorAdminException {
		logger.debug("checking if user " + user + " is secondary admin");
		if (this.secondaryAdmins.contains(user)) {
			logger.debug("is secondary admin");
			return true;
		} else {
			logger.debug("isn't secondary admin");
			return false;
		}
	}
	

	public void setMasterAdmins(String masterAdmins) {
		String[] recps = masterAdmins.split(",");

		for (String recp : recps) {
			recp = recp.trim();
			this.masterAdmins.add(recp);
		}
	}

	public void setSecondaryAdmins(String secondaryAdmins) {
		String[] recps = secondaryAdmins.split(",");

		for (String recp : recps) {
			recp = recp.trim();
			this.secondaryAdmins.add(recp);
		}
	}

	@Override
	public String sendRegistrationsReport(String dateFrom, String dateTo, String month, String year) throws ValidatorAdminException {
		try {
			List <StoredJob> jobs = null;
			Map<String, String> jobInfo = null;
			List<Map<String, String>> passedJobsList = new ArrayList<Map<String, String>>();
			List<Map<String, String>> failedJobsList = new ArrayList<Map<String, String>>();
			
			Map<String, StoredJob> passedJobsMap = new HashMap<String, StoredJob>();
			Map<String, StoredJob> failedJobsMap = new HashMap<String, StoredJob>();
			
			logger.debug("Sending registrations Report for period: " + dateFrom + " to: " + dateTo);
			jobs = jobsDao.getJobs(null, "Registration Request", null, null, dateFrom, dateTo);
			for (StoredJob job : jobs) {
				String key = job.getBaseUrl()+"|"+job.getDesiredCompatibilityLevel();
				if (job.getContentJobScore() >= 50 && job.getContentJobScore() >= 50) {
					failedJobsMap.remove(key);
					passedJobsMap.put (key, job);
				} else {
					if (!passedJobsMap.containsKey(key)) {
						failedJobsMap.put (key, job);
					}
				}
			}
			boolean oldFormat = false;
			for (StoredJob job : passedJobsMap.values()) {
				jobInfo = new HashMap<String, String>();
				jobInfo.put("name", job.getOfficialName());
				jobInfo.put("baseUrl", job.getBaseUrl());
				jobInfo.put("email", job.getUserEmail());
				jobInfo.put("compliance", job.getDesiredCompatibilityLevel());
				if (job.getOfficialName() == null)
					oldFormat = true;
				passedJobsList.add(jobInfo);
			}
			for (StoredJob job : failedJobsMap.values()) {
				jobInfo = new HashMap<String, String>();
				jobInfo.put("name", job.getOfficialName());
				jobInfo.put("baseUrl", job.getBaseUrl());
				jobInfo.put("email", job.getUserEmail());
				jobInfo.put("compliance", job.getDesiredCompatibilityLevel());
				if (job.getOfficialName() == null)
					oldFormat = true;
				failedJobsList.add(jobInfo);
			}
			String report = emailBuilder.buildXml(passedJobsList, failedJobsList, month, oldFormat);
			ArrayList<String> recps = new ArrayList<String>();
			recps.add(registrationsReportEmail);
			emailer.sendMail(recps,"Datasource Registrations -- " + month + " " + year, report, true, null);
			logger.debug("email : " + report);
			return report;
		} catch (Exception e) {
			logger.error("Error deleting old jobs", e);
			throw new ValidatorAdminException(e);
		}
	}

}

