package eu.dnetlib.validator.admin.actions.login;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Action;

import eu.dnetlib.validator.admin.actions.BaseValidatorAction;

public class SendPassword extends BaseValidatorAction {

	private static final long serialVersionUID = 7540821478579531671L;
	private Logger logger = Logger.getLogger(SendPassword.class);
	
	private String email;

	public String execute() {
		this.clearErrorsAndMessages();
		try {
			logger.debug("sending password to user " + email);
			List<String> to = new ArrayList<String>();
			to.add(this.getEmail());
			String securityCode = this.getUserAPI().prepareResetPassword(this.getEmail());
			this.getEmailer().sendMail(to, this.getText("forgotPassword.mail.Subject"), this.getText("forgotPassword.mail.Body1") + ": " + this.getValBaseUrl() + "/resetPassword.action?securityCode=" + securityCode + "\n\n" + this.getText("forgotPassword.mail.Body2") + ": " + securityCode, false, null);
			this.addActionMessage(this.getText("forgotPassword.message"));
			return Action.SUCCESS;
		} catch (Exception e) {
			logger.error("error sending password to user " + email, e);
			this.addActionError(this.getText("generic.error"));
			return "exception";
		}
	}

	public void validate() {
		this.clearErrors();
		try {
			if (!this.getUserAPI().userExists(this.getEmail())) {
				this.addActionError(this.getText("login.userNotExists"));
				return;
			}
//			if (!this.getUserAPI().isUserActivated(this.getEmail())) {
//				this.addActionError(this.getText("login.notActivated"));
//				return;
//			}
		} catch (Exception e) {
			this.addActionError(e.toString());
		}
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
