package eu.dnetlib.validator.admin.api.impls;

import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

public class EmailBuilder {

	private static Logger logger = Logger.getLogger(EmailBuilder.class);
	
	private VelocityEngine ve = null;
	
	public void init() throws Exception {
		this.ve = new VelocityEngine();
		this.ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		this.ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
		this.ve.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.NullLogSystem");
		this.ve.init();	
	}
	
	public String buildXml(List<Map<String, String>> passedJobsList, List<Map<String, String>> failedJobsList, String month, boolean oldFormat) {
		logger.debug("Building the registrations report Email");
		String finalEmail = null;
		
		try {
        
			VelocityContext context = new VelocityContext();

			context.put("passedJobs", passedJobsList);
			context.put("failedJobs", failedJobsList);
			context.put("month", month);
			if (passedJobsList.isEmpty())
				context.put("nonePassed", "There are no successful registration attempts.");
			else
				context.put("nonePassed", " ");
			if (failedJobsList.isEmpty())
				context.put("noneFailed", "There are no failed registration attempts.");
			else
				context.put("noneFailed", " ");
			
			Template t;
			
			if (oldFormat)
				t = this.ve.getTemplate("/eu/dnetlib/validator/admin/api/emailTeplateOld.vm");
			else
				t = this.ve.getTemplate("/eu/dnetlib/validator/admin/api/emailTeplate.vm");
			
			StringWriter writer = new StringWriter();
			
			t.merge(context, writer);
			finalEmail = writer.toString();
			
		} catch (TransformerConfigurationException e) {
			logger.error("Error while building XML file", e);
		} catch (TransformerFactoryConfigurationError e) {
			logger.error("Error while building XML file", e);
		} catch (TransformerException e) {
			logger.error("Error while building XML file", e);
		} catch (ResourceNotFoundException e) {
			logger.error("Error while building XML file", e);
		} catch (ParseErrorException e) {
			logger.error("Error while building XML file", e);
		} catch (Exception e) {
			logger.error("Error while building XML file", e);
		}
		
        return finalEmail;
	}
}
