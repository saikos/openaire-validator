package eu.dnetlib.validator.admin.actions.users;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Action;

import eu.dnetlib.validator.admin.actions.BaseValidatorAction;

public class ActivateAccount extends BaseValidatorAction {

	private static final long serialVersionUID = 4680016183168923054L;
	private Logger logger = Logger.getLogger(ActivateAccount.class);
	
	private String activationId;

	public String execute() {
		this.clearErrorsAndMessages();
		try {
			logger.debug("activating user account with activation id " + activationId);
			if (this.getUserAPI().activateUser(this.getActivationId()))
				this.addActionMessage(this.getText("registration.okAccountActivation"));
			else
				this.addActionMessage(this.getText("registration.okAccountAlreadyActivation"));
			return Action.SUCCESS;
		} catch (Exception e) {
			logger.error("error activating user account with activation id " + activationId, e);
			this.addActionError(this.getText("generic.error"));
			reportException(e);
			return "exception";
		}
	}

	public void validate() {
		this.clearErrors();
		if (this.getActivationId() == null || this.getActivationId().length() == 0) {
			this.addActionError(this.getText("noActivationId"));
			return;
		}
	}

	public String getActivationId() {
		return activationId;
	}

	public void setActivationId(String activationId) {
		this.activationId = activationId;
	}
}