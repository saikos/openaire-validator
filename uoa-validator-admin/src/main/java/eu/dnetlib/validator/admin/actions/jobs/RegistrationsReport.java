package eu.dnetlib.validator.admin.actions.jobs;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Action;

import eu.dnetlib.validator.admin.actions.BaseValidatorAction;
import eu.dnetlib.validator.admin.api.ValidatorAdminException;

public class RegistrationsReport extends BaseValidatorAction {

	private static final long serialVersionUID = -1868055729316139461L;
	private static final Logger logger = Logger.getLogger(RegistrationsReport.class);
	private String dateFrom = null;
	private String dateTo = null;
	private String mode, month;
	private int year;

	public String execute() {
		this.clearErrorsAndMessages();
		try {
			
			int monthInt = Integer.parseInt(month.split("-")[1]);
			String monthStr = month.split("-")[0];
			dateFrom = new String("YYYY-MM-01").replaceFirst("MM", Integer.toString(monthInt)).replaceFirst("YYYY", Integer.toString(year));
			if (monthInt != 12)
				dateTo = new String("YYYY-MM-01").replaceFirst("MM", Integer.toString(monthInt+1)).replaceFirst("YYYY", Integer.toString(year));
			else
				dateTo = new String("YYYY-MM-01").replaceFirst("MM", "1").replaceFirst("YYYY", Integer.toString(year+1));
			this.getValidatorAdminAPI().sendRegistrationsReport(dateFrom, dateTo, monthStr, Integer.toString(year));
		} catch (ValidatorAdminException e) {
			logger.error("Error deleting old jobs", e);
			this.addActionError(this.getText("generic.error"));
			reportException(e);
			return "input-problem";
		}
		this.addActionMessage(this.getText("manageJobs.sendReports.success"));
		
		return Action.SUCCESS;
	}
	

	public void validate() {
		this.clearErrors();
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
}
