package eu.dnetlib.validator.admin.api;

import eu.dnetlib.domain.functionality.UserProfile;

public interface UserApi {

	public boolean activateUser(String activationId) throws Exception;

	public boolean isUserActivated(String email) throws Exception;

	public boolean userExists(String email) throws Exception;

	public boolean usernameExists(String username) throws Exception;

	public boolean correctCreds(String email, String password) throws Exception;

	public String prepareResetPassword(String email) throws Exception;

	public void resetPassword(String uuid, String password) throws Exception;

	public boolean isAdmin(String email) throws Exception;

	public void editUser(UserProfile user)
			throws Exception;

	public String addUser(String username, String email, String password,
			String firstName, String lastName, String institution) throws Exception;

	public String getEmailFromUsername(String username) throws Exception;

	public UserProfile getUser(String userIdentifier) throws Exception;
}