package eu.dnetlib.validator.admin.constants.fielddetails;

public class FieldDetails {

	private String tableName;
	private String fieldName;
	// true if the user must provide a value for this field when adding a new record in the table
	private boolean forDet;
	
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public boolean isForDet() {
		return forDet;
	}
	public void setForDet(boolean forDet) {
		this.forDet = forDet;
	}
	
	public String getBundleName() {
		return this.tableName+"."+this.fieldName;
	}
}
