package eu.dnetlib.validator.admin.api.impls;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import eu.dnetlib.domain.functionality.UserProfile;
import eu.dnetlib.validator.admin.api.UserApi;
import eu.dnetlib.validator.commons.dao.users.UsersDAO;

@Transactional(propagation = Propagation.REQUIRED)
public class UserApiLocal implements UserApi {

	private static Logger logger = Logger.getLogger(UserApiLocal.class);
	private UsersDAO usersDao;

	@Override
	@Transactional()
	public boolean activateUser(String activationId) throws Exception {
		usersDao.activateUser(activationId);
		return true;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public String addUser(String username, String email, String password, String firstName, String lastName,  String institution) throws Exception {
		String hashword;
		MessageDigest md5 = MessageDigest.getInstance("MD5");
		md5.update(password.getBytes());
		BigInteger hash = new BigInteger(1, md5.digest());
		hashword = hash.toString(16);
		String activationId = UUID.randomUUID().toString();
		UserProfile newUser = new UserProfile();
		newUser.setEmail(email);
		newUser.setPassword(hashword);
		newUser.setFirstname(firstName);
		newUser.setLastname(lastName);
		newUser.setUsername(username);
		newUser.setInstitution(institution);
		newUser.setActivationId(activationId);
		usersDao.save(newUser);
		return activationId;
	}

	@Override
	public boolean correctCreds(String email, String password) throws Exception {
		String hashword;
		MessageDigest md5 = MessageDigest.getInstance("MD5");
		md5.update(password.getBytes());
		BigInteger hash = new BigInteger(1, md5.digest());
		hashword = hash.toString(16);
		
		if (usersDao.checkCorrectCreds(email, hashword)) {
			logger.debug("Password verified");
			return true;
		} else {
			logger.debug("no user found with email: " + email + " and pass: " + password + " (" + hashword + ")");

			return false;
		}
	}

	@Override
	public boolean isAdmin(String email) throws Exception {
		return usersDao.isAdmin(email);
	}

	@Override
	public boolean isUserActivated(String email) throws Exception {
		return usersDao.isActivated(email);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public String prepareResetPassword(String email) throws Exception {
		String uuid = UUID.randomUUID().toString();
		usersDao.prepareResetPassword(uuid,email);
		return uuid;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void resetPassword(String uuid, String password) throws Exception {
		String hashword;
		MessageDigest md5 = MessageDigest.getInstance("MD5");
		md5.update(password.getBytes());
		BigInteger hash = new BigInteger(1, md5.digest());
		hashword = hash.toString(16);
		usersDao.ResetPassword(uuid, hashword);
	}

	@Override
	public boolean userExists(String email) throws Exception {
		return usersDao.userExists(email);
	}

	@Override
	public void editUser(UserProfile user) throws Exception {
		this.usersDao.save(user);

	}

	@Override
	public boolean usernameExists(String username) throws Exception {
		return usersDao.usernameExists(username);
	}

	@Override
	public String getEmailFromUsername(String username) throws Exception {
		UserProfile user = usersDao.get(username);
		if (user == null)
			return null;
		else
			return usersDao.get(username).getEmail();
	}

	@Override
	public UserProfile getUser(String userIdentifier) throws Exception {
		return usersDao.get(userIdentifier);
	}

	public UsersDAO getUsersDao() {
		return usersDao;
	}

	public void setUsersDao(UsersDAO usersDao) {
		this.usersDao = usersDao;
	}

	
}
