package eu.dnetlib.validator.admin.actions.rules;

import java.util.Set;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Action;

import eu.dnetlib.validator.admin.actions.BaseValidatorAction;
import eu.dnetlib.validator.admin.constants.TypesAndTableNames;

public class PopulateRuleCategories extends BaseValidatorAction {

	private static final long serialVersionUID = 3845749201114926778L;
	private Logger logger = Logger.getLogger(PopulateRuleCategories.class);
	
	private Set<String> ruleTypes;
//	private Set<String> jobTypes;
	private String defaultJobType;
	
	public String execute() {
		this.clearErrorsAndMessages();
		logger.debug("populating rule categories");
			defaultJobType="content";
//			jobTypes = TypesAndTableNames.getJobTypes();
			ruleTypes = TypesAndTableNames.getRuleTypes();

		return Action.SUCCESS;
	}

	public Set<String> getRuleTypes() {
		return ruleTypes;
	}

	public void setRuleTypes(Set<String> ruleTypes) {
		this.ruleTypes = ruleTypes;
	}

//	public Set<String> getJobTypes() {
//		return jobTypes;
//	}
//
//	public void setJobTypes(Set<String> jobTypes) {
//		this.jobTypes = jobTypes;
//	}

	public String getDefaultJobType() {
		return defaultJobType;
	}

	public void setDefaultJobType(String defaultJobType) {
		this.defaultJobType = defaultJobType;
	}
	
}
