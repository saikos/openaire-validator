package eu.dnetlib.validator.admin.actions.users;

import java.util.ArrayList;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Action;

import eu.dnetlib.validator.admin.actions.BaseValidatorAction;

public class Register extends BaseValidatorAction {

	private static final long serialVersionUID = 4906485913149844710L;
	private Logger logger = Logger.getLogger(Register.class);
	
	private String email, password, repassword;
	private String username, firstName, lastName, institution;

	public String execute() {
		this.clearErrorsAndMessages();
		try {
			logger.debug("registering user " + email);
			String activationId = null;

			activationId = this.getUserAPI().addUser(username, email, password, firstName, lastName, institution);

			this.addActionMessage((this.getText("registration.successful")));
			this.addActionMessage((this.getText("general.unblock") + " " + this.getEmailer().getFrom()));

			ArrayList<String> to = new ArrayList<String>();
			to.add(this.getEmail());

			this.getEmailer().sendMail(to, this.getText("registration.mail.subject"), this.getText("registration.mail.message") + ": " + this.getValBaseUrl() + "/activateAccount.action?activationId=" + activationId, false, null);

			return Action.SUCCESS;
		} catch (Exception e) {
			logger.error("error registering user " + email, e);
			this.addActionError(this.getText("generic.error"));
			reportException(e);
			return "exception";
		}
	}

	public void validate() {
		this.clearErrors();
		if (this.getEmail() == null || this.getEmail().length() == 0)
			this.addFieldError("email", this.getText("compulsoryField"));
		if (this.getPassword() == null || this.getPassword().length() == 0)
			this.addFieldError("password", this.getText("compulsoryField"));
		if (this.getRepassword() == null || this.getRepassword().length() == 0)
			this.addFieldError("repassword", this.getText("compulsoryField"));
		if (this.getUsername() == null || this.getUsername().length() == 0)
			this.addFieldError("username", this.getText("compulsoryField"));
		if (this.getFirstName() == null || this.getFirstName().length() == 0)
			this.addFieldError("firstName", this.getText("compulsoryField"));
		if (this.getLastName() == null || this.getLastName().length() == 0)
			this.addFieldError("lastName", this.getText("compulsoryField"));

		if (!this.getPassword().equals(this.getRepassword()))
			this.addFieldError("password", this.getText("identicalPasswords"));

		Pattern rfc2822 = Pattern.compile("^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");
		if (!rfc2822.matcher(this.getEmail().trim().toLowerCase()).matches()) {
			this.addFieldError("email", this.getText("notValidEmail"));
		}

		try {
			if (this.getUserAPI().userExists(this.getEmail()))
				this.addFieldError("email", this.getText("userAlreadyExists"));
			if (this.getUserAPI().usernameExists(this.getUsername()))
				this.addFieldError("username", this.getText("userAlreadyExists"));

		} catch (Exception e) {
			this.addFieldError("email", this.getText("userAlreadyExists"));
		}
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRepassword() {
		return repassword;
	}

	public void setRepassword(String repassword) {
		this.repassword = repassword;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}
	
}
