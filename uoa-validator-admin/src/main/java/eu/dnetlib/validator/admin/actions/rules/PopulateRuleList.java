package eu.dnetlib.validator.admin.actions.rules;

import java.util.List;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Action;

import eu.dnetlib.domain.functionality.validator.Rule;
import eu.dnetlib.validator.admin.actions.BaseValidatorAction;

public class PopulateRuleList extends BaseValidatorAction {

	private static final long serialVersionUID = -4035025449768015129L;
	private List<Rule> rules;
	private static final Logger logger = Logger.getLogger(PopulateRuleList.class);

	public String execute() {
		this.clearErrorsAndMessages();
		try {
			logger.debug("populating rule list");
			rules = this.getValidatorAdminAPI().getAllRules();
			return Action.SUCCESS;
		}
		catch(Exception e) {
			logger.error("Error redirecting to job", e);
			this.addActionError(this.getText("generic.error"));
			reportException(e);
			return "exception";
		}
	}

	public List<Rule> getRules() {
		return rules;
	}

	public void setRules(List<Rule> rules) {
		this.rules = rules;
	}
	
	
}
