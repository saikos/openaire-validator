package eu.dnetlib.validator.admin.actions.rules;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Action;

import eu.dnetlib.domain.functionality.validator.Rule;
import eu.dnetlib.validator.admin.actions.BaseValidatorAction;
import eu.dnetlib.validator.admin.api.ValidatorAdminException;

public class EditRule extends BaseValidatorAction {

	private static final long serialVersionUID = -8296545383286127915L;
	private Rule rule;
	private String[] inputs;
	private String funct = null;
	private static final Logger logger = Logger.getLogger(EditRule.class);

	public String execute() {
		this.clearErrorsAndMessages();
		String ret = null;
		logger.debug("editing rule " + rule.getId());
		try {
			logger.debug("ruleType: " + rule.getType());
			if (funct.equals("clone")) {
				if (rule.getJob_type().toLowerCase().contains("usage"))
					rule.setJob_type("usage");
				else if (rule.getJob_type().toLowerCase().contains("content"))
					rule.setJob_type("content");
				ret = this.getValidatorAdminAPI().storeRule(rule, inputs, false);
			} else {
				ret = this.getValidatorAdminAPI().storeRule(rule, inputs, true);
			}
			
		} catch (ValidatorAdminException e) {
			logger.error("Error editing rule", e);
			this.addActionError(this.getText("generic.error"));
			reportException(e);
			return "exception";
		}

		if (ret != null) {
			this.addActionError(this.getText(ret));
			return "input-problem";
		}

		this.addActionMessage(this.getText("editRule.success"));
		return Action.SUCCESS;
	}

	public Rule getRule() {
		return rule;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}

	public String[] getInputs() {
		return inputs;
	}

	public void setInputs(String[] inputs) {
		this.inputs = inputs;
	}

	public String getFunct() {
		return funct;
	}

	public void setFunct(String funct) {
		this.funct = funct;
	}
	
}
