package eu.dnetlib.validator.admin.actions.login;

import java.util.Calendar;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.Action;

import eu.dnetlib.validator.admin.actions.BaseValidatorAction;
import eu.dnetlib.validator.admin.config.Constants;

public class PortalLogin extends BaseValidatorAction implements ServletRequestAware, SessionAware {

	private static final long serialVersionUID = -6312800103845446815L;
	private transient Logger logger = Logger.getLogger(PortalLogin.class);

	private HttpServletRequest request;
	private Map<String, Object> session;

	private String user, ip, valid, signature;
	private String key;
	private String email;

	public void validate() {
		this.clearErrors();
		logger.debug("validating log-in from portal link " + user + ", " + ip + ", " + valid + ", " + signature);
		if (user == null || ip == null || valid == null) {
			this.addActionError(this.getText("login.manual"));
			return;
		}
		if (user.trim().equals("") || ip.trim().equals("") || valid.trim().equals("")) {
			this.addActionError(this.getText("login.manual"));
			return;
		}
		long millisUntil = 0;
		try {
			millisUntil = Long.parseLong(valid);
		} catch (Exception e) {
			logger.error("", e);
			this.addActionError(this.getText("login.manual"));
			return;
		}

		try {
			if (!signatureIsValid((user + ip + valid).trim(), signature.trim())) {
				logger.debug("signature was not valid");
				this.addActionError(this.getText("login.manual"));
				return;
			}
		} catch (Exception e) {
			logger.error("", e);
			this.addActionError(this.getText("login.manual"));
			return;
		}

		Calendar cal = Calendar.getInstance();
		if (cal.getTime().getTime() > millisUntil) {
			logger.debug("link has expired current-time: "+cal.getTime().getTime()+" link-time: "+millisUntil);
			this.addActionError(this.getText("login.manual"));
			return;
		}

		if (!ip.trim().equals(request.getRemoteAddr().trim())) {
			logger.debug("ips are different "+ip+" "+request.getRemoteAddr());
			this.addActionError(this.getText("login.manual"));
			return;
		}

		try {
//			if (isModeLdap())
				this.email = this.getUserAPI().getEmailFromUsername(user);

			if (this.email == null) {
				this.email = "";
				this.addActionError(this.getText("login.userNotExists"));
				return;
			}

			if (!this.getUserAPI().userExists(this.getEmail())) {
				this.email = "";
				this.addActionError(this.getText("login.userNotExists"));
				return;
			}

			if (!this.getUserAPI().isUserActivated(this.getEmail())) {
				this.email = "";
				this.addActionError(this.getText("login.notActivated"));
				return;
			}
		} catch (Exception e) {
			logger.error("", e);
			this.addActionError(e.toString());
		}
	}

	public String execute() {
		this.clearErrorsAndMessages();
		try {
			logger.debug("logging-in from portal link " + user + ", " + ip + ", " + valid + ", " + signature);
			session.put(Constants.loggedInField, this.getEmail());
			session.put("email", this.getEmail());
			if (this.getUserAPI().isAdmin(email))
				session.put("isAdmin", "true");

			this.addActionMessage("Logged-in successfully");

			return Action.SUCCESS;
		} catch (Exception e) {
			logger.error("error logging-in from portal link " + user + ", " + ip + ", " + valid + ", " + signature, e);
			this.addActionError(this.getText("generic.error"));
			reportException(e);
			return "exception";
		}
	}

	public boolean signatureIsValid(String message, String signature) throws Exception {
		byte[] encode = Hex.decodeHex(new String(Base64.decodeBase64(signature.getBytes())).toCharArray());

		SecretKeySpec key = new SecretKeySpec(this.key.getBytes(), "HmacSHA1");
		Mac mac = Mac.getInstance(key.getAlgorithm());

		mac.init(key);

		byte[] myEncode = mac.doFinal(message.getBytes());
		
		logger.debug("given encode: "+new String(encode)+" my-encode: "+new String(myEncode));

		if (encode.length != myEncode.length)
			return false;

		for (int i = 0; i < encode.length; i++)
			if (encode[i] != myEncode[i])
				return false;
		return true;
	}

	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletRequest getServletRequest() {
		return request;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getValid() {
		return valid;
	}

	public void setValid(String valid) {
		this.valid = valid;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
