package eu.dnetlib.validator.admin.config;


public class Constants {
	
	public static String loggedInField = "LOGGED_IN_FIELD";

	public static final String MODE_LOCAL = "local";
	public static final String MODE_DNET = "dnet";
	public static final String MODE_LDAP = "ldap";

}
