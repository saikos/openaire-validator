package eu.dnetlib.validator.admin.actions.rulesets;

import java.util.Set;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Action;

import eu.dnetlib.domain.functionality.validator.RuleSet;
import eu.dnetlib.validator.admin.actions.BaseValidatorAction;

public class CreateRuleSet extends BaseValidatorAction {

	private static final long serialVersionUID = 5623461974078831740L;
	
	private Set<Integer> chosenContentRules = null;
	private Set<Integer> chosenUsageRules = null;	
	private String funct = null;
	private RuleSet ruleSet;
	private static final Logger logger = Logger.getLogger(CreateRuleSet.class);
	
	public String execute() {
		this.clearErrorsAndMessages();
		try {
			logger.debug("creating a new rule set "+ruleSet.getName());
			ruleSet.setUsageRulesIds(chosenUsageRules);
			ruleSet.setContentRulesIds(chosenContentRules);
			if(funct.equals("create")) {
				this.getValidatorAdminAPI().storeRuleSet(ruleSet, false);
				this.addActionMessage(this.getText("createRuleSet.success"));
			}
			else if(funct.equals("edit")) {
				logger.debug("chosen content: " + chosenContentRules);
				this.getValidatorAdminAPI().storeRuleSet(ruleSet, true);
				this.addActionMessage(this.getText("createRuleSet.success"));
			}
			return Action.SUCCESS;
		}
		catch(Exception e) {
			logger.error("Error creating/editing ruleset", e);
			this.addActionError(this.getText("generic.error"));
			reportException(e);
			return "exception";
		}
	}
	
	public void validate() {
		this.clearErrors();
		if((chosenContentRules == null || chosenContentRules.isEmpty())||(chosenUsageRules == null || chosenUsageRules.isEmpty()))
			this.addActionError(this.getText("createRuleSets.atLeastOneRule"));
		if(funct == null)
			this.addActionError(this.getText("uknownError"));
	}
	
	public String getFunct() {
		return funct;
	}
	public void setFunct(String funct) {
		this.funct = funct;
	}	

	public Set<Integer> getChosenContentRules() {
		return chosenContentRules;
	}

	public void setChosenContentRules(Set<Integer> chosenContentRules) {
		this.chosenContentRules = chosenContentRules;
	}

	public Set<Integer> getChosenUsageRules() {
		return chosenUsageRules;
	}

	public void setChosenUsageRules(Set<Integer> chosenUsageRules) {
		this.chosenUsageRules = chosenUsageRules;
	}

	public RuleSet getRuleSet() {
		return ruleSet;
	}

	public void setRuleSet(RuleSet ruleSet) {
		this.ruleSet = ruleSet;
	}
	
	
}	
