package eu.dnetlib.validator.admin.config;

import java.util.Properties;

import javax.servlet.ServletContext;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;

public class ValidatorAdminContextLoaderListener extends ContextLoaderListener {
	private static Logger logger = Logger.getLogger(ValidatorAdminContextLoaderListener.class);


	public ValidatorAdminContextLoaderListener() {
		super();
	}

	public ValidatorAdminContextLoaderListener(WebApplicationContext context) {
		super(context);
	}

	@Override
	protected WebApplicationContext createWebApplicationContext(
			ServletContext servletContext)
			throws BeansException {
		logger.debug("Creating web application context");
		Properties props = this.loadProperties();
		String repoMode = props.getProperty("services.validator.mode.repo");
		String userMode = props.getProperty("services.validator.mode.user");
		Boolean standaloneMode = Boolean.parseBoolean(props.getProperty("services.validator.mode.standalone"));
		
		logger.debug("User mode: " + userMode);
		logger.debug("Repo mode: " + repoMode);
		logger.debug("Standalone mode: " + standaloneMode);
//		logger.debug("Dnet workflow enabled: " + repoMode);
		XmlWebApplicationContext ctx = new XmlWebApplicationContext();

		ctx.setServletContext(servletContext);
		
		
		String[] springContextCore = new String[] {
				"classpath*:/eu/dnetlib/validator/admin/actions/springContext-validator-struts.xml",
				"classpath*:/eu/dnetlib/validator/admin/api/springContext-validator-user-" + userMode + ".xml",
				"classpath*:/eu/dnetlib/validator/admin/api/springContext-validator.xml",
				"classpath*:/eu/dnetlib/validator/admin/config/springContext-validator-config.xml",
				"classpath*:/eu/dnetlib/validator/commons/dao/springContext-*.xml",
				"classpath*:/eu/dnetlib/validator/commons/email/springContext-*.xml"
		};
		
		String[] springContextForIS = new String[] {
		};
		
		if (standaloneMode) {
			logger.debug("Loading contexts for standalone mode");
			ctx.setConfigLocations(springContextCore);
		} else {
			logger.debug("Loading contexts for dnet");
			ctx.setConfigLocations((String[])ArrayUtils.addAll(springContextCore,springContextForIS));
		}
		
		ctx.refresh();
		
		logger.debug("done");
		
		return ctx;
	}	
	
	private Properties loadProperties() {
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(new String[] {
				"classpath*:/eu/dnetlib/validator/admin/config/springContext-validator-config.xml"
		});
		
		CascadingPropertyLoader pLoader = (CascadingPropertyLoader) ctx.getBean("propertyLoader");
		Properties props = pLoader.getProperties();
		
		ctx.destroy();
		ctx.close();
		return props;
	}

}
