package eu.dnetlib.validator.admin.api;

import java.util.List;

import eu.dnetlib.domain.functionality.validator.Rule;
import eu.dnetlib.domain.functionality.validator.RuleSet;
import eu.dnetlib.validator.admin.actions.rules.FieldPair;

/**
 * 
 * @author Nikon Gasparis
 * 
 */
public interface ValidatorAdminAPI {

	public List<Rule> getAllRules() throws  ValidatorAdminException;

	public List<FieldPair> getRuleValuePairs(Rule rule) throws  ValidatorAdminException;
	
	public Rule getRule(int ruleId) throws ValidatorAdminException;
	
	public List<String> getRuleLabels(String ruleType, Boolean forUsage, Boolean forCris) throws ValidatorAdminException;
	
	public String storeRule(Rule rule, String[] inputs, Boolean updateExisting) throws ValidatorAdminException;
	
	public void storeRuleSet(RuleSet ruleSet, boolean updateExisting) throws ValidatorAdminException;	
	
	public void deleteRuleSet(int setId) throws  ValidatorAdminException;
	
	public String deleteRule(int ruleId) throws  ValidatorAdminException;

	public List<RuleSet> getRuleSets() throws  ValidatorAdminException;

	public RuleSet getRuleSet(int setId) throws  ValidatorAdminException;

	public boolean userIsSecondaryAdmin(String user)
			throws ValidatorAdminException;
	
	public boolean userIsMasterAdmin(String user)
			throws ValidatorAdminException;
			
	public int deleteOldJobs(String date, String period, String target) throws ValidatorAdminException;

	public String sendRegistrationsReport(String dateFrom, String dateTo, String month, String year) throws ValidatorAdminException;
	

//	public List<String> getConstantRuleTypes() throws  AdminAPIException;
	
//	public String[] getJobTypesAsArray() throws  AdminAPIException;


	/*
	public String getRuleSetName(int setId) throws  AdminAPIException;
	

	

	public List<String> getRuleSetRuleIdsByJobType(int setId, String jobType)
			throws  AdminAPIException;

	public List<RuleS> getAllRulesForPresentationByJobType(String jobType)
			throws  AdminAPIException;

	public List<RuleS> getAllRulesForPresentationByJobTypeByRuleSet(
			List<String> ruleSetRuleIds, String jobType)
			throws  AdminAPIException;

	public List<RuleD> getRulesForJobValidationByJobType(String jobType)
			throws  AdminAPIException;

	public List<RuleD> getRulesForJobValidationByJobTypeByRuleSet(int setId,
			String jobType) throws  AdminAPIException;

	public List<RuleStored> getAllRulesByJobType(String jobType)
			throws  AdminAPIException;

	public List<RulePair> getAllRulesToRulePair() throws  AdminAPIException;

	public Map<String, List<JobSubmitted>> getJobsOfUser(String userName)
			throws  AdminAPIException;



	public Properties getRuleProperties(String ruleId)
			throws  AdminAPIException;



	public Serializable getRuleClassInstanceByType(String type, Properties pros,int id) throws  AdminAPIException;

	public List<String> getAllRuleIdsByJobType(String jobType)
			throws  AdminAPIException;

	public List<RuleD> convertRuleIdsListToRuleDList(List<String> ruleIds);

	public List<String> getValidationErrors(int jobId, int ruleId)
			throws  AdminAPIException;

	public JobSubmitted getJobSubmitted(String jobId) throws  AdminAPIException;

	public List<TaskStored> getJobTasks(String jobId, JobSubmitted job)
			throws  AdminAPIException;

	public List<Entry> getJobSummary(String jobId, String groupBy) throws AdminAPIException;


	public Map<String, String> getRuleSetsMap() throws AdminAPIException;


	public void restore();

	public List<String> getDistinctGroupByValues(String jobId) throws  AdminAPIException;

	public String getFilteredScore(String jobId, String groupBy) throws  AdminAPIException;

	public void preRegistrationValidations(JobForRegistration job) throws AdminAPIException;

	public void storeJobForRegistration(JobForRegistration job) throws AdminAPIException;

	public JobForRegistration getJobForRegistration(String activationId) throws AdminAPIException;

	public void deleteJobForRegistration(String activationId) throws AdminAPIException;

	public void cleanUncompletedJobs() throws AdminAPIException;

	public Map<String, Set<Integer>> prepareCrisJobs(Set<Integer> chosenContentRules, Boolean referential, String selectedEntities) throws AdminAPIException;

	public Map<String, String> getRuleSetsMap(String deployEnvironment)
			throws AdminAPIException;

	public Boolean isRepoAdmin(String user) throws AdminAPIException;
	
	*
	*/

}