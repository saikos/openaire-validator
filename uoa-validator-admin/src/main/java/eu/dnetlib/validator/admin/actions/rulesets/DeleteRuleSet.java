package eu.dnetlib.validator.admin.actions.rulesets;


import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Action;

import eu.dnetlib.validator.admin.actions.BaseValidatorAction;

public class DeleteRuleSet extends BaseValidatorAction {

	private static final long serialVersionUID = 4858147485197818532L;
	private int setId;
	private static final Logger logger = Logger.getLogger(DeleteRuleSet.class);
		
	public int getSetId() {
		return setId;
	}

	public void setSetId(int setId) {
		this.setId = setId;
	}

	public String execute() {
		this.clearErrorsAndMessages();
		try {
			logger.debug("deleting rule set "+setId);
			this.getValidatorAdminAPI().deleteRuleSet(setId);
			this.addActionMessage(this.getText("deleteRuleSet.success"));
			return Action.SUCCESS;
		}
		catch(Exception e) {
			logger.error("Error deleting ruleset", e);
			this.addActionError(this.getText("generic.error"));
			reportException(e);
			return "exception";
		}
	}
}