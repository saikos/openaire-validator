package eu.dnetlib.validator.admin.actions.login;

import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.Action;

import eu.dnetlib.validator.admin.actions.BaseValidatorAction;
import eu.dnetlib.validator.admin.config.Constants;

public class Login extends BaseValidatorAction implements SessionAware, ServletResponseAware {

	private static final long serialVersionUID = -6021271413570776229L;
	private String email_username, password_login;
	private boolean rememberme;
	private Map<String, Object> session;
	private HttpServletResponse response;

	private String url;
	private String params;

	private transient Logger logger = Logger.getLogger(Login.class);

	public String execute() {
		this.clearErrorsAndMessages();
		try {
			if(this.email_username == null || this.email_username.trim().equals(""))
				return "login";	
			logger.debug("logging-in user " + this.getEmail());
			session.put(Constants.loggedInField, this.getEmail());
			session.put("email", this.getEmail());
			if (this.getUserAPI().isAdmin(email_username) || this.getValidatorAdminAPI().userIsMasterAdmin(email_username))
				session.put("isAdmin", "true");
			if (this.getValidatorAdminAPI().userIsMasterAdmin(email_username))
				session.put("isAdmin", "true");
			if (this.getValidatorAdminAPI().userIsSecondaryAdmin(email_username))
				session.put("isAdmin", "true");
			this.addActionMessage("Logged-in successfully");

			if (url != null && (url.equals("/go2login") || url.equals("/Login") || url.equals("/portalLogin"))) {
				url = "";
				return "welcome";
			}

			return Action.SUCCESS;
		} catch (Exception e) {
			logger.error("error logging-in user " + this.getEmail(), e);
			this.addActionError(this.getText("generic.error"));
			reportException(e);
			return "exception";
		}
	}

	public void validate() {
		this.clearErrors();
		try {
			if(this.email_username == null || this.email_username.trim().equals(""))
				return;
			
			
			String username = this.email_username;

			Pattern rfc2822 = Pattern.compile("^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");
			if (!rfc2822.matcher(this.getEmail().trim().toLowerCase()).matches()) {
				logger.debug("user logged in using username");
				this.email_username = this.getUserAPI().getEmailFromUsername(this.getEmail());
			}

			if (this.email_username == null) {
				this.email_username = username;
				this.addActionError(this.getText("login.userNotExists"));
				return;
			}

			if (!this.getUserAPI().userExists(this.getEmail())) {
				this.email_username = username;
				this.addActionError(this.getText("login.userNotExists"));
				return;
			}
			if (!this.getUserAPI().isUserActivated(this.getEmail())) {
				this.email_username = username;
				this.addActionError(this.getText("login.notActivated"));
				return;
			}
			if (!this.getUserAPI().correctCreds(this.getEmail(), this.getPassword_login())) {
				this.email_username = username;
				this.addActionError(this.getText("login.incorrectCreds"));
				return;
			}
		} catch (Exception e) {
			logger.error("", e);
			this.addActionError(e.toString());
			reportException(e);
		}
	}


	public String getEmail() {
		return email_username;
	}

	public void setEmail_username(String email_username) {
		this.email_username = email_username;
	}

	public String getPassword_login() {
		return password_login;
	}

	public void setPassword_login(String password_login) {
		this.password_login = password_login;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public void setRememberme(boolean rememberme) {
		this.rememberme = rememberme;
	}

	public boolean isRememberme() {
		return rememberme;
	}

	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
	}

	public HttpServletResponse getServletResponse() {
		return response;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}
}
