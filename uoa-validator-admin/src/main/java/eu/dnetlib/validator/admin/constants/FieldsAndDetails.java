package eu.dnetlib.validator.admin.constants;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import eu.dnetlib.validator.admin.constants.fielddetails.FieldDetails;

public class FieldsAndDetails {

	static private Map<String, List<FieldDetails>> map;
	private static Logger logger = Logger.getLogger(FieldsAndDetails.class);

	static {
		logger.debug("initializing map of FieldsAndDetails");
		map = new HashMap<String, List<FieldDetails>>();
		BufferedReader br = new BufferedReader(
				new InputStreamReader(
						FieldsAndDetails.class
								.getResourceAsStream("/eu/dnetlib/validator/admin/constants/fieldDetails.txt")));
		String line;
		try {
			while ((line = br.readLine()) != null) {
				List<FieldDetails> lfd = new ArrayList<FieldDetails>();
				String tableName = line.trim();
				while ((line = br.readLine()) != null) {
					if (line.trim().equals(""))
						break;
					String[] args = line.split(" ");
					FieldDetails details = new FieldDetails();
					details.setTableName(tableName);
					details.setFieldName(args[0].trim());
					details.setForDet(Boolean.parseBoolean(args[1].trim()
							.toLowerCase()));
					lfd.add(details);
				}
				map.put(tableName.toLowerCase(), lfd);
			}
		} catch (Exception e) {
			logger.error("Error initialiazing map of FieldsAndDetails", e);
		}
	}
	
	static public List<FieldDetails> getFieldsAndDetails(String tablename) {
		logger.debug("getting fielddetails of table "+tablename);
		if(tablename != null) {
			tablename = tablename.toLowerCase();
			List<FieldDetails> ret = map.get(tablename);
			if(ret == null) {
				ret = map.get(TypesAndTableNames.getTableNameOfType(tablename));
			}
			return ret;
		}
		return null;
	}
}
