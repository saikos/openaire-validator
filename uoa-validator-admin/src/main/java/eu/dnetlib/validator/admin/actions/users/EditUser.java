package eu.dnetlib.validator.admin.actions.users;

import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.Action;

import eu.dnetlib.domain.functionality.UserProfile;
import eu.dnetlib.validator.admin.actions.BaseValidatorAction;
import eu.dnetlib.validator.admin.config.Constants;

public class EditUser extends BaseValidatorAction implements SessionAware {

	private static final long serialVersionUID = -362061491263257713L;
	private Logger logger = Logger.getLogger(EditUser.class);

	private UserProfile user;

	private Map<String, Object> session;

	public String execute() {
		this.clearErrorsAndMessages();
		try {
			logger.debug("getting for edit user " + (String) session.get(Constants.loggedInField));
				user = this.getUserAPI().getUser((String) session.get(Constants.loggedInField));
			return Action.SUCCESS;
		} catch (Exception e) {
			logger.error("error getting for edit user " + (String) session.get(Constants.loggedInField), e);
			this.addActionError(this.getText("generic.error"));
			reportException(e);
			return "exception";
		}
	}

	public String edit() {
		this.clearErrorsAndMessages();
		try {
			logger.debug("editing user " + (String) session.get(Constants.loggedInField));
			this.getUserAPI().editUser(user);
			this.addActionMessage(this.getText("editUser.success"));
			return Action.SUCCESS;
		} catch (Exception e) {
			logger.error("error editing user " + (String) session.get(Constants.loggedInField), e);
			this.addActionError(this.getText("generic.error"));
			reportException(e);
			return "exception";
		}

	}


	public UserProfile getUser() {
		return user;
	}

	public void setUser(UserProfile user) {
		this.user = user;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}
}
