package eu.dnetlib.validator.admin.constants;

public class FieldNames {
	
	static public String RULE_ID="id";
	static public String RULE_NAME="name";
	static public String RULE_DESCRIPTION="description";
	static public String RULE_TYPE="type";
	static public String RULE_MANDATORY="mandatory";
	static public String RULE_SUCCESS="success";
	static public String RULE_WEIGHT="weight";
	static public String RULE_PROVIDER_INFORMATION="provider_information";
	static public String RULE_VISIBLE="visible";
	static public String RULE_JOBTYPE="job_type";
	
	static public String RULE_RULESFK="Rules_id";
	
	static public String RULE_VOCABULARY_WORDS="terms";
	static public String RULE_VOCABULARY_WORDS_TYPE="terms_type";
	
	static public String RULE_CRIS_ENTITY="cris_entity";
	
	static public String RULE_CRIS_CLASS_VOCABULARY_SCHEME_ID="scheme_id";
	
	static public String RULE_CRIS_REFERENTIAL_INTEGRITY_ENTITY_ACRONYM = "entity_acronym";
	static public String RULE_CRIS_REFERENTIAL_INTEGRITY_RELATED_ENTITY_ACRONYM = "related_entity_acronym";
	static public String RULE_CRIS_REFERENTIAL_INTEGRITY_RELATED_ENTITY_XPATH = "related_entity_xpath";
	
	static public String RULE_CARDINALITY_LESSTHAN="less_than";
	static public String RULE_CARDINALITY_GREATERTHAN="greater_than";
	
	static public String RULE_REGULAREXPRESSION_REGEXPR ="regular_expression";
	
	static public String RULE_NOTCONNFUSEDFIELDS ="fields";
	
//	static public String JOB_CONTENT="OAI Content Validation";
}
