package eu.dnetlib.validator.admin.actions.rules;

import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Action;

import eu.dnetlib.domain.functionality.validator.Rule;
import eu.dnetlib.validator.admin.actions.BaseValidatorAction;
import eu.dnetlib.validator.admin.api.ValidatorAdminException;
import eu.dnetlib.validator.admin.constants.TypesAndTableNames;

public class PrepareEditRule extends BaseValidatorAction {

	private static final long serialVersionUID = 8825889540939707747L;
	private int ruleId;

	private Rule rule;
	private List<Rule> rules;
	private List<FieldPair> ruleValues;
	private Set<String> entityTypes;
	
	private String funct = null;
	private static final Logger logger = Logger.getLogger(PrepareEditRule.class);


	public String execute() {
		this.clearErrorsAndMessages();
		try {
//			logger.debug("preparing edit of rule");
			entityTypes = TypesAndTableNames.getCrisEntities().keySet();
			logger.debug("preparing edit of rule " + ruleId);
			rule = this.getValidatorAdminAPI().getRule(ruleId);
			this.ruleValues = this.getValidatorAdminAPI().getRuleValuePairs(rule);
			if (rule.getType().equals("ChainRule")) {
				try {
					rules = this.getValidatorAdminAPI().getAllRules();
				} catch (ValidatorAdminException e) {
					return Action.ERROR;
				}
			}			
			logger.debug("ruleType: " + rule.getType());
			rule.setJob_type(TypesAndTableNames.jobTypes.get(rule.getJob_type()));
			return Action.SUCCESS;
		} catch (Exception e) {
			logger.error("preparing edit of rule " + ruleId, e);
			this.addActionError(this.getText("generic.error"));
			reportException(e);
			return "exception";
		}
	}

	
	public Rule getRule() {
		return rule;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}

	public List<Rule> getRules() {
		return rules;
	}

	public void setRules(List<Rule> rules) {
		this.rules = rules;
	}

	public List<FieldPair> getRuleValues() {
		return ruleValues;
	}

	public void setRuleValues(List<FieldPair> ruleValues) {
		this.ruleValues = ruleValues;
	}

	public int getRuleId() {
		return ruleId;
	}

	public void setRuleId(int ruleId) {
		this.ruleId = ruleId;
	}


	public String getFunct() {
		return funct;
	}


	public void setFunct(String funct) {
		this.funct = funct;
	}


	public Set<String> getEntityTypes() {
		return entityTypes;
	}


	public void setEntityTypes(Set<String> entityTypes) {
		this.entityTypes = entityTypes;
	}
	
	
}