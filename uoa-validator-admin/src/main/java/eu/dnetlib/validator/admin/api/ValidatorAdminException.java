package eu.dnetlib.validator.admin.api;

public class ValidatorAdminException extends Exception {

	/**
	 * 
	 * @author Nikon Gasparis
	 *
	 */
	
	private static final long serialVersionUID = -80175916822821147L;

	public ValidatorAdminException() {
		super();
	}
	
	public  ValidatorAdminException(String message) {
		super(message);
	}

	public  ValidatorAdminException(String message, Throwable cause) {
		super(message, cause);
	}

	public  ValidatorAdminException(Throwable cause) {
		super(cause);
	}
}

