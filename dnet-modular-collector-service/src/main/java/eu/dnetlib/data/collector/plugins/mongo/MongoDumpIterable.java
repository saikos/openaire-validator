package eu.dnetlib.data.collector.plugins.mongo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;

import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.information.collectionservice.rmi.CollectionServiceException;

/**
 * The Class MongoDumpIterable.
 */
public class MongoDumpIterable implements Iterable<String> {

	/** The input stream. */
	private final FileReader inputStream;

	/**
	 * Instantiates a new mongo dump iterable.
	 *
	 * @param inputFile the input file
	 * @throws CollectionServiceException the collection service exception
	 */
	public MongoDumpIterable(final File inputFile) throws CollectorServiceException {
		try {
			this.inputStream = new FileReader(inputFile);
		} catch (FileNotFoundException e) {
			throw new CollectorServiceException("Error unable to open inputStream", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<String> iterator() {
		return new MongoDumpIterator(inputStream);
	}

}
