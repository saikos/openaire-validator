package eu.dnetlib.data.collector.plugins.oai;

import java.util.Iterator;

import org.springframework.beans.factory.annotation.Required;

import eu.dnetlib.data.collector.plugins.HttpConnector;

public class OaiIteratorFactory {

	private HttpConnector httpConnector;

	public Iterator<String> newIterator(final String baseUrl, final String mdFormat, final String set, final String fromDate, final String untilDate) {
		return new OaiIterator(baseUrl, mdFormat, set, fromDate, untilDate, httpConnector);
	}

	public HttpConnector getHttpConnector() {
		return httpConnector;
	}

	@Required
	public void setHttpConnector(HttpConnector httpConnector) {
		this.httpConnector = httpConnector;
	}
}
