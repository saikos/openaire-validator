package eu.dnetlib.data.collector.plugins.oaisets;

import java.util.Iterator;

import org.springframework.beans.factory.annotation.Required;

import eu.dnetlib.data.collector.plugin.AbstractCollectorPlugin;
import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;

public class OaiSetsCollectorPlugin extends AbstractCollectorPlugin {

	private OaiSetsIteratorFactory oaiSetsIteratorFactory;

	@Override
	public Iterable<String> collect(final InterfaceDescriptor interfaceDescriptor, final String fromDate, final String untilDate)
			throws CollectorServiceException {
		final String baseUrl = interfaceDescriptor.getBaseUrl();

		if (baseUrl == null || baseUrl.isEmpty()) { throw new CollectorServiceException("Param 'baseurl' is null or empty"); }

		return new Iterable<String>() {

			@Override
			public Iterator<String> iterator() {
				return oaiSetsIteratorFactory.newIterator(baseUrl);
			}
		};
	}

	public OaiSetsIteratorFactory getOaiSetsIteratorFactory() {
		return oaiSetsIteratorFactory;
	}

	@Required
	public void setOaiSetsIteratorFactory(final OaiSetsIteratorFactory oaiSetsIteratorFactory) {
		this.oaiSetsIteratorFactory = oaiSetsIteratorFactory;
	}

}
