package eu.dnetlib.data.collector.plugins;

import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class HttpCollectorPlugin extends AbstractSplittedRecordPlugin {

	@Override
	protected BufferedInputStream getBufferedInputStream(final String baseUrl) throws CollectorServiceException {
		final HttpGet method = new HttpGet(baseUrl);

		try(CloseableHttpResponse response = HttpClients.createDefault().execute(method)) {

			int responseCode = response.getStatusLine().getStatusCode();

			if (HttpStatus.SC_OK != responseCode) {
				throw new CollectorServiceException("Error " + responseCode + " dowloading url: " + baseUrl);
			}

			byte[] content = IOUtils.toByteArray(response.getEntity().getContent());

			try(InputStream in = new ByteArrayInputStream(content)) {
				return new BufferedInputStream(in);
			}
		} catch (IOException e) {
			throw new CollectorServiceException("Error dowloading url: " + baseUrl);
		}
	}

}
