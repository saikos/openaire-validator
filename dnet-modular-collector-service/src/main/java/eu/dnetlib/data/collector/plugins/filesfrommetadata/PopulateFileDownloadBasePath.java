package eu.dnetlib.data.collector.plugins.filesfrommetadata;

import java.util.List;
import java.util.Map;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import eu.dnetlib.data.collector.functions.ParamValuesFunction;
import eu.dnetlib.data.collector.rmi.ProtocolParameterValue;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpService;
import eu.dnetlib.enabling.locators.UniqueServiceLocator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

/**
 * Created by alessia on 17/12/15.
 */
public class PopulateFileDownloadBasePath implements ParamValuesFunction {

	private static final Log log = LogFactory.getLog(PopulateFileDownloadBasePath.class);
	@Autowired
	private UniqueServiceLocator serviceLocator;

	@Value("${services.objectstore.basePathList.xquery}")
	private String xQueryForObjectStoreBasePath;

	@Override
	public List<ProtocolParameterValue> findValues(final String s, final Map<String, String> map) {
		try {
			return Lists.transform(serviceLocator.getService(ISLookUpService.class).quickSearchProfile(xQueryForObjectStoreBasePath),
					new Function<String, ProtocolParameterValue>() {
						@Override
						public ProtocolParameterValue apply(final String s) {
							return new ProtocolParameterValue(s, s);
						}
					});
		} catch (ISLookUpException e) {
			log.error("Cannot read Object store service properties", e);
		}
		return Lists.newArrayList();
	}

	public UniqueServiceLocator getServiceLocator() {
		return serviceLocator;
	}

	public void setServiceLocator(final UniqueServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	public String getxQueryForObjectStoreBasePath() {
		return xQueryForObjectStoreBasePath;
	}

	public void setxQueryForObjectStoreBasePath(final String xQueryForObjectStoreBasePath) {
		this.xQueryForObjectStoreBasePath = xQueryForObjectStoreBasePath;
	}
}
