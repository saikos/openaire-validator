package eu.dnetlib.data.collector.plugins.filesystem;

import eu.dnetlib.data.collector.plugin.AbstractCollectorPlugin;
import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;

/**
 *
 * @author andrea
 *
 */
public class FilesystemCollectorPlugin extends AbstractCollectorPlugin {

	@Override
	public Iterable<String> collect(final InterfaceDescriptor interfaceDescriptor, final String fromDate, final String untilDate)
			throws CollectorServiceException {

		final String baseUrl = interfaceDescriptor.getBaseUrl();
		if (baseUrl == null || baseUrl.isEmpty()) { throw new CollectorServiceException("Param 'baseurl' is null or empty"); }
		return new FilesystemIterable(interfaceDescriptor);
	}

}
