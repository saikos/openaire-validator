package eu.dnetlib.data.collector.plugins;

import java.io.BufferedInputStream;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;

import eu.dnetlib.data.collector.plugin.AbstractCollectorPlugin;
import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;
import eu.dnetlib.miscutils.iterators.xml.XMLIterator;

public abstract class AbstractSplittedRecordPlugin extends AbstractCollectorPlugin {

	@Override
	public Iterable<String> collect(final InterfaceDescriptor interfaceDescriptor, final String fromDate, final String untilDate)
			throws CollectorServiceException {
		final String baseUrl = interfaceDescriptor.getBaseUrl();
		final String element = interfaceDescriptor.getParams().get("splitOnElement");

		if (StringUtils.isBlank(baseUrl)) { throw new CollectorServiceException("Param 'baseurl' is null or empty"); }

		if (StringUtils.isBlank(element)) { throw new CollectorServiceException("Param 'splitOnElement' is null or empty"); }

		final BufferedInputStream bis = getBufferedInputStream(baseUrl);

		return new Iterable<String>() {

			@Override
			public Iterator<String> iterator() {
				return new XMLIterator(element, bis);
			}
		};
	}

	abstract protected BufferedInputStream getBufferedInputStream(final String baseUrl) throws CollectorServiceException;

}
