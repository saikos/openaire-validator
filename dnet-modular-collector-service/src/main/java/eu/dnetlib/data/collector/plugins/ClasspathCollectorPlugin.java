package eu.dnetlib.data.collector.plugins;

import java.io.BufferedInputStream;
import java.net.URL;

import eu.dnetlib.data.collector.rmi.CollectorServiceException;

public class ClasspathCollectorPlugin extends AbstractSplittedRecordPlugin {

	@Override
	protected BufferedInputStream getBufferedInputStream(final String baseUrl) throws CollectorServiceException {
		try {
			return new BufferedInputStream(getClass().getResourceAsStream(new URL(baseUrl).getPath()));
		} catch (Exception e) {
			throw new CollectorServiceException("Error dowloading url: " + baseUrl);
		}
	}

}
