package eu.dnetlib.data.collector.plugins.mongo;

import java.io.File;

import eu.dnetlib.data.collector.plugin.AbstractCollectorPlugin;
import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;

public class MongoDumpPlugin extends AbstractCollectorPlugin {

	@Override
	public Iterable<String> collect(final InterfaceDescriptor interfaceDescriptor, final String fromDate, final String untilDate)
			throws CollectorServiceException {
		final String baseUrl = interfaceDescriptor.getBaseUrl();
		if (baseUrl == null || baseUrl.isEmpty()) { throw new CollectorServiceException("Param 'baseurl' is null or empty"); }
		final File f = new File(baseUrl);
		if (f.exists() == false) { throw new CollectorServiceException("the file at url " + baseUrl + " does not exists"); }

		return new MongoDumpIterable(f);

	}

}
