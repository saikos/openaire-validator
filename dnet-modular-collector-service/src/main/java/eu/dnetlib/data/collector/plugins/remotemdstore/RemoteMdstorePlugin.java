package eu.dnetlib.data.collector.plugins.remotemdstore;

import org.apache.commons.lang3.StringUtils;

import eu.dnetlib.data.collector.plugin.AbstractCollectorPlugin;
import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;

public class RemoteMdstorePlugin extends AbstractCollectorPlugin {

	// URL Example: mongodb://[username:password@]host[:port]

	@Override
	public Iterable<String> collect(final InterfaceDescriptor interfaceDescriptor, final String fromDate, final String untilDate)
			throws CollectorServiceException {

		final String baseUrl = interfaceDescriptor.getBaseUrl();
		final String dbName = interfaceDescriptor.getParams().get("remote_database");
		final String mdId = interfaceDescriptor.getParams().get("remote_mdstore_id");

		if (StringUtils.isBlank(baseUrl)) { throw new CollectorServiceException("Invalid baseUrl, example: mongodb://[username:password@]host[:port]"); }
		if (StringUtils.isBlank(dbName)) { throw new CollectorServiceException("Empty parameter: remote_database"); }
		if (StringUtils.isBlank(mdId)) { throw new CollectorServiceException("Empty parameter: remote_mdstore_id"); }

		return () -> {
			try {
				return new RemoteMdStoreIterator(baseUrl, dbName, mdId);
			} catch (final CollectorServiceException e) {
				throw new RuntimeException(e);
			}
		};
	}
}
