package eu.dnetlib.data.collector.plugins.oaisets;

import java.util.Iterator;

import org.springframework.beans.factory.annotation.Required;

import eu.dnetlib.data.collector.plugins.HttpConnector;

public class OaiSetsIteratorFactory {
	
	private HttpConnector httpConnector;

	public Iterator<String> newIterator(String baseUrl) {
		return new OaiSetsIterator(baseUrl, httpConnector);
	}
	
	public HttpConnector getHttpConnector() {
		return httpConnector;
	}

	@Required
	public void setHttpConnector(HttpConnector httpConnector) {
		this.httpConnector = httpConnector;
	}

}
