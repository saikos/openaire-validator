package eu.dnetlib.data.collector.plugins.ftp;

import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author Author: Andrea Mannocci
 *
 */
public class FtpIteratorFactory {

	public Iterator<String> newIterator(final String baseUrl,
										final String username,
										final String password,
										final boolean isRecursive,
										final Set<String> extensionsSet, final String fromDate) {
		return new FtpIterator(baseUrl, username, password, isRecursive, extensionsSet, fromDate);
	}
}
