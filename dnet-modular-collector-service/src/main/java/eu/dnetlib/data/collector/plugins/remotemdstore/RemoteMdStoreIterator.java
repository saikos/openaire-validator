package eu.dnetlib.data.collector.plugins.remotemdstore;

import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.Document;

import com.google.common.collect.Iterables;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import eu.dnetlib.data.collector.rmi.CollectorServiceException;

public class RemoteMdStoreIterator implements Iterator<String> {

	private static final String COLL_METADATA_MANAGER = "metadataManager";

	private static final Log log = LogFactory.getLog(RemoteMdStoreIterator.class);

	private final MongoClient client;
	private final MongoDatabase db;
	private final Iterator<String> innerIterator;

	public RemoteMdStoreIterator(final String baseUrl, final String dbName, final String mdId) throws CollectorServiceException {
		this.client = new MongoClient(new MongoClientURI(baseUrl));
		this.db = getDb(client, dbName);
		this.innerIterator = getMdstoreColl(db, mdId).find().map(doc -> doc.getString("body")).iterator();
	}

	@Override
	public boolean hasNext() {
		return innerIterator.hasNext();
	}

	@Override
	public String next() {
		try {
			return innerIterator.next();
		} finally {
			if (!innerIterator.hasNext()) {
				log.info("Mongo Client correctly closed");
				client.close();
			}
		}
	}

	private MongoDatabase getDb(final MongoClient client, final String dbName) throws CollectorServiceException {
		if (!Iterables.contains(client.listDatabaseNames(), dbName)) {
			final String err = String.format("Database '%s' not found in %s", dbName, client.getAddress());
			log.warn(err);
			throw new CollectorServiceException(err);
		}
		return client.getDatabase(dbName);
	}

	private MongoCollection<Document> getColl(final MongoDatabase db, final String collName) throws CollectorServiceException {
		if (!Iterables.contains(db.listCollectionNames(), collName)) {
			final String err = String.format(String.format("Missing collection '%s' in database '%s'", collName, db.getName()));
			log.warn(err);
			throw new CollectorServiceException(err);
		}
		return db.getCollection(collName);
	}

	private MongoCollection<Document> getMdstoreColl(final MongoDatabase db, final String mdId)
			throws CollectorServiceException {

		final MongoCollection<Document> collManager = getColl(db, COLL_METADATA_MANAGER);

		for (final Document entry : collManager.find()) {
			if (entry.getString("mdId").equals(mdId)
					&& StringUtils.isNotBlank(entry.getString("currentId"))) { return getColl(db, entry.getString("currentId")); }
		}
		throw new CollectorServiceException("Mdstore not found in " + COLL_METADATA_MANAGER);
	}

}
