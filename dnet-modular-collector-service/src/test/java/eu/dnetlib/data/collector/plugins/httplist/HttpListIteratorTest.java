package eu.dnetlib.data.collector.plugins.httplist;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import eu.dnetlib.data.collector.plugins.HttpConnector;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class HttpListIteratorTest {

	private HttpConnector httpConnector;
	// Under test
	private Iterator<String> iter;

	@Before
	public void setUp() throws Exception {
		httpConnector = new HttpConnector();
		iter = new HttpListIterator("http://www.dlib.org/", "http://www.dlib.org/metadata/dlib_meta_files.txt", httpConnector);
	}

	@Test
	@Ignore
	public void testHttpListIterator() throws IOException {
		FileUtils.forceMkdir(new File("/tmp/dlibmagazine"));

		int i = 0;
		while (iter.hasNext()) {
			final String file = "/tmp/dlibmagazine/" + i++ + ".xml";
			final FileWriter fw = new FileWriter(file);
			System.out.println("Download " + file);
			fw.append(iter.next());
			fw.flush();
			fw.close();
		}
		System.out.println("* END *");
	}
}
