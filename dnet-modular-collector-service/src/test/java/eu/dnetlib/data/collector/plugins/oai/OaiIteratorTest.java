package eu.dnetlib.data.collector.plugins.oai;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import eu.dnetlib.data.collector.plugins.HttpConnector;

public class OaiIteratorTest {
	
	private static final String BASE_URL = "http://oai.d.efg.research-infrastructures.eu/oai.do";
	private static final String FORMAT = "oai_dc";
	private static final String SET = "d937bab1-d44c-44aa-bf7d-df5312a3b623";
	
	private OaiIterator oai;
	
	@Before
	public void setUp() {
		HttpConnector httpConnector = new HttpConnector();
		httpConnector.initTrustManager();
		oai = new OaiIterator(BASE_URL, FORMAT, SET, null, null, httpConnector);
	}
	
	@Test
	@Ignore
	public void test() {
		int count = 0;
		while (oai.hasNext()) {
			oai.next();
			count++;
		}
		System.out.println("TOTAL: " + count);
	}
}
