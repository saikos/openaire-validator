package eu.dnetlib.data.collector.plugins.oai;

import java.util.HashMap;

import eu.dnetlib.data.collector.plugins.HttpConnector;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class OaiCollectorPluginRealTest {

	private OaiCollectorPlugin oai;

	private static final String BASE_URL = "http://oai.d.efg.research-infrastructures.eu/oai.do";
	private static final String FORMAT = "oai_dc";
	private static final String SETS = "d937bab1-d44c-44aa-bf7d-df5312a3b623, e5b14959-1e87-4c07-9f85-942c9cdd9136, 13302eb6-764a-4ed2-8d08-2a1c9526f442, 31701e97-096f-4266-81b5-30b9bc3a06b0";

	@Before
	public void setUp() {
		oai = new OaiCollectorPlugin();
		HttpConnector connector = new HttpConnector();
		OaiIteratorFactory oif = new OaiIteratorFactory();
		oif.setHttpConnector(connector);
		oai.setOaiIteratorFactory(oif);
	}

	@Test
	@Ignore
	public void testCollect() throws Exception {
		final InterfaceDescriptor iface = new InterfaceDescriptor();
		iface.setId("123");
		iface.setProtocol("OAI");
		iface.setBaseUrl(BASE_URL);
		iface.setParams(new HashMap<String, String>());
		iface.getParams().put("format", FORMAT);
		iface.getParams().put("set", SETS);

		int count = 0;
		for (String s : oai.collect(iface, null, null)) {
			System.out.println(s);
			count++;
		}
		System.out.println("TOTAL: " + count);
	}

}
