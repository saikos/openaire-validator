package eu.dnetlib.data.collector.plugins.archives.zip;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import junit.framework.Assert;

import org.junit.Ignore;
import org.junit.Test;

import eu.dnetlib.data.collector.plugins.archive.zip.ZipIterator;

@Ignore
public class ZipIteratorTest {

	@Ignore
	@Test
	public void test() {
		try {
			int nFiles = 100;
			File f1 = createZipArchive(nFiles);
			iterate(nFiles, f1);
			f1.delete();

			nFiles = 2000;
			File f2 = createZipArchive(nFiles);
			iterate(nFiles, f2);
			f2.delete();

			nFiles = 124569;
			File f3 = createZipArchive(nFiles);
			iterate(nFiles, f3);
			f3.delete();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private File createZipArchive(final int nFiles) throws IOException {
		final StringBuilder sb = new StringBuilder();
		sb.append("Test String");

		File zip = File.createTempFile("testZip", ".zip");
		final ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zip));

		for (int i = 0; i < nFiles; i++) {
			ZipEntry e = new ZipEntry(String.format("%d.txt", i));
			out.putNextEntry(e);

			byte[] data = sb.toString().getBytes();
			out.write(data, 0, data.length);
			out.closeEntry();
		}

		out.close();
		return zip;
	}

	private void iterate(final int nFiles, final File zipFile) {
		ZipIterator testedIterator = new ZipIterator(zipFile);
		int counter = 0;
		while (testedIterator.hasNext()) {
			testedIterator.next();
			counter++;
		}
		Assert.assertEquals(nFiles, counter);
	}
}
