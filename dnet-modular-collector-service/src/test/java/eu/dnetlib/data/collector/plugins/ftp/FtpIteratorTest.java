package eu.dnetlib.data.collector.plugins.ftp;

import java.util.Set;

import com.google.common.collect.Sets;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Ignore
public class FtpIteratorTest {

	private String baseUrl = "ftp://ftp.eagle.research-infrastructures.eu/content/ELTE";
	private String username = "eaglecontent";
	private String password = "$eagl3$CP";
	private boolean isRecursive = true;
	private Set<String> extensions = Sets.newHashSet("xml");

	@Test
	public void test() {
		final FtpIterator iterator = new FtpIterator(baseUrl, username, password, isRecursive, extensions, null);
		int i =5;
		while (iterator.hasNext() && i > 0) {
			iterator.next();
			i--;
		}
	}

	@Test
	public void testIncremental() {
		final FtpIterator iterator = new FtpIterator(baseUrl, username, password, isRecursive, extensions, "2016-01-04");
		assertTrue(iterator.hasNext());
		int i =5;
		while (iterator.hasNext() && i > 0) {
			iterator.next();
			i--;
		}
	}

	@Test
	public void testIncrementalNoRecords() {
		final FtpIterator iterator = new FtpIterator(baseUrl, username, password, isRecursive, extensions, "2017-01-04");
		assertFalse(iterator.hasNext());

	}

}
