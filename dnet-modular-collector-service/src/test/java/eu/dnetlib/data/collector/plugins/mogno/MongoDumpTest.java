package eu.dnetlib.data.collector.plugins.mogno;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import eu.dnetlib.data.collector.plugins.mongo.MongoDumpPlugin;
import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;

public class MongoDumpTest {

	@Test
	public void test() throws CollectorServiceException, IOException {

		ClassPathResource resource = new ClassPathResource("eu/dnetlib/data/collector/plugins/inputTest.json");

		InterfaceDescriptor id = new InterfaceDescriptor();
		id.setBaseUrl(resource.getFile().getAbsolutePath());
		MongoDumpPlugin plugin = new MongoDumpPlugin();

		int i = 0;
		for (String s : plugin.collect(id, null, null)) {
			Assert.assertNotNull(s);
			i++;
		}
		Assert.assertEquals(i, 10);

	}
}
