package eu.dnetlib.data.collector.plugins;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;

public class FileGZipCollectorPluginTest {

	private Resource gzResource = new ClassPathResource("eu/dnetlib/data/collector/plugins/opendoar.xml.gz");
	private InterfaceDescriptor descr;
	private FileGZipCollectorPlugin plugin;

	@Before
	public void setUp() throws Exception {
		descr = new InterfaceDescriptor();
		String thePath = gzResource.getFile().getAbsolutePath();
		descr.setBaseUrl("file://" + thePath);
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("splitOnElement", "repository");
		descr.setParams(params);
		plugin = new FileGZipCollectorPlugin();
	}

	@Test
	public void test() throws CollectorServiceException {
		int i = 0;
		for (String s : plugin.collect(descr, null, null)) {
			Assert.assertTrue(s.length() > 0);
			i++;
			break;
		}
		Assert.assertTrue(i > 0);
	}

}
