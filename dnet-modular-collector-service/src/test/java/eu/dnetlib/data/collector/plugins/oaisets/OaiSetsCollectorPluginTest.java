package eu.dnetlib.data.collector.plugins.oaisets;

import java.util.HashMap;
import javax.annotation.Resource;

import com.google.common.base.Joiner;
import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "applicationContext-OaiSetsCollectorPluginTest.xml")
public class OaiSetsCollectorPluginTest {

	private OaiSetsCollectorPlugin oaiSetsPlugin;
	@Resource
	OaiSetsIteratorFactory oaiSetsIteratorFactory;

	private static final String BASE_URL = "http://oai.cwi.nl/oai";
	private static final String BASE_URL_DATACITE = "http://oai.datacite.org/oai";

	@Before
	public void setUp() throws Exception {
		oaiSetsPlugin = new OaiSetsCollectorPlugin();
		oaiSetsPlugin.setOaiSetsIteratorFactory(oaiSetsIteratorFactory);
	}

	@Ignore
	@Test
	public void testCollect() throws CollectorServiceException {
		final InterfaceDescriptor iface = new InterfaceDescriptor();
		iface.setId("123");
		iface.setProtocol("oai_set");
		iface.setBaseUrl(BASE_URL);
		iface.setParams(new HashMap<String, String>());

		Iterable<String> sets = oaiSetsPlugin.collect(iface, null, null);
		System.out.println(Joiner.on(",").join(sets));
	}

	@Ignore
	@Test
	public void testCollectDatacite() throws CollectorServiceException {
		final InterfaceDescriptor iface = new InterfaceDescriptor();
		iface.setId("123");
		iface.setProtocol("oai_set");
		iface.setBaseUrl(BASE_URL_DATACITE);
		iface.setParams(new HashMap<String, String>());

		Iterable<String> sets = oaiSetsPlugin.collect(iface, null, null);
		System.out.println(Joiner.on(",").join(sets));
	}
}
