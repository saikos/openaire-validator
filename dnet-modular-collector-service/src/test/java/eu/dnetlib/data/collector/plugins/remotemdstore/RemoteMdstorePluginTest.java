package eu.dnetlib.data.collector.plugins.remotemdstore;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;

public class RemoteMdstorePluginTest {

	private RemoteMdstorePlugin plugin;

	@Before
	public void setUp() throws Exception {
		plugin = new RemoteMdstorePlugin();
	}

	@Test
	@Ignore
	public void testCollect() throws CollectorServiceException {
		final InterfaceDescriptor interfaceDescriptor = new InterfaceDescriptor();
		interfaceDescriptor.setBaseUrl("mongodb://localhost");
		interfaceDescriptor.getParams().put("remote_database", "mdstore_isti");
		interfaceDescriptor.getParams().put("remote_mdstore_id", "f9161d2e-b247-4f47-81c2-70e4cf44e7ce_TURTdG9yZURTUmVzb3VyY2VzL01EU3RvcmVEU1Jlc291cmNlVHlwZQ==");

		int count = 0;
		for (final String s : plugin.collect(interfaceDescriptor, null, null)) {
			count++;
		}
		System.out.println("TOTAL: " + count);

	}

}
