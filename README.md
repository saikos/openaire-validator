# README #

This repo contains the current openaire validator source code, imported from their SVN.

## The dnet-* projects

These repos practically contain poms (maven build files) to be shared by multiple projects (defining common dependencies, settings, etc.).

## uoa-validator-engine

The validator engine, defining the main interfaces that establish their validation API. We are starting with this one --we will rewrite it to make it embeddable and more robust.

## uoa-validator-admin

A web app that allows users to define rules. It offers a struts-based UI. All rule definitions end-up in a postgreSQL db.

## uoa-validator-service

A web app that acts as a service (think of microservices), offering no UI. It practically acts as the runner of the validator jobs, that ulitimately end up populating the postgreSQL db with their results.

## uoa-validator-commons

Common functionality shared by the uoa-validator-* projects, haven't looked much into it.