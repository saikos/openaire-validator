package eu.dnetlib.validator.engine;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

import eu.dnetlib.validator.engine.data.Provider;
import eu.dnetlib.validator.engine.data.Rule;
import eu.dnetlib.validator.engine.execution.Job;
import eu.dnetlib.validator.engine.execution.JobListener;

/**
 * <p>
 * The front-end interface for a Validator.
 * </p>
 * <p>
 * A Validator receives Job execution requests and executes them.
 * </p>
 * <p>
 * It is very important that a Validator is Persistable, i.e., it can be stored
 * in permanent storage and may be retrieved later.
 * </p>
 * 
 * @author Manos Karvounis
 */
public interface Validator {

	/**
	 * Used to store an object to a Registry. Usually these objects are the
	 * rules and providers that are contained in the validator.
	 * 
	 * @param <T>
	 *            The serializable object to be added in the registry.
	 * @param objid
	 *            A unique id for the object.
	 * @param obj
	 *            The object itself.
	 * @param registryName
	 *            The name of the registry. The registry must have been already
	 *            added to the Validator.
	 * @see Provider
	 * @see Rule
	 * @see Validator#addRegistry(String)
	 */
	public <T extends Serializable> void addToRegistry(int objid, T obj,
			String registryName);

	/**
	 * Retrieves a previously stored object from a Registry.
	 * 
	 * @param objid
	 *            The id of the object, as specified when the object was stored
	 *            using
	 *            {@link Validator#addToRegistry(int, Serializable, String)}
	 * @param registryName
	 *            The name of the registry
	 * @return The object
	 * @see Validator#addToRegistry(int, Serializable, String)
	 */
	public Serializable getFromRegistry(int objid, String registryName)
			throws ValidatorException;

	/**
	 * Adds a new registry to the Validator. The registries are used to store
	 * objects the validator needs. Usually they are rules and providers.
	 * 
	 * @param <T>
	 *            The type of objects that the registry will contain.
	 * @param name
	 *            A unique name for the registry.
	 * @see Validator#addToRegistry(int, Serializable, String)
	 */
	public <T extends Serializable> void addRegistry(String name);

	/**
	 * Submit a new validation job to be executed at some time. The submission
	 * must not be blocking. The external application is notified on the
	 * execution progress by registering a listener.
	 * 
	 * @param job
	 * @param workers
	 * @param listener
	 */
	public void submitJob(Job job, int workers, JobListener... listeners)
			throws ValidatorException;

	/**
	 * Submit a new validation job to be executed at some time. The submission
	 * must not be blocking. The external application is notified on the
	 * execution progress by registering a listener.
	 * 
	 * @param job
	 * @param workers
	 * @param listener
	 */

	public void submitJobForCris(Job job,
			Map<String, Set<Rule>> rulesPerEntity,
			Map<String, Set<Rule>> entityChosenRulesMapReferential,
			JobListener... listeners) throws ValidatorException;

	/**
	 * Shuts down the validator. All resources held by the validator are
	 * released. 
	 * 
	 * @throws ValidatorException
	 */
	public void shutdown() throws ValidatorException;

	/**
	 * Starts the validator. This method must be invoked before any other method
	 * is invoked.

	 * @throws ValidatorException
	 */
	public void start() throws ValidatorException;
}
