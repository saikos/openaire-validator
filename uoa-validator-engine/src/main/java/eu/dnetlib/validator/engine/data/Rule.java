package eu.dnetlib.validator.engine.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import eu.dnetlib.validator.engine.execution.ValidationObject;

/**
 * A rule is applied on a Validation Object retrieved by a {@link Provider}
 * @author Manos Karvounis
 * @author Nikon Gasparis
 *
 */
public abstract class Rule extends ConfigurableObject {
	
	private static final long serialVersionUID = 5118099751892948569L;
	
	private Provider provider;
	private String valObjId;
	private List<String> errors;

	public Rule(Properties pros, int id) {
		super(pros, id);
		setErrors(new ArrayList<String>());
	}

	/**
	 * @param obj
	 * @return True if the rule was successful, otherwise false
	 * @throws RuleException If an exception was raised during the application of the rule.
	 */
	
	public abstract boolean apply(ValidationObject obj) throws RuleException;

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}

	public String getValObjId() {
		return valObjId;
	}

	public void setValObjId(String valObjId) {
		this.valObjId = valObjId;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
	
	

}
