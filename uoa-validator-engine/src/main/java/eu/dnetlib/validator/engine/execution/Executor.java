package eu.dnetlib.validator.engine.execution;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;

public interface Executor {

	public Future<?> execute(TaskList ltasks) throws TimeoutException, ExecutionException;
}
