package eu.dnetlib.validator.engine.execution;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import eu.dnetlib.validator.engine.data.Rule;

/**
 * <p>
 * Represents a validation job. A validation job consists of a <i>single</i>
 * provider and a set of rules that will be applied on the validation objects
 * retrieved by the provider.
 * </p>
 * <p>
 * Please note that to execute a validation, you might need to submit multiple
 * jobs, containing various rules and providers.
 * </p>
 * 
 * @author Manos Karvounis
 * 
 */
public class Job implements Serializable {

	private static final long serialVersionUID = -8331921926659185511L;

	public final int providerId;
	public final Properties providerProps;
	public final Set<Rule> rules;
	public final int id;

	public Job(int id, int providerId, Set<Rule> rules, Properties providerProps) {
		super();
		this.id = id;
		this.providerId = providerId;
		this.rules = new HashSet<Rule>();
		this.rules.addAll(rules);
		this.providerProps = new Properties();
		this.providerProps.putAll(providerProps);
	}

}
