package eu.dnetlib.validator.engine.data;

import eu.dnetlib.validator.engine.ValidatorException;

public class DataException extends ValidatorException {

	private static final long serialVersionUID = -4341747976225287737L;

	
	public DataException() {
		super();
	}
	
	public DataException(String msg) {
		super(msg);
	}
}
