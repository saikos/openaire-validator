package eu.dnetlib.validator.engine.data;

import eu.dnetlib.validator.engine.ValidatorException;

public class RuleException extends ValidatorException {

	private static final long serialVersionUID = 7060357707549236243L;

	public RuleException() {
		super();
	}

	public RuleException(String msg) {
		super(msg);
	}
}
