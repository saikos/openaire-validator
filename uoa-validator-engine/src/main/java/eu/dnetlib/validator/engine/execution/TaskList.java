package eu.dnetlib.validator.engine.execution;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A List of Tasks to be executed.
 * @author Manos Karvounis
 * @author Nikon Gasparis
 * @see Task
 */
public class TaskList implements Runnable {

	public final List<Task> tasks;

	private List<CompletedTask> ctasks;

	public TaskList(List<Task> tasks) {
		super();
		this.tasks = tasks;
		ctasks = new ArrayList<CompletedTask>();
	}

	@Override
	public void run() {
		for (Task task : tasks) {
			Date started = new Date();
			task.run();
			Date finished = new Date();
			CompletedTask ctask = new CompletedTask(task.isSuccess(), task.valobj.getId(), task.rule.getId(), started, finished, task.getException(), task.getErrors());
			ctasks.add(ctask);
		}
	}

	public List<CompletedTask> getCtasks() {
		return ctasks;
	}

}
