package eu.dnetlib.validator.engine.execution;

import java.util.List;

import org.apache.log4j.Logger;

import eu.dnetlib.validator.engine.data.Rule;
import eu.dnetlib.validator.engine.data.RuleException;

/**
 * A Rule that is applied on a Validation Object.
 * @author manos
 * @author Nikon Gasparis
 *
 */
public class Task implements Runnable {

	public final ValidationObject valobj;
	public final Rule rule;

	private boolean success;
	private List<String> errors;
	private Exception exception = null;;

	public Task(ValidationObject valobj, Rule rule) {
		super();
		this.valobj = valobj;
		this.rule = rule;
	}

	@Override
	public void run() {
		Logger log = Logger.getLogger(Task.class);
		try {
			this.success = rule.apply(valobj);
			this.setErrors(rule.getErrors());
		} catch (RuleException e) {
			log.error("Error applyling rule on task", e);
			this.exception = e;
		}
	}

	public boolean isSuccess() {
		return success;
	}

	public Exception getException() {
		return exception;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

}
