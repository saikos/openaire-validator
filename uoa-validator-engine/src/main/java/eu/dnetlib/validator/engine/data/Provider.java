package eu.dnetlib.validator.engine.data;

import java.util.Properties;

import org.apache.log4j.Logger;

import eu.dnetlib.validator.engine.ValidatorException;
import eu.dnetlib.validator.engine.execution.ValidationObject;

/**
 * A Provider is used to retrieve Validation Objects on which rules are applied.
 * Certain rules may only work with certain kinds of providers.
 * 
 * @author manos
 * @see Rule
 */
public abstract class Provider extends ConfigurableObject {

	private static final long serialVersionUID = -5297066032993163543L;
	
	public Provider(Integer id) {
		super(null, id);
	}

	public void setConfiguration(Properties pros) {
		Logger log = Logger.getLogger(Provider.class);
		log.debug("Setting properties of provider type "+this.getId()+" to "+this.pros);
		this.pros = pros;
	}

	
	/**
	 * Retrieves the Validation Objects.
	 * 
	 * @return The validation objects.
	 * @throws ProviderException
	 */
	public abstract ResultSet<ValidationObject> getValidationObjects() throws ProviderException;
	
	/**
	 * Retrieves the Validation Objects given an entity name.
	 * 
	 * @param valObjId
	 * 
	 * @return The validation objects.
	 * @throws ProviderException
	 */
	public abstract ResultSet<ValidationObject> getValidationObjects(String entity) throws ProviderException;

	/**
	 * <p>
	 * Retrieves the ids of the Validation Objects.
	 * </p>
	 * <p>
	 * The Validation Objects themselves might be retrieved by
	 * {@link Provider#getValidationObject(String)}.
	 * </p>
	 * <p>
	 * Not all providers need to support such a method.
	 * </p>
	 * 
	 * @return The ids of the validation objects
	 * @throws ProviderException
	 * @throws UnsupportedOperationException
	 *             If the provider does not support this method.
	 */
	public abstract ResultSet<String> getValidationObjectIds() throws ProviderException, UnsupportedOperationException;

	/**
	 * Retrieves a validation object given its id. Not all providers need to
	 * support such a method.
	 * 
	 * @param valObjId
	 *            The id of the object.
	 * @return The object itself.
	 * @throws ProviderException
	 * @throws UnsupportedOperationException
	 *             UnsupportedOperationException
	 */
	public abstract ValidationObject getValidationObject(String valObjId) throws ProviderException, UnsupportedOperationException;

	public class ProviderException extends ValidatorException {

		private static final long serialVersionUID = 5574587409338211253L;

		public ProviderException() {
			super();
		}

		public ProviderException(String msg) {
			super(msg);
		}
	}
}
