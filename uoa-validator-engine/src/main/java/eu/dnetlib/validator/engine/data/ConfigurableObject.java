package eu.dnetlib.validator.engine.data;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * An object identified by a unique id and parametrized by Properties.
 * 
 * @author Manos Karvounis
 * @see Rule
 * @see Provider
 */
public class ConfigurableObject implements Serializable {

	protected transient Logger log = Logger.getLogger(ConfigurableObject.class);

	private static final long serialVersionUID = -1573173142135631726L;

	protected Properties pros;
	private int id;

	public ConfigurableObject(Properties pros, int id) {
		super();
//		log.debug("Creating a configurable object with id " + id + " and props " + pros);
		log.debug("Creating a configurable object with id " + id);
		this.pros = pros;
		this.id = id;
	}

	public Properties getConfiguration() {
		return this.pros;
	}
	
	public int getId() {
		return this.id;
	}

	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		log = Logger.getLogger(ConfigurableObject.class);
	}

}
