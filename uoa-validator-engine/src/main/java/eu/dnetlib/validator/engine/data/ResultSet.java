package eu.dnetlib.validator.engine.data;

public interface ResultSet<T> {

	/**
	 * The first call on next() must position the cursor to the first object.
	 * @return
	 * @throws DataException
	 */
	public boolean next() throws DataException;
	public T get() throws DataException;
	public String getError();
	
}
