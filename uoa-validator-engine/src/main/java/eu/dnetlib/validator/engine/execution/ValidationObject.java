package eu.dnetlib.validator.engine.execution;

import eu.dnetlib.validator.engine.data.Provider;
import eu.dnetlib.validator.engine.data.Rule;

/**
 * Represents an on which a {@link Rule} may be applied
 * @author Manos Karvounis
 * @see Provider
 */
public interface ValidationObject {

	public String getId();
	public String getStatus();
	public void setId(String id);
	public Object getContentAsObject();
}
