package eu.dnetlib.validator.engine.execution;

import java.util.Date;
import java.util.List;

/**
 * Represents a CompletedTask.
 * @author Manos Karvounis
 * @author Nikon Gasparis
 *
 */
public class CompletedTask {

	/**
	 * If the rule was applied successfully.
	 */
	public final boolean success;
	/**
	 * The id of the object on which the rule was applied.
	 */
	public final String valobjId;
	/**
	 * The id of the rule that was applied.
	 */
	public final int ruleId;
	/**
	 * The date the task started.
	 */
	public final Date started;
	/**
	 * The date the task finished.
	 */
	public final Date finished;
	/**
	 * The reasons why the rule failed to apply on the object.
	 */
	public final List<String> errors;
	/**
	 * Any exception that might have been raised while the rule was applied on the object.
	 */
	public final Exception exception;

	public CompletedTask(boolean success, String valobjId, int ruleId, Date started, Date finished, Exception exception, List<String> errors) {
		super();
		this.success = success;
		this.valobjId = valobjId;
		this.ruleId = ruleId;
		this.started = started;
		this.finished = finished;
		this.exception = exception;
		this.errors = errors;
	}

}
