package eu.dnetlib.validator.engine.execution;

import java.util.List;
import java.util.Map;

import eu.dnetlib.validator.engine.ValidatorException;

/**
 * A Listener observing an executing job.
 * @author Manos Karvounis.
 * @author Nikon Gasparis
 *
 */
public interface JobListener {
	/**
	 * <p>Indicates that some tasks of the submitted job have finished unsuccessfully because of an exception.</p>
	 * <p>There might be other tasks waiting to be executed.</p>
	 * @param tasks The tasks that finished.
	 * @param jobId The id of the job that defined the tasks.
	 * @param object 
	 * @param t The exception that was thrown.
	 * @throws ValidatorException
	 */
	public void currentResults(List<CompletedTask> tasks, int jobId, Object record, Map<String, Object> recordContext, Throwable t) throws ValidatorException;
	
	/**
	 * <p>Indicates that some tasks of the submitted job have finished.</p>
	 * <p>There might be other tasks waiting to be executed.</p>
	 * @param tasks The tasks that finished.
	 * @param jobId The id of the job that defined the tasks.
	 * @throws ValidatorException 
	 */
	public void currentResults(List<CompletedTask> tasks, int jobId, Object record, Map<String, Object> recordContext) throws ValidatorException;
	
	/**
	 * Indicates that all tasks of a job have finished.
	 * @param jobId
	 */
	public void finished(int jobId, Map<String, Object> jobContext);
	
	/**
	 * Indicates that a job finished unsuccessfully because of an exception.
	 * @param jobId
	 * @param t
	 */
	public void failed(int jobId, Map<String, Object> jobContext, Throwable t);
}