package eu.dnetlib.validator.engine.persistance;

import java.io.Serializable;

import eu.dnetlib.validator.engine.Validator;

/**
 * A Registry used to store serializable objects.
 * Registries are used by {@link Validator} to store the objects they need to run.
 * @author Manos Karvounis
 *
 * @param <T>
 */
public abstract class Registry<T extends Serializable> {

	public final String name;
	
	public Registry(String name) {
		super();
		this.name = name;
	}

	public abstract T getObject(int id);

	public abstract void addObject(int id, T obj);
	
}
