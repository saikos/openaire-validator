package eu.dnetlib.validator.engine;

public class ValidatorException extends Exception {

	private static final long serialVersionUID = -5124043465552948242L;

	public ValidatorException() {
		super();
	}

	public ValidatorException(String message) {
		super(message);
	}

	public ValidatorException(String message, Throwable cause) {
		super(message, cause);
	}

	public ValidatorException(Throwable cause) {
		super(cause);
	}
}
