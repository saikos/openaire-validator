package eu.dnetlib.validator2.validation.guideline;

import eu.dnetlib.validator2.engine.Helper;
import eu.dnetlib.validator2.engine.Rule;
import eu.dnetlib.validator2.engine.RuleContext;
import eu.dnetlib.validator2.engine.RuleEvaluationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;

public final class SyntheticGuideline extends AbstractGuideline<Document> {

    private static final Logger logger = LogManager.getLogger();

    private CompilationResult compilationResult;
    private static final ThreadLocal<GuidelineEvaluation> evaluation = new ThreadLocal<>();

    private SyntheticGuideline(String name, int weight) {
        super(name, weight);
    }

    @Override
    public Result validate(String id, Document document) {
        logger.debug("Validating document {} in {}", id, Thread.currentThread());
        evaluation.set(new GuidelineEvaluation(id, document, getWeight()));
        Result result = evaluation.get().evaluate(compilationResult);
        logger.debug("Evaluated: {} in {} with result {}", evaluation.get(), Thread.currentThread(), result);
        evaluation.remove();
        return result;
    }

    public static SyntheticGuideline of(String name, int weight, ElementSpec elementSpec) {
        String canonical = Helper.ensureNonEmpty(name, () -> new IllegalArgumentException("Name cannot be empty"));

        // Here we define a guideline with a specific elementSpec as origin, we cannot allow null
        if (elementSpec == null) {
            throw new IllegalArgumentException("ElementSpec cannot be empty");
        }

        if (weight < 0) {
            throw new IllegalArgumentException("Weight cannot be negative");
        }

        SyntheticGuideline guideline = new SyntheticGuideline(canonical, weight);
        guideline.compilationResult = new ElementSpecCompiler().compile(elementSpec, evaluation::get);
        return guideline;
    }

    // Note: the synthetic rules that will be created are completely independent of each other
    public static SyntheticGuideline of(String name, int weight, RequirementLevel requirementLevel, Rule<Document> rule) {
        String canonical = Helper.ensureNonEmpty(name, () -> new IllegalArgumentException("Name cannot be empty"));

        if (weight < 0) {
            throw new IllegalArgumentException("Weight cannot be negative");
        }

        if (requirementLevel == null) {
            throw new IllegalArgumentException("Requirement level cannot be empty");
        }

        if (rule == null) {
            throw new IllegalArgumentException("Rule cannot be empty");
        }

        SyntheticGuideline guideline = new SyntheticGuideline(canonical, weight);
        CompilationResult compilationResult = new CompilationResult();
        compilationResult.rootNodeRule = new SyntheticRule<Document>() {
            @Override
            public SyntheticRule<Document> parentRule() {
                return null;
            }

            @Override
            public Rule<Document> applicabilityRule() {
                return null;
            }

            @Override
            public <C extends RuleContext> C getContext() {
                return rule.getContext();
            }

            @Override
            public boolean test(Document document) throws RuleEvaluationException {
                return rule.test(document);
            }
        };
        compilationResult.ruleIdToRequirementLevel.put(rule.getContext().getIdProperty().getValue(), requirementLevel);
        guideline.compilationResult = compilationResult;
        return guideline;
    }
}
