package eu.dnetlib.validator2.validation.guideline.openaire.cris;

import eu.dnetlib.validator2.validation.guideline.Builders;
import eu.dnetlib.validator2.validation.guideline.Cardinality;
import eu.dnetlib.validator2.validation.guideline.ElementSpec;
import eu.dnetlib.validator2.validation.guideline.Guideline;
import org.w3c.dom.Document;

import java.util.Collection;

public class CrisFundingV111Profile extends AbstractCrisProfile {

    private static final String[] TYPE_VOCABULARY = {
            "https://www.openaire.eu/cerif-profile/vocab/OpenAIRE_Funding_Types#FundingProgramme",
            "https://www.openaire.eu/cerif-profile/vocab/OpenAIRE_Funding_Types#Call",
            "https://www.openaire.eu/cerif-profile/vocab/OpenAIRE_Funding_Types#Tender",
            "https://www.openaire.eu/cerif-profile/vocab/OpenAIRE_Funding_Types#Gift",
            "https://www.openaire.eu/cerif-profile/vocab/OpenAIRE_Funding_Types#InternalFunding",
            "https://www.openaire.eu/cerif-profile/vocab/OpenAIRE_Funding_Types#Contract",
            "https://www.openaire.eu/cerif-profile/vocab/OpenAIRE_Funding_Types#Award",
            "https://www.openaire.eu/cerif-profile/vocab/OpenAIRE_Funding_Types#Grant"
    };

    //TODO: check namespace?
    private static final Builders.ElementSpecBuilder TYPE_SPEC = Builders.
            forMandatoryElement("Type", Cardinality.ONE).
            allowedValues(TYPE_VOCABULARY);

    private static final Builders.ElementSpecBuilder AMOUNT_SPEC = Builders.
            forOptionalElement("Amount");

    private static final Builders.ElementSpecBuilder FUNDER_SPEC = Builders.
            forOptionalRepeatableElement("Funder").
            withSubElement(Builders.
                    forOptionalElement("DisplayName")).
            //TODO: Optional 1 of 2 (OrgUnit, Person)
            withSubElement(ORG_UNIT_SPEC). //TODO: Pass proper spec
            withSubElement(PERSON_SPEC); //TODO: Pass proper spec

    private static final Builders.ElementSpecBuilder PART_OF_SPEC = Builders.
            forOptionalElement("PartOf").
            withSubElement(null); //TODO: Pass self spec

    private static final Builders.ElementSpecBuilder DURATION_SPEC = Builders.
            forOptionalElement("Duration");

    private static final ElementSpec FUNDING_SPEC = Builders.
            forMandatoryElement("Funding", Cardinality.ONE).
            withMandatoryAttribute("id").
            withSubElement(TYPE_SPEC).
            withSubElement(ACRONYM_SPEC).
            withSubElement(NAME_SPEC).
            withSubElement(AMOUNT_SPEC).
            withSubElement(IDENTIFIER_SPEC).
            withSubElement(DESCRIPTION_SPEC).
            withSubElement(SUBJECT_SPEC).
            withSubElement(KEYWORD_SPEC).
            withSubElement(FUNDER_SPEC).
            withSubElement(PART_OF_SPEC).
            withSubElement(DURATION_SPEC).
            withSubElement(OA_MANDATE_SPEC).
            build();

    public CrisFundingV111Profile(String name) {
        super(name);
    }

    @Override
    public Collection<? extends Guideline<Document>> guidelines() {
        return null;
    }

    @Override
    public Guideline<Document> guideline(String guidelineName) {
        return null;
    }

    @Override
    public int maxScore() {
        return 0;
    }
}
