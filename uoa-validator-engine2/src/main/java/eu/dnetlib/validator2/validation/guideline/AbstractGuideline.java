package eu.dnetlib.validator2.validation.guideline;

public abstract class AbstractGuideline<T> implements Guideline<T> {

    private final String name;
    private final int weight;

    public AbstractGuideline(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public String getName() {
        return name;
    }
}
