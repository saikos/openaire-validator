package eu.dnetlib.validator2.validation.guideline;

import eu.dnetlib.validator2.engine.Rule;
import org.w3c.dom.Document;

import java.util.function.Predicate;

public interface NodeSpec {

    String nodeName();

    RequirementLevel requirementLevel();

    Cardinality cardinality();

    Predicate<String> allowedValuesPredicate();

    // This can be null
    Rule<Document> applicabilityRule();
}
