package eu.dnetlib.validator2.validation.guideline;

public enum RequirementLevel {

    MANDATORY,
    MANDATORY_IF_APPLICABLE,
    RECOMMENDED,
    OPTIONAL,
    NOT_APPLICABLE;

}
