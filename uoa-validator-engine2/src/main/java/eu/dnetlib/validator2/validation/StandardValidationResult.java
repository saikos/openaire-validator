package eu.dnetlib.validator2.validation;

import java.util.Map;

import static eu.dnetlib.validator2.validation.guideline.Guideline.Result;

public class StandardValidationResult implements XMLApplicationProfile.ValidationResult {

    private final String              id;
    private final double              score;
    private final Map<String, Result> results;

    public StandardValidationResult(String id, double score, Map<String, Result> results) {
        this.id = id;
        this.score = score;
        this.results = results;
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public double score() {
        return score;
    }

    @Override
    public Map<String, Result> results() {
        return results;
    }

}
