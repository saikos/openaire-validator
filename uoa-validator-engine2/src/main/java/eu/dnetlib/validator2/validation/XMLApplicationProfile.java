package eu.dnetlib.validator2.validation;

import eu.dnetlib.validator2.engine.Rule;
import eu.dnetlib.validator2.engine.Status;
import eu.dnetlib.validator2.engine.builtins.XMLCardinalityRule;
import eu.dnetlib.validator2.validation.guideline.ElementSpec;
import eu.dnetlib.validator2.validation.guideline.Guideline;
import eu.dnetlib.validator2.validation.guideline.SyntheticGuideline;
import org.w3c.dom.Document;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * An application-specific collection of guidelines
 */
public interface XMLApplicationProfile {

    String name();

    Collection<? extends Guideline<Document>> guidelines();

    Guideline<Document> guideline(String guidelineName);

    ValidationResult validate(String id, Document document);

    int maxScore();

    interface ValidationResult {

        String id();

        double score();

        Map<String, Guideline.Result> results();
    }
}
