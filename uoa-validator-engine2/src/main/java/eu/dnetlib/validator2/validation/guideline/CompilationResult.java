package eu.dnetlib.validator2.validation.guideline;

import eu.dnetlib.validator2.engine.Helper;
import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class CompilationResult {

    SyntheticRule<Document> rootNodeRule;
    final List<SyntheticRule<Document>> nodeRules = new ArrayList<>();
    final Map<String, RequirementLevel> ruleIdToRequirementLevel = new HashMap<>();

    @Override
    public String toString() {
        return "### CompilationResult ###\n" +
               "Root = " + Helper.stringify(rootNodeRule) + "\n" +
               "Node = [" + nodeRules.stream().map(Helper::stringify).collect(Collectors.joining(", ")) + "]";
    }
}
