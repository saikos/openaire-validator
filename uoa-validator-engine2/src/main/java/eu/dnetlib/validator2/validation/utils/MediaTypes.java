package eu.dnetlib.validator2.validation.utils;

import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MediaTypes {
    private static Set<String> mediaTypes;

    static {
        loadMediaTypes();
    }

    private static void loadMediaTypes() {
        try (InputStream in = MediaTypes.class.getClassLoader().getResourceAsStream("mediaTypes.csv")) {
            //TODO: remove regex
            List<String> l = Arrays.asList(IOUtils.toString(in, "UTF-8").split("\\s*,\\s*"));
            mediaTypes = new HashSet<>(l);
        }
        catch (Exception e) {}
    }


    public static boolean contains(String mediaType) {
        if (mediaTypes == null) {
            throw new IllegalStateException("Media types not loaded from file");
        }

        if (mediaType == null || mediaType.isEmpty()) {
            return false;
        }

        return mediaTypes.contains(mediaType);
    }

}
