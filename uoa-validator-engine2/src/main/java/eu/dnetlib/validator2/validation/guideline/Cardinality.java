package eu.dnetlib.validator2.validation.guideline;

public enum Cardinality {

    ONE(1, 1),
    ONE_TO_N(1, Long.MAX_VALUE),
    TWO(2, 2),
    FOUR_TO_N(4, Long.MAX_VALUE);

    private final long lowerBound;
    private final long upperBound;

    private Cardinality(long lowerBound, long upperBound) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }

    long getLowerBound() {
        return lowerBound;
    }

    long getUpperBound() {
        return upperBound;
    }

    public String asText() {
        return (lowerBound == upperBound)
                ? String.valueOf(lowerBound)
                : lowerBound + "-" + ((upperBound == Long.MAX_VALUE) ? "n" : upperBound);
    }
}
