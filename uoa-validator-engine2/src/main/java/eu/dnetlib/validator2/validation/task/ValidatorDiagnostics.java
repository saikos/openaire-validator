package eu.dnetlib.validator2.validation.task;

import eu.dnetlib.validator2.engine.Rule;
import eu.dnetlib.validator2.engine.RuleDiagnostics;

import java.util.concurrent.ConcurrentHashMap;

class ValidatorDiagnostics<T, R extends Rule<T>> implements RuleDiagnostics<T, R>, ValidationTaskOutput {

    private final ConcurrentHashMap<String, RuleEvaluationResult> results = new ConcurrentHashMap<>();

    @Override
    public RuleEvaluationResult statusFor(String ruleId) {
        return results.get(ruleId);
    }

    @Override
    public long score() {
        //TODO: How do we calculate the score?
        return 0;
    }

    void setInterrupted(String ruleId) {
        results.put(ruleId, RuleEvaluationResult.forInterrupted());
    }

    @Override
    public void success(R rule, T t) {
        results.put(rule.getContext().getIdProperty().getValue(), RuleEvaluationResult.forSuccess());
    }

    @Override
    public void failure(R rule, T t) {
        results.put(rule.getContext().getIdProperty().getValue(), RuleEvaluationResult.forFailure());
    }

    @Override
    public void error(R rule, T t, Throwable err) {
        results.put(rule.getContext().getIdProperty().getValue(), RuleEvaluationResult.forError(err.getMessage()));
    }
}
