package eu.dnetlib.validator2.validation.guideline.openaire.cris;

import eu.dnetlib.validator2.validation.guideline.Builders;
import eu.dnetlib.validator2.validation.guideline.Cardinality;
import eu.dnetlib.validator2.validation.guideline.ElementSpec;
import eu.dnetlib.validator2.validation.guideline.Guideline;
import org.w3c.dom.Document;

import java.util.Collection;

public class CrisEquipmentV111Profile extends AbstractCrisProfile {

    private static final Builders.ElementSpecBuilder TYPE_SPEC = Builders.
            forOptionalRepeatableElement("Type").
            withMandatoryAttribute("scheme");

    private static final Builders.ElementSpecBuilder OWNER_SPEC = Builders.
            forOptionalRepeatableElement("Owner").
            withSubElement(Builders.
                    forOptionalElement("DisplayName")).
            //TODO: Optional 1 of 2 (OrgUnit, Person)
            withSubElement(ORG_UNIT_SPEC). //TODO: Pass proper spec
            withSubElement(PERSON_SPEC); //TODO: Pass proper spec

    private static final ElementSpec EQUIPMENT_SPEC = Builders.
            forMandatoryElement("Equipment", Cardinality.ONE).
            withMandatoryAttribute("id").
            withSubElement(TYPE_SPEC).
            withSubElement(ACRONYM_SPEC).
            withSubElement(NAME_SPEC).
            withSubElement(IDENTIFIER_SPEC).
            withSubElement(DESCRIPTION_SPEC).
            withSubElement(OWNER_SPEC).
            build();

    public CrisEquipmentV111Profile(String name) {
        super(name);
    }

    @Override
    public Collection<? extends Guideline<Document>> guidelines() {
        return null;
    }

    @Override
    public Guideline<Document> guideline(String guidelineName) {
        return null;
    }

    @Override
    public int maxScore() {
        return 0;
    }
}
