package eu.dnetlib.validator2.validation.utils;


import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ISOLangCodes {

    private static Set<String> langs;

    static {
        loadLangCodes();
    }

    private static void loadLangCodes() {
        try (InputStream in = MediaTypes.class.getClassLoader().getResourceAsStream("iso639XLangCodes.csv")) {
            //TODO:Remove the regex
            List<String> l = Arrays.asList(IOUtils.toString(in, "UTF-8").split("\\s*,\\s*"));
            langs = new HashSet<>(l);
        }
        catch (Exception e) {}
    }


    public static boolean contains(String langCode) {
        if (langs == null) {
            throw new IllegalStateException("Languages  not loaded from file");
        }

        if (langCode == null || langCode.isEmpty()) {
            return false;
        }

        return langs.contains(langCode);
    }

}
