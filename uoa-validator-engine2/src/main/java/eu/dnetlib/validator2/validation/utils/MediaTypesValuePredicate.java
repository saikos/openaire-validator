package eu.dnetlib.validator2.validation.utils;

import java.util.function.Predicate;

public class MediaTypesValuePredicate implements Predicate<String> {

    @Override
    public boolean test(String s) {
        if (s == null || s.isEmpty()) {
            return false;
        }

        return MediaTypes.contains(s);
    }
}
