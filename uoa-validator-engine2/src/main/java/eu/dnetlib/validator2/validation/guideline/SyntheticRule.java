package eu.dnetlib.validator2.validation.guideline;

import eu.dnetlib.validator2.engine.Rule;
import org.w3c.dom.Document;

interface SyntheticRule<T> extends Rule<T> {

    SyntheticRule<T> parentRule();

    Rule<Document> applicabilityRule();
}
