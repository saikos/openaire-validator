package eu.dnetlib.validator2.validation.task;

import eu.dnetlib.validator2.engine.Rule;

import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;

public interface ValidationTask<T, R extends Rule<T>> {

    Collection<R> ruleSet();

    T subject();

    void run(Consumer<ValidationTaskOutput> outputConsumer);

    class Factory {
        public static <T, R extends Rule<T>> ValidationTask<T, R> newTask(ExecutorService executor, Collection<R> rules, T subject) {
            return new ExecutorBasedValidationTask<>(subject, rules, executor);
        }
    }

}
