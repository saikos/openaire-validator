package eu.dnetlib.validator2.validation.guideline;

import eu.dnetlib.validator2.engine.Rule;
import eu.dnetlib.validator2.engine.RuleDiagnostics;

public interface RuleEvaluator<T, R extends Rule<T>> {

    RuleDiagnostics<T, R> evaluate(T subject);

}
