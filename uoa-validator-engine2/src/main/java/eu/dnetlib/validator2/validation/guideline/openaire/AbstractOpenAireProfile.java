package eu.dnetlib.validator2.validation.guideline.openaire;

import eu.dnetlib.validator2.engine.Helper;
import eu.dnetlib.validator2.engine.Rule;
import eu.dnetlib.validator2.engine.Status;
import eu.dnetlib.validator2.engine.builtins.*;
import eu.dnetlib.validator2.validation.StandardValidationResult;
import eu.dnetlib.validator2.validation.XMLApplicationProfile;
import eu.dnetlib.validator2.validation.guideline.ElementSpec;
import eu.dnetlib.validator2.validation.guideline.Guideline;
import eu.dnetlib.validator2.validation.guideline.SyntheticGuideline;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class AbstractOpenAireProfile implements XMLApplicationProfile {

    private final String name;

    public AbstractOpenAireProfile(String name) {
        this.name = name;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public ValidationResult validate(String id, Document document) {
        int maxScore = maxScore();
        double score    = 0;
        final Map<String, Guideline.Result> results  = new HashMap<>();

        for (Guideline<Document> guideline: guidelines()) {

            Guideline.Result result = guideline.validate(document);
            results.put(guideline.getName(), result);

            score += (result.status() == Status.SUCCESS ? result.score() : 0);
            // System.out.println("Score after " + guideline.getName() + " = " + score);
        }

        double percentScore = (score / maxScore) * 100;

        return new StandardValidationResult(id, percentScore, results);
    }

    static Rule<Document> elementIsPresent(String elementName) {
        return XMLCardinalityRule
                .builder()
                .setId(ElementSpec.APPLICABILITY_RULE_ID)
                .setXPathExpression("//*[local-name()='" + elementName + "']")
                .setIsInclusive(true).setRange(1, Long.MAX_VALUE)
                .build();
    }

    static Rule<Document> elementIsPresentAndHasAttributeWithValue(String elementName,
                                                                   String attrName,
                                                                   String attrValue) {
        StandardXMLContext context = new StandardXMLContext();
        context.getIdProperty().setValue(ElementSpec.APPLICABILITY_RULE_ID);
        context.getNodeListActionProperty().setValue("custom");
        context.getXPathExpressionProperty().setValue("//*[name()='" + elementName + "']");
        return new XMLRule<>(context, (NodeList nodes) -> {
            // The nodes contain all the elementName nodes
            int len = nodes.getLength();
            if (len == 0) { return false; } // element is not present
            for(int i = 0; i < len; i++) {
                Node elem = nodes.item(i);
                String value = Helper.getAttributeValue(elem, attrName);
                if (!attrValue.equals(value)) { return false; }
            }
            return true;
        });
    }

}
