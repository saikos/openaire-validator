package eu.dnetlib.validator2.validation.guideline;

import eu.dnetlib.validator2.engine.Status;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class StandardResult implements Guideline.Result {

    private static final List<String> EMPTY = Collections.emptyList();

    private final List<String> warnings;
    private final List<String> errors;
    private final String internalError;
    private final Status status;
    private final int score;

    private StandardResult(int score, Status status, List<String> warnings, List<String> errors, String internalError) {
        this.status = status;
        this.score = score;
        this.warnings = warnings;
        this.errors = errors;
        this.internalError = internalError;
    }

    @Override
    public int score() {
        return score;
    }

    @Override
    public Status status() {
        return status;
    }

    @Override
    public Iterable<String> warnings() {
        return warnings;
    }

    @Override
    public Iterable<String> errors() {
        return errors;
    }

    @Override
    public String internalError() {
        return internalError;
    }

    public static StandardResult forError(String message) {
        return new StandardResult(-1, Status.ERROR, EMPTY, EMPTY, message);
    }

    public static StandardResult forSuccess(int score, List<String> warnings) {
        return new StandardResult(score, Status.SUCCESS, sanitize(warnings), EMPTY, null);
    }

    public static StandardResult forFailure(List<String> warnings, List<String> errors) {
        return new StandardResult(0, Status.FAILURE, sanitize(warnings), sanitize(errors), null);
    }

    private static List<String> sanitize(List<String> list) {
        if (list == null || list.size() == 0) return EMPTY;
        return Collections.unmodifiableList(list);
    }

    @Override
    public String toString() {
        if (status == Status.SUCCESS || status == Status.FAILURE) {
            return status + " (" + warnings.size() + " warnings) - score " + score;
        }
        else {
            return status + " (" + warnings.size() + " warnings) - " + internalError;
        }
    }
}
