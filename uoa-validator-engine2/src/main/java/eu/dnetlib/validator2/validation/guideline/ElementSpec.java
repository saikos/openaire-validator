package eu.dnetlib.validator2.validation.guideline;

import java.util.Set;

public interface ElementSpec extends NodeSpec {

    String APPLICABILITY_RULE_ID = "ApplicabilityRule";

    Set<String> parents();

    Set<ElementSpec> subElementSpecs();

    Set<AttributeSpec> attributeSpecs();

    /**
     *
     * @return the required prefix or null if no prefix has been defined for the value
     */
    String valuePrefix();

    /**
     *
     * @return the specific position (occurrence) of the element the spec refers to
     */
    ElementPosition position();

}
