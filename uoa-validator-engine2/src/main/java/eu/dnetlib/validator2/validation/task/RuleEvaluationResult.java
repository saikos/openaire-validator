package eu.dnetlib.validator2.validation.task;

public final class RuleEvaluationResult {

    private static final byte SUCCESS     = 0;
    private static final byte FAILURE     = 2;
    private static final byte ERROR       = 4;
    private static final byte INTERRUPTED = 8;

    private final byte result;
    private final String errorMessage;

    RuleEvaluationResult(byte result, String errorMessage) {
        this.result = result;
        this.errorMessage = errorMessage;
    }

    public boolean succeeded() { return result == SUCCESS; }

    public boolean failed() { return result == FAILURE; }

    public boolean errored() { return result == ERROR; }

    public boolean interrupted() { return result == INTERRUPTED; }

    public String errorMessage() {
        return errorMessage;
    }

    static RuleEvaluationResult forSuccess() {
        return new RuleEvaluationResult(SUCCESS, null);
    }

    static RuleEvaluationResult forFailure() {
        return new RuleEvaluationResult(FAILURE, null);
    }

    static RuleEvaluationResult forError(String errorMessage) {
        return new RuleEvaluationResult(ERROR, errorMessage);
    }

    static RuleEvaluationResult forInterrupted() {
        return new RuleEvaluationResult(INTERRUPTED, null);
    }
}
