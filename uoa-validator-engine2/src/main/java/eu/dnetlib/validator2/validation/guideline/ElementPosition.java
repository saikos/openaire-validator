package eu.dnetlib.validator2.validation.guideline;

public enum ElementPosition {

    ALL(null),
    FIRST("position()=1"),
    SECOND("position()=2");

    public final String xpath;

    ElementPosition(String xpath) {
        this.xpath = xpath;
    }
}
