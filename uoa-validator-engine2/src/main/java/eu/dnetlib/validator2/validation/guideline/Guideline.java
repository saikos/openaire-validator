package eu.dnetlib.validator2.validation.guideline;

import eu.dnetlib.validator2.engine.Status;

public interface Guideline<T> {

    int getWeight(); //that's the "score" of the guideline if it succeeds

    String getName();

    default Result validate(T t) {
        return validate(t == null ? "Object" : t.getClass().getSimpleName(), t);
    }

    Result validate(String id, T t);

    interface Result {

        int score();

        Status status();

        // When status == SUCCESS, potential warnings are held here
        // This may also contain messages when status == FAILURE
        Iterable<String> warnings();

        // When status == FAILURE, the errors are held here
        // We currently hold a single error (and fail fast)
        Iterable<String> errors();

        // When status == ERROR, the internal error is held here
        String internalError();
    }

}
