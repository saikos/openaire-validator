package eu.dnetlib.validator2.validation.task;

public interface ValidationTaskOutput {

    RuleEvaluationResult statusFor(String ruleId);

    long score();

}
