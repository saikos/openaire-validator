package eu.dnetlib.validator2.validation.guideline.openaire.cris;

import eu.dnetlib.validator2.validation.guideline.Builders;
import eu.dnetlib.validator2.validation.guideline.Cardinality;
import eu.dnetlib.validator2.validation.guideline.ElementSpec;
import eu.dnetlib.validator2.validation.guideline.Guideline;
import org.w3c.dom.Document;

import java.util.Collection;

public class CrisOrgUnitV111Profile extends AbstractCrisProfile {

    private static final Builders.ElementSpecBuilder TYPE_SPEC = Builders.
            forOptionalRepeatableElement("Type").
            withMandatoryAttribute("scheme");

    private static final Builders.ElementSpecBuilder PART_OF_SPEC = Builders.
            forOptionalRepeatableElement("PartOf").
            withSubElement(null); //TODO: Pass self spec

    private static final ElementSpec ORG_UNIT_SPEC = Builders.
            forMandatoryElement("OrgUnit", Cardinality.ONE).
            withMandatoryAttribute("id").
            withSubElement(TYPE_SPEC).
            withSubElement(ACRONYM_SPEC).
            withSubElement(NAME_SPEC).
            withSubElement(IDENTIFIER_SPEC).
            withSubElement(ELECTRONIC_ADDRESS_SPEC).
            withSubElement(PART_OF_SPEC).
            build();

    public CrisOrgUnitV111Profile() {
        super("OpenAIRE Guidelines for OrgUnit element of CRIS Managers v1.1.1");
    }

    @Override
    public Collection<? extends Guideline<Document>> guidelines() {
        return null;
    }

    @Override
    public Guideline<Document> guideline(String guidelineName) {
        return null;
    }

    @Override
    public int maxScore() {
        return 0;
    }
}
