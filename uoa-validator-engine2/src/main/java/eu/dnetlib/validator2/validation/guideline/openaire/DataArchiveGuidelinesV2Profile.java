package eu.dnetlib.validator2.validation.guideline.openaire;

import eu.dnetlib.validator2.engine.Rule;
import eu.dnetlib.validator2.engine.builtins.XMLCardinalityRule;
import eu.dnetlib.validator2.validation.guideline.Builders;
import eu.dnetlib.validator2.validation.guideline.ElementPosition;
import eu.dnetlib.validator2.validation.guideline.ElementSpec;
import eu.dnetlib.validator2.validation.guideline.Guideline;
import eu.dnetlib.validator2.validation.guideline.SyntheticGuideline;
import eu.dnetlib.validator2.validation.utils.ISO639ValuePredicate;
import eu.dnetlib.validator2.validation.utils.RegexValuePredicate;
import org.w3c.dom.Document;

import java.util.*;
import java.util.stream.Collectors;

import static eu.dnetlib.validator2.validation.guideline.Cardinality.*;
import static eu.dnetlib.validator2.validation.utils.SupportedRegExs.*;

public class DataArchiveGuidelinesV2Profile extends AbstractOpenAireProfile {

    private static final String[] identifierTypes = {
            "ARK",
            "DOI",
            "Handle",
            "PURL",
            "URN",
            "URL"
    };

    private static final String[] titleTypes = {
            "AlternativeTitle",
            "Subtitle",
            "TranslatedTitle"
    };

    private static final String[] contributorTypes = {
            "ContactPerson", "DataCollector", "DataCurator",
            "DataManager", "Distributor", "Editor", "Funder",
            "HostingInstitution", "Producer", "ProjectLeader",
            "ProjectManager", "ProjectMember", "RegistrationAgency",
            "RegistrationAuthority", "RelatedPerson", "Researcher",
            "ResearchGroup", "RightsHolder", "Sponsor",
            "Supervisor", "WorkPackageLeader", "Other"
    };

    private static final String[] dateTypes = {
            "Accepted",
            "Available",
            "Copyrighted",
            "Collected",
            "Created",
            "Issued",
            "Submitted",
            "Updated",
            "Valid"
    };

    private static final String[] resourceTypeGeneralValues = {
            "Audiovisual",
            "Collection",
            "Dataset",
            "Event",
            "Image",
            "InteractiveResource",
            "Model",
            "PhysicalObject",
            "Service",
            "Software",
            "Sound",
            "Text",
            "Workflow",
            "Other"
    };

    private static final String[] relatedIdentifierTypes = {
            "ARK", "arXiv", "bibcode", "DOI", "EAN13",
            "EISSN", "Handle", "IGSN", "ISBN", "ISSN",
            "ISTC", "LISSN", "LSID", "PISSN", "PMID",
            "PURL", "UPC", "URL", "URN", "WOS"
    };

    private static final String[] relationTypes = {
            "IsCitedBy", "Cites", "IsSupplementTo", "IsSupplementedBy",
            "IsContinuedBy", "Continues", "HasMetadata", "IsMetadataFor",
            "IsNewVersionOf", "IsPreviousVersionOf", "IsPartOf", "HasPart",
            "IsReferencedBy", "References", "IsDocumentedBy", "Documents",
            "IsCompiledBy", "Compiles", "IsVariantFormOf", "IsOriginalFormOf",
            "IsIdenticalTo", "IsReviewedBy", "Reviews", "IsDerivedFrom",
            "IsSourceOf"
    };

    private static final String[] rightsURIList = {
            "info:eu-repo/semantics/closedAccess",
            "info:eu-repo/semantics/embargoedAccess",
            "info:eu-repo/semantics/restrictedAccess",
            "info:eu-repo/semantics/openAccess"
    };

    private static final String[] descriptionTypes = {
            "Abstract",
            "Methods",
            "SeriesInformation",
            "TableOfContents",
            "Other"
    };

    private static final ElementSpec IDENTIFIER_SPEC = Builders
            .forMandatoryElement("identifier", ONE).inContext("metadata", "oai_datacite", "payload", "resource")
            .withMandatoryAttribute("identifierType", identifierTypes)
            .build();

    private static final ElementSpec CREATOR_SPEC = Builders
            .forMandatoryElement("creator", ONE_TO_N)
            .withSubElement(Builders.forMandatoryElement("creatorName", ONE))
            .withSubElement(Builders
                                    .forRecommendedElement("nameIdentifier")
                                    .withRecommendedAttribute("nameIdentifierScheme")
                                    .withRecommendedAttribute("schemeURI")
            )
            .withSubElement(Builders.forRecommendedRepeatableElement("affiliation"))
            .build();

    private static final ElementSpec TITLE_SPEC = Builders
            .forMandatoryElement("title", ONE_TO_N)
            .withOptionalAttribute("titleType", titleTypes)
            .build();

    private static final ElementSpec PUBLISHER_SPEC = Builders
            .forMandatoryElement("publisher", ONE)
            .build();

    private static final ElementSpec PUBLICATION_YEAR_SPEC = Builders
            .forMandatoryElement("publicationYear", ONE).allowedValues(new RegexValuePredicate(COMPILED_YEAR_YYYY_REG_EX))
            .build();

    private static final ElementSpec SUBJECT_SPEC = Builders
            .forRecommendedRepeatableElement("subject")
            .withOptionalAttribute("subjectScheme")
            .withOptionalAttribute("schemeURI")
            .build();


    //TODO mandatory if contributorType=Funder (for element and subelements/properties)
    private static final ElementSpec CONTRIBUTOR_SPEC = Builders
            .forMandatoryIfApplicableElement("contributor", ONE_TO_N, AbstractOpenAireProfile.elementIsPresent("contributor"))
            .withMandatoryIfApplicableAttribute("contributorType", appRuleForContributorType(), contributorTypes) //TODO
            .withSubElement(Builders
                                    .forMandatoryIfApplicableElement("contributorName", ONE, AbstractOpenAireProfile.elementIsPresent("contributor"))
            )
            //TODO the RegexValueProdicate for info:eu-repo/grantAgreement must be applied if contributorType is Funder
            .withSubElement(Builders.forMandatoryIfApplicableElement("nameIdentifier", ONE, appRuleForContributorNameIdentifier())
                                    .withMandatoryIfApplicableAttribute("nameIdentifierScheme", appRuleForContributorNameIdentifier())
                                    .withOptionalAttribute("schemeURI ")
            )
            .withSubElement(Builders.forOptionalRepeatableElement("affiliation"))
            .build();

    //TODO: guideline is invalid (0..n). Since Date is mandatory 1..n
    //TODO how to enforce that available dateTypes appear only once?
    private static final ElementSpec DATE_SPEC = Builders
            .forMandatoryElement("date", ONE_TO_N).allowedValues(new RegexValuePredicate(COMPILED_ISO_8601_DATE_REG_EX))
            .withMandatoryAttribute("dateType", dateTypes)
            .build();

    private static final ElementSpec LANGUAGE_SPEC = Builders
            .forRecommendedRepeatableElement("language").allowedValues(new ISO639ValuePredicate())
            .build();

    private static final ElementSpec RESOURCE_TYPE_SPEC = Builders
            .forRecommendedElement("resourceType")
            .withRecommendedAttribute("resourceTypeGeneral", resourceTypeGeneralValues)
            .build();

    private static final ElementSpec ALTERNATE_IDENTIFIER_SPEC = Builders
            .forOptionalRepeatableElement("alternateIdentifier")
            .withOptionalAttribute("alternateIdentifierType")
            .build();

    private static final ElementSpec RELATED_IDENTIFIER_SPEC = Builders
            .forMandatoryIfApplicableElement("relatedIdentifier", ONE_TO_N, appRuleForRelatedIdentifier())
            .withMandatoryAttribute("relatedIdentifierType", relatedIdentifierTypes)
            .withMandatoryAttribute("relationType", relationTypes)
            //TODO use the following three attributes only when relationType=HasMetadata|IsMetadataFor
            .withOptionalAttribute("relatedMetadataScheme")
            .withOptionalAttribute("schemeURI")
            .withOptionalAttribute("schemeType")
            .build();

    private static final ElementSpec SIZE_SPEC = Builders
            .forOptionalRepeatableElement("size")
            .build();

    private static final ElementSpec FORMAT_SPEC = Builders
            .forOptionalRepeatableElement("format")
            .build();

    private static final ElementSpec VERSION_SPEC = Builders
            .forOptionalRepeatableElement("version")
            .build();

    private static final ElementSpec RIGHTS_SPEC = Builders
            .forMandatoryIfApplicableElement("rights", ONE_TO_N, AbstractOpenAireProfile.elementIsPresent("rights"))
            .atPosition(ElementPosition.FIRST)
            .withMandatoryAttribute("rightsURI", rightsURIList)
            .build();

    private static final ElementSpec RIGHTS_SPEC2 = Builders
            .forMandatoryIfApplicableElement("rights", ONE_TO_N, AbstractOpenAireProfile.elementIsPresent("rights"))
            .atPosition(ElementPosition.SECOND)
            .withMandatoryAttribute("rightsURI")
            .build();

    private static final ElementSpec DESCRIPTION_SPEC = Builders
            .forMandatoryIfApplicableElement("description", ONE_TO_N, AbstractOpenAireProfile.elementIsPresent("rights"))
            .withMandatoryAttribute("descriptionType", descriptionTypes)
            .build();

    private static final ElementSpec GEOLOCATION_SPEC = Builders
            .forOptionalRepeatableElement("geoLocation")
            .withSubElement(Builders.forOptionalElement("geoLocationPoint"))
            .withSubElement(Builders.forOptionalElement("geoLocationBox"))
            .withSubElement(Builders.forOptionalElement("geoLocationPlace"))
            .build();

    private static Rule<Document> appRuleForContributorType() {
        //TODO
        return XMLCardinalityRule
                .builder()
                .setId(ElementSpec.APPLICABILITY_RULE_ID)
                .setXPathExpression("//*[name()='contributor']/@contributorType")
                .setIsInclusive(true).setRange(1,1).build();
    }

    private static Rule<Document> appRuleForContributorNameIdentifier() {
        return XMLCardinalityRule
                .builder()
                .setId(ElementSpec.APPLICABILITY_RULE_ID)
                .setXPathExpression("//*[name()='contributor' and @contributorType='Funder']")
                .setIsInclusive(true).setRange(1,1).build();
    }

    private static Rule<Document> appRuleForRelatedIdentifier() {
        //TODO
        return XMLCardinalityRule
                .builder()
                .setId(ElementSpec.APPLICABILITY_RULE_ID)
                .setXPathExpression("//*[name()='relatedIdentifier']")
                .setIsInclusive(true).setRange(1,Long.MAX_VALUE - 1).build();
    }

    //TODO: weights for guidelines haven't been finalized. They've been given an arbitrary value of 1.
    public static final SyntheticGuideline IDENTIFIER = SyntheticGuideline.of("Identifier", 1, IDENTIFIER_SPEC);
    public static final SyntheticGuideline CREATOR = SyntheticGuideline.of("Creator", 1, CREATOR_SPEC);
    public static final SyntheticGuideline TITLE = SyntheticGuideline.of("Title", 1, TITLE_SPEC);
    public static final SyntheticGuideline PUBLISHER = SyntheticGuideline.of("Publisher", 1, PUBLISHER_SPEC);
    public static final SyntheticGuideline PUBLICATION_YEAR = SyntheticGuideline.of("PublicationYear", 1, PUBLICATION_YEAR_SPEC);
    public static final SyntheticGuideline SUBJECT = SyntheticGuideline.of("Subject", 1, SUBJECT_SPEC);
    public static final SyntheticGuideline CONTRIBUTOR = SyntheticGuideline.of("Contributor", 1, CONTRIBUTOR_SPEC);
    public static final SyntheticGuideline DATE = SyntheticGuideline.of("Date", 1, DATE_SPEC);
    public static final SyntheticGuideline LANGUAGE = SyntheticGuideline.of("Language", 1, LANGUAGE_SPEC);
    public static final SyntheticGuideline RESOURCE_TYPE = SyntheticGuideline.of("ResourceType", 1, RESOURCE_TYPE_SPEC);
    public static final SyntheticGuideline ALTERNATE_IDENTIFIER = SyntheticGuideline.of("AlternateIdentifier", 1, ALTERNATE_IDENTIFIER_SPEC);
    public static final SyntheticGuideline RELATED_IDENTIFIER = SyntheticGuideline.of("RelatedIdentifier", 1, RELATED_IDENTIFIER_SPEC);
    public static final SyntheticGuideline SIZE = SyntheticGuideline.of("Size", 1, SIZE_SPEC);
    public static final SyntheticGuideline FORMAT = SyntheticGuideline.of("Format", 1, FORMAT_SPEC);
    public static final SyntheticGuideline VERSION = SyntheticGuideline.of("Version", 1, VERSION_SPEC);
    public static final SyntheticGuideline RIGHTS = SyntheticGuideline.of("Rights", 1, RIGHTS_SPEC);
    public static final SyntheticGuideline DESCRIPTION = SyntheticGuideline.of("Description", 1, DESCRIPTION_SPEC);
    public static final SyntheticGuideline GEOLOCATION = SyntheticGuideline.of("Geolocation", 1, GEOLOCATION_SPEC);

    private static final List<SyntheticGuideline> GUIDELINES = Collections.unmodifiableList(
        Arrays.asList(
            IDENTIFIER,
            CREATOR,
            TITLE,
            PUBLISHER,
            PUBLICATION_YEAR,
            SUBJECT,
            CONTRIBUTOR,
            DATE,
            LANGUAGE,
            RESOURCE_TYPE,
            ALTERNATE_IDENTIFIER,
            RELATED_IDENTIFIER,
            SIZE,
            FORMAT,
            VERSION,
            RIGHTS,
            DESCRIPTION,
            GEOLOCATION
        )
    );

    private static final Map<String, SyntheticGuideline> GUIDELINE_MAP = GUIDELINES.
        stream().
        collect(Collectors.toMap(SyntheticGuideline::getName, (guideline) -> guideline));

    private static final int MAX_SCORE = GUIDELINES.stream().map(SyntheticGuideline::getWeight).reduce(0, Integer::sum);


    public DataArchiveGuidelinesV2Profile() {
        super("OpenAIRE Guidelines for Data Archives Profile v2");
    }

    @Override
    public Collection<? extends Guideline<Document>> guidelines() {
        return GUIDELINES;
    }

    @Override
    public SyntheticGuideline guideline(String guidelineName) {
        return GUIDELINE_MAP.get(guidelineName);
    }

    @Override
    public int maxScore() {
        return MAX_SCORE;
    }
}
