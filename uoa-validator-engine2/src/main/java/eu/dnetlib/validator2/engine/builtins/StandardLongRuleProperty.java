package eu.dnetlib.validator2.engine.builtins;

import eu.dnetlib.validator2.engine.contexts.LongRuleProperty;

public class StandardLongRuleProperty extends StandardRuleProperty implements LongRuleProperty {

    private long longValue;

    StandardLongRuleProperty(String name) {
        super(name);
    }

    @Override
    public long getLongValue() {
        return longValue;
    }

    @Override
    public void setLongValue(long value) {
        this.longValue = value;
        super.setValue(String.valueOf(value));
    }

    @Override
    public void setValue(String value) throws IllegalArgumentException {
        try {
            super.setValue(value);
            longValue = Long.parseLong(getValue());
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Not a valid number for property " + getName());
        }
    }

}
