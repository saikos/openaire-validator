package eu.dnetlib.validator2.engine.contexts;

public interface XMLContextWithVocabulary extends XMLContext, VocabularyContext {
}
