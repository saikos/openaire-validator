package eu.dnetlib.validator2.engine.builtins;

import eu.dnetlib.validator2.engine.Predicates;
import eu.dnetlib.validator2.engine.contexts.TermsProperty;

class StandardTermsProperty extends StandardRuleProperty implements TermsProperty {

    private Predicates.SetOfCaseInsensitiveAllowedValues terms;

    public StandardTermsProperty(String name) {
        super(name);
    }

    @Override
    public void setValue(String value) throws IllegalArgumentException {
        super.setValue(value);
        terms = new Predicates.SetOfCaseInsensitiveAllowedValues(getValue().split(","));
        if (terms.isEmpty()) throw new IllegalArgumentException("Empty value for terms property.");
    }

    @Override
    public boolean termExists(String text) {
        return terms.test(text);
    }

}
