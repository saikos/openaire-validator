package eu.dnetlib.validator2.engine.contexts;

import eu.dnetlib.validator2.engine.RuleProperty;

public interface FieldsProperty extends RuleProperty {

    String[] getXpaths();

}
