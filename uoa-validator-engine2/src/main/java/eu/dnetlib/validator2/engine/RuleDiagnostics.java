package eu.dnetlib.validator2.engine;

public interface RuleDiagnostics<T, R extends Rule<T>> {

    void success(R rule, T t);

    void failure(R rule, T t);

    void error(R rule, T t, Throwable err);

}
