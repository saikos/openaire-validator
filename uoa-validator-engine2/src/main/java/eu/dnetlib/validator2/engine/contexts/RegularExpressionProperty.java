package eu.dnetlib.validator2.engine.contexts;

import eu.dnetlib.validator2.engine.RuleProperty;

import java.util.regex.PatternSyntaxException;

public interface RegularExpressionProperty extends RuleProperty {

    boolean matches(String text) throws IllegalStateException, PatternSyntaxException;
}
