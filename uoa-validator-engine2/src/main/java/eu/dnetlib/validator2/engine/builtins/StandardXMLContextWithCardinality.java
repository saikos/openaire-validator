package eu.dnetlib.validator2.engine.builtins;

import eu.dnetlib.validator2.engine.RuleProperty;
import eu.dnetlib.validator2.engine.contexts.BooleanRuleProperty;
import eu.dnetlib.validator2.engine.contexts.CardinalityContext;
import eu.dnetlib.validator2.engine.contexts.XMLContextWithCardinality;

import java.util.ArrayList;
import java.util.Collection;

public class StandardXMLContextWithCardinality
        extends StandardXMLContext
        implements XMLContextWithCardinality {

    private final StandardLongRuleProperty lowerBound =
            new StandardLongRuleProperty(CardinalityContext.LOWER_BOUND_PROPERTY_NAME);
    private final StandardLongRuleProperty upperBound =
            new StandardLongRuleProperty(CardinalityContext.UPPER_BOUND_PROPERTY_NAME);
    private final StandardBooleanRuleProperty isInclusive =
            new StandardBooleanRuleProperty(CardinalityContext.IS_INCLUSIVE_PROPERTY_NAME);

    @Override
    public StandardLongRuleProperty getLowerBoundProperty() {
        return lowerBound;
    }

    @Override
    public StandardLongRuleProperty getUpperBoundProperty() {
        return upperBound;
    }

    @Override
    public BooleanRuleProperty getIsInclusiveProperty() {
        return isInclusive;
    }

    @Override
    public Collection<RuleProperty> getProperties() {
        Collection<RuleProperty> props = new ArrayList<>(super.getProperties());
        props.add(lowerBound);
        props.add(upperBound);
        props.add(isInclusive);
        return props;
    }

}
