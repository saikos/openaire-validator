package eu.dnetlib.validator2.engine.contexts;

import eu.dnetlib.validator2.engine.RuleProperty;

public interface TermsProperty extends RuleProperty {

    boolean termExists(String term);

}
