package eu.dnetlib.validator2.engine.builtins;

import eu.dnetlib.validator2.engine.Rule;
import eu.dnetlib.validator2.engine.RuleEvaluationException;

public class ConditionalRule<T> extends ForwardingRule<T> {

    public enum WhenConditionFails {
        RULE_FAILS,
        RULE_SUCCEEDS
    }

    protected final Rule<T> conditionRule;
    protected final WhenConditionFails whenConditionFails;

    public ConditionalRule(Rule<T> actualRule, Rule<T> conditionRule, WhenConditionFails whenConditionFails) {
        super(actualRule);
        this.conditionRule = conditionRule;
        this.whenConditionFails = whenConditionFails;
    }

    @Override
    public boolean test(T t) throws RuleEvaluationException {
        if (conditionRule.test(t)) {
            return super.test(t);
        }
        else {
            return whenConditionFails == WhenConditionFails.RULE_SUCCEEDS;
        }
    }
}
