package eu.dnetlib.validator2.engine.contexts;

public interface XMLContextWithCardinality extends XMLContext, CardinalityContext {
}
