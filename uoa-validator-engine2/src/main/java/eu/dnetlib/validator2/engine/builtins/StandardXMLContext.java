package eu.dnetlib.validator2.engine.builtins;

import eu.dnetlib.validator2.engine.RuleProperty;
import eu.dnetlib.validator2.engine.contexts.*;

import java.util.ArrayList;
import java.util.Collection;

public class StandardXMLContext extends StandardRuleContext implements XMLContext {

    private final StandardXPathExpressionProperty xpath =
            new StandardXPathExpressionProperty(XPathExpressionContext.PROPERTY_NAME);
    private final StandardNodeListActionProperty nodeListAction =
            new StandardNodeListActionProperty(NodeListContext.PROPERTY_NAME);


    @Override
    public NodeListActionProperty getNodeListActionProperty() {
        return nodeListAction;
    }

    @Override
    public XPathExpressionProperty getXPathExpressionProperty() {
        return xpath;
    }

    @Override
    public Collection<RuleProperty> getProperties() {
        Collection<RuleProperty> props = new ArrayList<>(super.getProperties());
        props.add(xpath);
        props.add(nodeListAction);
        return props;
    }

}
