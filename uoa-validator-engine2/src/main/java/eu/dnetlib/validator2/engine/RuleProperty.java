package eu.dnetlib.validator2.engine;

public interface RuleProperty extends PropertyDriven {

    String getName();

    String getValue();

    void setValue(String value);
}
