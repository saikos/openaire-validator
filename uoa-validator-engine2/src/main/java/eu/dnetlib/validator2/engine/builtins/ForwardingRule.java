package eu.dnetlib.validator2.engine.builtins;

import eu.dnetlib.validator2.engine.Rule;
import eu.dnetlib.validator2.engine.RuleContext;
import eu.dnetlib.validator2.engine.RuleEvaluationException;

public class ForwardingRule<T> implements Rule<T> {

    protected final Rule<T> rule;

    public ForwardingRule(Rule<T> rule) {
        this.rule = rule;
    }

    @Override
    public <C extends RuleContext> C getContext() {
        return rule.getContext();
    }

    @Override
    public boolean test(T t) throws RuleEvaluationException {
        return rule.test(t);
    }
}
