package eu.dnetlib.validator2.engine.contexts;

import eu.dnetlib.validator2.engine.RuleContext;

public interface RegularExpressionContext extends RuleContext {

    String PROPERTY_NAME = "regexp";

    RegularExpressionProperty getRegularExpressionProperty();

}
