package eu.dnetlib.validator2.engine.contexts;

import org.w3c.dom.NodeList;

import java.util.function.Predicate;

public interface NodeListAction {
    boolean test(NodeList nodes, Predicate<?> predicate) throws IllegalStateException;
}
