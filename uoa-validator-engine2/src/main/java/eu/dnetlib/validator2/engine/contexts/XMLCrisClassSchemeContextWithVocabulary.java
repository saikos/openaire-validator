package eu.dnetlib.validator2.engine.contexts;

public interface XMLCrisClassSchemeContextWithVocabulary extends XMLContextWithVocabulary, CrisClassSchemeContext {
}
