package eu.dnetlib.validator2.engine.builtins;

public class AlwaysSucceedRule<T> extends SimpleRule<T, SimpleContext> {

    public AlwaysSucceedRule() {
        super(new SimpleContext("succeed"), (T t) -> true);
    }
}
