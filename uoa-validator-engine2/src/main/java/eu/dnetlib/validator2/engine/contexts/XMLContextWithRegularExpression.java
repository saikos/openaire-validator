package eu.dnetlib.validator2.engine.contexts;

public interface XMLContextWithRegularExpression extends XMLContext, RegularExpressionContext {
}
