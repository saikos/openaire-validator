package eu.dnetlib.validator2.engine.builtins;

public class AlwaysFailRule<T> extends SimpleRule<T, SimpleContext> {
    public AlwaysFailRule() {
        super(new SimpleContext("fail"), (T t) -> false);
    }
}
