package eu.dnetlib.validator2.engine;

public class Reporter<T, R extends Rule<T>> {

    private final RuleDiagnostics<T, R> diagnostics;

    public Reporter(RuleDiagnostics<T, R> diagnostics) {
        this.diagnostics = diagnostics;
    }

    public void reportSuccessFor(R rule, T t) {
        try {
            diagnostics.success(rule, t);
        }
        catch(Throwable throwable) {
            System.err.println("Failed to report success of applying " + rule + " to value: " + t);
            throwable.printStackTrace();
        }
    }

    public void reportFailureFor(R rule, T t) {
        try {
            diagnostics.failure(rule, t);
        }
        catch(Throwable throwable) {
            System.err.println("Failed to report failure of applying " + rule + " to value: " + t);
            throwable.printStackTrace();
        }
    }

    public void reportErrorFor(R rule, T t, Throwable throwable) {
        try {
            diagnostics.error(rule, t, throwable);
        }
        catch(Throwable throwable1) {
            System.err.println("Failed to report error of applying " + rule + " to value: " + t);
            throwable1.printStackTrace();
        }
    }
}
