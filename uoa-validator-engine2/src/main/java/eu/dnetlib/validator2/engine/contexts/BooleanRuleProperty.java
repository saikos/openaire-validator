package eu.dnetlib.validator2.engine.contexts;

import eu.dnetlib.validator2.engine.RuleProperty;

public interface BooleanRuleProperty extends RuleProperty {

    boolean isTrue();

    void setTrue(boolean isTrue);

}
