package eu.dnetlib.validator2.engine.builtins;

import eu.dnetlib.validator2.engine.RuleEvaluationException;

public class AlwaysErrRule<T> extends SimpleRule<T, SimpleContext> {
    public AlwaysErrRule() {
        super(new SimpleContext("Err"), (T t) -> { throw new RuleEvaluationException(null); });
    }
}
