package eu.dnetlib.validator2.engine.builtins;

import eu.dnetlib.validator2.engine.contexts.BooleanRuleProperty;

class StandardBooleanRuleProperty extends StandardRuleProperty implements BooleanRuleProperty {

    private boolean isTrue;

    StandardBooleanRuleProperty(String name) {
        super(name);
        super.setValue(Boolean.toString(false));
    }

    @Override
    public boolean isTrue() {
        return isTrue;
    }

    @Override
    public void setTrue(boolean isTrue) {
        this.isTrue = isTrue;
        super.setValue(Boolean.toString(isTrue));
    }

    @Override
    public void setValue(String value) throws IllegalArgumentException {
        try {
            super.setValue(value);
            isTrue = Boolean.parseBoolean(getValue());
        } catch (RuntimeException re) {
            throw new IllegalArgumentException("Not a boolean for property " + getName());
        }
    }
}
