package eu.dnetlib.validator2.engine.builtins;

import eu.dnetlib.validator2.engine.Helper;
import eu.dnetlib.validator2.engine.RuleProperty;

import java.util.Map;

public class StandardRuleProperty implements RuleProperty {

    private final String name;
    private String value;

    public StandardRuleProperty(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void readFrom(Map<String, String> map) {
        /*
         TODO Condition has been added to (hopefully) handle properly:
          - optional properties (e.g. termsType in XMLVocabularyRule)
          - private properties / not exposed to or set by the user (e.g. nodeListAction = "length" in XMLCardinalityRule)
          These could/would not be provided from user map
        */
        if (map.containsKey(getName())) {
            setValue(map.get(getName()));
        }
    }

    @Override
    public void writeTo(Map<String, String> map) {
        map.put(name, getValue());
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) throws IllegalArgumentException {
        String newValue = Helper.canonicalize(value);
        if (newValue.isEmpty()) throw new IllegalArgumentException("Empty value for property " + name);
        this.value = newValue;
    }

}
