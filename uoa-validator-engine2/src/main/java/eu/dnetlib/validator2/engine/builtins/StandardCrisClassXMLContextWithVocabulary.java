package eu.dnetlib.validator2.engine.builtins;

import eu.dnetlib.validator2.engine.RuleProperty;
import eu.dnetlib.validator2.engine.contexts.CrisClassSchemeContext;
import eu.dnetlib.validator2.engine.contexts.XMLCrisClassSchemeContextWithVocabulary;

import java.util.ArrayList;
import java.util.Collection;

class StandardCrisClassXMLContextWithVocabulary
        extends StandardXMLContextWithVocabulary
        implements XMLCrisClassSchemeContextWithVocabulary {

    private final StandardRuleProperty schemeId = new StandardRuleProperty(CrisClassSchemeContext.ID_PROPERTY_NAME);

    @Override
    public StandardRuleProperty getCrisClassSchemeIdProperty() {
        return schemeId;
    }

    @Override
    public Collection<RuleProperty> getProperties() {
        Collection<RuleProperty> props = new ArrayList<>(super.getProperties());
        props.add(schemeId);
        return props;
    }

}
