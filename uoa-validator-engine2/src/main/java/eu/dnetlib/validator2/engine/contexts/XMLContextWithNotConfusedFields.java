package eu.dnetlib.validator2.engine.contexts;

public interface XMLContextWithNotConfusedFields extends XMLContext, NotConfusedFieldsContext {
}
