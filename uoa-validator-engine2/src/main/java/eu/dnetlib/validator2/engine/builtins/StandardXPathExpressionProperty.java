package eu.dnetlib.validator2.engine.builtins;

import eu.dnetlib.validator2.engine.Helper;
import eu.dnetlib.validator2.engine.contexts.XPathExpressionProperty;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPathExpression;

public class StandardXPathExpressionProperty extends StandardRuleProperty implements XPathExpressionProperty {

    public StandardXPathExpressionProperty(String name) {
        super(name);
    }

    @Override
    public void setValue(String value) throws IllegalArgumentException {
        XPathExpressionHelper.compile(Helper.canonicalize(value));
        super.setValue(value);
    }

    public NodeList evaluate(Document doc) {
        String xpath = Helper.canonicalize(getValue());
        return XPathExpressionHelper.nodeList(xpath, doc);
    }

}
