package eu.dnetlib.validator2.engine.builtins;

import eu.dnetlib.validator2.engine.Rule;
import eu.dnetlib.validator2.engine.RuleDiagnostics;
import eu.dnetlib.validator2.engine.Status;

public class StandardRuleDiagnostics<T, R extends Rule<T>> implements RuleDiagnostics<T, R> {

    private Status lastReportedStatus;
    private R lastReportedRule;
    private T lastReportedValue;
    private Throwable lastReportedError;

    @Override
    public void success(R rule, T t) {
        setReported(Status.SUCCESS, rule, t, null);
    }

    @Override
    public void failure(R rule, T t) {
        setReported(Status.FAILURE, rule, t, null);
    }

    @Override
    public void error(R rule, T t, Throwable err) {
        setReported(Status.ERROR, rule, t, err);
    }

    private void setReported(Status status, R rule, T value, Throwable error) {
        this.lastReportedStatus = status;
        this.lastReportedRule = rule;
        this.lastReportedValue = value;
        this.lastReportedError = error;
    }

    public Status getLastReportedStatus() {
        return lastReportedStatus;
    }

    public Rule<T> getLastReportedRule() {
        return lastReportedRule;
    }

    public T getLastReportedValue() {
        return lastReportedValue;
    }

    public Throwable getLastReportedError() {
        return lastReportedError;
    }
}
