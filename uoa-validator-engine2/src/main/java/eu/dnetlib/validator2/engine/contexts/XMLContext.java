package eu.dnetlib.validator2.engine.contexts;

import eu.dnetlib.validator2.engine.RuleContext;

public interface XMLContext extends RuleContext, XPathExpressionContext, NodeListContext {
}
