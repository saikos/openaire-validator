package eu.dnetlib.validator2.engine.contexts;

import eu.dnetlib.validator2.engine.RuleContext;

public interface XPathExpressionContext extends RuleContext {

    String PROPERTY_NAME = "xpath";

    XPathExpressionProperty getXPathExpressionProperty();
}
