package eu.dnetlib.validator2.engine.builtins;

import eu.dnetlib.validator2.engine.RuleContext;
import eu.dnetlib.validator2.engine.RuleProperty;

import java.util.Arrays;
import java.util.Collection;

public class StandardRuleContext implements RuleContext {

    private final StandardRuleProperty id =
            new StandardRuleProperty(RuleContext.ID_PROPERTY_NAME);

    @Override
    public StandardRuleProperty getIdProperty() {
        return id;
    }

    @Override
    public Collection<RuleProperty> getProperties() {
        return Arrays.asList(id);
    }
}
