package eu.dnetlib.validator2.engine.builtins;

import eu.dnetlib.validator2.engine.RuleProperty;
import eu.dnetlib.validator2.engine.contexts.VocabularyContext;
import eu.dnetlib.validator2.engine.contexts.XMLContextWithVocabulary;

import java.util.ArrayList;
import java.util.Collection;

class StandardXMLContextWithVocabulary
        extends StandardXMLContext
        implements XMLContextWithVocabulary {

    private final StandardTermsProperty terms    = new StandardTermsProperty(VocabularyContext.TERMS_PROPERTY_NAME);
    private final StandardRuleProperty termsType = new StandardRuleProperty(VocabularyContext.TERMS_TYPE_PROPERTY_NAME);

    @Override
    public StandardTermsProperty getTermsProperty() {
        return terms;
    }

    @Override
    public StandardRuleProperty getTermsTypeProperty() {
        return termsType;
    }

    @Override
    public Collection<RuleProperty> getProperties() {
        Collection<RuleProperty> props = new ArrayList<>(super.getProperties());
        props.add(terms);
        props.add(termsType);
        return props;
    }

}
