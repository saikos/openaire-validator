package eu.dnetlib.validator2.engine.contexts;

import eu.dnetlib.validator2.engine.RuleProperty;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPathExpressionException;

public interface XPathExpressionProperty extends RuleProperty {

    NodeList evaluate(Document doc);

}
