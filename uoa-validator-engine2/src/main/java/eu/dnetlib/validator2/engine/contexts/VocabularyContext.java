package eu.dnetlib.validator2.engine.contexts;

import eu.dnetlib.validator2.engine.RuleContext;
import eu.dnetlib.validator2.engine.RuleProperty;

public interface VocabularyContext extends RuleContext {

    String TERMS_PROPERTY_NAME = "terms";
    String TERMS_TYPE_PROPERTY_NAME = "terms_type";

    TermsProperty getTermsProperty();

    RuleProperty getTermsTypeProperty();

}
