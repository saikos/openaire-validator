package eu.dnetlib.validator2.engine.builtins;

import eu.dnetlib.validator2.engine.contexts.FieldsProperty;

class StandardFieldsProperty extends StandardRuleProperty implements FieldsProperty {

    private String[] xpaths;

    public StandardFieldsProperty(String name) {
        super(name);
    }

    public String[] getXpaths() {
        return xpaths;
    }

    @Override
    public void setValue(String value) throws IllegalArgumentException {
        super.setValue(value);
        xpaths = getValue().split(",");
    }

}
