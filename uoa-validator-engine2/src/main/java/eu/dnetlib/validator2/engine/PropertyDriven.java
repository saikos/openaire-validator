package eu.dnetlib.validator2.engine;

import java.util.Map;

public interface PropertyDriven {

    void readFrom(Map<String, String> map);

    void writeTo(Map<String, String> map);
}
