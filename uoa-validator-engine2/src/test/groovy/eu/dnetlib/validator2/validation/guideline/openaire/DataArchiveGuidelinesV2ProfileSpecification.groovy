package eu.dnetlib.validator2.validation.guideline.openaire

import eu.dnetlib.validator2.engine.Status
import eu.dnetlib.validator2.engine.XMLHelper
import org.w3c.dom.Document
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll


class DataArchiveGuidelinesV2ProfileSpecification extends Specification {

    static Record_1042 = "Record_1042.xml"
    static Record_complete_valid = "Record_complete_valid.xml"
    static Record_complete_invalid = "Record_complete_invalid.xml"
    static Record_title_order = "Record_title_order.xml"
    @Shared DataArchiveGuidelinesV2Profile profile = new DataArchiveGuidelinesV2Profile()
    @Shared Document doc1042 = XMLHelper.parse("/openaireguidelinesV2/www.clarin.si/$Record_1042")
    @Shared Document docCompleteValid = XMLHelper.parse("/openaireguidelinesV2/www.clarin.si/$Record_complete_valid")
    @Shared Document docCompleteInvalid = XMLHelper.parse("/openaireguidelinesV2/www.clarin.si/$Record_complete_invalid")
    @Shared Document docTitleOrder = XMLHelper.parse("/openaireguidelinesV2/www.clarin.si/$Record_title_order")

    @Unroll
    def "validation of guideline: #guidelineName Record #record Status: #resultStatus"() {
        given:
        def guideline = profile.guideline(guidelineName)

        when:
        def result = guideline.validate(doc)

        then:
        result.status() == resultStatus
        result.score() == expectedScore
        result.errors().size() == errorNum
        result.warnings().size() == warningNum

        where:

        doc                 | record                    |guidelineName           || resultStatus     | expectedScore                       | errorNum | warningNum

        doc1042             | Record_1042               |"Identifier"            || Status.SUCCESS   | profile.IDENTIFIER.weight           | 0        | 0
        doc1042             | Record_1042               |"Creator"               || Status.SUCCESS   | profile.CREATOR.weight              | 0        | 4
        doc1042             | Record_1042               |"Title"                 || Status.SUCCESS   | profile.TITLE.weight                | 0        | 1
        doc1042             | Record_1042               |"Publisher"             || Status.SUCCESS   | profile.PUBLISHER.weight            | 0        | 0
        doc1042             | Record_1042               |"PublicationYear"       || Status.SUCCESS   | profile.PUBLICATION_YEAR.weight     | 0        | 0
        doc1042             | Record_1042               |"Subject"               || Status.SUCCESS   | 0                                   | 0        | 3
        doc1042             | Record_1042               |"Contributor"           || Status.SUCCESS   | profile.CONTRIBUTOR.weight          | 0        | 2 //TODO: Not yet implemented in DataArchiveGuidelinesV2Profile
        doc1042             | Record_1042               |"Date"                  || Status.SUCCESS   | profile.DATE.weight                 | 0        | 0
        doc1042             | Record_1042               |"Language"              || Status.SUCCESS   | 0                                   | 0        | 1
        doc1042             | Record_1042               |"ResourceType"          || Status.SUCCESS   | profile.RESOURCE_TYPE.weight        | 0        | 0
        doc1042             | Record_1042               |"AlternateIdentifier"   || Status.SUCCESS   | 0                                   | 0        | 2
        doc1042             | Record_1042               |"RelatedIdentifier"     || Status.SUCCESS   | 0                                   | 0        | 6
        doc1042             | Record_1042               |"Size"                  || Status.SUCCESS   | 0                                   | 0        | 1
        doc1042             | Record_1042               |"Format"                || Status.SUCCESS   | 0                                   | 0        | 1
        doc1042             | Record_1042               |"Version"               || Status.SUCCESS   | 0                                   | 0        | 1
        doc1042             | Record_1042               |"Rights"                || Status.SUCCESS   | profile.RIGHTS.weight               | 0        | 0
        doc1042             | Record_1042               |"Description"           || Status.SUCCESS   | profile.DESCRIPTION.weight          | 0        | 0
        doc1042             | Record_1042               |"Geolocation"           || Status.SUCCESS   | 0                                   | 0        | 4

        docCompleteValid     | Record_complete_valid    |"Identifier"            || Status.SUCCESS   | profile.IDENTIFIER.weight           | 0        | 0
        docCompleteValid     | Record_complete_valid    |"Creator"               || Status.SUCCESS   | profile.CREATOR.weight              | 0        | 0
        docCompleteValid     | Record_complete_valid    |"Title"                 || Status.SUCCESS   | profile.TITLE.weight                | 0        | 1
        docCompleteValid     | Record_complete_valid    |"Publisher"             || Status.SUCCESS   | profile.PUBLISHER.weight            | 0        | 0
        docCompleteValid     | Record_complete_valid    |"PublicationYear"       || Status.SUCCESS   | profile.PUBLICATION_YEAR.weight     | 0        | 0
        docCompleteValid     | Record_complete_valid    |"Subject"               || Status.SUCCESS   | profile.SUBJECT.weight              | 0        | 2
        docCompleteValid     | Record_complete_valid    |"Contributor"           || Status.SUCCESS   | profile.CONTRIBUTOR.weight          | 0        | 2 //TODO: Not yet implemented in DataArchiveGuidelinesV2Profile
        docCompleteValid     | Record_complete_valid    |"Date"                  || Status.SUCCESS   | profile.DATE.weight                 | 0        | 0
        docCompleteValid     | Record_complete_valid    |"Language"              || Status.SUCCESS   | profile.LANGUAGE.weight             | 0        | 0
        docCompleteValid     | Record_complete_valid    |"ResourceType"          || Status.SUCCESS   | profile.RESOURCE_TYPE.weight        | 0        | 0
        docCompleteValid     | Record_complete_valid    |"AlternateIdentifier"   || Status.SUCCESS   | profile.ALTERNATE_IDENTIFIER.weight | 0        | 0
        docCompleteValid     | Record_complete_valid    |"RelatedIdentifier"     || Status.SUCCESS   | profile.RELATED_IDENTIFIER.weight   | 0        | 3
        docCompleteValid     | Record_complete_valid    |"Size"                  || Status.SUCCESS   | profile.SIZE.weight                 | 0        | 0
        docCompleteValid     | Record_complete_valid    |"Format"                || Status.SUCCESS   | profile.FORMAT.weight               | 0        | 0
        docCompleteValid     | Record_complete_valid    |"Version"               || Status.SUCCESS   | profile.VERSION.weight              | 0        | 0
        docCompleteValid     | Record_complete_valid    |"Rights"                || Status.SUCCESS   | profile.RIGHTS.weight               | 0        | 0
        docCompleteValid     | Record_complete_valid    |"Description"           || Status.SUCCESS   | profile.DESCRIPTION.weight          | 0        | 0
        docCompleteValid     | Record_complete_valid    |"Geolocation"           || Status.SUCCESS   | profile.GEOLOCATION.weight          | 0        | 0

        docCompleteInvalid   | Record_complete_invalid  |"Identifier"            || Status.SUCCESS   | profile.IDENTIFIER.weight           | 0        | 1
        docCompleteInvalid   | Record_complete_invalid  |"Creator"               || Status.SUCCESS   | profile.CREATOR.weight              | 0        | 0
        docCompleteInvalid   | Record_complete_invalid  |"Title"                 || Status.SUCCESS   | profile.TITLE.weight                | 0        | 1
        docCompleteInvalid   | Record_complete_invalid  |"Publisher"             || Status.SUCCESS   | profile.PUBLISHER.weight            | 0        | 0
        docCompleteInvalid   | Record_complete_invalid  |"PublicationYear"       || Status.FAILURE   | 0                                   | 1        | 0
        docCompleteInvalid   | Record_complete_invalid  |"Subject"               || Status.SUCCESS   | profile.SUBJECT.weight              | 0        | 2
        docCompleteInvalid   | Record_complete_invalid  |"Contributor"           || Status.SUCCESS   | profile.CONTRIBUTOR.weight          | 0        | 0 //TODO: Not yet implemented in DataArchiveGuidelinesV2Profile
        docCompleteInvalid   | Record_complete_invalid  |"Date"                  || Status.FAILURE   | 0                                   | 1        | 0
        docCompleteInvalid   | Record_complete_invalid  |"Language"              || Status.SUCCESS   | 0                                   | 0        | 1
        docCompleteInvalid   | Record_complete_invalid  |"ResourceType"          || Status.SUCCESS   | profile.RESOURCE_TYPE.weight        | 0        | 0
        docCompleteInvalid   | Record_complete_invalid  |"AlternateIdentifier"   || Status.SUCCESS   | profile.ALTERNATE_IDENTIFIER.weight | 0        | 0
        docCompleteInvalid   | Record_complete_invalid  |"RelatedIdentifier"     || Status.SUCCESS   | profile.RELATED_IDENTIFIER.weight   | 0        | 5 //MA // Failing mandatory attributes. Is SUCCESS-Score expected?
        docCompleteInvalid   | Record_complete_invalid  |"Size"                  || Status.SUCCESS   | profile.SIZE.weight                 | 0        | 0
        docCompleteInvalid   | Record_complete_invalid  |"Format"                || Status.SUCCESS   | profile.FORMAT.weight               | 0        | 0
        docCompleteInvalid   | Record_complete_invalid  |"Version"               || Status.SUCCESS   | profile.VERSION.weight              | 0        | 0
        docCompleteInvalid   | Record_complete_invalid  |"Rights"                || Status.SUCCESS   | profile.RIGHTS.weight               | 0        | 1
        docCompleteInvalid   | Record_complete_invalid  |"Description"           || Status.SUCCESS   | profile.DESCRIPTION.weight          | 0        | 1
        docCompleteInvalid   | Record_complete_invalid  |"Geolocation"           || Status.SUCCESS   | profile.GEOLOCATION.weight          | 0        | 0

        docTitleOrder        | Record_title_order       |"Title"                 || Status.SUCCESS   | profile.TITLE.weight                | 0        | 1

    }


}
