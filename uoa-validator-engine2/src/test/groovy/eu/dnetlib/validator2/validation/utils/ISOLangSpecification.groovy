package eu.dnetlib.validator2.validation.utils

import spock.lang.Specification


class ISOLangSpecification extends Specification {

    def "existence of iso-639-(1|2|3) codes "() {
        expect:
        ISOLangCodes.contains(value) == result

        where:
        value        || result
        "el"         || true
        "gre"        || true
        "invalidTag" || false
        null         || false
    }
}
