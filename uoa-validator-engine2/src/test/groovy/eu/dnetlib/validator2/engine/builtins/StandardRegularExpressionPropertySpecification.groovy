package eu.dnetlib.validator2.engine.builtins

import eu.dnetlib.validator2.engine.contexts.RegularExpressionContext
import spock.lang.Specification

class StandardRegularExpressionPropertySpecification extends Specification {

    static final String TEST_REGEX = "\\d{3}"

    def compiledPatterns = StandardRegularExpressionProperty.compiledPatterns
    def regexProp1 = new StandardRegularExpressionProperty(RegularExpressionContext.PROPERTY_NAME)
    def regexProp2 = new StandardRegularExpressionProperty(RegularExpressionContext.PROPERTY_NAME)

    def "Regular expressions get compiled properly"() {
        given: "set the same regular expression in 2 different instances"
        regexProp1.setValue(TEST_REGEX)
        regexProp2.setValue(TEST_REGEX)
        assert compiledPatterns.isEmpty()

        when: "match (compile) each expression against mock texts"
        regexProp1.matches("mock text 1")
        regexProp1.matches("mock text 2")
        regexProp2.matches("mock text 3")

        then: "the expression is compiled only once"
        compiledPatterns.size() == 1
    }

}
