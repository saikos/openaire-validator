package eu.dnetlib.validator2.engine;

import eu.dnetlib.validator2.validation.XMLApplicationProfile;
import eu.dnetlib.validator2.validation.guideline.Guideline;
import eu.dnetlib.validator2.validation.guideline.openaire.LiteratureGuidelinesV3Profile;
import groovy.xml.DOMBuilder;
import org.w3c.dom.Document;

import java.io.FileReader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Test {

    private static final String[] FILES = new String[] {
        "src/test/resources/openaireguidelinesV3/dia.library.tuc.gr/Record_21811.xml",
        "src/test/resources/openaireguidelinesV3/cris.vtt.fi/01.xml",
        "src/test/resources/openaireguidelinesV3/cris.vtt.fi/02.xml",
        "src/test/resources/openaireguidelinesV3/cris.vtt.fi/03.xml"
    };

    public static void main(String[] args) {
        // String xmlFile = args[0];
        LiteratureGuidelinesV3Profile profile = new LiteratureGuidelinesV3Profile();
        System.out.println("Max score: " + profile.maxScore());
        Map<String, Double> scorePerDoc = new LinkedHashMap<>();
        for (String file: FILES) {
            try {
                System.out.println("Processing " + file);
                Document doc = DOMBuilder.parse(new FileReader(file), false, true, true);
                XMLApplicationProfile.ValidationResult result = profile.validate(file, doc);
                scorePerDoc.put(file, result.score());
                Map<String, Guideline.Result> results = result.results();
                for (Map.Entry entry : results.entrySet()) {
                    System.out.println(entry.getKey() + " = " + entry.getValue());
                }
            }
            catch(Exception e) {
                System.out.println(e.getMessage());
                System.out.println(e);
                e.printStackTrace();
            }
        }
        String printout = scorePerDoc.entrySet().stream().
                map(entry -> entry.getValue() + ": " + entry.getKey()).collect(Collectors.joining("\n"));
        System.out.println(printout);
    }

}
