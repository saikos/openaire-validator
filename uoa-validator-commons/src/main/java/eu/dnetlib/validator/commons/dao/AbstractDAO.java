package eu.dnetlib.validator.commons.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;


public abstract class AbstractDAO<T> implements DAO<T> {

	protected DataSource datasource = null;
	
	protected abstract PreparedStatement getUpdateStatement(T t,Connection con) throws SQLException;
	protected abstract PreparedStatement getInsertStatement(T t,Connection con) throws SQLException;
	protected abstract PreparedStatement getDeleteStatement(int id,Connection con) throws SQLException;
	protected abstract int getLastId() throws SQLException, DaoException;
	
	protected static Logger logger = Logger.getLogger(AbstractDAO.class);
	
	@Override
	public Integer save(T t) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		Integer retId = -1;
		logger.debug("Accessing DB to save/update ");
		try {
			con = getConnection();
			stmt = getUpdateStatement(t,con);
			
			if (stmt.executeUpdate() == 0) {
				stmt.close();
				stmt = getInsertStatement(t,con);
				stmt.executeUpdate();
				retId=this.getLastId();
			}
			
		} catch (SQLException e) {
			logger.error("Error while accessing DB to save/update: ", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to save/update: ", e);
					throw new DaoException(e);
				}
			}
		}
		return retId;
	}

	@Override
	public String delete(int id) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		logger.debug("Accessing DB to delete ");
		try {
			con = getConnection();
			stmt = getDeleteStatement(id,con);
			
			if (stmt.executeUpdate() == 0) {
				stmt.close();
			}
		} catch (Exception e) {
			logger.error("Error while accessing DB to delete: ", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (Exception e) {
					logger.error("Error while accessing DB to delete: ", e);
					throw new DaoException(e);
				}
			}
		}
		return null;
	}

	protected Connection getConnection() throws DaoException {
		try {
			Connection conn = DataSourceUtils.getConnection(datasource);
			return conn;
		} catch (Exception e){
			throw new DaoException(e);
		}
	}

	protected void closeConnection(Connection con) {
		if(con!= null) {
			DataSourceUtils.releaseConnection(con, datasource);
		}
	}


	public DataSource getDatasource() {
		return datasource;
	}
	public void setDatasource(DataSource datasource) {
		this.datasource = datasource;
	}
}
