package eu.dnetlib.validator.commons.dao.users;

import eu.dnetlib.domain.functionality.UserProfile;
import eu.dnetlib.validator.commons.dao.DAO;
import eu.dnetlib.validator.commons.dao.DaoException;


public interface UsersDAO extends DAO<UserProfile> {

	public boolean checkCorrectCreds(String email, String password) throws DaoException;

	public boolean isAdmin(String email) throws DaoException;

	public boolean isRepoAdmin(String email) throws DaoException;
	
	public boolean isActivated(String email) throws DaoException;
	
	public boolean activateUser(String activation_id) throws DaoException;

	public boolean userExists(String email) throws DaoException;
	
	public boolean usernameExists(String username) throws DaoException;

	public void prepareResetPassword(String uuid, String email) throws DaoException;

	public void ResetPassword(String activation_id, String password) throws DaoException;
	
	public UserProfile get(String email) throws DaoException;

}
