package eu.dnetlib.validator.commons.dao.jobs;

import java.util.List;

public class Entry {

	private String name, description;
	private String successes;
	private int weight;
	private List<String> errors;
	private int ruleId;
	private boolean hasErrors, mandatory;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSuccesses() {
		return successes;
	}
	public void setSuccesses(String successes) {
		this.successes = successes;
	}
	public List<String> getErrors() {
		return errors;
	}
	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
	public void setRuleId(int ruleId) {
		this.ruleId = ruleId;
	}
	public int getRuleId() {
		return ruleId;
	}
	public void setHasErrors(boolean hasErrors) {
		this.hasErrors = hasErrors;
	}
	public boolean isHasErrors() {
		return hasErrors;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public int getWeight() {
		return weight;
	}
	public boolean isMandatory() {
		return mandatory;
	}
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	
}
