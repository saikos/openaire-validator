package eu.dnetlib.validator.commons.dao.rules;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import eu.dnetlib.domain.functionality.validator.Rule;
import eu.dnetlib.domain.functionality.validator.RuleSet;
import eu.dnetlib.validator.commons.dao.AbstractDAO;
import eu.dnetlib.validator.commons.dao.DaoException;
import eu.dnetlib.validator.commons.dao.Utilities;

public class RulesetsDAOimpl extends AbstractDAO<RuleSet> implements RulesetsDAO{

	@Override
	public Integer save(RuleSet t) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		Integer retId = -1;
		logger.debug("Accessing DB to save/update Rule");
		try {
			logger.debug("Accessing DB to update Rule");
			con = getConnection();
			String query="UPDATE rulesets SET name=?, description=?, guidelines_acronym = ?, visibility = ?, short_name=? WHERE id=?";
			stmt = con.prepareStatement(query);
			stmt.setString(1, t.getName());
			stmt.setString(2, t.getDescription());
			stmt.setString(3, t.getGuidelinesAcronym());
			String[] data = t.getVisibility().toArray(new String[t.getVisibility().size()]);
			stmt.setArray(4, con.createArrayOf("text", data));
			stmt.setString(5, t.getShortName());
			stmt.setInt(6, t.getId());
			
			if (stmt.executeUpdate() == 0) {
				stmt.close();
				logger.debug("Accessing DB to save RuleSet with name: "+t.getName());
				query="INSERT INTO rulesets(name,description,guidelines_acronym,visibility,short_name) VALUES(?,?,?,?,?)";
				stmt = con.prepareStatement(query);
				stmt.setString(1, t.getName());
				stmt.setString(2, t.getDescription());
				stmt.setString(3, t.getGuidelinesAcronym());
				data = t.getVisibility().toArray(new String[t.getVisibility().size()]);
				stmt.setArray(4, con.createArrayOf("text", data));
				stmt.setString(5, t.getShortName());
				stmt.executeUpdate();
				retId = this.getLastId();
			} else {
				logger.debug("Accessing DB to update RuleSet-done");
				retId=t.getId();
			}
			
			stmt.close();
			logger.debug("Accessing DB to delete ruleSet_has_rules values");
			query="DELETE FROM ruleset_has_rules " + " WHERE ruleset_id=?";
			stmt = con.prepareStatement(query);
			stmt.setInt(1, retId);		
			if (stmt.executeUpdate() == 0) {
				stmt.close();
			}			
			logger.debug("Accessing DB to insert ruleSet_has_rules properties");
		    query="INSERT INTO ruleset_has_rules(ruleset_id, rule_id) VALUES (?,?)";
			stmt= con.prepareStatement(query);
			for (int id : t.getContentRulesIds()) {
				stmt.setInt(1,retId);
				stmt.setInt(2,id);
				stmt.addBatch();
			}
			for (int id : t.getUsageRulesIds()) {
				stmt.setInt(1,retId);
				stmt.setInt(2,id);
				stmt.addBatch();
			}
			stmt.executeBatch();
		    logger.debug("Ruleset_has_rules values inserted/updated");
		    
		} catch (Exception e) {
			logger.error("Error accessing DB to save ruleset.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error accessing DB to save ruleset.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return retId;
	}
	
	@Override
	public RuleSet get(int id) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		RuleSet retSet = null;
		logger.debug("Accessing DB to get RuleSet with id: " + id);
		try {
			con = getConnection();
			String query="SELECT name, description, guidelines_acronym, visibility, short_name FROM rulesets WHERE id=?";
			stmt = con.prepareStatement(query);
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
			if (rs!=null){
				retSet = new RuleSet();
				while (rs.next()) {
					retSet.setName(rs.getString(1));
					retSet.setDescription(rs.getString(2));
					retSet.setGuidelinesAcronym(rs.getString(3));
					List<String> visibility = new ArrayList<String>();
					Array tt = rs.getArray("visibility");
					if (tt != null) {
						String[] ent = (String[])tt.getArray();
						visibility.addAll(Arrays.asList(ent));
					}
					retSet.setVisibility(visibility);
					retSet.setShortName(rs.getString(5));
					retSet.setId(id);
					List <Rule> rules = this.getRulesOfRuleset(id);
					List <Rule> contentRules = new ArrayList<Rule>();
					List <Rule> usageRules = new ArrayList<Rule>();
					Set <Integer> contentRulesIds = new HashSet<Integer>();
					Set <Integer> usageRulesIds = new HashSet<Integer>();
					for (Rule rule : rules) {
						if (rule.getJob_type().equals("content")) {
							contentRules.add(rule);
							contentRulesIds.add(rule.getId());
						} else if (rule.getJob_type().equals("usage")) {
							usageRules.add(rule);
							usageRulesIds.add(rule.getId());
						}
					}
					retSet.setContentRules(contentRules);
					retSet.setUsageRules(usageRules);
					retSet.setContentRulesIds(contentRulesIds);
					retSet.setUsageRulesIds(usageRulesIds);
				}				
			}
		} catch (Exception e) {
			logger.error("Accessing DB to get RuleSet.", e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Accessing DB to get RuleSet.", e);
				}
			}
			closeConnection(con);
		}
		return retSet;
	}
	
	public List<RuleSet> updateRuleSetHasRules() throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null, stmt1 = null;;
		List<RuleSet> retList = new ArrayList<RuleSet>();
		RuleSet retSet = null;
		logger.debug("Accessing DB to get all RuleSets");
		try {
			con = getConnection();
			String query="SELECT name, description, guidelines_acronym, visibility, content_rules, usage_rules, id, short_name FROM rulesets";
			stmt = con.prepareStatement(query);
			rs = stmt.executeQuery();
			if (rs!=null){
				
				while (rs.next()) {
					retSet = new RuleSet();
					retSet.setName(rs.getString(1));
					retSet.setDescription(rs.getString(2));
					retSet.setGuidelinesAcronym(rs.getString(3));
					retSet.setVisibility(Utilities.convertStringToList(rs.getString(4)));
					retSet.setId(rs.getInt(7));
					retSet.setShortName(rs.getString(8));
					retList.add(retSet);
					String query1="INSERT INTO ruleset_has_rules(ruleset_id, rule_id) values (?,?)";
					stmt1 = con.prepareStatement(query1);
					for (String id : Utilities.convertStringToList(rs.getString(5))) {
						stmt1.setInt(1,retSet.getId());
						stmt1.setInt(2,Integer.parseInt(id));
						stmt1.addBatch();
					}
					for (String id : Utilities.convertStringToList(rs.getString(6))) {
						stmt1.setInt(1,retSet.getId());
						stmt1.setInt(2,Integer.parseInt(id));
						stmt1.addBatch();
					}
					stmt1.executeBatch();
				}				
			}

		} catch (Exception e) {
			logger.error("Accessing DB to get all RuleSets.", e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Accessing DB to get all RuleSets.", e);
				}
			}
			closeConnection(con);
		}
		return retList;
	}

	@Override
	public List<RuleSet> getRuleSets() throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		List<RuleSet> retList = new ArrayList<RuleSet>();
		logger.debug("Accessing DB to get all RuleSets");

		try {
			con = getConnection();
			String query="SELECT name, description, guidelines_acronym, visibility, id, short_name FROM rulesets ORDER BY id";
			stmt = con.prepareStatement(query);
			rs = stmt.executeQuery();

			while (rs.next()) {
				RuleSet retSet = new RuleSet();

				retSet.setName(rs.getString(1));
				retSet.setDescription(rs.getString(2));
				retSet.setGuidelinesAcronym(rs.getString(3));
				List<String> visibility = new ArrayList<String>();
				Array tt = rs.getArray("visibility");
				if (tt != null) {
					String[] ent = (String[])tt.getArray();
					visibility.addAll(Arrays.asList(ent));
				}
				retSet.setVisibility(visibility);
				retSet.setId(rs.getInt(5));
				retSet.setShortName(rs.getString(6));
				List <Rule> rules = this.getRulesOfRuleset(retSet.getId());
				List <Rule> contentRules = new ArrayList<Rule>();
				List <Rule> usageRules = new ArrayList<Rule>();
				Set <Integer> contentRulesIds = new HashSet<Integer>();
				Set <Integer> usageRulesIds = new HashSet<Integer>();
				for (Rule rule : rules) {
					if (rule.getJob_type().equals("content")) {
						contentRules.add(rule);
						contentRulesIds.add(rule.getId());
					} else if (rule.getJob_type().equals("usage")) {
						usageRules.add(rule);
						usageRulesIds.add(rule.getId());
					}
				}
				retSet.setContentRules(contentRules);
				retSet.setUsageRules(usageRules);
				retSet.setContentRulesIds(contentRulesIds);
				retSet.setUsageRulesIds(usageRulesIds);
				retList.add(retSet);
			}
		} catch (Exception e) {
			logger.error("Accessing DB to get all RuleSets.", e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Accessing DB to get all RuleSets.", e);
				}
			}
			closeConnection(con);
		}
		return retList;
	}

	@Override
	public List<RuleSet> getRuleSets(String guidelineAcronym) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		List<RuleSet> retList = new ArrayList<RuleSet>();
		logger.debug("Accessing DB to get all RuleSets");

		try {
			con = getConnection();
			String query="SELECT name, description, guidelines_acronym, visibility, id, short_name FROM rulesets WHERE guidelines_acronym=? ORDER BY id";
			stmt = con.prepareStatement(query);
			stmt.setString(1,guidelineAcronym);
			rs = stmt.executeQuery();

			while (rs.next()) {
				RuleSet retSet = new RuleSet();

				retSet.setName(rs.getString(1));
				retSet.setDescription(rs.getString(2));
				retSet.setGuidelinesAcronym(rs.getString(3));
				List<String> visibility = new ArrayList<String>();
				Array tt = rs.getArray("visibility");
				if (tt != null) {
					String[] ent = (String[])tt.getArray();
					visibility.addAll(Arrays.asList(ent));
				}
				retSet.setVisibility(visibility);
				retSet.setId(rs.getInt(5));
				retSet.setShortName(rs.getString(6));
				List <Rule> rules = this.getRulesOfRuleset(retSet.getId());
				List <Rule> contentRules = new ArrayList<Rule>();
				List <Rule> usageRules = new ArrayList<Rule>();
				Set <Integer> contentRulesIds = new HashSet<Integer>();
				Set <Integer> usageRulesIds = new HashSet<Integer>();
				for (Rule rule : rules) {
					if (rule.getJob_type().equals("content")) {
						contentRules.add(rule);
						contentRulesIds.add(rule.getId());
					} else if (rule.getJob_type().equals("usage")) {
						usageRules.add(rule);
						usageRulesIds.add(rule.getId());
					}
				}
				retSet.setContentRules(contentRules);
				retSet.setUsageRules(usageRules);
				retSet.setContentRulesIds(contentRulesIds);
				retSet.setUsageRulesIds(usageRulesIds);
				retList.add(retSet);
			}
		} catch (Exception e) {
			logger.error("Accessing DB to get all RuleSets.", e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Accessing DB to get all RuleSets.", e);
				}
			}
			closeConnection(con);
		}
		return retList;
	}

	public List<Rule> getRulesOfRuleset(int ruleSetId) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		Rule retRule = null;
		List<Rule> retList = null;
		logger.debug("Accessing DB to get All Rules of ruleset");
		try {
			con = getConnection();
			String query="SELECT * FROM rules r, ruleset_has_rules rhs WHERE rhs.rule_id = r.id AND rhs.ruleset_id=?";
			stmt = con.prepareStatement(query);
			stmt.setInt(1, ruleSetId);
			rs = stmt.executeQuery();
			if (rs!=null){
				retList = new ArrayList<Rule>();				
				while (rs.next()) {
					retRule = new Rule();
					retRule.setName(rs.getString("name"));
					retRule.setDescription(rs.getString("description"));
					retRule.setType(rs.getString("type"));
					retRule.setMandatory(rs.getBoolean("mandatory"));
					retRule.setWeight(rs.getInt("weight"));
					retRule.setProvider_information(rs.getString("provider_information"));
					retRule.setId(rs.getInt("id"));
					retRule.setEntity_type(rs.getString("entity_type"));
					retRule.setFor_cris(rs.getBoolean("for_cris"));
					retRule.setJob_type(rs.getString("job_type"));
//					retRule.setConfiguration(this.getProperties(rs.getInt("id")));
					retList.add(retRule);
				}				
			}
			logger.debug("rules: " + retList.size());

		} catch (Exception e) {
			logger.error("Error accessing DB to get All Rules by jobType.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error accessing DB to get All Rules by jobType.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return retList;

	}

	@Override
	protected PreparedStatement getDeleteStatement(int id, Connection con)
			throws SQLException {
		String query="DELETE FROM rulesets WHERE id=?";
		PreparedStatement stmt = con.prepareStatement(query);
		stmt.setInt(1,id);
		return stmt;
	}

	@Override
	protected int getLastId() throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		int retId = -1;
		logger.debug("Accessing DB to get RuleSet's next available id");
		try {
			con = getConnection();
			String query="SELECT currval(pg_get_serial_sequence(?,?)) FROM rulesets";
			stmt = con.prepareStatement(query);
			stmt.setString(1, "rulesets");
			stmt.setString(2, "id");
			
			rs = stmt.executeQuery();
			if (rs!=null){
				rs.next();
				retId=rs.getInt(1);
			}
		} catch (Exception e) {
			logger.error("Error while accessing DB to get RuleSet's next available id.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to get RuleSet's next available id.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return retId;
	}

	@Override
	protected PreparedStatement getUpdateStatement(RuleSet t, Connection con)
			throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PreparedStatement getInsertStatement(RuleSet t, Connection con)
			throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
}
