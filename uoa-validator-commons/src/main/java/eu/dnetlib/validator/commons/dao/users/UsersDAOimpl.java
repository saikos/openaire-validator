package eu.dnetlib.validator.commons.dao.users;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import eu.dnetlib.domain.functionality.UserProfile;
import eu.dnetlib.validator.commons.dao.AbstractDAO;
import eu.dnetlib.validator.commons.dao.DaoException;

public class UsersDAOimpl extends AbstractDAO<UserProfile> implements UsersDAO {


	@Override
	protected PreparedStatement getUpdateStatement(UserProfile t, Connection con)
			throws SQLException {
		String query="UPDATE users SET email=?, institution=?, firstname=?, lastname=? WHERE username=?";
		PreparedStatement stmt = con.prepareStatement(query);
		stmt.setString(1, t.getEmail());
		stmt.setString(2, t.getInstitution());
		stmt.setString(3, t.getFirstname());
		stmt.setString(4, t.getLastname());
		stmt.setString(5, t.getUsername());
		return stmt;
	}

	
	@Override
	protected PreparedStatement getInsertStatement(UserProfile t, Connection con)
			throws SQLException {
		String query="INSERT INTO users(email,password,institution,username,firstname,lastname,activation_id) VALUES(?,?,?,?,?,?,?)";
		PreparedStatement stmt = con.prepareStatement(query);
		stmt.setString(1, t.getEmail());
		stmt.setString(2, t.getPassword());
		stmt.setString(3, t.getInstitution());
		stmt.setString(4, t.getUsername());
		stmt.setString(5, t.getFirstname());
		stmt.setString(6, t.getLastname());
		stmt.setString(7, t.getActivationId());		
		return stmt;
	}

	@Override
	protected PreparedStatement getDeleteStatement(int id, Connection con)
			throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected int getLastId() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public UserProfile get(String email) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		UserProfile retUser = null;
		logger.debug("Accessing DB to get User with email: " + email);
		try {
			con = getConnection();
			String query="SELECT * FROM users WHERE email=? OR username=?";
			stmt = con.prepareStatement(query);
			stmt.setString(1, email);
			stmt.setString(2, email);
			rs = stmt.executeQuery();
			if (rs!=null){
				if (rs.next()) {
					retUser = new UserProfile();
					retUser.setEmail(rs.getString("email"));
					retUser.setUsername(rs.getString("username"));
					retUser.setFirstname(rs.getString("firstname"));
					retUser.setLastname(rs.getString("lastname"));
					retUser.setInstitution(rs.getString("institution"));
					retUser.setActivationId(rs.getString("activation_id"));
				}				
			}
		} catch (Exception e) {
			logger.error("Error Accessing DB to get User.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error Accessing DB to get User.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return retUser;
	
	}
	
	@Override
	public boolean activateUser(String activation_id) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		logger.debug("Accessing DB to activate user");
		try {
			con = getConnection();
			String query="UPDATE users SET activation_id=? WHERE activation_id=?";
			stmt = con.prepareStatement(query);
			stmt.setString(1, null);
			stmt.setString(2, activation_id);
			if (stmt.executeUpdate() > 0){
				return true;
			}
		} catch (Exception e) {
			logger.error("Error while Accessing DB to activate user.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while Accessing DB to activate user.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return false;
	}
	
	@Override
	public UserProfile get(int id) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public boolean checkCorrectCreds(String email, String password) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		logger.debug("Accessing DB to check correct credentials");
		try {
			con = getConnection();
			String query="SELECT * FROM users WHERE email=? AND password=?";
			stmt = con.prepareStatement(query);
			stmt.setString(1,email);
			stmt.setString(2,password);
			rs = stmt.executeQuery();
			if (rs.next()){
				return true;
			}
		} catch (Exception e) {
			logger.error("Error while accessing DB to check correct credentials.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to check correct credentials.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return false;
	}


	@Override
	public boolean isAdmin(String email) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		logger.debug("Accessing DB to check if user is admin");
		try {
			con = getConnection();
			String query="SELECT * FROM admins WHERE username=? AND level=?";
			stmt = con.prepareStatement(query);
			stmt.setString(1,email);
			stmt.setString(2,"master");
			rs = stmt.executeQuery();
			if (rs.next()){
				return true;
			}
		} catch (Exception e) {
			logger.error("Error while accessing DB to check if user is admin.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to check if user is admin.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return false;
	}

	@Override
	public boolean isRepoAdmin(String email) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		logger.debug("Accessing DB to check if user is repoAdmin");
		try {
			con = getConnection();
			String query="SELECT * FROM admins WHERE username=? AND level=?";
			stmt = con.prepareStatement(query);
			stmt.setString(1,email);
			stmt.setString(2,"secondary");
			rs = stmt.executeQuery();
			if (rs.next()){
				return true;
			}
		} catch (Exception e) {
			logger.error("Error while accessing DB to check if user is repoAdmin.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to check if user is repoAdmin.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return false;
	}

	@Override
	public boolean isActivated(String email) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		logger.debug("Accessing DB to check if user is activated");
		try {
			con = getConnection();
			String query="SELECT * FROM users WHERE email=? AND activation_id is null";
			stmt = con.prepareStatement(query);
			stmt.setString(1,email);
//			stmt.setString(2,"NULL");
			rs = stmt.executeQuery();
			if (rs.next()){
				return true;
			}
		} catch (Exception e) {
			logger.error("Error while accessing DB to check if user is activated .", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to check if user is activated .", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return false;
	}


	@Override
	public boolean userExists(String email) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		logger.debug("Accessing DB to check if user "+email+" exists");
		try {
			con = getConnection();
			String query="SELECT * FROM users WHERE email=?";
			stmt = con.prepareStatement(query);
			stmt.setString(1,email);
			rs = stmt.executeQuery();
			if (rs.next()){
				logger.debug("user exists");
				return true;
			}
		} catch (Exception e) {
			logger.error("Error while accessing DB to check if user exists.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to check if user exists.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return false;
	}
	
	@Override
	public boolean usernameExists(String username) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		logger.debug("Accessing DB to check if user "+ username +" exists");
		try {
			con = getConnection();
			String query="SELECT * FROM users WHERE username=?";
			stmt = con.prepareStatement(query);
			stmt.setString(1,username);
			rs = stmt.executeQuery();
			if (rs.next()){
				logger.debug("user exists");
				return true;
			}
		} catch (Exception e) {
			logger.error("Error while accessing DB to check if user exists.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to check if user exists.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return false;
	}


	@Override
	public void prepareResetPassword(String uuid, String email) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		logger.debug("Accessing DB to prepare reset password");
		try {
			con = getConnection();
			String query="UPDATE users SET activation_id=? WHERE email=?";;
			stmt = con.prepareStatement(query);
			stmt.setString(1,uuid);
			stmt.setString(2,email);
			stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Error while accessing DB to prepare reset password.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to prepare reset password.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
	}


	@Override
	public void ResetPassword(String uuid, String password) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		logger.debug("Accessing DB to reset password");
		try {
			con = getConnection();
			String query="UPDATE users SET password=?, activation_id=? WHERE activation_id=?";;
			stmt = con.prepareStatement(query);
			stmt.setString(1,password);
			stmt.setString(2,null);
			stmt.setString(3,uuid);
			stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Error while accessing DB to reset password.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to reset password.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
	}

}
