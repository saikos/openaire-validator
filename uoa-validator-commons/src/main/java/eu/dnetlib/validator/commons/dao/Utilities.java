package eu.dnetlib.validator.commons.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Utilities {

	public static Set<Integer> convertArrayToSet (String[] array){
		Set<Integer> ret= new HashSet<Integer>();
		for (String id : array) {
			ret.add(Integer.parseInt(id));
		}
		return ret;
	}
	
	public static List<String> convertArrayToList (String[] array){
		List<String> ret= new ArrayList<String>();
		for (String id : array) {
			ret.add(id);
		}
		return ret;
	}

	public static Set<Integer> convertListToSet (List<String> list){
		Set<Integer> ret= new HashSet<Integer>();
		for (String id : list) {
			ret.add(Integer.parseInt(id));
		}
		return ret;
	}	
	
	public static String convertListToString(List<String> ruleIds) {
		String rules = "";
		for (int i = 0; i < ruleIds.size() - 1; i++)
			rules += ruleIds.get(i) + ",";

		rules += ruleIds.get(ruleIds.size() - 1);
		return rules;
	}

	public static List<String> convertStringToList(String rules) {
		List<String> ruleIds = new ArrayList<String>();
		String[] arrRules = rules.split(",");
		for (String sRule : arrRules) {
			ruleIds.add(sRule.trim());
		}
		return ruleIds;
	}

	public static String convertSetToString(Set<Integer> ruleIds) {
		String rules = "";
		for (int i  : ruleIds)
			rules += i + ",";
		rules = rules.substring(0,rules.length()-1);
		return rules;
	}
	
	public static Set<Integer> convertStringToSet(String rules) {
		Set<Integer> ruleIds = new HashSet<Integer>();
		String[] arrRules = rules.split(",");
		for (String sRule : arrRules) {
			ruleIds.add(Integer.parseInt(sRule.trim()));
		}
		return ruleIds;
	}
	
	public static String formatTime(long mills) {
		long secs = mills / 1000;
		long mins = secs / 60;
		if (mins == 0)
			return secs + " secs";
		secs = secs % 60;
		long hours = mins / 60;
		if (hours == 0)
			return mins + " mins " + secs + " secs";
		mins = mins % 60;
		return hours + " hours " + mins + " mins " + secs + " secs";
	}	
}