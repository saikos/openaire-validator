package eu.dnetlib.validator.commons.dao.jobs;

import java.util.List;
import java.util.Map;

import eu.dnetlib.domain.functionality.validator.JobResultEntry;
import eu.dnetlib.domain.functionality.validator.StoredJob;
import eu.dnetlib.validator.commons.dao.DAO;
import eu.dnetlib.validator.commons.dao.DaoException;
import eu.dnetlib.validator.commons.dao.rules.RuleStatus;

public interface JobsDAO extends DAO<StoredJob> {

	
	int deleteOld(String date, String period, String jobType) throws DaoException;

	int setJobFinished(int jobId,
			Map<String, Map<Integer, RuleStatus>> scoreMapPerGroupBy,
			String error, Boolean failed, int objsValidated, String validationType) throws DaoException;
	void setTotalJobFinished(int jobId, String error, Boolean failed) throws DaoException;

	void setStatus(int jobId, String status, int recordsTested, String validationType) throws DaoException;


	List<StoredJob> getJobs(String userName, String jobType, Integer offset, Integer limit, String dateFrom, String dateTo) throws DaoException;

	List<StoredJob> getJobs(String userName, String jobType, Integer offset, Integer limit, String dateFrom, String dateTo, String validationStatus) throws DaoException;

	int getJobsTotalNumber(String userName, String jobType) throws DaoException;

	int getJobsTotalNumber(String userName, String jobType, String validationStatus) throws DaoException;

	StoredJob getJobSummary(int jobId, String groupby) throws DaoException;

	List<StoredJob> getJobSummary(List<String> baseUrls, int size) throws DaoException;

	//TODO update
	List<StoredJob> getUncompletedJobs() throws DaoException;
	
	int deleteUncompletedJobs() throws DaoException;
	
	
	void importOldJobs() throws DaoException;
	
	/*
	public Map<String, List<StoredJob>> getJobsOfUserSplitted(String userName);

	public boolean getJobError(int jobId);

	public void deleteOldCompatibilityTestsOnly(String date);
	
	public void deleteJobForRegistration(String activationId);

	public void deleteSemiCompletedRegistrationJobs(String activationId);
	
	public void deleteJobsForRegistration();


	*/

}
