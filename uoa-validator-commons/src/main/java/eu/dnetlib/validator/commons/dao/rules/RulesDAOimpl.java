package eu.dnetlib.validator.commons.dao.rules;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import eu.dnetlib.domain.functionality.validator.CustomProperties;
import eu.dnetlib.domain.functionality.validator.Rule;
import eu.dnetlib.validator.commons.dao.AbstractDAO;
import eu.dnetlib.validator.commons.dao.DaoException;

public class RulesDAOimpl extends AbstractDAO<Rule> implements RulesDAO {

	@Override
	public Integer save(Rule t) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		Integer retId = -1;
		logger.debug("Accessing DB to save/update Rule");
		try {
			logger.debug("Accessing DB to update Rule");
			con = getConnection();
			String query="UPDATE rules SET name=?, description=?, type=?, mandatory=?, weight=?, provider_information=?, entity_type=?, for_cris=? WHERE id=?";
			stmt = con.prepareStatement(query);

			stmt.setString(1, t.getName());
			stmt.setString(2, t.getDescription());
			stmt.setString(3, t.getType());
			stmt.setBoolean(4, t.isMandatory());
			stmt.setInt(5, t.getWeight());
			stmt.setString(6, t.getProvider_information());
			stmt.setString(7, t.getEntity_type());
			stmt.setBoolean(8, t.isFor_cris());
			stmt.setInt(9, t.getId());
			
			if (stmt.executeUpdate() == 0) {
				stmt.close();
				logger.debug("Accessing DB to save Rule with name:"+t.getName()+",desc:"+t.getDescription()+",type:"+t.getType()+",mand:"+t.isMandatory()+",weight:"+t.getWeight()+",pr_inf:"+t.getProvider_information()+",jb_tp:"+t.getJob_type());
				query="INSERT INTO rules(name, description, type, mandatory, weight, provider_information, job_type, entity_type, for_cris) VALUES(?,?,?,?,?,?,?,?,?)";
				stmt = con.prepareStatement(query);
				stmt.setString(1, t.getName());
				stmt.setString(2, t.getDescription());
				stmt.setString(3, t.getType());
				stmt.setBoolean(4, t.isMandatory());
				stmt.setInt(5, t.getWeight());
				stmt.setString(6, t.getProvider_information());
				stmt.setString(7, t.getJob_type());
				stmt.setString(8, t.getEntity_type());
				stmt.setBoolean(9, t.isFor_cris());
				stmt.executeUpdate();
				retId = this.getLastId();
			} else {
				logger.debug("Accessing DB to update Rule-done");
				retId=t.getId();
			}
			
			stmt.close();
			logger.debug("Accessing DB to delete Rule properties");
			query="DELETE FROM rule_properties " + " WHERE rule_id=?";
			stmt = con.prepareStatement(query);
			stmt.setInt(1, retId);		
			if (stmt.executeUpdate() == 0) {
				stmt.close();
			}			
			logger.debug("Accessing DB to insert Rule properties");
			query="INSERT INTO rule_properties(rule_id, property_name, property_value) VALUES(?,?,?)";
		    stmt = con.prepareStatement(query);
			CustomProperties pros = t.getConfiguration();
		    for (Map.Entry<String, String> prop : pros.getProperties().entrySet()) {
			    String key = prop.getKey();
			    logger.debug("Accessing DB to add property:"+key+"-"+pros.getProperty(key));
				stmt.setInt(1, retId);
				stmt.setString(2, key);
				stmt.setString(3, pros.getProperty(key));
				stmt.addBatch();
		    }
		    stmt.executeBatch();
		    logger.debug("Rule + Properties inserted/updated");
		    
		} catch (Exception e) {
			logger.error("Error accessing DB to get save/update Rule.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error accessing DB to get save/update Rule.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return retId;
	}

	@Override
	public String delete(int id) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		logger.debug("Accessing DB to delete Rule");
		try {
			con = getConnection();
			String query="DELETE FROM rules " + " WHERE id=?";
			stmt = con.prepareStatement(query);
			stmt.setInt(1, id);			
			if (stmt.executeUpdate() == 0) {
				stmt.close();
			}
			query="DELETE FROM rule_properties " + " WHERE rule_id=?";
			stmt = con.prepareStatement(query);
			stmt.setInt(1, id);		
			if (stmt.executeUpdate() == 0) {
				stmt.close();
			}			
			
		} catch (Exception e) {
			logger.error("Error accessing DB to delete Rule.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error accessing DB to delete Rule.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return null;
	}

	
	@Override
	protected PreparedStatement getDeleteStatement(int id, Connection con)
			throws SQLException {
		String query="DELETE FROM rules " + " WHERE id=?";
		PreparedStatement stmt = con.prepareStatement(query);
		stmt.setInt(1, id);	
		return stmt;
	}

	@Override
	public List<Rule> getAllRulesByJobType(String jobType) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		Rule retRule = null;
		List<Rule> retList = null;
		logger.debug("Accessing DB to get All Rules by jobType");
		try {
			con = getConnection();
			String query="SELECT * FROM rules WHERE job_type=? ORDER BY name";
			stmt = con.prepareStatement(query);
			stmt.setString(1, jobType);
			rs = stmt.executeQuery();
			if (rs!=null){
				retList = new ArrayList<Rule>();				
				while (rs.next()) {
					retRule = new Rule();
					retRule.setName(rs.getString("name"));
					retRule.setDescription(rs.getString("description"));
					retRule.setType(rs.getString("type"));
					retRule.setMandatory(rs.getBoolean("mandatory"));
					retRule.setWeight(rs.getInt("weight"));
					retRule.setProvider_information(rs.getString("provider_information"));
					retRule.setId(rs.getInt("id"));
					retRule.setEntity_type(rs.getString("entity_type"));
					retRule.setFor_cris(rs.getBoolean("for_cris"));
					retRule.setJob_type(rs.getString("job_type"));
					retRule.setConfiguration(this.getProperties(rs.getInt("id")));
					retList.add(retRule);
				}				
			}


		} catch (Exception e) {
			logger.error("Error accessing DB to get All Rules by jobType.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error accessing DB to get All Rules by jobType.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return retList;

	}

	@Override
	public List<Rule> getAllRulesByJobTypeEntityType(String jobType, String entityType) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		Rule retRule = null;
		List<Rule> retList = null;
		logger.debug("Accessing DB to get All Rules by jobType");
		try {
			con = getConnection();
			String query="SELECT * FROM rules WHERE job_type=? AND entity_type=? ORDER BY name";
			stmt = con.prepareStatement(query);
			stmt.setString(1, jobType);
			stmt.setString(2, entityType);
			rs = stmt.executeQuery();
			if (rs!=null){
				retList = new ArrayList<Rule>();				
				while (rs.next()) {
					retRule = new Rule();
					retRule.setName(rs.getString("name"));
					retRule.setDescription(rs.getString("description"));
					retRule.setType(rs.getString("type"));
					retRule.setMandatory(rs.getBoolean("mandatory"));
					retRule.setWeight(rs.getInt("weight"));
					retRule.setProvider_information(rs.getString("provider_information"));
					retRule.setId(rs.getInt("id"));
					retRule.setEntity_type(rs.getString("entity_type"));
					retRule.setFor_cris(rs.getBoolean("for_cris"));
					retRule.setJob_type(rs.getString("job_type"));
					retRule.setConfiguration(this.getProperties(rs.getInt("id")));
					retList.add(retRule);
				}				
			}


		} catch (Exception e) {
			logger.error("Error accessing DB to get All Rules by jobType.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error accessing DB to get All Rules by jobType.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return retList;

	}

	@Override
	public List<Rule> getAllRules() throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		List<Rule> retList = null;
		logger.debug("Accessing DB to get All Rules");
		try {
			con = getConnection();
			String query="SELECT * FROM rules ORDER BY name";
			stmt = con.prepareStatement(query);
			rs = stmt.executeQuery();
			if (rs!=null){
				retList = new ArrayList<Rule>();				
				while (rs.next()) {
					Rule retRule = new Rule();
					retRule.setName(rs.getString("name"));
					retRule.setDescription(rs.getString("description"));
					retRule.setType(rs.getString("type"));
					retRule.setMandatory(rs.getBoolean("mandatory"));
					retRule.setWeight(rs.getInt("weight"));
					retRule.setProvider_information(rs.getString("provider_information"));
					retRule.setId(rs.getInt("id"));
					retRule.setEntity_type(rs.getString("entity_type"));
					retRule.setFor_cris(rs.getBoolean("for_cris"));
					retRule.setJob_type(rs.getString("job_type"));
					retRule.setConfiguration(this.getProperties(rs.getInt("id")));
					retList.add(retRule);
				}				
			}

			logger.debug("rules: " + retList.size());
		} catch (Exception e) {
			logger.error("Error accessing DB to get All Rule-pairs.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error accessing DB to get All Rule-pairs.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return retList;

	}

	public CustomProperties getProperties(int ruleId) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		CustomProperties pros = null;
//		logger.debug("Accessing DB to get Rule Properties");
		try {
			con = getConnection();
			String query="SELECT property_name, property_value FROM rule_properties WHERE rule_id=?";
			stmt = con.prepareStatement(query);
			stmt.setInt(1, ruleId);
			rs = stmt.executeQuery();
			if (rs!=null){
				pros = new CustomProperties();
				while (rs.next()) {
					pros.setProperty(rs.getString(1), rs.getString(2));
				}				
			}

		} catch (Exception e) {
			logger.error("Accessing DB to get Rule Properties.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Accessing DB to get Rule Properties.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return pros;
	}

	@Override
	public Rule get(int id) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		Rule retRule = null;
//		logger.debug("Accessing DB to get Rule with id: "+id);
		try {
			con = getConnection();
			String query="SELECT name, description, type, mandatory, weight, provider_information, job_type, entity_type, for_cris FROM rules WHERE id=?";
			stmt = con.prepareStatement(query);
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
			if (rs!=null){
				if (rs.next()) {
					retRule = new Rule();
					retRule.setName(rs.getString(1));
					retRule.setDescription(rs.getString(2));
					retRule.setType(rs.getString(3));
					retRule.setMandatory(rs.getBoolean(4));
					retRule.setWeight(rs.getInt(5));
					retRule.setProvider_information(rs.getString(6));
					retRule.setJob_type(rs.getString(7));
					retRule.setEntity_type(rs.getString(8));
					retRule.setFor_cris(rs.getBoolean(9));
					retRule.setId(id);
					retRule.setConfiguration(this.getProperties(id));
					
				}				
			}


		} catch (Exception e) {
			logger.error("Accessing DB to get Rule.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Accessing DB to get Rule.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
//		logger.debug("Accessing DB to get Rule with name: "+retRule.getName());
		return retRule;
	}

	@Override
	protected int getLastId() throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		int retId = -1;
		logger.debug("Accessing DB to get Rule's next available id");
		try {
			con = getConnection();
			String query="SELECT currval(pg_get_serial_sequence(?,?)) FROM rules";
			stmt = con.prepareStatement(query);
			stmt.setString(1, "rules");
			stmt.setString(2, "id");
			
			rs = stmt.executeQuery();
			if (rs!=null){
				rs.next();
				retId=rs.getInt(1);
			}


		} catch (Exception e) {
			logger.error("Error while accessing DB to get Rule's next available id.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to get Rule's next available id.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return retId;
	}
	
	@Override
	protected PreparedStatement getUpdateStatement(Rule t, Connection con)
			throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected PreparedStatement getInsertStatement(Rule t, Connection con)
			throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
