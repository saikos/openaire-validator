package eu.dnetlib.validator.commons.email;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;


public class BugReporter {

	private transient Logger logger = Logger.getLogger(BugReporter.class);
	
	private String ex = null;

	public void report() {
		List<String> recipients = new ArrayList<String>();
		try {
			recipients.add("antleb@di.uoa.gr");
			String message = "An exception has occurred:\n"+ex;
			String subject = "Automatic Bug Report";
//			this.getEmailer().sendMail(recipients, subject, message, false, null);
		} catch (Exception e) {
			logger.error("error sending error report", e);
		}
	}

	public void setEx(String ex) {
		this.ex = ex;
	}

	public String getEx() {
		return ex;
	}
	
	

}
