package eu.dnetlib.validator.commons.dao.jobs;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import eu.dnetlib.domain.functionality.validator.JobForValidation;
import eu.dnetlib.domain.functionality.validator.JobResultEntry;
import eu.dnetlib.domain.functionality.validator.StoredJob;
import eu.dnetlib.validator.commons.dao.AbstractDAO;
import eu.dnetlib.validator.commons.dao.DaoException;
import eu.dnetlib.validator.commons.dao.Utilities;
import eu.dnetlib.validator.commons.dao.rules.RuleStatus;

public class JobsDAOImpl extends AbstractDAO<StoredJob> implements JobsDAO {

	@Override
	public Integer save(StoredJob job) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		Integer retId = -1;
		logger.debug("Accessing DB to save/update Job");
		try {
			logger.debug("Accessing DB to update Job");
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			con = getConnection();
			logger.debug("getting submittedjob updateStatement");
			String query="UPDATE jobs SET validation_type=?, started=to_timestamp(?, 'YYYY-MM-DD HH24:MI:SS'), guidelines=?, content_job_status=?, repo=?, duration=? WHERE id=?";

			stmt = con.prepareStatement(query);
			stmt.setString(1, job.getValidationType());
			stmt.setString(2, sdf.format(cal.getTime()));
			stmt.setString(3, job.getDesiredCompatibilityLevel());
			stmt.setString(4, job.getContentJobStatus());
			stmt.setString(5, job.getBaseUrl());
			stmt.setString(6, job.getDuration());
			stmt.setInt(7, job.getId());
			
			if (stmt.executeUpdate() == 0) {
				stmt.close();
				logger.debug("Accessing DB to save Job");
				query="INSERT INTO jobs(validation_type,started,guidelines,user_email,content_job_status, usage_job_status, repo, duration, rules, records, set, groupby_xpath, metadata_prefix, job_type) VALUES(?,to_timestamp(?, 'YYYY-MM-DD HH24:MI:SS'),?,?,?,?,?,?,?,?,?,?,?,?)";
				stmt = con.prepareStatement(query);
				stmt.setString(1, job.getValidationType());
				stmt.setString(2, sdf.format(cal.getTime()));
//				stmt.setTimestamp(2, getCurrentTimeStamp());
				stmt.setString(3, job.getDesiredCompatibilityLevel());
				stmt.setString(4, job.getUserEmail());
				stmt.setString(5, job.getContentJobStatus());
				stmt.setString(6, job.getUsageJobStatus());
				stmt.setString(7, job.getBaseUrl());
				stmt.setString(8, job.getDuration());
				stmt.setString(9, Utilities.convertSetToString(job.getRules()));
				stmt.setString(10, Integer.toString(job.getRecords()));
				stmt.setString(11, job.getValidationSet());
				stmt.setString(12, job.getGroupByXpath());
				stmt.setString(13, job.getMetadataPrefix());
				stmt.setString(14, job.getJobType());
				stmt.executeUpdate();
				retId = this.getLastId();
			} else {
				logger.debug("Accessing DB to update job-done");
				retId=job.getId();
			}
			
			if (job.isRegistration()) {
				this.storeJobForRegistration(job, retId);
			}
			if (job.isCris()) {
				this.storeJobForCris(job, retId);
			}
			stmt.close();
		    
		} catch (Exception e) {
			logger.error("Error accessing DB to get save/update Rule.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error accessing DB to get save/update Rule.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return retId;
	}

	@Override
	protected PreparedStatement getDeleteStatement(int id, Connection con) throws SQLException {
		String query="DELETE FROM jobs " + " WHERE id=?";
		PreparedStatement stmt = con.prepareStatement(query);
		stmt.setInt(1, id);
		return stmt;
	}
	
	@Override
	public StoredJob get(int id) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		StoredJob retJob = null;
		logger.debug("Accessing DB to get Submitted Job");
		try {
			con = getConnection();
			String query="SELECT j.validation_type, j.content_job_status, j.started, j.ended, j.content_job_score, j.user_email, j.repo, j.duration, rs.short_name, j.error_information, j.groupby_xpath, j.set, j.records, j.metadata_prefix, j.job_type, j.usage_job_status, j.usage_job_score, j.records_tested, j.rules, j.guidelines FROM jobs j, rulesets rs WHERE j.guidelines = rs.guidelines_acronym AND j.id=?";
			stmt = con.prepareStatement(query);
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
			if (rs!=null){
				retJob = new StoredJob();
				while (rs.next()) {
					retJob.setValidationType(rs.getString(1));
					retJob.setContentJobStatus(rs.getString(2));
					retJob.setUsageJobStatus(rs.getString(16));
					retJob.setStarted(rs.getString(3));
					retJob.setEnded(rs.getString(4));
					retJob.setContentJobScore(rs.getInt(5));
					retJob.setUsageJobScore(rs.getInt(17));
					retJob.setUserEmail(rs.getString(6));
					retJob.setBaseUrl(rs.getString(7));
					retJob.setDuration(rs.getString(8));
					retJob.setGuidelinesShortName(rs.getString(9));
					retJob.setDesiredCompatibilityLevel(rs.getString(20));
					retJob.setError(rs.getString(10));
					retJob.setGroupByXpath(rs.getString(11));
					retJob.setValidationSet(rs.getString(12));
					retJob.setRecords(Integer.parseInt(rs.getString(13)));
					retJob.setMetadataPrefix(rs.getString(14));
					retJob.setJobType(rs.getString(15));
					retJob.setRecordsTested(rs.getInt(18));
					retJob.setRules(Utilities.convertStringToSet(rs.getString(19)));
					retJob.setId(id);
				}	
				if (retJob.getJobType().equals("Registration Request")) {
					retJob.setRegistration(true);
					this.getJobForRegistration(retJob);
				} 
				if (retJob.getDesiredCompatibilityLevel().contains("cris")) {
					retJob.setCris(true);
					this.getJobForCris(retJob);
				}
			}


		} catch (Exception e) {
			logger.error("Error while accessing DB to get Submitted Job.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to get Submitted Job.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return retJob;

	}
	
	@Override
	public void importOldJobs() throws DaoException {
		Map<Integer,Integer> idsMap = new HashMap<Integer, Integer>(); 
		List<StoredJob> oldJobs = this.getOldJobs(idsMap);
		this.updateOldJobResults(idsMap);
		this.insertJobsBatch(oldJobs);
		this.updateNeededTables();
	}
		
	private void updateNeededTables() throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		logger.debug("Accessing DB to update other tables");
		try {
			con = getConnection();
			String query="INSERT INTO job_results (SELECT * FROM job_results_old WHERE job_id IN (SELECT j1.id as id FROM jobs_old j1, jobs_old j2 "
					+ "WHERE j1.validation_type = 'OAI Content' AND j2.validation_type = 'OAI Usage' AND j2.id = (j1.id+1) AND j1.repo = j2.repo AND j1.guidelines = j2.guidelines AND j1.activation_id = j2.activation_id AND j1.user = j2.user "
					+ "ORDER BY j1.id));";
			stmt = con.prepareStatement(query);
		    stmt.executeUpdate();
		    
		    stmt.close();
		    query="INSERT INTO tasks (SELECT * FROM tasks_old WHERE job_id IN (SELECT j1.id as id FROM jobs_old j1, jobs_old j2 "
					+ "WHERE j1.validation_type = 'OAI Content' AND j2.validation_type = 'OAI Usage' AND j2.id = (j1.id+1) AND j1.repo = j2.repo AND j1.guidelines = j2.guidelines AND j1.activation_id = j2.activation_id AND j1.user = j2.user "
					+ "ORDER BY j1.id));";
			stmt = con.prepareStatement(query);
		    stmt.executeUpdate();
		    
		    stmt.close();
		    query="INSERT INTO jobs_filtered_scores (SELECT * FROM jobs_filtered_scores_old WHERE job_id IN (SELECT j1.id as id FROM jobs_old j1, jobs_old j2 "
					+ "WHERE j1.validation_type = 'OAI Content' AND j2.validation_type = 'OAI Usage' AND j2.id = (j1.id+1) AND j1.repo = j2.repo AND j1.guidelines = j2.guidelines AND j1.activation_id = j2.activation_id AND j1.user = j2.user "
					+ "ORDER BY j1.id));";
			stmt = con.prepareStatement(query);
		    stmt.executeUpdate();

		    
		} catch (Exception e) {
			logger.error("Error while accessing DB to update jobs results batch.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to update jobs results batch.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}

	}
	private void updateOldJobResults(Map<Integer, Integer> idsMap) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		logger.debug("Accessing DB to update jobs results batch");
		try {
			con = getConnection();
			String query="UPDATE job_results_old SET job_id = ? WHERE job_id = ?";
			stmt = con.prepareStatement(query);
			for (Map.Entry<Integer, Integer> entry : idsMap.entrySet()) {
				stmt.setInt(1, entry.getValue());
				stmt.setInt(2, entry.getKey());
				stmt.addBatch();
		    }
		    stmt.executeBatch();
			
		} catch (Exception e) {
			logger.error("Error while accessing DB to update jobs results batch.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to update jobs results batch.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}

	}

	private void insertJobsBatch(List<StoredJob> jobs) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		logger.debug("Accessing DB to insert jobs batch ");
		try {
			con = getConnection();
			String query="INSERT INTO jobs(validation_type,started,ended,guidelines,user_email,content_job_status, usage_job_status, repo, duration, rules, records, records_tested, set, groupby_xpath, metadata_prefix, job_type, usage_job_score, content_job_score,id,error_information) VALUES(?,to_timestamp(?, 'YYYY-MM-DD HH24:MI:SS'),to_timestamp(?, 'YYYY-MM-DD HH24:MI:SS'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			stmt = con.prepareStatement(query);
			for (StoredJob job : jobs) {
				stmt.setString(1, job.getValidationType());
				stmt.setString(2, job.getStarted());
				stmt.setString(3, job.getEnded());
				stmt.setString(4, job.getDesiredCompatibilityLevel());
				stmt.setString(5, job.getUserEmail());
				stmt.setString(6, job.getContentJobStatus());
				stmt.setString(7, job.getUsageJobStatus());
				stmt.setString(8, job.getBaseUrl());
				stmt.setString(9, job.getDuration());
				stmt.setString(10, Utilities.convertSetToString(job.getRules()));
				stmt.setInt(11, job.getRecords());
				stmt.setInt(12, job.getRecordsTested());
				stmt.setString(13, job.getValidationSet());
				stmt.setString(14, job.getGroupByXpath());
				stmt.setString(15, job.getMetadataPrefix());
				stmt.setString(16, job.getJobType());
				stmt.setInt(17, job.getUsageJobScore());
				stmt.setInt(18, job.getContentJobScore());
				stmt.setInt(19, job.getId());
				stmt.setString(20, job.getError());
				stmt.addBatch();
		    }
		    stmt.executeBatch();
			
		} catch (Exception e) {
			logger.error("Error while accessing DB to insert jobs batch.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to insert jobs batch.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
	}
	private List<StoredJob> getOldJobs(Map<Integer, Integer> idsMap) throws DaoException{	
		
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		List<StoredJob> retList = new ArrayList<StoredJob>();
		logger.debug("Accessing DB to get old jobs..");
		try {
			con = getConnection();
			String query= "SELECT j1.id as id, j2.id as id_to_update,'CU' as validation_type, j1.score as content_job_score, j2.score as usage_job_score, j1.status as content_job_status, j2.status as usage_job_status,  j1.guidelines as guidelines, j1.started as started, j2.started,j1.ended as ended,  j1.user as user_email, j1.repo as repo, j1.duration as duration, j1.set as set, j1.groupby_xpath as grouby_xpath, j1.metadata_prefix as metadata_prefix, j1.job_type as job_type, j1.records as records, j1.error_information as error_information, concat_ws(',', j1.rules, j2.rules) as rules,  (select total from job_results_old where job_id = j1.id ORDER BY job_id DESC LIMIT 1 ) as records_tested"
					+ " FROM jobs_old j1, jobs_old j2"
					+ " WHERE j1.validation_type = 'OAI Content' AND j2.validation_type = 'OAI Usage' AND j2.id = (j1.id+1) AND j1.repo = j2.repo AND j1.guidelines = j2.guidelines AND j1.activation_id = j2.activation_id AND j1.user = j2.user"
					+ " ORDER BY j1.id;";
			stmt = con.prepareStatement(query);

			rs = stmt.executeQuery();
				while (rs.next()) {
					StoredJob retJob = new StoredJob();
					retJob.setValidationType(rs.getString("validation_type"));
					retJob.setContentJobStatus(rs.getString("content_job_status"));
					retJob.setUsageJobStatus(rs.getString("usage_job_status"));
					retJob.setStarted(rs.getString("started"));
					retJob.setEnded(rs.getString("ended"));
					retJob.setContentJobScore(rs.getInt("content_job_score"));
					retJob.setUsageJobScore(rs.getInt("usage_job_score"));
					retJob.setUserEmail(rs.getString("user_email"));
					retJob.setBaseUrl(rs.getString("repo"));
					retJob.setDuration(rs.getString("duration"));
					retJob.setDesiredCompatibilityLevel(rs.getString("guidelines"));
					retJob.setId(rs.getInt("id"));
					retJob.setError(rs.getString("error_information"));
					retJob.setJobType(rs.getString("job_type"));
					retJob.setMetadataPrefix(rs.getString("metadata_prefix"));
					retJob.setRules(Utilities.convertStringToSet(rs.getString("rules")));
					retJob.setRecordsTested(rs.getInt("records_tested"));
					retJob.setValidationSet(rs.getString("set"));
					if (retJob.getContentJobScore() == 0)
						retJob.setRecordsTested(0);
					retJob.setRecords(rs.getInt("records"));
					if (retJob.getJobType().equals("Registration Request")) {
						retJob.setRegistration(true);
//						this.getJobForRegistration(retJob);
					} 
					idsMap.put(rs.getInt("id_to_update"),rs.getInt("id"));
					retList.add(retJob);
				}				

		} catch (Exception e) {
			logger.error("Error while accessing DB to .", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to get Submitted Jobs of user.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return retList;
	}


	@Override
	public List<StoredJob> getJobs(String userName, String jobType, Integer offset, Integer limit, String dateFrom, String dateTo) throws DaoException {
		return this.getJobs(userName, jobType, offset, limit, dateFrom, dateTo, null);
	}

	@Override
	public List<StoredJob> getJobs(String userName, String jobType, Integer offset, Integer limit, String dateFrom, String dateTo, String validationStatus) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		List<StoredJob> retList = new ArrayList<StoredJob>();
		logger.debug("Accessing DB to get Submitted Jobs of user: "+userName + " and type: " + jobType);
		try {
			con = getConnection();
			String beginQuery="SELECT j.validation_type, j.content_job_status, j.usage_job_status, j.content_job_score, j.usage_job_score, j.started, j.ended, j.user_email, j.repo, j.duration, r.short_name, j.guidelines, j.id, j.error_information, j.job_type, j.records_tested FROM jobs j, rulesets r WHERE j.guidelines = r.guidelines_acronym"; 
			String endQuery=" ORDER BY j.id DESC";
			if (userName != null) {
				beginQuery += " AND j.user_email=?";
			}
			if (jobType != null) {
				beginQuery += " AND j.job_type=?";
			}
			if (dateFrom != null && dateTo != null) {
				beginQuery += " AND j.started BETWEEN date(?) AND date(?)";
			}
			if (validationStatus != null) {
				if (validationStatus.equalsIgnoreCase("ongoing"))
					beginQuery += " AND (j.content_job_status='ongoing' OR j.usage_job_status='ongoing')";
				else if (validationStatus.equalsIgnoreCase("successful"))
					beginQuery += " AND ((j.validation_type='CU' AND j.content_job_status='finished' AND j.usage_job_status='finished' AND j.content_job_score::integer > '50' AND j.usage_job_score::integer > '50')" +
							" OR (j.validation_type='C' AND j.content_job_status='finished' AND j.usage_job_status='none' AND j.content_job_score::integer > '50')" +
							" OR (j.validation_type='U' AND j.content_job_status='none' AND j.usage_job_status='finished' AND j.usage_job_score::integer > '50'))";
				else if (validationStatus.equalsIgnoreCase("failed"))
					beginQuery += " AND ((j.validation_type='CU' AND j.content_job_status='finished' AND j.usage_job_status='finished' AND (j.content_job_score::integer <= '50' OR j.usage_job_score::integer <= '50'))" +
							" OR (j.validation_type='C' AND j.content_job_status='finished' AND j.usage_job_status='none' AND j.content_job_score::integer <= '50')" +
							" OR (j.validation_type='U' AND j.content_job_status='none' AND j.usage_job_status='finished' AND j.usage_job_score::integer <= '50'))";
			}
			if (offset != null) {
				endQuery += " OFFSET ?";
			}
			if (limit != null) {
				endQuery += " LIMIT ?";
			}
			String finalQuery = beginQuery + endQuery;
			logger.debug("finalQuery" + finalQuery);
			stmt = con.prepareStatement(finalQuery);
			int index = 1;
			if (userName != null) {
				stmt.setString(index++, userName);
			}
			if (jobType != null) {
				stmt.setString(index++, jobType);
			}
			if ((dateFrom != null && dateTo != null)) {
				stmt.setString(index++, dateFrom);
				stmt.setString(index++, dateTo);
			}
			if (offset != null) {
				stmt.setInt(index++, offset);
			}
			if (limit != null) {
				stmt.setInt(index++, limit);
			}
				
			rs = stmt.executeQuery();
			if (rs!=null){
				while (rs.next()) {
					StoredJob retJob = new StoredJob();
					retJob.setValidationType(rs.getString("validation_type"));
					retJob.setContentJobStatus(rs.getString("content_job_status"));
					retJob.setUsageJobStatus(rs.getString("usage_job_status"));
					retJob.setStarted(rs.getString("started"));
					retJob.setEnded(rs.getString("ended"));
					retJob.setContentJobScore(rs.getInt("content_job_score"));
					retJob.setUsageJobScore(rs.getInt("usage_job_score"));
					retJob.setUserEmail(rs.getString("user_email"));
					retJob.setBaseUrl(rs.getString("repo"));
					retJob.setDuration(rs.getString("duration"));
					retJob.setGuidelinesShortName(rs.getString("short_name"));
					retJob.setDesiredCompatibilityLevel(rs.getString("guidelines"));
					retJob.setId(rs.getInt("id"));
					retJob.setError(rs.getString("error_information"));
					retJob.setJobType(rs.getString("job_type"));
					retJob.setRecordsTested(rs.getInt("records_tested"));
					if (retJob.getJobType().equals("Registration Request")) {
						retJob.setRegistration(true);
						this.getJobForRegistration(retJob);
					} 
					if (retJob.getDesiredCompatibilityLevel().contains("cris")) {
						retJob.setCris(true);
						this.getJobForCris(retJob);
					}
                    if (validationStatus != null) {
                        retJob.setValidationStatus(validationStatus);
                    } else {
                        if (retJob.getContentJobStatus().equals("ongoing") || retJob.getUsageJobStatus().equals("ongoing")) {
                            retJob.setValidationStatus("ongoing");
                        } else if ((retJob.getValidationType().equals("CU") && retJob.getContentJobStatus().equals("finished") && retJob.getUsageJobStatus().equals("finished") && retJob.getContentJobScore() > 50 && retJob.getUsageJobScore() > 50)
                                || (retJob.getValidationType().equals("C") && retJob.getContentJobStatus().equals("finished") && retJob.getUsageJobStatus().equals("none") && retJob.getContentJobScore() > 50)
                                || (retJob.getValidationType().equals("U") && retJob.getContentJobStatus().equals("none") && retJob.getUsageJobStatus().equals("finished") && retJob.getUsageJobScore() > 50)) {
                            retJob.setValidationStatus("successful");
                        } else if ((retJob.getValidationType().equals("CU") && retJob.getContentJobStatus().equals("finished") && retJob.getUsageJobStatus().equals("finished") && (retJob.getContentJobScore() <= 50 || retJob.getUsageJobScore() <= 50))
                                || (retJob.getValidationType().equals("C") && retJob.getContentJobStatus().equals("finished") && retJob.getUsageJobStatus().equals("none") && retJob.getContentJobScore() <= 50)
                                || (retJob.getValidationType().equals("U") && retJob.getContentJobStatus().equals("none") && retJob.getUsageJobStatus().equals("finished") && retJob.getUsageJobScore() <= 50) ) {
                            retJob.setValidationStatus("failed");
                        }
                    }
					retList.add(retJob);
				}				
			}


		} catch (Exception e) {
			logger.error("Error while accessing DB to .", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to get Submitted Jobs of user.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return retList;
	}

	@Override
	public int getJobsTotalNumber(String userName, String jobType) throws DaoException {
		return this.getJobsTotalNumber(userName, jobType, null);
	}

	@Override
	public int getJobsTotalNumber(String userName, String jobType, String validationStatus) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		int sum = 0;
		logger.debug("Accessing DB to get total number of Jobs of user: "+userName + " and type: " + jobType);
		try {
			con = getConnection();
			String query="SELECT count(*) as count FROM jobs j WHERE";
			if (userName != null) {
				query += " user_email=?";
			}

			if (jobType != null) {
				if (userName != null)
					query += " AND";
				query += " job_type=?";
			}
			if (validationStatus != null) {
				if ((userName != null) || (jobType != null))
					query += " AND";
				if (validationStatus.equalsIgnoreCase("ongoing"))
					query += " (j.content_job_status='ongoing' OR j.usage_job_status='ongoing')";
				else if (validationStatus.equalsIgnoreCase("successful"))
					query += " ((j.validation_type='CU' AND j.content_job_status='finished' AND j.usage_job_status='finished' AND j.content_job_score::integer > '50' AND j.usage_job_score::integer > '50')" +
							" OR (j.validation_type='C' AND j.content_job_status='finished' AND j.usage_job_status='none' AND j.content_job_score::integer > '50')" +
							" OR (j.validation_type='U' AND j.content_job_status='none' AND j.usage_job_status='finished' AND j.usage_job_score::integer > '50'))";
				else if (validationStatus.equalsIgnoreCase("failed"))
					query += " ((j.validation_type='CU' AND j.content_job_status='finished' AND j.usage_job_status='finished' AND (j.content_job_score::integer <= '50' OR j.usage_job_score::integer <= '50'))" +
							" OR (j.validation_type='C' AND j.content_job_status='finished' AND j.usage_job_status='none' AND j.content_job_score::integer <= '50')" +
							" OR (j.validation_type='U' AND j.content_job_status='none' AND j.usage_job_status='finished' AND j.usage_job_score::integer <= '50'))";
			}
			logger.debug(query);
			stmt = con.prepareStatement(query);
			int index = 1;
			if (userName != null) {
				stmt.setString(index++, userName);
			}
			if (jobType != null) {
				stmt.setString(index++, jobType);
			}
			rs = stmt.executeQuery();
			if (rs!=null){
				if (rs.next()) {
					sum = rs.getInt("count");
				}				
			}

		} catch (Exception e) {
			logger.error("Error while accessing DB to get total number of Jobs of user.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to get total number of Jobs of user.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return sum;
	}

	
	@Override
	public void setTotalJobFinished(int jobId, String error, Boolean failed) throws DaoException {
		logger.debug("Accessing DB to set Total Submitted Job: "+jobId+" as finished");

		Connection con = null;
		PreparedStatement stmt = null;
		StoredJob job = this.get(jobId);
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		try {
			
			if (failed) 
				error = "Server responded with error while issuing the request to retrieve the records.";
			con = getConnection();
			String query="UPDATE jobs SET ended=to_timestamp(?, 'YYYY-MM-DD HH24:MI:SS'), duration=?, error_information=? WHERE id=?";
			stmt = con.prepareStatement(query);
			
			String endedStr = sdf.format(cal.getTime());
			Date started = sdf.parse(job.getStarted());
			Date ended = sdf.parse(endedStr);
			long diff = ended.getTime() - started.getTime();
			Calendar cDiff = Calendar.getInstance();
			cDiff.setTimeInMillis(diff);
			long mills = cDiff.getTimeInMillis();
			
			stmt.setString(1, endedStr);
			stmt.setString(2, Utilities.formatTime(mills));
			if (error!=null) {
//				stmt.setString(1, "finished-failed");
				stmt.setString(3, error);
//				stmt.setString(4, "an error occured");
				logger.debug("error: "+error);
			} else {
				stmt.setString(3, "no errors");
			}
			
			stmt.setInt(4, jobId);

			stmt.executeUpdate();
			stmt.close();
		} catch (Exception e) {
			logger.error("Error while accessing DB to set Submitted Job as finished.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to set Submitted Job as finished.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
	}

	@Override
	public int setJobFinished(int jobId, Map<String,Map<Integer,RuleStatus>> scoreMapPerGroupBy, String error, Boolean failed, int objsValidated, String validationType) throws DaoException {
		int retScore = 0;
		Connection con = null;
		PreparedStatement stmt = null, stmt1 = null, stmt2 = null;
		
		logger.debug("Accessing DB to set Submitted Job: "+jobId+" as finished");
		try {
			if (failed) 
				error = "Server responded with error while issuing the request to retrieve the records.";
			con = getConnection();
			String query = null;
			if (validationType.equalsIgnoreCase("content"))	{
				query="UPDATE jobs SET " + validationType + "_job_status=? , records_tested=? WHERE id=?";
				stmt = con.prepareStatement(query);
				stmt.setString(1, "finished");
				stmt.setInt(2, objsValidated);
				stmt.setInt(3, jobId);
			}
			else if (validationType.equalsIgnoreCase("usage")) {	
				query="UPDATE jobs SET " + validationType + "_job_status=? WHERE id=?";
				stmt = con.prepareStatement(query);
				stmt.setString(1, "finished");
				stmt.setInt(2, jobId);
			}

			int res = stmt.executeUpdate();
			stmt.close();
			
			logger.debug("job lines updated: "+res + " for query: " +query);
			
			if (error == null) {
				logger.debug("Inserting job results..");
				query="INSERT INTO job_results(rule_id,job_id,total,successes,groupby) VALUES(?,?,?,?,?)";
				stmt2 = con.prepareStatement(query);
				stmt1 = con.prepareStatement("INSERT INTO jobs_filtered_scores(job_id,groupby,score) VALUES(?,?,?)");
				for (Entry<String, Map<Integer, RuleStatus>> entry : scoreMapPerGroupBy.entrySet()) {
					logger.debug("| JOB_ID | RULEID | TOTAL | SUCCESS | GROUPBY |");
					String groupBy = entry.getKey();
					Map<Integer, RuleStatus> scoreMapPerRule = entry.getValue();
					float score = 0;
					float weights = 0 ;
					for (Entry<Integer, RuleStatus> entry2 : scoreMapPerRule.entrySet()) {
						Integer ruleId = entry2.getKey();
						RuleStatus ruleSt = entry2.getValue();
						float perc = 0;
						if (ruleSt.isMandatory()) {
							weights += ruleSt.getWeight();
							perc=100*ruleSt.getSuccess()/ruleSt.getTotal();
							score += perc * ruleSt.getWeight();
						}
						logger.debug("| " + jobId + " | " + ruleId + " | " + ruleSt.getTotal() + " | " + ruleSt.getSuccess() + " | " +groupBy + " |");
						stmt2.setInt(1, ruleId);
						stmt2.setInt(2, jobId);
						stmt2.setInt(3, ruleSt.getTotal());
						stmt2.setInt(4, ruleSt.getSuccess());
						stmt2.setString(5, groupBy);
						stmt2.addBatch();
					}
					score /= weights;
					logger.debug("score: " + score);
					logger.debug("scoreInt: " + (int)Math.ceil(score));
					if (groupBy.equals("all")) {
						query="UPDATE jobs SET " + validationType + "_job_score=? WHERE id=?";
						stmt = con.prepareStatement(query);
						stmt.setString(1, Integer.toString((int)Math.ceil(score)));
						stmt.setInt(2, jobId);
						if (stmt.executeUpdate()>0 )
							logger.debug("Job scored successfully set with value: " + Integer.toString((int)Math.ceil(score)));
						else
							logger.debug("error while setting score");
						stmt.close();
						retScore = (int)Math.ceil(score);
					} else {
						stmt1.setInt(1, jobId);
						stmt1.setString(2, groupBy );
						stmt1.setInt(3, (int)Math.ceil(score));
						stmt1.addBatch();
					}
				}
				int result = stmt2.executeBatch().length;
				logger.debug("job results inserted: "+result);
				result = stmt1.executeBatch().length;
				logger.debug("filtered scores inserted: "+result);
			} else {
//				logger.debug("errors..");
				query="UPDATE jobs SET " + validationType + "_job_score=? WHERE id=?";
				stmt2 = con.prepareStatement(query);
				stmt2.setString(1, "0");
				stmt2.setInt(2, jobId);
				stmt2.executeUpdate();
				stmt2.close();
				retScore = 0;
			}			
		} catch (Exception e) {
			logger.error("Error while accessing DB to set Submitted Job as finished: ", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to set Submitted Job as finished: ", e);
					throw new DaoException(e);
				}
			}
			if (stmt1 != null) {
				try {
					stmt1.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to set Submitted Job as finished: ", e);
					throw new DaoException(e);
				}
			}
			if (stmt2 != null) {
				try {
					stmt2.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to set Submitted Job as finished: ", e);
					throw new DaoException(e);
				}
			}

			closeConnection(con);
		}
		return retScore;
	}

	@Deprecated
	public boolean getJobError(int jobId) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		logger.debug("Accessing DB to see if a Submitted Job has an error: ");
		try {
			con = getConnection();
			String query="SELECT error_information FROM jobs WHERE id=?";
			stmt = con.prepareStatement(query);
			stmt.setInt(1, jobId);
			rs = stmt.executeQuery();
			rs.next();
			if (rs.getString(1) != null && rs.getString(1).equals("no errors"))
				return false;				


			

		} catch (Exception e) {
			logger.error("Error while accessing DB to see if a Submitted Job has an error.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to see if a Submitted Job has an error.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return true;
	}

	@Override
	protected int getLastId() throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		int retId = -1;
		logger.debug("Accessing DB to get Submitted Job's next available id");
		try {
			con = getConnection();
			String query="SELECT currval(pg_get_serial_sequence(?,?)) FROM jobs";
			stmt = con.prepareStatement(query);
			stmt.setString(1, "jobs");
			stmt.setString(2, "id");
			
			rs = stmt.executeQuery();
			if (rs!=null){
				rs.next();
				retId=rs.getInt(1);
			}
		} catch (Exception e) {
			logger.error("Error while accessing DB to get Submitted Job's next available id.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to get Submitted Job's next available id.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return retId;


	}

	@Override
	public int deleteOld(String date, String period, String jobType) throws DaoException {
		int jobsDeleted = 0;
		Connection con = null;
		PreparedStatement stmt = null;
		logger.debug("Accessing DB to delete old Jobs");
		String interval = "<";
		if (period.equalsIgnoreCase("older"))
			interval = "<";
		else if (period.equalsIgnoreCase("newer"))
			interval = ">";
		if (period.equalsIgnoreCase("exact"))
			interval = "=";
		
		try {
			con = getConnection();
			logger.debug("Deleting jobs..");
			if (jobType != null) {
				String query="DELETE FROM jobs WHERE date(started)" + interval + "to_timestamp(?, 'YYYY-MM-DD') AND job_type = ?";
				stmt = con.prepareStatement(query);
				stmt.setString(1, date);
				stmt.setString(2, jobType);
			} else {
				String query="DELETE FROM jobs WHERE date(started)" + interval + "to_timestamp(?, 'YYYY-MM-DD')";
				stmt = con.prepareStatement(query);
				stmt.setString(1, date);
			}
			jobsDeleted = stmt.executeUpdate();
		} catch (Exception e) {
			logger.error("Error while Accessing DB to delete old Jobs.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while Accessing DB to delete old Jobs.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return jobsDeleted;
	}

	@Override
	public void setStatus(int jobId, String status, int recordsTested, String validationType) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		
		logger.debug("Accessing DB to set Submitted Job status");
		try {
			con = getConnection();
			String query="UPDATE jobs SET " + validationType + "_job_status=? , records_tested=? WHERE id=?";
			stmt = con.prepareStatement(query);
			stmt.setString(1, status);
			stmt.setInt(2, recordsTested);
			stmt.setInt(3, jobId);
			if (stmt.executeUpdate() == 0)
				stmt.close();
		} catch (Exception e) {
			logger.error("Error while accessing DB to set Submitted Job status.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to set Submitted Job status.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}

	}

	@Override
	public StoredJob getJobSummary(int jobId, String groupby) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		StoredJob retJob = this.get(jobId);
		logger.debug("Accessing DB to get all Jobs entries with jobId:"+jobId+" and groupBy:"+groupby);
		try {
			con = getConnection();
			String query="select rules.name, rules.description, rules.weight, rules.mandatory, total, successes, rules.id, rules.job_type from job_results join rules on job_results.rule_id = rules.id where (job_results.job_id=? AND job_results.groupby=?) order by rules.name";
			stmt = con.prepareStatement(query);
			stmt.setInt(1, jobId);
			stmt.setString(2, groupby);
			rs = stmt.executeQuery();
			if (rs!=null){
				List<JobResultEntry> resultEntries = new ArrayList<JobResultEntry>();
				while (rs.next()) {
					JobResultEntry retEntry = new JobResultEntry();
					retEntry.setName(rs.getString(1));
					retEntry.setDescription(rs.getString(2));
					retEntry.setWeight(rs.getInt(3));
					retEntry.setMandatory(rs.getBoolean(4));
					retEntry.setRuleId(rs.getInt(7));
					retEntry.setType(rs.getString("job_type"));
					retEntry.setErrors(this.getValidationErrors(jobId, retEntry.getRuleId()));
					int total = rs.getInt(5);
					int successes = rs.getInt(6);
					if (rs.getInt(5) > 0)
						retEntry.setSuccesses(successes + "/" + total);
					else
						retEntry.setSuccesses("--");
					if (successes < total)
						retEntry.setHasErrors(true);
					else
						retEntry.setHasErrors(false);
					
					resultEntries.add(retEntry);
				}		
				retJob.setResultEntries(resultEntries);
			}
		} catch (Exception e) {
			logger.error("Error while Accessing DB to get all Jobs entries.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while Accessing DB to get all Jobs entries.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		retJob.setFilteredScores(this.getScoresPerGroupBy(jobId));
		retJob.getFilteredScores().put("all", retJob.getContentJobScore());
		return retJob;
	}

	@Override
	public List<StoredJob> getJobSummary(List<String> baseUrls, int size) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		List<JobResultEntry> resultEntries = new ArrayList<JobResultEntry>();
		StringBuilder builder = new StringBuilder();
		List<StoredJob> storedJobs = new ArrayList<>();
		for(int i = 0; i < baseUrls.size(); i++ ) {
			builder.append("?,");
		}
		Map<Integer, List<JobResultEntry>> results = new HashMap<>();
		try {
			con = getConnection();
			String query="select rules.name, rules.description, rules.weight, rules.mandatory, total, successes, rules.id, rules.job_type, job_results.job_id from job_results join rules on job_results.rule_id = rules.id join (SELECT * FROM jobs WHERE jobs.repo in ("+ builder.deleteCharAt( builder.length() -1 ).toString() +") LIMIT "+size+ " ) as foo on foo.id = job_results.job_id order by job_results.job_id ";
			stmt = con.prepareStatement(query);
			int index = 1;
			for( String o : baseUrls) {
				stmt.setString(  index++, o ); // or whatever it applies
			}
			rs = stmt.executeQuery();
			logger.info("Final query: " + stmt.toString());
			JobResultEntry retEntry = new JobResultEntry();
			if (rs!=null){
				int oldId = 0;
				while (rs.next()) {
					if(oldId!=rs.getInt("job_id")){
						logger.debug("Changing job_id from " + oldId + " to "+ rs.getInt("job_id"));
						results.put(oldId,resultEntries);
						oldId=rs.getInt("job_id");
						resultEntries = new ArrayList<>();
					}
					retEntry.setName(rs.getString(1));
					retEntry.setDescription(rs.getString(2));
					retEntry.setWeight(rs.getInt(3));
					retEntry.setMandatory(rs.getBoolean(4));
					retEntry.setRuleId(rs.getInt(7));
					retEntry.setType(rs.getString("job_type"));
					retEntry.setErrors(this.getValidationErrors(rs.getInt("job_id"), retEntry.getRuleId()));
					int total = rs.getInt(5);
					int successes = rs.getInt(6);
					if (rs.getInt(5) > 0)
						retEntry.setSuccesses(successes + "/" + total);
					else
						retEntry.setSuccesses("--");
					if (successes < total)
						retEntry.setHasErrors(true);
					else
						retEntry.setHasErrors(false);

					resultEntries.add(retEntry);
				}
				if(oldId!=0)
					results.put(oldId,resultEntries);

				logger.debug("Totals job_idS:"+results.size());
				if(results.get(0)!=null)
					results.remove(0);
				for(Map.Entry<Integer,List<JobResultEntry>> entry : results.entrySet()){
					logger.debug("Creating StoredJob object for " + entry.getKey());
					StoredJob storedJob = this.get(entry.getKey());
					storedJob.setResultEntries(entry.getValue());
					storedJob.setFilteredScores(this.getScoresPerGroupBy(entry.getKey()));
					storedJob.getFilteredScores().put("all", storedJob.getContentJobScore());
					storedJobs.add(storedJob);
				}
			}
		} catch (Exception e) {
			logger.error("Error while Accessing DB to get all Jobs entries.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while Accessing DB to get all Jobs entries.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return storedJobs;
	}

	public List<String> getValidationErrors(int jobId, int ruleId) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		List<String> retList = null; 
		logger.debug("Accessing DB to get Validation Errors of JobId " + jobId + " and RuleId " + ruleId);
		try {
			con = getConnection();
			String query="SELECT record_identifier FROM tasks WHERE job_id=? AND rule_id=? AND success=? LIMIT 30";
			stmt = con.prepareStatement(query);
			stmt.setInt(1, jobId);
			stmt.setInt(2, ruleId);
			stmt.setBoolean(3, false);
			rs = stmt.executeQuery();
			if (rs!=null){
				retList = new ArrayList<String>();
				
				while (rs.next()) {
//					if (!rs.getBoolean(1))
					retList.add(rs.getString(1));
				}				
			}
		} catch (Exception e) {
			logger.error("Accessing DB to get Validation Errors of a JobId and RuleId.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Accessing DB to get Validation Errors of a JobId and RuleId.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return retList;


	}
	
	@Override
	public List<StoredJob> getUncompletedJobs() throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		StoredJob retJob = null;
		List<StoredJob> retList = null;
		logger.debug("Accessing DB to get uncompleted jobs");
		try {
			con = getConnection();
			String query="SELECT * FROM jobs j, rulesets r WHERE j.guidelines = r.guidelines_acronym AND (usage_job_status=? OR content_job_status=?)"; 

			stmt = con.prepareStatement(query);
			stmt.setString(1,"ongoing");
			stmt.setString(2,"ongoing");
			
			rs = stmt.executeQuery();
			if (rs!=null){
				retList = new ArrayList<StoredJob>();				
				while (rs.next()) {
					retJob = new StoredJob();
					retJob.setValidationType(rs.getString("validation_type"));
					retJob.setContentJobStatus(rs.getString("content_job_status"));
					retJob.setUsageJobStatus(rs.getString("usage_job_status"));
					retJob.setStarted(rs.getString("started"));
					retJob.setEnded(rs.getString("ended"));
					retJob.setContentJobScore(rs.getInt("content_job_score"));
					retJob.setUsageJobScore(rs.getInt("usage_job_score"));
					retJob.setUserEmail(rs.getString("user_email"));
					retJob.setBaseUrl(rs.getString("repo"));
					retJob.setDuration(rs.getString("duration"));
					retJob.setGuidelinesShortName(rs.getString("short_name"));
					retJob.setDesiredCompatibilityLevel(rs.getString("guidelines"));
					retJob.setId(rs.getInt("id"));
					retJob.setError(rs.getString("error_information"));
					retJob.setJobType(rs.getString("job_type"));
					retJob.setRecordsTested(rs.getInt("records_tested"));
					retJob.setGroupByXpath(rs.getString("groupby_xpath"));
					retJob.setValidationSet(rs.getString("set"));
					retJob.setRecords(Integer.parseInt(rs.getString("records")));
					retJob.setMetadataPrefix(rs.getString("metadata_prefix"));
					retJob.setRules(Utilities.convertStringToSet(rs.getString("rules")));
					
					if (retJob.getJobType().equals("Registration Request")) {
						retJob.setRegistration(true);
						this.getJobForRegistration(retJob);
					} 
					if (retJob.getDesiredCompatibilityLevel().contains("cris")) {
						retJob.setCris(true);
						this.getJobForCris(retJob);
					}
					retList.add(retJob);
				}				
			}

		} catch (Exception e) {
			logger.error("Error while accessing DB get uncompleted jobs .", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to get uncompleted jobs.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return retList;

	}

	@Override
	public int deleteUncompletedJobs() throws DaoException {
		int jobsDeleted = 0;
		Connection con = null;
		PreparedStatement stmt = null;
		logger.debug("Accessing DB to delete uncompleted Jobs and their tasks");
		try {
			con = getConnection();
			logger.debug("Deleting jobs..");
			String query="DELETE FROM jobs WHERE content_job_status=? OR usage_job_status=?";
			stmt = con.prepareStatement(query);
			stmt.setString(1, "ongoing");
			stmt.setString(2, "ongoing");
			jobsDeleted = stmt.executeUpdate();
			logger.debug("Finish Deleting jobs..");
		} catch (Exception e) {
			logger.error("Error while Accessing DB to delete uncompleted Jobs and their tasks.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while Accessing DB to delete uncompleted Jobs and their tasks.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return jobsDeleted;
	}
	
	
	/*
	@Override
	public void deleteSemiCompletedRegistrationJobs(String activationId) {
		Connection con = null;
		PreparedStatement stmt = null;
		logger.debug("Accessing DB to delete uncompleted Jobs and their tasks");
		try {
			con = getConnection();
			logger.debug("Deleting Semi Completed Registration Jobs..");
			String query="DELETE FROM jobs WHERE activation_id=?";
			stmt = con.prepareStatement(query);
			stmt.setString(1, activationId);
			stmt.executeUpdate();
			logger.debug("Finish Deleting jobs..");
		} catch (Exception e) {
			logger.error("Error while Accessing DB to delete uncompleted Jobs and their tasks.", e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while Accessing DB to delete uncompleted Jobs and their tasks.", e);
				}
			}
		}

	}
	*/

	public Map<String, Integer> getScoresPerGroupBy(int jobId) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		Map<String, Integer> retMap = null;
		
		logger.debug("Accessing DB to get filtered score for jobId:"+jobId);
		try {
			con = getConnection();
			String query="SELECT groupBy, score FROM jobs_filtered_scores WHERE job_id=?";
			stmt = con.prepareStatement(query);
			stmt.setInt(1, jobId);
			rs = stmt.executeQuery();
			if (rs!=null){
				logger.debug("filtered scores found");
				retMap = new HashMap<String, Integer>();
				while (rs.next()) {
					logger.debug("score: " + rs.getInt(2) + " groupBy: " + rs.getString(1) );
					retMap.put(rs.getString(1), rs.getInt(2));
				}				
			}
		} catch (Exception e) {
			logger.error("Error while Accessing DB to get filtered scores .", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while Accessing DB to get filtered scores .", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return retMap;

	}	
	
	private void storeJobForRegistration(JobForValidation job, int jobId) throws DaoException {
		
		Connection con = null;
		PreparedStatement stmt = null;
		
		logger.debug("Accessing DB to store job values for Registration");
		try {
			con = getConnection();
			String query="INSERT INTO jobs_for_registration (activation_id, official_name, admin_emails, datasource_id, interface_id, interface_id_old, repo_type, update_existing, job_id) VALUES (?,?,?,?,?,?,?,?,?)";
			stmt = con.prepareStatement(query);
			stmt.setString(1, job.getActivationId());
			stmt.setString(2, job.getOfficialName());
			String[] data = job.getAdminEmails().toArray(new String[job.getAdminEmails().size()]);
			stmt.setArray(3, con.createArrayOf("text", data));
			stmt.setString(4, job.getDatasourceId());
			stmt.setString(5, job.getInterfaceId());
			stmt.setString(6, job.getInterfaceIdOld());
			stmt.setString(7, job.getRepoType());
			stmt.setBoolean(8, job.isUpdateExisting());
			stmt.setInt(9, jobId);
		
			stmt.executeUpdate();

		} catch (Exception e) {
			logger.error("Error while Accessing DB to store job values for Registration .", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error Accessing DB to store job values for Registration .", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		
	}
	
	private void storeJobForCris(JobForValidation job, int jobId) throws DaoException {
		
		Connection con = null;
		PreparedStatement stmt = null;
		
		logger.debug("Accessing DB to store job values for cris");
		try {
			con = getConnection();
			String query="INSERT INTO jobs_for_cris (job_id, entities, referential_checks) VALUES (?,?,?)";
			stmt = con.prepareStatement(query);
			stmt.setInt(1, jobId);
//			String[] data = job.getSelectedCrisEntities().toArray(new String[job.getSelectedCrisEntities().size()]);
			stmt.setArray(2, con.createArrayOf("text", job.getSelectedCrisEntities().toArray()));
			stmt.setBoolean(3, job.isCrisReferentialChecks());
			stmt.executeUpdate();

		} catch (Exception e) {
			logger.error("Error while Accessing DB to store job values for cris .", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error Accessing DB to store job values for cris .", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		
	}
	
	private void getJobForCris(StoredJob job) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		
		logger.debug("Accessing DB to get job values for cris for jobId: " + job.getId());
		try {
			con = getConnection();
			String query="SELECT * FROM jobs_for_cris WHERE job_id = ?";
			stmt = con.prepareStatement(query);
			stmt.setInt(1, job.getId());
		
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				logger.debug("cris job found");
				Set<String> entities = new HashSet<String>();
				Array tt = rs.getArray("entities");
				String[] ent = (String[])tt.getArray();
				
//				String[] ret = (String[])rs.getArray("entities").getArray();
				
				entities.addAll(Arrays.asList(ent));
				logger.debug("Entities size: " + entities);
				job.setSelectedCrisEntities(entities);
				
				job.setCrisReferentialChecks(rs.getBoolean("referential_checks"));
				
				logger.debug("ref checks: " + job.isCrisReferentialChecks());
			}	

		} catch (Exception e) {
			logger.error("Error while Accessing DB to get job values for cris .", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error Accessing DB to get job values for cris .", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
	}
	
	private void getJobForRegistration(StoredJob job) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null;
		
//		logger.debug("Accessing DB to get job values for Registration");
		try {
			con = getConnection();
			String query="SELECT * FROM jobs_for_registration WHERE job_id = ?";
			stmt = con.prepareStatement(query);
			stmt.setInt(1, job.getId());
		
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				job.getAdminEmails().addAll(Arrays.asList((String[])rs.getArray("admin_emails").getArray()));
				job.setDatasourceId(rs.getString("datasource_id"));
				job.setInterfaceId(rs.getString("interface_id"));
				job.setInterfaceIdOld(rs.getString("interface_id_old"));
				job.setActivationId(rs.getString("activation_id"));
				job.setRepoType(rs.getString("repo_type"));
				job.setOfficialName(rs.getString("official_name"));
				job.setUpdateExisting(rs.getBoolean("update_existing"));
			}	

		} catch (Exception e) {
			logger.error("Error while Accessing DB to get job values for Registration .", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error Accessing DB to get job values for Registration .", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
	}
	
	@Override
	protected PreparedStatement getUpdateStatement(StoredJob t,
			Connection con) throws SQLException {
		logger.debug("getting submittedjob updateStatement");
		String query="UPDATE jobs SET validation_type=?, started=to_timestamp(?, 'YYYY-MM-DD HH24:MI:SS'), guidelines=?, content_job_status=?, repo=?, duration=? WHERE id=?";

		PreparedStatement stmt = con.prepareStatement(query);
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		stmt.setString(1, t.getValidationType());
		stmt.setString(2, sdf.format(cal.getTime()));
//		stmt.setTimestamp(2, getCurrentTimeStamp());
		stmt.setString(3, t.getDesiredCompatibilityLevel());
		stmt.setString(4, t.getContentJobStatus());
		stmt.setString(5, t.getBaseUrl());
		stmt.setString(6, t.getDuration());
		stmt.setInt(7, t.getId());

		return stmt;
	}

	@Override
	protected PreparedStatement getInsertStatement(StoredJob t,
			Connection con) throws SQLException {
		logger.debug("getting submittedjob insertStatement");
		Calendar cal = Calendar.getInstance();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		String query="INSERT INTO jobs(validation_type,started,guidelines,user_email,content_job_status, usage_job_status, repo, duration, rules, records, set, groupby_xpath, metadata_prefix, job_type) VALUES(?,to_timestamp(?, 'YYYY-MM-DD HH24:MI:SS'),?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement stmt = con.prepareStatement(query);
		stmt.setString(1, t.getValidationType());
		stmt.setString(2, sdf.format(cal.getTime()));
//		stmt.setTimestamp(2, getCurrentTimeStamp());
		stmt.setString(3, t.getDesiredCompatibilityLevel());
		stmt.setString(4, t.getUserEmail());
		stmt.setString(5, t.getContentJobStatus());
		stmt.setString(6, t.getUsageJobStatus());
		stmt.setString(7, t.getBaseUrl());
		stmt.setString(8, t.getDuration());
		stmt.setString(9, Utilities.convertSetToString(t.getRules()));
		stmt.setString(10, Integer.toString(t.getRecords()));
		stmt.setString(11, t.getValidationSet());
		stmt.setString(12, t.getGroupByXpath());
		stmt.setString(13, t.getMetadataPrefix());
		stmt.setString(14, t.getJobType());
		
		return stmt;
	}

	
	@SuppressWarnings("unused")
	private static java.sql.Timestamp getCurrentTimeStamp() {
		 
		java.util.Date today = new java.util.Date();
		return new java.sql.Timestamp(today.getTime());
	 
	}
		
}
