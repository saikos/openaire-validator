package eu.dnetlib.validator.commons.dao.tasks;


public class TaskStored {

	private String status, started, ended, valObjId, recordIdentifier, recordUrl;
	private int jobId, ruleId;
	private boolean success;
//	private RuleD rule;
	
	public String getRecordUrl() {
		return recordUrl;
	}
	public void setRecordUrl(String recordUrl) {
		this.recordUrl = recordUrl;
	}
	public String getStatus() { 
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public boolean getSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getStarted() {
		return started;
	}
	public void setStarted(String started) {
		this.started = started;
	}
	public String getEnded() {
		return ended;
	}
	public void setEnded(String ended) {
		this.ended = ended;
	}
	
	public String getValObjId() {
		return valObjId;
	}
	public void setValObjId(String valObjId) {
		this.valObjId = valObjId;
	}
	public String getRecordIdentifier() {
		return recordIdentifier;
	}
	public void setRecordIdentifier(String recordIdentifier) {
		this.recordIdentifier = recordIdentifier;
	}
	public int getJobId() {
		return jobId;
	}
	public void setJobId(int jobId) {
		this.jobId = jobId;
	}
	public int getRuleId() {
		return ruleId;
	}
	public void setRuleId(int ruleId) {
		this.ruleId = ruleId;
	}
	
}
