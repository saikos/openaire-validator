package eu.dnetlib.validator.commons.dao.rules;

import java.util.List;

import eu.dnetlib.domain.functionality.validator.RuleSet;
import eu.dnetlib.validator.commons.dao.DAO;
import eu.dnetlib.validator.commons.dao.DaoException;

/**
 * 
 * @author nickonas
 *
 */
public interface RulesetsDAO extends DAO<RuleSet> {

	/**
	 * 
	 * @return
	 */
	public List<RuleSet> getRuleSets() throws DaoException;

	/**
	 *
	 * @return
	 */
	public List<RuleSet> getRuleSets(String guidelineAcronym) throws DaoException;

	
}
