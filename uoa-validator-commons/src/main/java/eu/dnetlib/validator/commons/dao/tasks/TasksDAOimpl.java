package eu.dnetlib.validator.commons.dao.tasks;

import eu.dnetlib.validator.commons.dao.AbstractDAO;
import eu.dnetlib.validator.commons.dao.DaoException;
import eu.dnetlib.validator.commons.dao.rules.RuleStatus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class TasksDAOimpl extends AbstractDAO<TaskStored> implements TasksDAO{

	@Override
	protected PreparedStatement getUpdateStatement(TaskStored t, Connection con)
			throws SQLException {
		String query="UPDATE tasks set status=?, success=?, started=?, ended=?, record_url=? WHERE job_id=? AND rule_id=? AND record_identifier=?";
		PreparedStatement stmt = con.prepareStatement(query);
//		logger.debug("getting taskStored updateStatement");
		stmt.setString(1, t.getStatus());
		stmt.setBoolean(2, t.getSuccess());
		stmt.setString(3, t.getStarted());
		stmt.setString(4, t.getEnded());
		stmt.setString(5, t.getRecordUrl());	
		stmt.setInt(6, t.getJobId());
		stmt.setInt(7, t.getRuleId());
		stmt.setString(8, t.getRecordIdentifier());
		return stmt;
	}

	@Override
	protected PreparedStatement getInsertStatement(TaskStored t, Connection con) throws SQLException {
		String query="INSERT INTO tasks(status, success, started, ended, job_id, rule_id, record_identifier, record_url) VALUES(?,?,?,?,?,?,?,?)";
		PreparedStatement stmt = con.prepareStatement(query);
//		logger.debug("getting taskStored insertStatement");
		stmt.setString(1, t.getStatus());
		stmt.setBoolean(2, t.getSuccess());
		stmt.setString(3, t.getStarted());
		stmt.setString(4, t.getEnded());
		stmt.setInt(5, t.getJobId());
		stmt.setInt(6, t.getRuleId());
		stmt.setString(7, t.getRecordIdentifier());
		stmt.setString(8, t.getRecordUrl());		
	
		return stmt;
	}

	@Override
	protected PreparedStatement getDeleteStatement(int id, Connection con)
			throws SQLException {
		String query="DELETE FROM tasks WHERE job_id=?";
		PreparedStatement stmt = con.prepareStatement(query);
		stmt.setInt(1, id);
		return stmt;
	}


	@Override
	public List<TaskStored> getTasksOfJob(int id) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		TaskStored retTask = null;
		List<TaskStored> retList = null; 
		logger.debug("Accessing DB to get all Tasks of Job");
		try {
			con = getConnection();
			String query="SELECT status, success, started, ended, rule_id, record_identifier, record_url FROM tasks WHERE job_id=?";
			stmt = con.prepareStatement(query);
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
			if (rs!=null){
				retList = new ArrayList<TaskStored>();
				
				while (rs.next()) {
					retTask = new TaskStored();
					retTask.setStatus(rs.getString(1));
					retTask.setSuccess(rs.getBoolean(2));
					retTask.setStarted(rs.getString(3));
					retTask.setEnded(rs.getString(4));
					retTask.setRuleId(rs.getInt(5));
					retTask.setRecordIdentifier(rs.getString(6));
					retTask.setRecordUrl(rs.getString(7));
					retTask.setJobId(id);
					retList.add(retTask);
				}				
			}
		} catch (Exception e) {
			logger.error("Accessing DB to get all Tasks of Job.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Accessing DB to get all Tasks of Job.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return retList;

	}
	
	@Override
	public TaskStored get(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getValidationErrors(int jobId, int ruleId) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		List<String> retList = null; 
		logger.debug("Accessing DB to get Validation Errors of JobId " + jobId + " and RuleId " + ruleId);
		try {
			con = getConnection();
			String query="SELECT record_identifier FROM tasks WHERE job_id=? AND rule_id=? AND success=? LIMIT 30";
			stmt = con.prepareStatement(query);
			stmt.setInt(1, jobId);
			stmt.setInt(2, ruleId);
			stmt.setBoolean(3, false);
			rs = stmt.executeQuery();
			if (rs!=null){
				retList = new ArrayList<String>();
				
				while (rs.next()) {
//					if (!rs.getBoolean(1))
					retList.add(rs.getString(1));
				}				
			}
		} catch (Exception e) {
			logger.error("Accessing DB to get Validation Errors of a JobId and RuleId.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Accessing DB to get Validation Errors of a JobId and RuleId.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return retList;


	}

	@Override
	public List<String> getDistinctTasksOfJob(int jobId) throws DaoException {
		List<String> retList = null;
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		logger.debug("Accessing DB to get Distinct Rule ids of Tasks");
		try {
			con = getConnection();
			String query="SELECT distinct rule_id from tasks where job_id=?";
			stmt = con.prepareStatement(query);
			stmt.setInt(1, jobId);
			rs = stmt.executeQuery();
			if (rs!=null){
				retList = new ArrayList<String>();
				
				while (rs.next()) {
					retList.add(Integer.toString(rs.getInt(1)));
				}				
			}
		} catch (Exception e) {
			logger.error("Accessing DB to get Distinct Rule ids of Tasks.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Accessing DB to get Distinct Rule ids of Tasks.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return retList;

		
	}

	@Override
	public List<TaskStored> getFinishedTasks(int jobId, int ruleId) throws DaoException {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		TaskStored retTask = null;
		List<TaskStored> retList = null; 
		logger.debug("Accessing DB to get Finished Tasks");
		try {
			con = getConnection();
			String query="SELECT success, record_identifier FROM tasks WHERE job_id=? AND rule_id=? AND status=?";
			stmt = con.prepareStatement(query);
			stmt.setInt(1, jobId);
			stmt.setInt(2, ruleId);
			stmt.setString(3, "finished");
			rs = stmt.executeQuery();
			if (rs!=null){
				retList = new ArrayList<TaskStored>();
				
				while (rs.next()) {
					retTask = new TaskStored();
					retTask.setSuccess(rs.getBoolean(1));
					retTask.setRecordIdentifier(rs.getString(2));
					retList.add(retTask);
				}				
			}
		} catch (Exception e) {
			logger.error("Accessing DB to get Finished Tasks.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Accessing DB to get Finished Tasks.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return retList;


	}

	@Override
	public void saveTasksBatch(List<TaskStored> tasks, Map<String,List<String>> groupByMap) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null, stmt1 = null;
		logger.debug("Accessing DB to save batch of tasks");
		try {
			
			con = getConnection();
			String query="INSERT INTO tasks(status, success, started, ended, job_id, rule_id, record_identifier, record_url) VALUES(?,?,?,?,?,?,?,?)";stmt = con.prepareStatement(query);
			stmt = con.prepareStatement(query);
						
			for (TaskStored t : tasks ) {
				stmt.setString(1, t.getStatus());
				stmt.setBoolean(2, t.getSuccess());
				stmt.setString(3, t.getStarted());
				stmt.setString(4, t.getEnded());
				stmt.setInt(5, t.getJobId());
				stmt.setInt(6, t.getRuleId());
				stmt.setString(7, t.getRecordIdentifier());
				stmt.setString(8, t.getRecordUrl());	
				stmt.addBatch();
		    }
		    stmt.executeBatch();
		    logger.debug("Tasks inserted: "+tasks.size());

		    if (!groupByMap.isEmpty()) {
		    	logger.debug("Inserting record's groupBy values..");
		    	for (Map.Entry<String, List<String>> entry : groupByMap.entrySet()) {
			    	query="INSERT INTO record_groupby(record_id, groupby, job_id) VALUES(?,?,?)";
			    	stmt1 = con.prepareStatement(query);
			    	for (String value :entry.getValue()) {
			    		stmt1.setString(1, entry.getKey());
			    		stmt1.setString(2, value);
			    		stmt1.setInt(3, tasks.get(0).getJobId());		
			    		stmt1.addBatch();
			    	}
		    	}
		    	stmt1.executeBatch();
//		    	logger.debug("groupBy values inserted: "+groupBy_values.size());		    	
		    }
		    
		} catch (Exception e) {
			logger.error("Error Accessing DB to save batch of tasks.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Accessing DB to save batch of tasks.", e);
					throw new DaoException(e);
				}
			}
			if (stmt1 != null) {
				try {
					stmt1.close();
				} catch (SQLException e) {
					logger.error("Accessing DB to save batch of tasks.", e);
					throw new DaoException(e);
				}
			}

			closeConnection(con);
		}
		
	}

	@Override
	public void saveTasks(List<TaskStored> tasks, List<String> groupBy_values) throws DaoException {
		Connection con = null;
		PreparedStatement stmt = null, stmt1 = null;
		logger.debug("Accessing DB to save batch of tasks");
		try {
			
			con = getConnection();
			String query="INSERT INTO tasks(status, success, started, ended, job_id, rule_id, record_identifier, record_url) VALUES(?,?,?,?,?,?,?,?)";stmt = con.prepareStatement(query);
			stmt = con.prepareStatement(query);
						
			for (TaskStored t : tasks ) {
				stmt.setString(1, t.getStatus());
				stmt.setBoolean(2, t.getSuccess());
				stmt.setString(3, t.getStarted());
				stmt.setString(4, t.getEnded());
				stmt.setInt(5, t.getJobId());
				stmt.setInt(6, t.getRuleId());
				stmt.setString(7, t.getRecordIdentifier());
				stmt.setString(8, t.getRecordUrl());	
				stmt.addBatch();
		    }
		    stmt.executeBatch();
		    logger.debug("Tasks inserted: "+tasks.size());

		    if (groupBy_values != null) {
		    	logger.debug("Inserting record's groupBy values..");
		    	query="INSERT INTO record_groupby(record_id, groupby, job_id) VALUES(?,?,?)";
		    	stmt1 = con.prepareStatement(query);
		    	for (String value : groupBy_values) {
		    		stmt1.setString(1, tasks.get(0).getRecordIdentifier());
		    		stmt1.setString(2, value);
		    		stmt1.setInt(3, tasks.get(0).getJobId());		
		    		stmt1.addBatch();
		    	}
		    	stmt1.executeBatch();
		    	logger.debug("groupBy values inserted: "+groupBy_values.size());		    	
		    }
		    
		} catch (Exception e) {
			logger.error("Error Accessing DB to save batch of tasks.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Accessing DB to save batch of tasks.", e);
					throw new DaoException(e);
				}
			}
			if (stmt1 != null) {
				try {
					stmt1.close();
				} catch (SQLException e) {
					logger.error("Accessing DB to save batch of tasks.", e);
				}
			}
			closeConnection(con);
		}
		
	}
	
	@Override
	public void saveTasks(Map<Integer, RuleStatus> scoreMapPerRule) throws DaoException{
		Connection con = null;
		PreparedStatement stmt = null;
		logger.debug("Accessing DB to save batch of failed tasks");
		try {
			
			con = getConnection();
			String query="INSERT INTO tasks(status, success, started, ended, job_id, rule_id, record_identifier, record_url) VALUES(?,?,?,?,?,?,?,?)";
			stmt = con.prepareStatement(query);
			
			for (Entry<Integer, RuleStatus> entry : scoreMapPerRule.entrySet()) {
				for (TaskStored t : entry.getValue().getFailedTasks() ) {
					stmt.setString(1, t.getStatus());
					stmt.setBoolean(2, t.getSuccess());
					stmt.setString(3, t.getStarted());
					stmt.setString(4, t.getEnded());
					stmt.setInt(5, t.getJobId());
					stmt.setInt(6, t.getRuleId());
					stmt.setString(7, t.getRecordIdentifier());
					stmt.setString(8, t.getRecordUrl());	
					stmt.addBatch();
				}
			}
		    int result = stmt.executeBatch().length;
		    logger.debug("Tasks inserted: "+ result);

		} catch (Exception e) {
			logger.error("Error Accessing DB to save batch of failed tasks", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Accessing DB to save batch of failed tasks.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		
	}

	
	@Override
	protected int getLastId() throws DaoException {
		return 1;
//		ResultSet rs = null;
//		Connection con = null;
//		PreparedStatement stmt = null;
//		int retId = -1;
//		logger.debug("Accessing DB to get Task's next available id");
//		try {
//			con = getConnection();
//			String query="SELECT currval(pg_get_serial_sequence(?,?)) FROM tasks";
//			stmt = con.prepareStatement(query);
//			stmt.setString(1, "tasks");
//			stmt.setString(2, "rule_id");
//			
//			rs = stmt.executeQuery();
//			if (rs!=null){
//				rs.next();
//				retId=rs.getInt(1);
//			}
//
//
//		} catch (SQLException e) {
//			logger.error("Error while accessing DB to get Task's next available id.", e);
//		} finally {
//			if (stmt != null) {
//				try {
//					stmt.close();
//				} catch (SQLException e) {
//					logger.error("Error while accessing DB to get Task's next available id.", e);
//				}
//			}
//		}
//		return retId;
	}

	@Override
	public void cleanTasks(int jobId) throws DaoException{
		Connection con = null;
		PreparedStatement stmt = null;
		logger.debug("Accessing DB to delete unneeded tasks");
		try {
			con = getConnection();
			String query="DELETE FROM tasks WHERE job_id = ? AND success = ?";
			stmt = con.prepareStatement(query);
			stmt.setInt(1, jobId);
			stmt.setBoolean(2, true);
			stmt.executeUpdate();
			stmt.close();
		} catch (Exception e) {
			logger.error("Error while Accessing DB to delete unneeded tasks.", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while Accessing DB to delete unneeded tasks.", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}

	}

}
