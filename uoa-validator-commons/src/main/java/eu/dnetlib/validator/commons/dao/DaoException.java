package eu.dnetlib.validator.commons.dao;

public class DaoException extends Exception {

	/**
	 * 
	 * @author Nikon Gasparis
	 *
	 */
	
	private static final long serialVersionUID = 1030647214820156752L;

	public DaoException() {
		super();
	}
	
	public  DaoException(String message) {
		super(message);
	}

	public  DaoException(String message, Throwable cause) {
		super(message, cause);
	}

	public  DaoException(Throwable cause) {
		super(cause);
	}
}

