package eu.dnetlib.validator.commons.dao.repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import eu.dnetlib.validator.commons.dao.AbstractDAO;
import eu.dnetlib.validator.commons.dao.DaoException;

public class RepositoriesDAOImpl extends AbstractDAO<RepositoryStored> implements RepositoriesDAO {

	@Override
	public List<String> getBaseUrls() throws DaoException{
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement stmt = null;
		List<String> retList = null;
		logger.debug("Accessing DB to get all activated Repositories Stored");
		try {
			con = getConnection();
			String query="SELECT base_url FROM repositories WHERE activation_id is null";
			stmt = con.prepareStatement(query);
			rs = stmt.executeQuery();
			if (rs!=null){
				retList = new ArrayList<String>();				
				while (rs.next()) {
					retList.add(rs.getString(1));
				}				
			}

		} catch (Exception e) {
			logger.error("Error while accessing DB to get all activated Repositories Stored: .", e);
			throw new DaoException(e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error("Error while accessing DB to get all activated Repositories Stored: .", e);
					throw new DaoException(e);
				}
			}
			closeConnection(con);
		}
		return retList;

		
	}
	
	@Override
	protected PreparedStatement getUpdateStatement(RepositoryStored t,
			Connection con) throws SQLException {
		String query="UPDATE repositories SET activation_id=? WHERE activation_id=?";
		PreparedStatement stmt = con.prepareStatement(query);
		stmt.setString(1, null);
		stmt.setString(2, t.getActivationId());
		return stmt;
	}

	@Override
	protected PreparedStatement getInsertStatement(RepositoryStored t,
			Connection con) throws SQLException {
		String query="INSERT INTO repositories(base_url,activation_id) VALUES(?,?)";
		PreparedStatement stmt = con.prepareStatement(query);
		stmt.setString(1, t.getBaseUrl());
		stmt.setString(2, t.getBaseUrl());
		return stmt;
	}

	@Override
	protected PreparedStatement getDeleteStatement(int id, Connection con)
			throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected int getLastId() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public RepositoryStored get(int id) {
		// TODO Auto-generated method stub
		return null;
	}


}
