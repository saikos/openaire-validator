package eu.dnetlib.validator.commons.dao.repositories;

import java.util.List;

import eu.dnetlib.validator.commons.dao.DAO;
import eu.dnetlib.validator.commons.dao.DaoException;

public interface RepositoriesDAO extends DAO<RepositoryStored> {

	public List<String> getBaseUrls() throws DaoException;
	
	

}
