package eu.dnetlib.validator.commons.email;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import eu.dnetlib.utils.MailLibrary;

public class Emailer {

	private static Logger logger = Logger.getLogger(Emailer.class);

	private List<String> specialRecipients = new ArrayList<String>();
	private boolean override = false, logonly = false;
	private String overrideEmail = null, from = null;

	private MailLibrary mailer = null;

	public void sendMail(List<String> recipients, String subject, String message, boolean sendToSpecial, List<String> repoAdminMails) throws Exception {

		try {
			if (sendToSpecial) {
				recipients.addAll(this.specialRecipients);
			}

			if (repoAdminMails != null)
				recipients.addAll(repoAdminMails);

			if (this.override) {
				recipients.clear();
				recipients.add(overrideEmail);
			}

			logger.debug("Sending mail to Recipients: " + recipients + " Subject: " + subject + " Message: " + message);
			if (!logonly)
				mailer.sendEmail(recipients.toArray(new String[] {}), subject, message);
		} catch (Exception e) {
			logger.error("Error sending mail to Recipients: " + recipients + " Subject: " + subject + " Message: " + message, e);
			throw new Exception(e);
		}
	}

	public void setSpecialRecipients(String specialRecipients) {
		String[] recps = specialRecipients.split(",");

		for (String recp : recps) {
			recp = recp.trim();

			this.specialRecipients.add(recp);
		}
	}

	public void setOverride(boolean override) {
		this.override = override;
	}

	public void setOverrideEmail(String overrideEmail) {
		this.overrideEmail = overrideEmail;
	}

	public void setMailer(MailLibrary mailer) {
		this.mailer = mailer;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public boolean isLogonly() {
		return logonly;
	}

	public void setLogonly(boolean logonly) {
		this.logonly = logonly;
	}

}
