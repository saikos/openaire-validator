package eu.dnetlib.validator.commons.dao.tasks;

import eu.dnetlib.validator.commons.dao.DAO;
import eu.dnetlib.validator.commons.dao.DaoException;
import eu.dnetlib.validator.commons.dao.rules.RuleStatus;

import java.util.List;
import java.util.Map;

public interface TasksDAO extends DAO<TaskStored> {

	public List<TaskStored> getTasksOfJob(int id) throws DaoException;

	public List<String> getValidationErrors(int jobId, int ruleId) throws DaoException;

	public List<String> getDistinctTasksOfJob(int jobId) throws DaoException;

	public List<TaskStored> getFinishedTasks(int jobId, int ruleId) throws DaoException;

	public void saveTasks(List<TaskStored> tasksStored, List<String> sets) throws DaoException;
	
	public void cleanTasks(int jobId) throws DaoException;

	public void saveTasksBatch(List<TaskStored> tasks, Map<String, List<String>> groupByMap) throws DaoException;

	public void saveTasks(Map<Integer, RuleStatus> scoreMapPerRule) throws DaoException;
}
