package eu.dnetlib.validator.commons.dao.rules;

import java.util.List;

import eu.dnetlib.domain.functionality.validator.Rule;
import eu.dnetlib.validator.commons.dao.DAO;
import eu.dnetlib.validator.commons.dao.DaoException;

public interface RulesDAO extends DAO<Rule> {

	public List<Rule> getAllRulesByJobTypeEntityType(String jobType, String entityType) throws DaoException;
	public List<Rule> getAllRulesByJobType(String jobType) throws DaoException;
	public List<Rule> getAllRules() throws DaoException;

}
