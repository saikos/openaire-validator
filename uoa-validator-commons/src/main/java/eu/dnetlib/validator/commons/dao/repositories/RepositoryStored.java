package eu.dnetlib.validator.commons.dao.repositories;

/**
 * 
 * @author Nikon Gasparis
 *
 */
public class RepositoryStored {
	
	private String baseUrl;
	private String activationId;
	
	
	public String getBaseUrl() {
		return baseUrl;
	}
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	public String getActivationId() {
		return activationId;
	}
	public void setActivationId(String activationId) {
		this.activationId = activationId;
	}
	
	
	

}
