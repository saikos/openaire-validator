package eu.dnetlib.validator.commons.dao.rules;

import eu.dnetlib.validator.commons.dao.tasks.TaskStored;

import java.util.ArrayList;
import java.util.List;

public class RuleStatus {

	public int total = 0;
	public int success = 0;
	private int weight = 0;
	private List<TaskStored> failedTasks = new ArrayList<TaskStored>();
	boolean mandatory;

	public RuleStatus() {
		super();
	}

	public boolean isMandatory() {
		return mandatory;
	}

	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getSuccess() {
		return success;
	}

	public void setSuccess(int success) {
		this.success = success;
	}

	public List<TaskStored> getFailedTasks() {
		return failedTasks;
	}

	public void setFailedTasks(List<TaskStored> failedTasks) {
		this.failedTasks = failedTasks;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

}
