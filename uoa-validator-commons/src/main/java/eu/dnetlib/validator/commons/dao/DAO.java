package eu.dnetlib.validator.commons.dao;

public interface DAO<T> {

	public Integer save (T t) throws DaoException;
//	public String delete (T t);
	public String delete (int id) throws DaoException;
	public T get (int id) throws DaoException;
}
