package eu.dnetlib.domain;


/**
 * An abstract class implementation for the SearchCriteria interface
 *
 */

public abstract class SearchCriteriaImpl implements SearchCriteria {
    protected String startsWith = null;
    protected String endsWith = null;
    protected String contains = null;
    
	public String getContains() {
		return contains;
	}
	public void setContains(String contains) {
		this.contains = contains;
	}
	public String getEndsWith() {
		return endsWith;
	}
	public void setEndsWith(String endsWith) {
		this.endsWith = endsWith;
	}
	public String getStartsWith() {
		return startsWith;
	}
	public void setStartsWith(String startsWith) {
		this.startsWith = startsWith;
	}
	
	public String toString() {
		String s = "";
		
		if (contains != null) {
			s += "contains: " + contains;
		}
		
		if (startsWith != null) {
			s += " startsWith: " + startsWith;
		}
		
		if (endsWith != null) {
			s += " endsWith: " + endsWith;
		}
		
		return "[" + s + "]";
	}
	
	public boolean equals(Object o) {
		if (!(o instanceof SearchCriteriaImpl))
			return false;
		else
			return this.equals((SearchCriteriaImpl) o);
	}
	
	public boolean equals(SearchCriteriaImpl crit) {
		if (contains != null && crit.contains != null) {
			if (!contains.equals(crit.contains))
				return false;
		} else if ( (contains == null && crit.contains != null) || (contains != null && crit.contains == null)) {
			return false;
		}

		if (startsWith != null && crit.startsWith != null) {
			if (!startsWith.equals(crit.startsWith))
				return false;
		} else if ( (startsWith == null && crit.startsWith != null) || (startsWith != null && crit.startsWith == null)) {
			return false;
		}

		if (endsWith != null && crit.endsWith != null) {
			if (!endsWith.equals(crit.endsWith))
				return false;
		} else if ( (endsWith == null && crit.endsWith != null) || (endsWith != null && crit.endsWith == null)) {
			return false;
		}
		
		return true;
	}
	
	public int hashCode() {
		int code = 0;
		
		if (contains != null)
			code |= contains.hashCode();
		
		if (startsWith != null)
			code |= startsWith.hashCode();
		
		if (endsWith != null)
			code |= endsWith.hashCode();
		
		return code;
	}
}
