package eu.dnetlib.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import eu.dnetlib.domain.enabling.SecurityProfile;

public abstract class DriverServiceProfile extends DriverResource {
	private static final long serialVersionUID = -3603230835510580092L;

	private String parentId = null;
	private String parentType = null;
	
	private List<String> protocolNames = new ArrayList<String>();
	private List<String> protocolAddresses = new ArrayList<String>();
	
	private String typology = null;
	private int maxDatastructureSize = 0;
	private int maxDatastructureNumber = 0;
	private int availabeDiskSpace = 0;
	
	private String availability = null;
	private String capacity = null;
	private int responseTime = 0;
	private double throughput = 0;
	
	private SecurityProfile securityProfile = null;
	
	public DriverServiceProfile(String ResourceKind, String resourceType, 
			Date dateOfCreation) {
		
		this.setResourceKind(ResourceKind);
		this.setResourceType(resourceType);
		this.setDateOfCreation(dateOfCreation);
	}
	
	public SecurityProfile getSecurityProfile() {
		return securityProfile;
	}

	public void setSecurityProfile(SecurityProfile securityProfile) {
		this.securityProfile = securityProfile;
	}
	
	public int getAvailabeDiskSpace() {
		return availabeDiskSpace;
	}

	public void setAvailabeDiskSpace(int availabeDiskSpace) {
		this.availabeDiskSpace = availabeDiskSpace;
	}

	public String getAvailability() {
		return availability;
	}

	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public int getMaxDatastructureNumber() {
		return maxDatastructureNumber;
	}

	public void setMaxDatastructureNumber(int maxDatastructureNumber) {
		this.maxDatastructureNumber = maxDatastructureNumber;
	}

	public int getMaxDatastructureSize() {
		return maxDatastructureSize;
	}

	public void setMaxDatastructureSize(int maxDatastructureSize) {
		this.maxDatastructureSize = maxDatastructureSize;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getParentType() {
		return parentType;
	}

	public void setParentType(String parentType) {
		this.parentType = parentType;
	}

	public List<String> getProtocolAddresses() {
		return protocolAddresses;
	}

	public void setProtocolAddresses(List<String> protocolAddresses) {
		this.protocolAddresses = protocolAddresses;
	}

	public List<String> getProtocolNames() {
		return protocolNames;
	}

	public void setProtocolNames(List<String> protocolNames) {
		this.protocolNames = protocolNames;
	}

	public int getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(int responseTime) {
		this.responseTime = responseTime;
	}

	public double getThroughput() {
		return throughput;
	}

	public void setThroughput(double throughput) {
		this.throughput = throughput;
	}

	public String getTypology() {
		return typology;
	}

	public void setTypology(String typology) {
		this.typology = typology;
	}
}
