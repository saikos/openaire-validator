package eu.dnetlib.domain;

/**
 * Identifies the service's version. Version syntax:
 * ${NAME}-${MAJOR}.${MINOR}.${MICRO}[-${LABEL}]
 */
public class ServiceIdentity {

	private String name = null;
	private String major = null;
	private String minor = null;
	private String micro = null;
	private String label = null;
	
	public ServiceIdentity(String name, String major, String minor,
			String micro, String label) {
		super();
		this.name = name;
		this.major = major;
		this.minor = minor;
		this.micro = micro;
		this.label = label;
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		
		buffer.append(name).append("-").append(major).append(".").append(minor);
		buffer.append(".").append(micro);
		
		if (label != null)
			buffer.append("-").append(label);
		
		return buffer.toString();
	}
}
