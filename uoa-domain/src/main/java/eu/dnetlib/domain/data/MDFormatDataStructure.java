package eu.dnetlib.domain.data;

import java.util.HashMap;
import java.util.List;

import eu.dnetlib.domain.DriverResource;
import eu.dnetlib.domain.functionality.LayoutField;

@SuppressWarnings("serial")
public class MDFormatDataStructure extends DriverResource {

	private String resourceName;
	private HashMap<String, List<LayoutField>> layouts;

	public MDFormatDataStructure() {

		this.setResourceType("MDFormatDSResourceType");
		this.setResourceKind("MDFormatDSResources");

		this.resourceName = null;
		this.layouts = new HashMap<String, List<LayoutField>>();
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public HashMap<String, List<LayoutField>> getLayouts() {
		return layouts;
	}

	public void setLayouts(HashMap<String, List<LayoutField>> layouts) {
		this.layouts = layouts;
	}
}