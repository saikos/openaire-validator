package eu.dnetlib.domain.data;

public class DataCollectionType {

	private DataCollectionInterface dataCollectionInterface;
	private String id;
	private String label;
	private String group;
	public DataCollectionInterface getDataCollectionInterface() {
		return dataCollectionInterface;
	}
	public void setDataCollectionInterface(
			DataCollectionInterface dataCollectionInterface) {
		this.dataCollectionInterface = dataCollectionInterface;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	
	
}
