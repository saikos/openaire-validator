package eu.dnetlib.domain.data;

import java.util.Date;

/**
 * Created by stefania on 12/19/16.
 */
public class PiwikInfo{

    private String repositoryId;
    private String openaireId;
    private String repositoryName;
    private String country;
    private String siteId;
    private String authenticationToken;
    private Date creationDate;
    private String requestorName;
    private String requestorEmail;
    private boolean validated;
    private Date validationDate;
    private String comment;

    public PiwikInfo() {

    }

    public PiwikInfo(String repositoryId, String openaireId, String repositoryName, String country, String siteId,
                     String authenticationToken, Date creationDate, String requestorName, String requestorEmail,
                     boolean validated, Date validationDate, String comment) {
        this.repositoryId = repositoryId;
        this.openaireId = openaireId;
        this.repositoryName = repositoryName;
        this.country = country;
        this.siteId = siteId;
        this.authenticationToken = authenticationToken;
        this.creationDate = creationDate;
        this.requestorName = requestorName;
        this.requestorEmail = requestorEmail;
        this.validated = validated;
        this.validationDate = validationDate;
        this.comment = comment;
    }

    public String getRepositoryId() {
        return repositoryId;
    }

    public void setRepositoryId(String repositoryId) {
        this.repositoryId = repositoryId;
    }

    public String getOpenaireId() {
        return openaireId;
    }

    public void setOpenaireId(String openaireId) {
        this.openaireId = openaireId;
    }

    public String getRepositoryName() {
        return repositoryName;
    }

    public void setRepositoryName(String repositoryName) {
        this.repositoryName = repositoryName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getRequestorName() {
        return requestorName;
    }

    public void setRequestorName(String requestorName) {
        this.requestorName = requestorName;
    }

    public String getRequestorEmail() {
        return requestorEmail;
    }

    public void setRequestorEmail(String requestorEmail) {
        this.requestorEmail = requestorEmail;
    }

    public boolean isValidated() {
        return validated;
    }

    public void setValidated(boolean validated) {
        this.validated = validated;
    }

    public Date getValidationDate() {
        return validationDate;
    }

    public void setValidationDate(Date validationDate) {
        this.validationDate = validationDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
