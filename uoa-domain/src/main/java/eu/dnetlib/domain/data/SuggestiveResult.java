package eu.dnetlib.domain.data;

import eu.dnetlib.domain.EPR;


public class SuggestiveResult {
	
	private EPR epr = null;
	private String alternativeTerm = null;
	private boolean autofollow = false;
	
	public EPR getEpr() {
		return epr;
	}
	public void setEpr(EPR epr) {
		this.epr = epr;
	}
	public String getAlternativeTerm() {
		return alternativeTerm;
	}
	public void setAlternativeTerm(String alternativeTerm) {
		this.alternativeTerm = alternativeTerm;
	}
	public boolean isAutofollow() {
		return autofollow;
	}
	public void setAutofollow(boolean autofollow) {
		this.autofollow = autofollow;
	}

}
