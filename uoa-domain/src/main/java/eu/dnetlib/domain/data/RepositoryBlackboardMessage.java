package eu.dnetlib.domain.data;

import java.util.ArrayList;
import java.util.List;

public class RepositoryBlackboardMessage {
	private String id = "";
	private final Action action;
	private final ActionStatus actionStatus;
	private final List<String> parameters = new ArrayList<String>();

	public RepositoryBlackboardMessage(String id, Action action,
			ActionStatus actionStatus) {
		this.id = id;
		this.action = action;
		this.actionStatus = actionStatus;
	}

	public String getId() {
		return id;
	}

	public Action getAction() {
		return action;
	}

	public ActionStatus getActionStatus() {
		return actionStatus;
	}

	public List<String> getParameters() {
		return parameters;
	}

	public static enum Action {
		CREATE("CREATE"), DELETE("DELETE"), UPDATE("UPDATE"), MANAGE("MANAGE"), 
			RELEASE("RELEASE"), CANCEL("CANCEL");

		private final String value;

		Action(String value) {
			this.value = value;
		}

		public String getValue() {
			return this.value;
		}

		public static Action getAction(String str) {
			for (Action act : Action.values()) {
				if (act.getValue().equals(str))
					return act;
			}
			throw new IllegalArgumentException("cannot find Action " + str);
		}
	}

	public static enum ActionStatus {
		DONE("DONE"), ONGOING("ONGOING"), FAILED("FAILED"), WAITING("WAITING"), 
			ASSIGNED("ASSIGNED");

		private final String value;

		ActionStatus(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public static ActionStatus getActionStatus(String str) {
			for (ActionStatus act : ActionStatus.values()) {
				if (act.getValue().equals(str))
					return act;
			}
			throw new IllegalArgumentException("cannot find ActionStatus "
					+ str);
		}
	}
}
