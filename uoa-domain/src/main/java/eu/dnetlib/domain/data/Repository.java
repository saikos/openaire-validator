package eu.dnetlib.domain.data;

import eu.dnetlib.domain.DriverResource;

import java.util.*;

/**
 * The domain object for the Repository resource data structure
 * 
 */
public class Repository extends DriverResource{
	private static final long serialVersionUID = -7241644234046760972L;

	// new
	private String id;
	private String officialName;
	private String englishName;
	private String websiteUrl;
	private String logoUrl;
	private String contactEmail;
	private String countryName;
	private String countryCode;
	private String organization;
	private Double latitude = 0.0;
	private Double longitude = 0.0;
	private Double timezone = 0.0;
	private String namespacePrefix;
	private String odNumberOfItems;
	private String odNumberOfItemsDate;
	private String odPolicies;
	private String odLanguages;
	private String odContentTypes;
	private String collectedFrom;
	private Boolean inferred = false;
	private Boolean deletedByInference = false;
	private Double trust = 0.9;
	private String inferenceProvenance;
	private Date dateOfValidation;
	private String datasourceClass;
	private String provenanceActionClass;
	private Date dateOfCollection;
	private String typology;
	private String activationId;
	private Boolean mergehomonyms = true;
	private String description;
	private Date releaseStartDate;
	private Date releaseEndDate;
	private String missionStatementUrl;
	private Boolean dataProvider = false;
	private Boolean serviceProvider = false;
	private String databaseAccessType;
	private String dataUploadType;
	private String databaseAccessRestriction;
	private String dataUploadRestriction;
	private Boolean versioning = false;
	private String citationGuidelineUrl;
	private String qualityManagementKind;
	private String pidSystems;
	private String certificates;
	private String aggregator;
	private String issn;
	private String eissn;
	private String lissn;
	private List<RepositoryInterface> interfaces = new ArrayList<RepositoryInterface>();

	private String availableDiskSpace = null;
	private String securityParameters = null;
	private String protocol = null;
	private String registeredBy = null;

	private String datasourceType = null;
	private String datasourceAggregatorId = null;
	private String datasourceOriginalIdValue = null;
	private String datasourceOriginalIdProvenance = null;
	private Boolean datasourceAggregated = null;
	private String datasourceComplianceDegreeValue = null;
	private String datasourceComplianceDegreeEncoding = null;

	private Integer numberOfObjects = Integer.valueOf(0);
	private Integer maxSizeOfDatastructure = Integer.valueOf(0);
	private Integer maxNumberOfDataStructures = Integer.valueOf(0);


	private Boolean registered = false;

	// QOS parameters
//	private String availability = null;
//	private String capacity = null;
//	private Integer responseTime = null;
//	private double throughput = 0.0D;

	// status parameters
//	private Integer handledDatastructure = null;
//	private Integer usedDiskspace = null;
//	private Date lastUpdate = null;

	// blackboard
//	private RepositoryBlackboard blackboard = null;

	private Map<String, String> extraFields = new HashMap<String, String>();

	// extra fields that need to be added to the XML schema
	// private String administratorEmail = null;
	// private String administratorName = null;
	// private String administratorContactInfo = null;

	// private List<RepositoryInterface> interfaces = new
	// ArrayList<RepositoryInterface>();
	private static List<DataCollectionType> dataCollectionTypes = new ArrayList<DataCollectionType>();


	private PiwikInfo piwikInfo;

	private List<String> environments = new ArrayList<String>();

	public Repository() {
		this.setDateOfCreation(new Date());
		this.setResourceKind("RepositoryServiceResources");
		this.setResourceType("RepositoryServiceResourceType");
	}

	public Map<String, String> getExtraFields() {
		return extraFields;
	}

	public void setExtraFields(Map<String, String> extraFields) {
		this.extraFields = extraFields;
	}

	public String getRegisteredBy() {
		return registeredBy;
	}

	public void setRegisteredBy(String registeredBy) {
		this.registeredBy = registeredBy;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	
//	public String getId() {
//		return this.getResourceId();
//	}
//
//	public void setId(String id) {
//		this.setResourceId(id);
//	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getNumberOfObjects() {
		return numberOfObjects;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setNumberOfObjects(Integer numberOfObjects) {
		this.numberOfObjects = numberOfObjects;
	}

	public String getOfficialName() {
		return officialName;
	}

	public void setOfficialName(String officialName) {
		this.officialName = officialName;
	}

	public Date getRegistrationDate() {
		return this.getDateOfCreation();
	}

	public void setRegistrationDate(Date registrationDate) {
		this.setDateOfCreation(registrationDate);
	}

	public List<RepositoryInterface> getInterfaces() {
		return interfaces;
	}

	public void setInterfaces(List<RepositoryInterface> interfaces) {
		this.interfaces = interfaces;
	}

	public String getTypology() {
		return typology;
	}

	public void setTypology(String typology) {
		this.typology = typology;
	}

	public Double getLatitude() {
		return latitude;
	}

	public Double getTimezone() {
		return timezone;
	}

	public void setTimezone(Double timezone) throws IllegalArgumentException {
		if (timezone < -12 || timezone > 12 || (timezone % 0.5) != 0) {
			String t = String.valueOf(timezone);
			throw new IllegalArgumentException(
					"timezone must be in the range [-12.0, 12.0] and must me divided by 0.5. Value given is "
							+ t);
		}
		this.timezone = timezone;
	}
	public boolean equals(Object o) {
		if (!(o instanceof Repository))
			return false;
		else
			return this.equals((Repository) o);
	}

	public boolean equals(Repository r) {
		// TODO: fill with required fields...

		if (this.getEnglishName() != null && r.getEnglishName() == null) {
			return false;
		} else if (this.getEnglishName() == null
				&& r.getEnglishName() != null) {
			return false;
		} else if (this.getEnglishName() != null
				&& r.getEnglishName() != null) {
			return this.getEnglishName().equals(r.getEnglishName());
		}

		return true;
	}

	@Override
	public int hashCode() {
        return officialName.hashCode();
	}

	public String getAvailableDiskSpace() {
		return availableDiskSpace;
	}

	public void setAvailableDiskSpace(String availableDiskSpace) {
		this.availableDiskSpace = availableDiskSpace;
	}

	public String getSecurityParameters() {
		return securityParameters;
	}

	public void setSecurityParameters(String securityParameters) {
		this.securityParameters = securityParameters;
	}

	public Integer getMaxSizeOfDatastructure() {
		return maxSizeOfDatastructure;
	}

	public void setMaxSizeOfDatastructure(Integer maxSizeOfDatastructure) {
		this.maxSizeOfDatastructure = maxSizeOfDatastructure;
	}

	public Integer getMaxNumberOfDataStructures() {
		return maxNumberOfDataStructures;
	}

	public void setMaxNumberOfDataStructures(Integer maxNumberOfDataStructures) {
		this.maxNumberOfDataStructures = maxNumberOfDataStructures;
	}


	public Boolean isVerified() {
		return extraFields.containsKey("VERIFIED")
				&& extraFields.get("VERIFIED").toLowerCase().equals("yes");
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setDatasourceType(String datasourceType) {
		this.datasourceType = datasourceType;
	}

	public String getDatasourceType() {
		return datasourceType;
	}

	public Boolean getDatasourceAggregated() {
		return datasourceAggregated;
	}

	public void setDatasourceAggregated(Boolean datasourceAggregated) {
		this.datasourceAggregated = datasourceAggregated;
	}

	public String getDatasourceComplianceDegreeValue() {
		return datasourceComplianceDegreeValue;
	}

	public void setDatasourceComplianceDegreeValue(
			String datasourceComplianceDegreeValue) {
		this.datasourceComplianceDegreeValue = datasourceComplianceDegreeValue;
	}

	public String getDatasourceComplianceDegreeEncoding() {
		return datasourceComplianceDegreeEncoding;
	}

	public void setDatasourceComplianceDegreeEncoding(
			String datasourceComplianceDegreeEncoding) {
		this.datasourceComplianceDegreeEncoding = datasourceComplianceDegreeEncoding;
	}

	public void setDatasourceAggregatorId(String datasourceAggregatorId) {
		this.datasourceAggregatorId = datasourceAggregatorId;
	}

	public String getDatasourceAggregatorId() {
		return datasourceAggregatorId;
	}

	public void setDatasourceOriginalIdValue(String datasourceOriginalIdValue) {
		this.datasourceOriginalIdValue = datasourceOriginalIdValue;
	}

	public String getDatasourceOriginalIdValue() {
		return datasourceOriginalIdValue;
	}

	public void setDatasourceOriginalIdProvenance(
			String datasourceOriginalIdProvenance) {
		this.datasourceOriginalIdProvenance = datasourceOriginalIdProvenance;
	}

	public String getDatasourceOriginalIdProvenance() {
		return datasourceOriginalIdProvenance;
	}

	public List<DataCollectionType> getDataCollectionTypes() {
		return dataCollectionTypes;
	}

	public void setDataCollectionTypes(
			List<DataCollectionType> dataCollectionTypes) {
		this.dataCollectionTypes = dataCollectionTypes;
	}

	public void setEnvironments(List<String> environments) {
		this.environments = environments;
	}

	public List<String> getEnvironments() {
		return environments;
	}

	// new

	public String getWebsiteUrl() {
		return websiteUrl;
	}

	public void setWebsiteUrl(String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getNamespacePrefix() {
		return namespacePrefix;
	}

	public void setNamespacePrefix(String namespacePrefix) {
		this.namespacePrefix = namespacePrefix;
	}

	public String getOdNumberOfItems() {
		return odNumberOfItems;
	}

	public void setOdNumberOfItems(String odNumberOfItems) {
		this.odNumberOfItems = odNumberOfItems;
	}

	public String getOdNumberOfItemsDate() {
		return odNumberOfItemsDate;
	}

	public void setOdNumberOfItemsDate(String odNumberOfItemsDate) {
		this.odNumberOfItemsDate = odNumberOfItemsDate;
	}

	public String getOdPolicies() {
		return odPolicies;
	}

	public void setOdPolicies(String odPolicies) {
		this.odPolicies = odPolicies;
	}

	public String getOdLanguages() {
		return odLanguages;
	}

	public void setOdLanguages(String odLanguages) {
		this.odLanguages = odLanguages;
	}

	public String getOdContentTypes() {
		return odContentTypes;
	}

	public void setOdContentTypes(String odContentTypes) {
		this.odContentTypes = odContentTypes;
	}

	public String getCollectedFrom() {
		return collectedFrom;
	}

	public void setCollectedFrom(String collectedFrom) {
		this.collectedFrom = collectedFrom;
	}

	public Boolean getInferred() {
		return inferred;
	}

	public void setInferred(Boolean inferred) {
		this.inferred = inferred;
	}

	public Boolean getDeletedByInference() {
		return deletedByInference;
	}

	public void setDeletedByInference(Boolean deletedByInference) {
		this.deletedByInference = deletedByInference;
	}

	public Double getTrust() {
		return trust;
	}

	public void setTrust(Double trust) {
		this.trust = trust;
	}

	public String getInferenceProvenance() {
		return inferenceProvenance;
	}

	public void setInferenceProvenance(String inferenceProvenance) {
		this.inferenceProvenance = inferenceProvenance;
	}

	public Date getDateOfValidation() {
		return dateOfValidation;
	}

	public void setDateOfValidation(Date dateOfValidation) {
		this.dateOfValidation = dateOfValidation;
	}

	public String getDatasourceClass() {
		return datasourceClass;
	}

	public void setDatasourceClass(String datasourceClass) {
		this.datasourceClass = datasourceClass;
	}

	public String getProvenanceActionClass() {
		return provenanceActionClass;
	}

	public void setProvenanceActionClass(String provenanceActionClass) {
		this.provenanceActionClass = provenanceActionClass;
	}

	public Date getDateOfCollection() {
		return dateOfCollection;
	}

	public void setDateOfCollection(Date dateOfCollection) {
		this.dateOfCollection = dateOfCollection;
	}

	public String getActivationId() {
		return activationId;
	}

	public void setActivationId(String activationId) {
		this.activationId = activationId;
	}

	public Boolean getMergehomonyms() {
		return mergehomonyms;
	}

	public void setMergehomonyms(Boolean mergehomonyms) {
		this.mergehomonyms = mergehomonyms;
	}

	public Date getReleaseStartDate() {
		return releaseStartDate;
	}

	public void setReleaseStartDate(Date releaseStartDate) {
		this.releaseStartDate = releaseStartDate;
	}

	public Date getReleaseEndDate() {
		return releaseEndDate;
	}

	public void setReleaseEndDate(Date releaseEndDate) {
		this.releaseEndDate = releaseEndDate;
	}

	public String getMissionStatementUrl() {
		return missionStatementUrl;
	}

	public void setMissionStatementUrl(String missionStatementUrl) {
		this.missionStatementUrl = missionStatementUrl;
	}

	public Boolean getDataProvider() {
		return dataProvider;
	}

	public void setDataProvider(Boolean dataProvider) {
		this.dataProvider = dataProvider;
	}

	public Boolean getServiceProvider() {
		return serviceProvider;
	}

	public void setServiceProvider(Boolean serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

	public String getDatabaseAccessType() {
		return databaseAccessType;
	}

	public void setDatabaseAccessType(String databaseAccessType) {
		this.databaseAccessType = databaseAccessType;
	}

	public String getDataUploadType() {
		return dataUploadType;
	}

	public void setDataUploadType(String dataUploadType) {
		this.dataUploadType = dataUploadType;
	}

	public String getDatabaseAccessRestriction() {
		return databaseAccessRestriction;
	}

	public void setDatabaseAccessRestriction(String databaseAccessRestriction) {
		this.databaseAccessRestriction = databaseAccessRestriction;
	}

	public String getDataUploadRestriction() {
		return dataUploadRestriction;
	}

	public void setDataUploadRestriction(String dataUploadRestriction) {
		this.dataUploadRestriction = dataUploadRestriction;
	}

	public Boolean getVersioning() {
		return versioning;
	}

	public void setVersioning(Boolean versioning) {
		this.versioning = versioning;
	}

	public String getCitationGuidelineUrl() {
		return citationGuidelineUrl;
	}

	public void setCitationGuidelineUrl(String citationGuidelineUrl) {
		this.citationGuidelineUrl = citationGuidelineUrl;
	}

	public String getQualityManagementKind() {
		return qualityManagementKind;
	}

	public void setQualityManagementKind(String qualityManagementKind) {
		this.qualityManagementKind = qualityManagementKind;
	}

	public String getPidSystems() {
		return pidSystems;
	}

	public void setPidSystems(String pidSystems) {
		this.pidSystems = pidSystems;
	}

	public String getCertificates() {
		return certificates;
	}

	public void setCertificates(String certificates) {
		this.certificates = certificates;
	}

	public String getAggregator() {
		return aggregator;
	}

	public void setAggregator(String aggregator) {
		this.aggregator = aggregator;
	}

	public String getIssn() {
		return issn;
	}

	public void setIssn(String issn) {
		this.issn = issn;
	}

	public String getEissn() {
		return eissn;
	}

	public void setEissn(String eissn) {
		this.eissn = eissn;
	}

	public String getLissn() {
		return lissn;
	}

	public void setLissn(String lissn) {
		this.lissn = lissn;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Boolean isRegistered() {
		return registered;
	}

	public void setRegistered(Boolean registered) {
		this.registered = registered;
	}

	public PiwikInfo getPiwikInfo() {
		return piwikInfo;
	}

	public void setPiwikInfo(PiwikInfo piwikInfo) {
		this.piwikInfo = piwikInfo;
	}
}