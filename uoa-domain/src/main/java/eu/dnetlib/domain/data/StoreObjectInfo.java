package eu.dnetlib.domain.data;

public class StoreObjectInfo {
	private StoreInfo storeInfo = null;
	private String objectId = null;

	public StoreInfo getStoreInfo() {
		return storeInfo;
	}

	public void setStoreInfo(StoreInfo storeInfo) {
		this.storeInfo = storeInfo;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
}
