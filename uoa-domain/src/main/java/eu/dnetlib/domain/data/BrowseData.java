package eu.dnetlib.domain.data;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class BrowseData {

	private HashMap<String, FieldData> data = null;
	
	public BrowseData() {
		data = new HashMap<String, FieldData>();
	}
	
	public HashMap<String, FieldData> getData() {
		return data;
	}

	public void setData(HashMap<String, FieldData> data) {
		this.data = data;
	}
	
	public void addFieldEntry(String field, String value, int count) {
		
		// lowercase field!
		field = field.toLowerCase();

		FieldData fieldData = data.get(field);		
		if(fieldData == null){
			fieldData = new FieldData();
		}

		FieldRow row = new FieldRow(value, count);
		fieldData.getFieldRowList().add(row);
		
		data.put(field, fieldData);
	}
	
	public void append(BrowseData browseData) {		
		for(String field: browseData.getFields()) {			
			// lowercase field!
			field = field.toLowerCase();
			
			FieldData fieldData = data.get(field);		
			if(fieldData == null){
				fieldData = new FieldData();
				data.put(field, fieldData);
			}

			fieldData.getFieldRowList().addAll
					(browseData.getData().get(field).getFieldRowList());
		}
	}
	
	public Set<String> getFields() {
		return data.keySet();
	}
	
	
	public List<String> getFieldValues(String field) {
		FieldData fd = data.get(field.toLowerCase());
		if (fd == null) {
			return Collections.emptyList();
		} else {
			return fd.getValues(); 
		}
	}
	
	public List<Integer> getFieldCounts(String field) {
		FieldData fd = data.get(field.toLowerCase());
		if (fd == null) {
			return Collections.emptyList();
		} else {
			return fd.getCount();
		}
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		
		for (FieldData field : data.values()) {
			buffer.append(field);
		}
		
		return buffer.toString();
	}
}
