package eu.dnetlib.domain.data;

import eu.dnetlib.domain.SearchCriteria;
import eu.dnetlib.domain.SearchCriteriaImpl;

public class RepositorySearchCriteria extends SearchCriteriaImpl implements
		SearchCriteria {
	private Boolean haveDocuments = null;
	private String protocolType = null;
	private String adminInfo = null;
	private String officialName = null;
	private String registeredBy = null;
	private String country = null;
	private Boolean verified = false;

	public String getRegisteredBy() {
		return registeredBy;
	}

	public void setRegisteredBy(String registeredBy) {
		this.registeredBy = registeredBy;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProtocolType() {
		return protocolType;
	}

	public void setProtocolType(String protocolType) {
		this.protocolType = protocolType;
	}

	public Boolean getHaveDocuments() {
		return haveDocuments;
	}

	public void setHaveDocuments(Boolean haveDocuments) {
		this.haveDocuments = haveDocuments;
	}

	public String getAdminInfo() {
		return adminInfo;
	}

	public void setAdminInfo(String adminInfo) {
		this.adminInfo = adminInfo;
	}

	public boolean equals(Object o) {
		return this.equals((RepositorySearchCriteria) o);
	}

	public boolean equals(RepositorySearchCriteria c) {
		if (!super.equals(c))
			return false;

		if (this.getHaveDocuments() != null && c.getHaveDocuments() == null)
			return false;
		else if (this.getHaveDocuments() == null
				&& c.getHaveDocuments() != null)
			return false;
		else if (this.getHaveDocuments() != null
				&& c.getHaveDocuments() != null)
			return this.haveDocuments == c.haveDocuments;

		if (this.getProtocolType() != null) {
			if (c.getProtocolType() == null
					|| !this.getProtocolType().equals(c.getProtocolType()))
				return false;
		} else if (c.getProtocolType() != null)
			return false;

		if (this.getAdminInfo() != null) {
			if (c.getAdminInfo() == null
					|| !this.getAdminInfo().equals(c.getAdminInfo()))
				return false;
		} else if (c.getAdminInfo() != null)
			return false;

		if (this.getOfficialName() != null) {
			if (c.getOfficialName() == null
					|| !this.getOfficialName().equals(c.getOfficialName()))
				return false;
		} else if (c.getOfficialName() != null)
			return false;
		
		if (this.getCountry() != null) {
			if (c.getCountry() == null || !this.getCountry().equals(c.getCountry()))
				return false;
		} else if (c.getCountry() != null)
			return false;
		
		if (this.isVerified() != null) {
			if (c.isVerified() == null || !this.isVerified().equals(c.isVerified()))
				return false;
		} else if (c.isVerified() != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int code = super.hashCode();

		if (haveDocuments != null)
			code |= haveDocuments.hashCode();

		if (protocolType != null) {
			code |= protocolType.hashCode();
		}

		if (adminInfo != null) {
			code |= adminInfo.hashCode();
		}

		if (officialName != null) {
			code |= officialName.hashCode();
		}
		
		if (country != null)
			code |= country.hashCode();
		
		if (verified != null)
			code |= verified.hashCode();

		return code;
	}

	public boolean matches(Object o) {
		if (!(o instanceof Repository)) {
			return false;
		}

		Repository repository = (Repository) o;

		if (this.getContains() != null) {
			if (repository.getOfficialName() == null
					|| !repository.getOfficialName().toLowerCase().contains(
							this.getContains().toLowerCase()))
				return false;
		}

		if (this.getStartsWith() != null) {
			if (repository.getOfficialName() == null
					|| !repository.getOfficialName().toLowerCase().startsWith(
							this.getStartsWith().toLowerCase()))
				return false;
		}

		if (this.getEndsWith() != null) {
			if (repository.getOfficialName() == null
					|| !repository.getOfficialName().toLowerCase().endsWith(
							this.getEndsWith().toLowerCase()))
				return false;
		}

		if (this.getHaveDocuments() != null) {
			if (repository.getNumberOfObjects() == null
					|| (repository.getNumberOfObjects() > 0) != this
							.getHaveDocuments())

				return false;
		}

		if (this.getOfficialName() != null) {
			if (!this.getOfficialName().equals(repository.getOfficialName()))
				return false;
		}
		
		if (this.getRegisteredBy() != null) {
			if (!this.getRegisteredBy().equals(repository.getRegisteredBy()))
				return false;
		}
		
		if (this.getCountry() != null) {
			if (!this.getCountry().equals(repository.getCountryName()))
				return false;
		}
		
		if (this.isVerified() != null) {
			if (!this.isVerified().equals(repository.isVerified()))
				return false;
		}

		return true;
	}

	public boolean isOAICompliant(Repository repository) {
		boolean retval = false;
		for (RepositoryInterface ri : repository.getInterfaces()) {
			if (ri.getAccessProtocol() != null
					&& ri.getAccessProtocol().equals("OAI")) {
				retval = true;
				break;
			}
		}

		return retval;
	}

	public String getOfficialName() {
		return officialName;
	}

	public void setOfficialName(String officialName) {
		this.officialName = officialName;
	}

	public Boolean isVerified() {
		return verified;
	}

	public void setVerified(Boolean verified) {
		this.verified = verified;
	}
}