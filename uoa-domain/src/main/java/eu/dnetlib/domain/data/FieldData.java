package eu.dnetlib.domain.data;

import java.util.ArrayList;
import java.util.List;

public class FieldData {

	private List<FieldRow> fieldRowList = new ArrayList<FieldRow>();

	public List<FieldRow> getFieldRowList() {
		return fieldRowList;
	}

	public void setFieldRowList(List<FieldRow> fieldRowList) {
		this.fieldRowList = fieldRowList;
	}

	public List<String> getValues() {
		List<String> values = new ArrayList<String>();		
		for(FieldRow row: fieldRowList){
			values.add(row.getValue());
		}
		
		return values;
	}

	public List<Integer> getCount() {
		List<Integer> counts = new ArrayList<Integer>();		
		for(FieldRow row: fieldRowList){
			counts.add(row.getCount());
		}
		
		return counts;
	}
}
