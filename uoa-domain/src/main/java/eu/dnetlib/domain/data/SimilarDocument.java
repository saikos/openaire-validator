package eu.dnetlib.domain.data;

public class SimilarDocument {

	private String id;
	private float score;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public float getScore() {
		return score;
	}

	public void setScore(float score) {
		this.score = score;
	}

}
