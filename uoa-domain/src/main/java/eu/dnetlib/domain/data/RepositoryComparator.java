package eu.dnetlib.domain.data;

import java.util.Comparator;

public class RepositoryComparator implements Comparator<Repository> {

	public int compare(Repository o1, Repository o2) {
		return o1.getOfficialName().compareToIgnoreCase(o2.getOfficialName());
	}

}
