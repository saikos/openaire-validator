package eu.dnetlib.domain.data;

import java.util.Collection;
import java.util.List;

/**
 * 
 * @author kiatrop
 *
 */

/**
 * Contains the results of a query together with 
 * information about the returned locale,
 * the total number of records , the current page, the page size  
 */
public class SearchResult {
	//the cql query
	private String query;	
	//the locale
	private String locale;	
	//the total number of records in the index
	//for the given query
	private int total;
	//the current page of results
	private int page;
	//the current number of records in the current page
	private int size;
	//refine fields
	private Collection<String> fields;
	
	//search results in xml
	private List<String> searchResults;
	
	//refine - browse results in xml
	private List<String> browseResults;
	
	public SearchResult() {
	}
	
	public SearchResult(String query, String locale, int total, 
			int page, int size, List<String> searchResults) {
		this(query, locale, total, page, size, searchResults, null, null);
	}
	
	public SearchResult(String query, String locale, Collection<String> fields,
			List<String> browseResults) {
		this(query, locale, 0, 0, 0, null, browseResults, fields);
	}
	
	public SearchResult(String query, String locale, int total, int page, int size, 
			List<String> searchResults, List<String> browseResults,  Collection<String> fields) {
		this.query = query;
		this.locale = locale;
		this.total = total;
		this.page = page;
		this.size = size;
		this.fields = fields;
		
		this.searchResults = searchResults;
		this.browseResults = browseResults;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public Collection<String> getFields() {
		return fields;
	}

	public void setFields(Collection<String> fields) {
		this.fields = fields;
	}

	public List<String> getSearchResults() {
		return searchResults;
	}

	public void setSearchResults(List<String> searchResults) {
		this.searchResults = searchResults;
	}

	public List<String> getBrowseResults() {
		return browseResults;
	}

	public void setBrowseResults(List<String> browseResults) {
		this.browseResults = browseResults;
	}
}
