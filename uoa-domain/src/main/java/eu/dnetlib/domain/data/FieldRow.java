package eu.dnetlib.domain.data;

public class FieldRow {
	
	private String value;
	private Integer count;
	
	public FieldRow(String value, Integer count) {
		this.value = value;
		this.count = count;
	}
	
	public FieldRow() {
		
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public Integer getCount() {
		return count;
	}
	
	public void setCount(Integer count) {
		this.count = count;
	}

	@Override
	public String toString() {	
		StringBuffer buffer = new StringBuffer();
		buffer.append(value).append(" : ").append(count);
		
		return buffer.toString();
	}

}


