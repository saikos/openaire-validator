package eu.dnetlib.domain.data;

public class FormattedSearchResult {

	// String containing the formatted results of search
	private String formattedResult;

	// the total number of records in the index
	// for the given query
	private int resultsNumber;

	public FormattedSearchResult() {
	}

	public FormattedSearchResult(String formattedResult, int resultsNumber) {
		this.formattedResult = formattedResult;
		this.resultsNumber = resultsNumber;
	}

	public String getFormattedResult() {
		return formattedResult;
	}

	public int getResultsNumber() {
		return resultsNumber;
	}

	public void setFormattedResult(String formattedResult) {
		this.formattedResult = formattedResult;
	}

	public void setResultsNumber(int resultsNumber) {
		this.resultsNumber = resultsNumber;
	}

}
