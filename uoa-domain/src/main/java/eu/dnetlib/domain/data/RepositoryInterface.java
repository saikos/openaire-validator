package eu.dnetlib.domain.data;


import java.util.HashMap;
import java.util.Map;

public class RepositoryInterface{

    private static final long serialVersionUID = 8013272950607614479L;

    // FOR VALIDATOR
    private String desiredCompatibilityLevel;
    private String complianceName;
    private String upgradeToV3;
    private boolean deleteApi;
    private String accessSet;
    private String accessFormat;
    private String metadataIdentifierPath;
    private String lastCollectionDate;
    private String nextScheduledExecution;

    private String status;
    private String collectedFrom;

    // FOR DATASOURCE MANAGER SERVICE
    private String id;
    private String typology;
    private String compliance;
    private String contentDescription;
    private String accessProtocol;
    private String baseUrl;
    private boolean active = false;
    private boolean removable = true;
    private Map<String, String> accessParams = new HashMap<String, String>();
    private Map<String, String> extraFields = new HashMap<String, String>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTypology() {
        return typology;
    }

    public void setTypology(String typology) {
        this.typology = typology;
    }

    public String getCompliance() {
        return compliance;
    }

    public void setCompliance(String compliance) {
        this.compliance = compliance;
    }

    public String getContentDescription() {
        return contentDescription;
    }

    public void setContentDescription(String contentDescription) {
        this.contentDescription = contentDescription;
    }

    public String getAccessProtocol() {
        return accessProtocol;
    }

    public void setAccessProtocol(String accessProtocol) {
        this.accessProtocol = accessProtocol;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isRemovable() {
        return removable;
    }

    public void setRemovable(boolean removable) {
        this.removable = removable;
    }

    public Map<String, String> getAccessParams() {
        return accessParams;
    }

    public void setAccessParams(Map<String, String> accessParams) {
        this.accessParams = accessParams;
    }

    public Map<String, String> getExtraFields() {
        return extraFields;
    }

    public void setExtraFields(Map<String, String> extraFields) {
        this.extraFields = extraFields;
    }

    public String getAccessSet() {
        this.accessSet = accessParams.get("set");
        return accessSet;
    }

    public void setAccessSet(String accessSet) {
        this.accessSet = accessSet;
        this.accessParams.put("set", accessSet);
    }

    public void removeAccessSet() {
        accessParams.remove("set");
    }

    public String getAccessFormat() {
        this.accessFormat = accessParams.get("format");
        return accessFormat;
    }

    public void setAccessFormat(String accessFormat) {
        this.accessFormat = accessFormat;
        this.accessParams.put("format", accessFormat);
    }

    public String getLastCollectionDate() {
        this.lastCollectionDate = extraFields.get("last_collection_date");
        return lastCollectionDate;
    }

    public void setLastCollectionDate(String lastCollectionDate) {
        this.lastCollectionDate = lastCollectionDate;
        this.extraFields.put("last_collection_date", lastCollectionDate);
    }

    public String getStatus() {
        this.status = extraFields.get("status");
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
        this.extraFields.put("status", status);
    }

    public String getNextScheduledExecution() {
        return nextScheduledExecution;
    }

    public void setNextScheduledExecution(String nextScheduledExecution) {
        this.nextScheduledExecution = nextScheduledExecution;
    }

    public String getMetadataIdentifierPath() {
        this.metadataIdentifierPath = extraFields.get("metadata_identifier_path");
        return metadataIdentifierPath;
    }

    public void setMetadataIdentifierPath(String metadataIdentifierPath) {
        this.metadataIdentifierPath = metadataIdentifierPath;
        this.extraFields.put("metadata_identifier_path", metadataIdentifierPath);
    }

    public String getUpgradeToV3() {
        return upgradeToV3;
    }

    public void setUpgradeToV3(String upgradeToV3) {
        this.upgradeToV3 = upgradeToV3;
    }

    public boolean isDeleteApi() {
        return deleteApi;
    }

    public void setDeleteApi(boolean deleteApi) {
        this.deleteApi = deleteApi;
    }

    public String getDesiredCompatibilityLevel() {
        return desiredCompatibilityLevel;
    }

    public void setDesiredCompatibilityLevel(String desiredCompatibilityLevel) {
        this.desiredCompatibilityLevel = desiredCompatibilityLevel;
    }

    public String getComplianceName() {
        return complianceName;
    }

    public void setComplianceName(String complianceName) {
        this.complianceName = complianceName;
    }

    public String getCollectedFrom() {
        return collectedFrom;
    }

    public void setCollectedFrom(String collectedFrom) {
        this.collectedFrom = collectedFrom;
    }
}
