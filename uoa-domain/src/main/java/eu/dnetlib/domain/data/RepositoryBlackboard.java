package eu.dnetlib.domain.data;

import java.util.ArrayList;
import java.util.List;

public class RepositoryBlackboard {

	private String lastrequest = "";
	private String lastresponse = "";
	private List<RepositoryBlackboardMessage> messages = new ArrayList<RepositoryBlackboardMessage>();

	public String getLastrequest() {
		return lastrequest;
	}

	public void setLastrequest(String lastrequest) {
		this.lastrequest = lastrequest;
	}

	public String getLastresponse() {
		return lastresponse;
	}

	public void setLastresponse(String lastresponse) {
		this.lastresponse = lastresponse;
	}

	public List<RepositoryBlackboardMessage> getMessages() {
		return messages;
	}

	public void setMessages(List<RepositoryBlackboardMessage> messages) {
		this.messages = messages;
	}

}
