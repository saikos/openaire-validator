/**
 *
 */
package eu.dnetlib.domain.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author kiatrop
 *
 */
public class Document {

	private Map<String, List<String>> map = null;

	public Document() {
		map = new HashMap<String, List<String>>();
	}
	
	public Document(Map<String, List<String>> map) {
		this.map = map;
	}

	public Set<String> getFieldNames() {
		return map.keySet();
	}

	public Map<String, List<String>> getMap() {
		return map;
	}

	public void setMap(Map<String, List<String>> map) {
		this.map = map;
	}

	public void addFieldValue(String fieldname, String value) {
		List<String> values = map.get(fieldname);
		if (values == null) {
			values = new ArrayList<String>();
			map.put(fieldname, values);
		}
		values.add(value);
	}

	public List<String> getFieldValues(String fieldname) {
		return map.get(fieldname);
	}

}
