package eu.dnetlib.domain.data;

public class Hint { 
	private String alternateTerm = null; 
	private boolean autoFollowHint = false;
	
	public String getAlternateTerm() {
		return alternateTerm;
	}
	public void setAlternateTerm(String alternateTerm) {
		this.alternateTerm = alternateTerm;
	}
	public boolean isAutoFollowHint() {
		return autoFollowHint;
	}
	public void setAutoFollowHint(boolean autoFollowHint) {
		this.autoFollowHint = autoFollowHint;
	}
	
}
