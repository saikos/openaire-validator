package eu.dnetlib.domain.data;


public class DataCollectionInterface {

	private DataCollectionAccessProtocol protocol = null;
	private String baseUrl;
	private String format;
	private String filter;
	
	public DataCollectionAccessProtocol getProtocol() {
		return protocol;
	}
	public void setProtocol(DataCollectionAccessProtocol protocol) {
		this.protocol = protocol;
	}
	public String getBaseUrl() {
		return baseUrl;
	}
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getFilter() {
		return filter;
	}
	public void setFilter(String filter) {
		this.filter = filter;
	}

	
}
