package eu.dnetlib.domain;

/**
 * This interface is used to define search criteria in order to search for resources in the IS_Lookup.
 * Provides only one method, <code>matches(Object)</code>
 */
public interface SearchCriteria {
	public boolean matches(Object o);
}
