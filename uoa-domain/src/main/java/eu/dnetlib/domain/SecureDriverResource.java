package eu.dnetlib.domain;

import eu.dnetlib.domain.enabling.SecurityProfile;

public abstract class SecureDriverResource extends DriverResource {
	private SecurityProfile securityProfile = new SecurityProfile();
	private static final long serialVersionUID = -3196808530359828895L;

	public SecurityProfile getSecurityProfile() {
		return securityProfile;
	}

	public void setSecurityProfile(SecurityProfile securityProfile) {
		this.securityProfile = securityProfile;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((securityProfile == null) ? 0 : securityProfile.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SecureDriverResource other = (SecureDriverResource) obj;
		if (securityProfile == null) {
			if (other.securityProfile != null)
				return false;
		} else if (!securityProfile.equals(other.securityProfile))
			return false;
		return true;
	}
}
