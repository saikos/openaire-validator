package eu.dnetlib.domain;

public enum ResourceType {
	USERDSRESOURCETYPE("UserDSResourceType"),
	COMMUNITYDSRESOURCETYPE("CommunityDSResourceType"),
	COLLECTIONDSRESOURCETYPE("CollectionDSResourceType"),
	RECOMMENDATIONDSRESOURCETYPE("RecommendationDSResourceType"),
	REPOSITORYSERVICERESOURCETYPE("RepositoryServiceResourceType"),
	VOCABULARYDSRESOURCETYPE("VocabularyDSResourceType"),
	SECURITYPROFILEDSRESOURCETYPE("SecurityProfileDSResourceType"),
	MDFORMATRESOURCETYPE("MDFormatDSResourceType"),
	QUERYHASHRESOURCETYPE("QueryHashDSResourceType"),
	WEBLAYOUTRESOURCETYPE("WebInterfaceLayoutResourceType"),
	INDEXDSRESOURCETYPE("IndexDSResourceType"),
	RSSFEEDRESOURCETYPE("RSSFeedDSResourceType"),
	
	CSHARVESTERSERVICERESOURCETYPE("CSHarvesterServiceResourceType"),
	STATSMANAGERSERVICERESOURCETYPE("StatsManagerServiceResourceType"),
	LOCALDOWNLOADMANAGERSERVICERESOURCETYPE("LocalDownloadManagerServiceResourceType"),
	VALIDATORSERVICERESOURCETYPE("ValidatorServiceResourceType"),
    SEARCHSERVICERESOURCETYPE("SearchServiceResourceType"),
	
	ANY("*");
	
	private final String value;
	
	ResourceType(String str) {
		this.value = str;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return this.getValue();
	}
}
