/**
 * 
 */
package eu.dnetlib.domain;

public enum ActionType {
	CREATE("CREATE"),
	UPDATE("UPDATE"),
	DELETE("DELETE");
	
	private final String value;

	ActionType(String str) {
		this.value = str;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return this.getValue();
	}
}