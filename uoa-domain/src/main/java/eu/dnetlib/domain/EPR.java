package eu.dnetlib.domain;

import java.util.HashMap;
import java.util.Map;

public class EPR {
	/** The w3c epr. Kept to avoid parsing/creating it if not needed. */
	private String epr = null;
	
	/** The reference parameters of the w3c epr */
	private Map<String, String> parameters = new HashMap<String, String>();
	
	/** The address of the w3c epr */
	private String address = null;
	
	/** Service name of the w3c epr */
	private String serviceName = null;
	
	/** Endpoint name of the w3c epr */
	private String endpointName = null;
	
	public String getParameter(String name) {
		return parameters.get(name);
	}
	
	public void setParameter(String name, String value) {
		this.epr = null;
		this.parameters.put(name, value);
	}
	
	public String[] getParameterNames() {
		return parameters.keySet().toArray(new String[] {});
	}
	
	public void removeParameter(String name) {
		this.epr = null;
		
		this.parameters.remove(name);
	}
	
	public void clearParameters() {
		this.epr = null;
		
		this.parameters.clear();
	}

	public String getEpr() {
		return epr;
	}

	public void setEpr(String epr) {
		this.epr = epr;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.epr = null;
		
		this.address = address;
	}
	
	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getEndpointName() {
		return endpointName;
	}

	public void setEndpointName(String endpointName) {
		this.endpointName = endpointName;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		builder.append("[address: ").append(address).append(", serviceName: ");
		builder.append(serviceName).append(", endpointName: ");
		builder.append(endpointName).append(", parameters: ");
		builder.append(parameters.toString()).append("]");
		
		return builder.toString();
	}
}