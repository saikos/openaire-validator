package eu.dnetlib.domain.functionality;

import java.util.Locale;
import java.util.Map;

public class DocumentExternalUrlDisplayType extends ExternalUrlDisplayType{

	public DocumentExternalUrlDisplayType(String field, Map<Locale, String> descriptionMap) {
		super(field, descriptionMap);
	}
	
/*	private StringBuilder generateHref(String fieldValue, StringBuilder builder,
			Document document) {
		String encodedFieldValue = null;
		try {
			encodedFieldValue = URLEncoder.encode(fieldValue, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			encodedFieldValue = fieldValue;
		}

		return builder.append("<a class=\"externallink\" href=\"").append(this.getAction())
						.append(".action?").append(this.getParameter())
						.append("=").append(encodedFieldValue)
						.append("&docId=").append(document.getFieldValues("id").get(0))
						.append("\" target=\"_blank\"").append(">");
	}
	
	@Override
	public int getDisplayMessage(StringBuilder builder, Document document, 
			String fieldValue) {
		String finalValue = replaceFieldValueExpression(document,
				getDescription(), fieldValue);
		builder = generateHref(fieldValue, builder, document);
		builder.append(finalValue);
		builder.append("</a>");

		return finalValue.length();
		
	}
	
	public int getDisplayMessage(StringBuilder builder, Document document, 
			String fieldValue, String secondaryLink) {
		
		String finalValue = replaceFieldValueExpression(document,
				secondaryLink, fieldValue);
		builder = generateHref(fieldValue, builder, document);
		builder.append(finalValue);
		builder.append("</a>");

		return finalValue.length();
		
	}
*/	
}


