package eu.dnetlib.domain.functionality;


public class CommunityRegistration {
	private int id = 0;
	private String communityId = null;
	private boolean alertOnChange = false;
	private Boolean includeInQueries = false;

	public boolean isAlertOnChange() {
		return alertOnChange;
	}

	public void setAlertOnChange(boolean alertOnChange) {
		this.alertOnChange = alertOnChange;
	}

	public String getCommunityId() {
		return communityId;
	}

	public void setCommunityId(String communityId) {
		this.communityId = communityId;
	}

	public Boolean getIncludeInQueries() {
		return includeInQueries;
	}

	public void setIncludeInQueries(Boolean includeInQueries) {
		this.includeInQueries = includeInQueries;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (alertOnChange ? 1231 : 1237);
		result = prime * result
				+ ((communityId == null) ? 0 : communityId.hashCode());
		result = prime * result + id;
		result = prime
				* result
				+ ((includeInQueries == null) ? 0 : includeInQueries.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommunityRegistration other = (CommunityRegistration) obj;
		if (alertOnChange != other.alertOnChange)
			return false;
		if (communityId == null) {
			if (other.communityId != null)
				return false;
		} else if (!communityId.equals(other.communityId))
			return false;
		if (id != other.id)
			return false;
		if (includeInQueries == null) {
			if (other.includeInQueries != null)
				return false;
		} else if (!includeInQueries.equals(other.includeInQueries))
			return false;
		return true;
	}
}
