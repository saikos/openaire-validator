package eu.dnetlib.domain.functionality;

import java.util.Locale;
import java.util.Map;

public abstract class DisplayType {

//	private final Pattern pattern = Pattern.compile("\\$\\{(\\w*)\\}");
	private Map<Locale, String> descriptionMap = null;
	private String field = null;
	
	public DisplayType(String field) {
		setField(field);
	}
	
	public DisplayType(String field, Map<Locale, String> descriptionMap) {
		setField(field);
		setDescriptionMap(descriptionMap);
	}
	
/*	abstract int getDisplayMessage(StringBuilder builder, Document document, 
			String fieldValue);
	
	abstract int getDisplayMessage(StringBuilder builder, Document document, 
			String fieldValue, String secondaryLink);

	String replaceFieldValueExpression(Document document, String expression, String fieldValue) {
		if (expression == null) {
			return fieldValue;
		}
		
		Matcher matcher = pattern.matcher(expression);
		
		if (!matcher.find()) {
			return expression;
			
		} else {
			StringBuffer sb = new StringBuffer();
			do {
				String field = matcher.group(0).
					substring(2, matcher.group(0).length() - 1);
				String replacement = "";
				
				if (!field.equals(getField())){
					List<String> values = document.getMap()
						.get(field);
			
					for (String s:values) {
						replacement += s + " ";
					}
				} else {
					replacement = fieldValue;
				}
				
				matcher.appendReplacement(sb, replacement.trim());
			
			} while(matcher.find());
			
			matcher.appendTail(sb);

			return sb.toString().trim();
		}
	}
*/
	
/*	public String getDescription() {
		System.out.println(descriptionMap);
		if (descriptionMap != null) {
			String description = descriptionMap.get(LocaleHolder.getLocale());
			System.out.println(description);
			if (description == null) {
				return descriptionMap.get(new Locale("en", "GB"));
			}
			return description;
			
		} else {
			return null;
			
		}
	}
*/
	public Map<Locale, String> getDescriptionMap() {
		return descriptionMap;
	}
	
	public void setDescriptionMap(Map<Locale, String> descriptionMap) {
		this.descriptionMap = descriptionMap;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}
}