package eu.dnetlib.domain.functionality;

import java.util.Date;

import eu.dnetlib.domain.DriverServiceProfile;

public class CommunityServiceProfile extends DriverServiceProfile {

	private static final long serialVersionUID = -7681096203717237115L;

	public CommunityServiceProfile() {
		super("ServiceResources", "CommunityServiceResourceType", new Date());
	}
	
}
