package eu.dnetlib.domain.functionality;


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import eu.dnetlib.domain.DriverResource;

/**
 * The domain object for the Recommendation resource data structure
 *
 */
public class Recommendation extends DriverResource {

    /**
	 * 
	 */
	private static final long serialVersionUID = -8055604728449435236L;
	
	public static final String ANOUCHMENT_TYPE = "Announcement";
    public static final String COMMUNITY_TYPE = "Community";
    public static final String USER_TYPE = "USER";
    public static final String QUERY_TYPE = "Query";
    private String content = null;
    private Date generationDate = null;
    private Date expirationDate = null;
    private String type = null;
    private Set<String> communityIds = new HashSet<String>();
    private Set<String> userIds = new HashSet<String>();
    private boolean active;
    private int index;
    private boolean showTitle;
    private String title;

    /**
     * The default constructor sets the resourceKind to <b>RecommendationDSResources</b>, 
     * resourceType to <b>RecommendationDSResourceType</b> and dateOfCreation to the current system date.
     */
    public Recommendation() {
        this.setResourceKind("RecommendationDSResources");
        this.setResourceType("RecommendationDSResourceType");
        this.setDateOfCreation(new Date());
        index = 0;
        active = true;
        this.showTitle = true;
        this.title = "";
        this.content = "";
        this.type = Recommendation.ANOUCHMENT_TYPE;
    }

    public String getType() {
        return type;
    }

    public void setType(String t) {
        this.type = t;
    }

    @Deprecated
    public String getId() {
        return this.getResourceId();
    }

    @Deprecated
    public void setId(String id) {
        this.setResourceId(id);
    }

    public String getContent() {
        return content;
    }

    public void setContent(String text) {
        content = text;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public void setGenerationDate(Date generationDate) {
        this.generationDate = generationDate;
    }

    public Date getGenerationDate() {
        return generationDate;
    }

    public Set<String> getCommunityIds() {
        return communityIds;
    }

    public void setCommunityIds(Set<String> communityIds) {
        this.communityIds = communityIds;
    }

    public Set<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(Set<String> userIds) {
        this.userIds = userIds;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isShowTitle() {
        return showTitle;
    }

    public void setShowTitle(boolean showTitle) {
        this.showTitle = showTitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
/*
	@Override
	public int compareTo(Object arg0) {
		Recommendation r = (Recommendation)arg0;
		if( r.getIndex() > this.getIndex() )
			return -1;
		else if ( r.getIndex() == this.getIndex() )
			return 0;
		else {
			return 1;
		}		
	}
	*/
}
