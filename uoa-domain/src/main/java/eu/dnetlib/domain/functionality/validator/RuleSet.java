package eu.dnetlib.domain.functionality.validator;

import java.util.List;
import java.util.Set;

public class RuleSet{

	private static final long serialVersionUID = -8620509939907943297L;
	private Integer id;
	private String name, description, guidelinesAcronym, shortName;
	private List<String> visibility;
	
	private List<Rule> contentRules;
	private List<Rule> usageRules;
	
	private Set<Integer> contentRulesIds;
	private Set<Integer> usageRulesIds;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public List<Rule> getContentRules() {
		return contentRules;
	}

	public void setContentRules(List<Rule> contentRules) {
		this.contentRules = contentRules;
	}

	public List<Rule> getUsageRules() {
		return usageRules;
	}

	public void setUsageRules(List<Rule> usageRules) {
		this.usageRules = usageRules;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public String getGuidelinesAcronym() {
		return guidelinesAcronym;
	}

	public void setGuidelinesAcronym(String guidelinesAcronym) {
		this.guidelinesAcronym = guidelinesAcronym;
	}

	public List<String> getVisibility() {
		return visibility;
	}

	public void setVisibility(List<String> visibility) {
		this.visibility = visibility;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public Set<Integer> getContentRulesIds() {
		return contentRulesIds;
	}

	public void setContentRulesIds(Set<Integer> contentRulesIds) {
		this.contentRulesIds = contentRulesIds;
	}

	public Set<Integer> getUsageRulesIds() {
		return usageRulesIds;
	}

	public void setUsageRulesIds(Set<Integer> usageRulesIds) {
		this.usageRulesIds = usageRulesIds;
	}
	
}
