package eu.dnetlib.domain.functionality;

import java.util.ArrayList;
import java.util.List;


public class DocumentDescription {
	
	private String view;
	
	private List<DocumentField> documentFields = 
		new ArrayList<DocumentField>();
	
	public DocumentDescription(String view) {
		this.view = view;
	}
	
 	public void putDocumentField(DocumentField documentField){
		documentFields.add(documentField);
	}

	public List<DocumentField> getDocumentFields(){
		return documentFields;
	}

	public DocumentField getTitleField() {
		return documentFields.get(0);
	}
	
	public List<DocumentField> getNonTitleFields() {
		if (documentFields.size() < 2) {
			return null;
		}		
		return documentFields.subList(1, documentFields.size());
	}

	public String getView() {
		return view;
	}

	public void setView(String view) {
		this.view = view;
	}
}