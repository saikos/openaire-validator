package eu.dnetlib.domain.functionality;

import eu.dnetlib.domain.SearchCriteria;
import eu.dnetlib.domain.SearchCriteriaImpl;

public class RSSFeedSearchCriteria extends SearchCriteriaImpl implements SearchCriteria {

	private String cqlQuery = null;
	private String userId = null;
	
	public RSSFeedSearchCriteria() {
		
	}
	
	public RSSFeedSearchCriteria(String cqlQuery) {
		super();
		this.cqlQuery = cqlQuery;
	}

	public boolean matches(Object o) {
		RSSFeed rf = (RSSFeed)o;
		if( cqlQuery != null ) {
			if( rf.getQueryCql() == null )
				return false;
			if( rf.getQueryCql().equals(cqlQuery) == false )
				return false;
		}
		if( userId != null ){
			if( rf.getUsedId() == null )
				return false;
			if( rf.getUsedId().equals(userId) == false )			
				return false;
		}
		return true;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCqlQuery() {
		return cqlQuery;
	}

	public void setCqlQuery(String cqlQuery) {
		this.cqlQuery = cqlQuery;
	}

}
