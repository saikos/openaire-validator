package eu.dnetlib.domain.functionality;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import eu.dnetlib.domain.DriverResource;

/**
 * 
 * The domain object for the Collection resource data structure
 * 
 */
public class Collection extends DriverResource {
	private static final long serialVersionUID = 6325631170397780724L;

	private String Name = null;  // +
	private String Owner = null; // +
	private String Father = null;  // -
	private List<String> description = new ArrayList<String>(); // +
	private String Subject = null; // -
	private boolean Private = true;
	private boolean Visible = false;
	private boolean Container = false;
	private Set<String> Children = new HashSet<String>(); // -
	private String Query = null;  // + (timi an container = false)
	private int DocumentCount = 0; 
	private Date LastUpdateDate = null; // -
	private boolean Frozen = false;
	private String iconUrl = null;  // -
	private String retrievalCondition = null;
	private List<String> documentsInBasket = new ArrayList<String>();
	
	public String getRetrievalCondition() {
		return retrievalCondition;
	}

	public void setRetrievalCondition(String retrievalCondition) {
		this.retrievalCondition = retrievalCondition;
	}

	/**
	 * The default constructor sets the resourceKind to <b>CollectionDSResources</b>, 
	 * resourceType to <b>CollectionDSResourceType</b> and dateOfCreation to the current system date.
	 */
	public Collection() {
		this.setResourceKind("CollectionDSResources");
		this.setResourceType("CollectionDSResourceType");
		this.setDateOfCreation(new Date());
	}
	
	@Deprecated
	public String getId() {
		return this.getResourceId();
	}
	@Deprecated
	public void setId(String id) {
		this.setResourceId(id);
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public Set<String> getChildren() {
		return Children;
	}
	public void setChildren(Set<String> children) {
		Children = children;
	}
	public boolean isContainer() {
		return Container;
	}
	public void setContainer(boolean container) {
		Container = container;
	}
	public int getDocumentCount() {
		return DocumentCount;
	}
	public void setDocumentCount(int documentCount) {
		DocumentCount = documentCount;
	}
	public String getFather() {
		return Father;
	}
	public void setFather(String father) {
		Father = father;
	}
	public boolean isFrozen() {
		return Frozen;
	}
	public void setFrozen(boolean frozen) {
		Frozen = frozen;
	}
	public Date getLastUpdateDate() {
		return LastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		LastUpdateDate = lastUpdateDate;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getOwner() {
		return Owner;
	}
	public void setOwner(String owner) {
		Owner = owner;
	}
	public boolean isPrivate() {
		return Private;
	}
	public void setPrivate(boolean private1) {
		Private = private1;
	}
	public String getQuery() {
		return Query;
	}
	public void setQuery(String query) {
		Query = query;
	}
	public String getSubject() {
		return Subject;
	}
	public void setSubject(String subject) {
		Subject = subject;
	}
	public boolean isVisible() {
		return Visible;
	}
	public void setVisible(boolean visible) {
		Visible = visible;
	}
	public List<String> getDescription() {
		return description;
	}
	public void setDescription(List<String> description) {
		this.description = description;
	}

	public List<String> getDocumentsInBasket() {
		return documentsInBasket;
	}

	public void setDocumentsInBasket(List<String> documentsInBasket) {
		this.documentsInBasket = documentsInBasket;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((Children == null) ? 0 : Children.hashCode());
		result = prime * result + (Container ? 1231 : 1237);
		result = prime * result + DocumentCount;
		result = prime * result + ((Father == null) ? 0 : Father.hashCode());
		result = prime * result + (Frozen ? 1231 : 1237);
		result = prime * result
				+ ((LastUpdateDate == null) ? 0 : LastUpdateDate.hashCode());
		result = prime * result + ((Name == null) ? 0 : Name.hashCode());
		result = prime * result + ((Owner == null) ? 0 : Owner.hashCode());
		result = prime * result + (Private ? 1231 : 1237);
		result = prime * result + ((Query == null) ? 0 : Query.hashCode());
		result = prime * result + ((Subject == null) ? 0 : Subject.hashCode());
		result = prime * result + (Visible ? 1231 : 1237);
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((iconUrl == null) ? 0 : iconUrl.hashCode());
		result = prime
				* result
				+ ((retrievalCondition == null) ? 0 : retrievalCondition
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Collection other = (Collection) obj;
		if (Children == null) {
			if (other.Children != null)
				return false;
		} else if (!Children.equals(other.Children))
			return false;
		if (Container != other.Container)
			return false;
		if (DocumentCount != other.DocumentCount)
			return false;
		if (Father == null) {
			if (other.Father != null)
				return false;
		} else if (!Father.equals(other.Father))
			return false;
		if (Frozen != other.Frozen)
			return false;
		if (LastUpdateDate == null) {
			if (other.LastUpdateDate != null)
				return false;
		} else if (!LastUpdateDate.equals(other.LastUpdateDate))
			return false;
		if (Name == null) {
			if (other.Name != null)
				return false;
		} else if (!Name.equals(other.Name))
			return false;
		if (Owner == null) {
			if (other.Owner != null)
				return false;
		} else if (!Owner.equals(other.Owner))
			return false;
		if (Private != other.Private)
			return false;
		if (Query == null) {
			if (other.Query != null)
				return false;
		} else if (!Query.equals(other.Query))
			return false;
		if (Subject == null) {
			if (other.Subject != null)
				return false;
		} else if (!Subject.equals(other.Subject))
			return false;
		if (Visible != other.Visible)
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (iconUrl == null) {
			if (other.iconUrl != null)
				return false;
		} else if (!iconUrl.equals(other.iconUrl))
			return false;
		if (retrievalCondition == null) {
			if (other.retrievalCondition != null)
				return false;
		} else if (!retrievalCondition.equals(other.retrievalCondition))
			return false;
		return true;
	}
}