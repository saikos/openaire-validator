package eu.dnetlib.domain.functionality;

import java.net.URI;

/**
 * This class represents a query of the notification service.
 * @author thanos@di.uoa.gr
 * @see eu.dnetlib.api.functionality.NotificationService
 * @see eu.dnetlib.functionality.notification.executor.NotificationQueryExecutor
 * @see NotificationSchedule
 * @see NotificationEvent
 * @see NotificationResult
 * @see NotificationSubscription
 * 
 */
public class NotificationQuery implements Comparable<NotificationQuery> {
	private String queryId;
	private String queryLanguage;
	private URI sourceUri;
	private String queryString;
	
	/**
	 * Construct a new notification query.
	 * @param queryId unique identifier of this query
	 * @param queryLanguage the language in which the query is expressed, determines which executor to use
	 * @param sourceUri the URI of the data source that can answer the query
	 * @param queryString the actual query
	 */
	public NotificationQuery(final String queryId, final String queryLanguage, final URI sourceUri, final String queryString) {
		this.queryId = queryId;
		this.queryLanguage = queryLanguage;
		this.sourceUri = sourceUri;
		this.queryString = queryString;
	}
	
	/**
	 * Construct a new notification query with query identifier, query language, source URI and query string set to null. 
	 */
	public NotificationQuery() {
		this(null, null, null, null);
	}

	/**
	 * Get the unique identifier of this query.
	 * @return the unique identifier of this query
	 */
	public String getQueryId() {
		return queryId;
	}
	
	/**
	 * Set the unique identifier of this query.
	 * @param queryId the unique identifier of this query
	 */
	public void setQueryId(final String queryId) {
		this.queryId = queryId;
	}
	
	/**
	 * Get the query language of this query.
	 * @return the query language of this query
	 */
	public String getQueryLanguage() {
		return queryLanguage;
	}
	
	/**
	 * Set the the query language of this query
	 * @param queryLanguage the query language of this query
	 */
	public void setQueryLanguage(final String queryLanguage) {
		this.queryLanguage = queryLanguage;
	}
	
	/**
	 * Get the source URI of this query.
	 * @return the URI of the data source of this query
	 */
	public URI getSourceUri() {
		return sourceUri;
	}

	/**
	 * Set the source URI of this query.
	 * @param sourceUri the URI of the data source of this query
	 */
	public void setSourceUri(final URI sourceUri) {
		this.sourceUri = sourceUri;
	}
	
	/**
	 * Get the query string of this query.
	 * @return the query string of this query
	 */
	public String getQueryString() {
		return queryString;
	}
	
	/**
	 * Set the query string of this query.
	 * @param queryString the query string of this query
	 */
	public void setQueryString(final String queryString) {
		this.queryString = queryString;
	}
	
	@Override
	public int compareTo(final NotificationQuery query) {
		return queryId.compareTo(query.queryId);
	}
	@Override
	public boolean equals(final Object object) {
		if (!(object instanceof NotificationQuery))
			return false;
		final NotificationQuery query = (NotificationQuery) object;
		return (queryId == null) ? (query.queryId == null) : queryId.equals(query.queryId);
	}
	
	@Override
	public int hashCode() {
		return (queryId == null) ? 0 : queryId.hashCode();
	}
	
	@Override
	public String toString() {
		return queryId;
	}
}
