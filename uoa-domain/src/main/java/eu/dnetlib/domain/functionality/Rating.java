package eu.dnetlib.domain.functionality;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * This class represents the rating of a document (average or by a user).
 * @author thanos@di.uoa.gr
 *
 */
@SuppressWarnings("unused")
@Entity
public class Rating {
	private static final float MAX_SCORE = 5;
	@Id @GeneratedValue 
	private long id;
	@Column(nullable = false)
	private String userId;
	@Column(nullable = false)
	private String documentId;
	@Column(nullable = false)
	private float score;
	
	/**
	 * Create a new rating with document and user set to null and score set to 0.
	 *
	 */
	public Rating() {
		this(null, null, 0.0f);
	}
	
	/**
	 * Create a new rating for the specified document and user with the specified score.
	 * @param userId the id of the user
	 * @param documentId the id of the document
	 * @param score the rating score
	 */
	public Rating(String userId, String documentId, float score) {
		this.userId = userId;
		this.documentId = documentId;
		this.score = (score > MAX_SCORE) ? MAX_SCORE : (score < 0 ? 0 : score); 
	}
	
	/**
	 * Create a new document average rating for the specified document with the specified score (user id is set to null).
	 * @param documentId the id of the document
	 * @param score the rating score
	 */
	public Rating(String documentId, float score) {
		this(null, documentId, score);
	}
	
	/**
	 * Get the user id.
	 * @return the id of the user
	 */
	public String getUserId() {
		return userId;
	}
	
	/**
	 * Set the user id.
	 * @param userId the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	/**
	 * Get the document id.
	 * @return the id of the document
	 */
	public String getDocumentId() {
		return documentId;
	}
	
	/**
	 * Set the document id.
	 * @param documentId the new document id
	 */
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}
	
	/**
	 * Get the score.
	 * @return the rating score
	 */
	public float getScore() {
		return score;
	}
	
	/**
	 * Set the score.
	 * @param score the new score
	 */
	public void setScore(float score) {
		this.score = (score > MAX_SCORE) ? MAX_SCORE : (score < 0 ? 0 : score);
	}
}
