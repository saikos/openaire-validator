package eu.dnetlib.domain.functionality;

import java.util.Date;

import eu.dnetlib.domain.SearchCriteria;
import eu.dnetlib.domain.SearchCriteriaImpl;


public class RecommendationSearchCriteria extends SearchCriteriaImpl implements
		SearchCriteria {
	
	private Date expiredBefore = null;
	private Date expiredAfter = null;
	private String recommendationType = null;
	private String communityId = null;
	private String userId = null;
	private Boolean active = null;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCommunityId() {
		return communityId;
	}

	public void setCommunityId(String communityId) {
		this.communityId = communityId;
	}

	public String getRecommendationType() {
		return recommendationType;
	}

	public void setRecommendationType(String recommendationType) {
		this.recommendationType = recommendationType;
	}

	public Date getExpiredAfter() {
		return expiredAfter;
	}

	public void setExpiredAfter(Date expiredAfter) {
		this.expiredAfter = expiredAfter;
	}

	public Date getExpiredBefore() {
		return expiredBefore;
	}

	public void setExpiredBefore(Date expiredBefore) {
		this.expiredBefore = expiredBefore;
	}

	public boolean matches(Object o) {
		Recommendation recommendation = (Recommendation) o;
		
		if (this.getCommunityId() != null) {
			if (recommendation.getCommunityIds() == null || 
					recommendation.getCommunityIds().size() == 0)
				return false;
			else {
				boolean found = false;
				
				for (String id:recommendation.getCommunityIds()) {
					if (id.equals(this.getCommunityId())) {
						found = true;
						
						break;
					}
				}
				
				if (!found)
					return false;
			}
		}
		
		if (this.getUserId() != null) {
			if (recommendation.getUserIds() == null || 
					recommendation.getUserIds().size() == 0)
				return false;
			else {
				boolean found = false;
				
				for (String id:recommendation.getUserIds()) {
					if (id.equals(this.getUserId())) {
						found = true;
						
						break;
					}
				}
				
				if (!found)
					return false;
			}
		}
		
		if (this.getExpiredAfter() != null) {
			if (recommendation.getExpirationDate() == null || 
					recommendation.getExpirationDate().before(this.getExpiredAfter()))
				return false;
		}
		
		if (this.getExpiredBefore() != null) {
			if (recommendation.getExpirationDate() == null || 
					recommendation.getExpirationDate().after(this.getExpiredBefore()))
				return false;
		}
		
		if (this.getRecommendationType() != null) {
			if (recommendation.getType() == null || 
					!recommendation.getType().toLowerCase().equals(this.getRecommendationType().toLowerCase()))
				return false;
		}
		
		if (this.active != null) {
			if (recommendation.isActive() != this.active) {
				return false;
			}
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((active == null) ? 0 : active.hashCode());
		result = prime * result
				+ ((communityId == null) ? 0 : communityId.hashCode());
		result = prime * result
				+ ((expiredAfter == null) ? 0 : expiredAfter.hashCode());
		result = prime * result
				+ ((expiredBefore == null) ? 0 : expiredBefore.hashCode());
		result = prime
				* result
				+ ((recommendationType == null) ? 0 : recommendationType
						.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		RecommendationSearchCriteria other = (RecommendationSearchCriteria) obj;
		if (active == null) {
			if (other.active != null)
				return false;
		} else if (!active.equals(other.active))
			return false;
		if (communityId == null) {
			if (other.communityId != null)
				return false;
		} else if (!communityId.equals(other.communityId))
			return false;
		if (expiredAfter == null) {
			if (other.expiredAfter != null)
				return false;
		} else if (!expiredAfter.equals(other.expiredAfter))
			return false;
		if (expiredBefore == null) {
			if (other.expiredBefore != null)
				return false;
		} else if (!expiredBefore.equals(other.expiredBefore))
			return false;
		if (recommendationType == null) {
			if (other.recommendationType != null)
				return false;
		} else if (!recommendationType.equals(other.recommendationType))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getActive() {
		return active;
	}
}

