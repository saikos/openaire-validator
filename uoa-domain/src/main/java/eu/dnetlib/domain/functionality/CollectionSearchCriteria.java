package eu.dnetlib.domain.functionality;

import eu.dnetlib.domain.SearchCriteria;
import eu.dnetlib.domain.SearchCriteriaImpl;

public class CollectionSearchCriteria extends SearchCriteriaImpl implements
		SearchCriteria {

	private String parentId = null;
	private Boolean root = null;
	private Boolean isPrivate = null;
	private String ownerId = null;
	private String name = null;
	private Boolean isContainer = null;

	public Boolean getIsContainer() {
		return isContainer;
	}

	public void setIsContainer(Boolean isContainer) {
		this.isContainer = isContainer;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public Boolean getRoot() {
		return root;
	}

	public void setRoot(Boolean root) {
		this.root = root;
	}

	public boolean matches(Object o) {
		Collection collection = (Collection) o;

		if (this.getContains() != null) {
			if (collection.getName() == null
					|| !collection.getName().toLowerCase().contains(
							this.getContains().toLowerCase()))
				return false;
		}

		if (this.getStartsWith() != null) {
			if (collection.getName() == null
					|| !collection.getName().toLowerCase().startsWith(
							this.getStartsWith().toLowerCase()))
				return false;
		}

		if (this.getEndsWith() != null) {
			if (collection.getName() == null
					|| !collection.getName().toLowerCase().endsWith(
							this.getEndsWith().toLowerCase()))
				return false;
		}

		if (this.getParentId() != null) {
			if (collection.getFather() == null
					|| !collection.getFather().equals(this.getParentId()))
				return false;
		}

		if (this.getRoot() != null) {
			if ((collection.getFather() == null) != this.getRoot())
				return false;
		}
		
		if (this.getIsContainer() != null) {
			if (!this.getIsContainer().equals(collection.isContainer()))
				return false;
		}

		if (this.getOwnerId() != null) {
			if (!this.getOwnerId().equals(collection.getOwner()))
				return false;
		}


		return true;
	}

	public Boolean getIsPrivate() {
		return isPrivate;
	}

	public void setIsPrivate(Boolean isPrivate) {
		this.isPrivate = isPrivate;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((isContainer == null) ? 0 : isContainer.hashCode());
		result = prime * result
				+ ((isPrivate == null) ? 0 : isPrivate.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((ownerId == null) ? 0 : ownerId.hashCode());
		result = prime * result
				+ ((parentId == null) ? 0 : parentId.hashCode());
		result = prime * result + ((root == null) ? 0 : root.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CollectionSearchCriteria other = (CollectionSearchCriteria) obj;
		if (isContainer == null) {
			if (other.isContainer != null)
				return false;
		} else if (!isContainer.equals(other.isContainer))
			return false;
		if (isPrivate == null) {
			if (other.isPrivate != null)
				return false;
		} else if (!isPrivate.equals(other.isPrivate))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (ownerId == null) {
			if (other.ownerId != null)
				return false;
		} else if (!ownerId.equals(other.ownerId))
			return false;
		if (parentId == null) {
			if (other.parentId != null)
				return false;
		} else if (!parentId.equals(other.parentId))
			return false;
		if (root == null) {
			if (other.root != null)
				return false;
		} else if (!root.equals(other.root))
			return false;
		return true;
	}
}
