package eu.dnetlib.domain.functionality;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import eu.dnetlib.domain.SecureDriverResource;

/**
 * User's Profile class
 * 
 * @author <a href="mailto:kiatrop@di.uoa.gr">Katerina Iatropoulou</a>
 * 
 */

public class UserProfile extends SecureDriverResource{
	private static final long serialVersionUID = -1565597762736460041L;

	// general info
	private String email = null;
	private String username = null;
	private String firstname = null;
	private String lastname = null;

	private String institution = null;

	private boolean active = false;
	private String activationId = null;

	// recommendations
	private Set<String> recommendations = new HashSet<String>();
	private Boolean recommendationSendEmail = null;
	private Integer recommendationPeriod = null;
	private Date lastNotificationDate = null;

	// communities
	private static Set<CommunityRegistration> communities = new HashSet<CommunityRegistration>();

	// queries
	private static List<SavedQuery> savedQueries = new ArrayList<SavedQuery>();
	
	// filter
	private static SavedQuery filter = null;
	
	// list of documents
	private List<String> documentIds = new ArrayList<String>();
	
	// a place to store images and other user data
	private static UserStore store = null;
	
	private String userPhotoID = null;
	

	/**
	 * The default constructor sets the resourceKind to
	 * <b>UserDSResourceType</b>, resourceType to <b>UserDSResources</b> and
	 * dateOfCreation to the current system date.
	 */
	public UserProfile() {
		this.setResourceKind("UserDSResources");
		this.setResourceType("UserDSResourceType");
		recommendationSendEmail = Boolean.valueOf(false);
	}

	@Deprecated
	public String getUserId() {
		return this.getResourceId();
	}

	@Deprecated
	public void setUserId(String userId) {
		this.setResourceId(userId);
	}

	public String getActivationId() {
		return activationId;
	}

	public void setActivationId(String activationId) {
		this.activationId = activationId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.getSecurityProfile().getPassword();
	}

	public void setPassword(String password) {
		this.getSecurityProfile().setPassword(password);
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public Set<String> getRecommendations() {
		return recommendations;
	}

	public void setRecommendations(Set<String> recommendations) {
		this.recommendations = recommendations;
	}

	public Date getLastNotificationDate() {
		return this.lastNotificationDate;
	}

	public void setLastNotificationDate(Date lastNotificationDate) {
		this.lastNotificationDate = lastNotificationDate;
	}

	public Set<CommunityRegistration> getCommunities() {
		return communities;
	}

	public void setCommunities(Set<CommunityRegistration> communities) {
		this.communities = communities;
	}

	public Date getRegistrationDate() {
		return this.getDateOfCreation();
	}

	public void setRegistrationDate(Date registrationDate) {
		this.setDateOfCreation(registrationDate);
	}

	public Integer getRecommendationPeriod() {
		return recommendationPeriod;
	}

	public List<String> getRoles() {
		return this.getSecurityProfile().getIdentities();
	}

	public void setRoles(List<String> roles) {
		this.getSecurityProfile().setIdentities(new ArrayList<String>());

		for (String role : roles) {
			if (!this.getSecurityProfile().getIdentities().contains(role))
				this.getSecurityProfile().getIdentities().add(role);
		}
	}

	public List<SavedQuery> getSavedQueries() {
		return savedQueries;
	}

	public void setSavedQueries(List<SavedQuery> savedQueries) {
		this.savedQueries = savedQueries;
	}

	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Boolean getRecommendationSendEmail() {
		if (recommendationSendEmail == null) {
			recommendationSendEmail = Boolean.valueOf(true);
		}
		return recommendationSendEmail;
	}

	public void setRecommendationSendEmail(Boolean recommendationSendEmail) {
		this.recommendationSendEmail = recommendationSendEmail;
	}

	public void setRecommendationPeriod(Integer recommendationPeriod) {
		this.recommendationPeriod = recommendationPeriod;
	}

	public boolean hasSecurityProfile() {
		return true;
	}

	public String toString() {
		return this.getEmail();
	}

	public boolean getIsCollectionManager() {
		return (this.getRoles().contains("collectionManager") || this
				.getIsSuperUser());
	}

	public void setIsCollectionManager(boolean tonPoulo) {
		// nothing
	}

	public boolean getIsCommunityManager() {
		return (this.getRoles().contains("communityManager") || this
				.getIsSuperUser());
	}

	public void setIsCommunityManager(boolean tonPoulo) {
		// nothing
	}

	public boolean getIsUserManager() {
		return (this.getRoles().contains("userManager") || this
				.getIsSuperUser());
	}

	public void setIsUserManager(boolean value) {
		// nothing
	}

	public boolean getIsSuperUser() {
		return this.getRoles().contains("superUser");
	}

	public void setIsSuperUser(boolean tonPoulo) {
		// nothing
	}

	public SavedQuery getFilter() {
		return filter;
	}

	public void setFilter(SavedQuery filter) {
		this.filter = filter;
	}

	public List<String> getDocumentIds() {
		return documentIds;
	}

	public void setDocumentIds(List<String> documentIds) {
		this.documentIds = documentIds;
	}

	public UserStore getStore() {
		return store;
	}

	public void setStore(UserStore store) {
		this.store = store;
	}

	public String getUserPhotoID() {
		return userPhotoID;
	}

	public void setUserPhotoID(String userPhotoID) {
		this.userPhotoID = userPhotoID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((activationId == null) ? 0 : activationId.hashCode());
		result = prime * result + (active ? 1231 : 1237);
		result = prime * result
				+ ((communities == null) ? 0 : communities.hashCode());
		result = prime * result
				+ ((documentIds == null) ? 0 : documentIds.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((filter == null) ? 0 : filter.hashCode());
		result = prime * result
				+ ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result
				+ ((institution == null) ? 0 : institution.hashCode());
		result = prime
				* result
				+ ((lastNotificationDate == null) ? 0 : lastNotificationDate
						.hashCode());
		result = prime * result
				+ ((lastname == null) ? 0 : lastname.hashCode());
		result = prime
				* result
				+ ((recommendationPeriod == null) ? 0 : recommendationPeriod
						.hashCode());
		result = prime
				* result
				+ ((recommendationSendEmail == null) ? 0
						: recommendationSendEmail.hashCode());
		result = prime * result
				+ ((recommendations == null) ? 0 : recommendations.hashCode());
		result = prime * result
				+ ((savedQueries == null) ? 0 : savedQueries.hashCode());
		result = prime * result + ((store == null) ? 0 : store.hashCode());
		result = prime * result
				+ ((userPhotoID == null) ? 0 : userPhotoID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserProfile other = (UserProfile) obj;
		if (activationId == null) {
			if (other.activationId != null)
				return false;
		} else if (!activationId.equals(other.activationId))
			return false;
		if (active != other.active)
			return false;
		if (communities == null) {
			if (other.communities != null)
				return false;
		} else if (!communities.equals(other.communities))
			return false;
		if (documentIds == null) {
			if (other.documentIds != null)
				return false;
		} else if (!documentIds.equals(other.documentIds))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		if (filter == null) {
			if (other.filter != null)
				return false;
		} else if (!filter.equals(other.filter))
			return false;
		if (firstname == null) {
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (institution == null) {
			if (other.institution != null)
				return false;
		} else if (!institution.equals(other.institution))
			return false;
		if (lastNotificationDate == null) {
			if (other.lastNotificationDate != null)
				return false;
		} else if (!lastNotificationDate.equals(other.lastNotificationDate))
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (recommendationPeriod == null) {
			if (other.recommendationPeriod != null)
				return false;
		} else if (!recommendationPeriod.equals(other.recommendationPeriod))
			return false;
		if (recommendationSendEmail == null) {
			if (other.recommendationSendEmail != null)
				return false;
		} else if (!recommendationSendEmail
				.equals(other.recommendationSendEmail))
			return false;
		if (recommendations == null) {
			if (other.recommendations != null)
				return false;
		} else if (!recommendations.equals(other.recommendations))
			return false;
		if (savedQueries == null) {
			if (other.savedQueries != null)
				return false;
		} else if (!savedQueries.equals(other.savedQueries))
			return false;
		if (store == null) {
			if (other.store != null)
				return false;
		} else if (!store.equals(other.store))
			return false;
		if (userPhotoID == null) {
			if (other.userPhotoID != null)
				return false;
		} else if (!userPhotoID.equals(other.userPhotoID))
			return false;
		return true;
	}
}