package eu.dnetlib.domain.functionality.validator;


import java.util.List;
import java.util.Map;
import java.util.Set;

public class StoredJob extends JobForValidation{
    private String contentJobStatus, usageJobStatus, started, ended, duration, error;
    private String validationType, jobType, guidelinesShortName;

    private String validationStatus;

    private int recordsTested, id, contentJobScore, usageJobScore;

    private Set<Integer> rules;
    private List<JobResultEntry> resultEntries;
    private Map<String, Integer> filteredScores;

    public StoredJob() {
        super();
    }

    public StoredJob(JobForValidation job) {
        super(job);
    }

    public Map<String, Integer> getFilteredScores() {
        return filteredScores;
    }

    public void setFilteredScores(Map<String, Integer> filteredScores) {
        this.filteredScores = filteredScores;
    }

    public List<JobResultEntry> getResultEntries() {
        return resultEntries;
    }

    public void setResultEntries(List<JobResultEntry> resultEntries) {
        this.resultEntries = resultEntries;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStarted() {
        return started;
    }

    public void setStarted(String started) {
        this.started = started;
    }

    public String getEnded() {
        return ended;
    }

    public void setEnded(String ended) {
        this.ended = ended;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Set<Integer> getRules() {
        return rules;
    }

    public void setRules(Set<Integer> rules) {
        this.rules = rules;
    }

    public String getValidationType() {
        return validationType;
    }

    public void setValidationType(String validationType) {
        this.validationType = validationType;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getContentJobStatus() {
        return contentJobStatus;
    }

    public void setContentJobStatus(String contentJobStatus) {
        this.contentJobStatus = contentJobStatus;
    }

    public String getUsageJobStatus() {
        return usageJobStatus;
    }

    public void setUsageJobStatus(String usageJobStatus) {
        this.usageJobStatus = usageJobStatus;
    }

    public int getContentJobScore() {
        return contentJobScore;
    }

    public void setContentJobScore(int contentJobScore) {
        this.contentJobScore = contentJobScore;
    }

    public int getUsageJobScore() {
        return usageJobScore;
    }

    public void setUsageJobScore(int usageJobScore) {
        this.usageJobScore = usageJobScore;
    }

    public int getRecordsTested() {
        return recordsTested;
    }

    public void setRecordsTested(int recordsTested) {
        this.recordsTested = recordsTested;
    }

    public String getGuidelinesShortName() {
        return guidelinesShortName;
    }

    public void setGuidelinesShortName(String guidelinesShortName) {
        this.guidelinesShortName = guidelinesShortName;
    }

    public String getValidationStatus() {
        return validationStatus;
    }

    public void setValidationStatus(String validationStatus) {
        this.validationStatus = validationStatus;
    }
}
