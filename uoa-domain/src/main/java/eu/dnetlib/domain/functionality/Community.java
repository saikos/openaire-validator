package eu.dnetlib.domain.functionality;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import eu.dnetlib.domain.DriverResource;

/**
 * The domain object for the Community resource data structure
 *
 */

public class Community extends DriverResource {
	private static final long serialVersionUID = -8513176941362373935L;
	
	private String name = null;
    private String description = null;
    private String owner = null;
    private Set<String> collections = new HashSet<String>();
    private Set<String> managers = new HashSet<String>();
    private Set<String> recommendations = new HashSet<String>();
    private Set<String> authoritativeDocuments = new HashSet<String>();
    
    /**
	 * The default constructor sets the resourceKind to <b>CommunityDSResources</b>, 
	 * resourceType to <b>CommunityDSResourceType</b> and dateOfCreation to the current system date.
	 */
    public Community() {
    	this.setResourceKind("CommunityDSResources");
    	this.setResourceType("CommunityDSResourceType");
    	this.setDateOfCreation(new Date());
    }

	@Deprecated
    public String getCommunityId() {
        return getResourceId();
    }

	@Deprecated
    public void setCommunityId(String id) {
        this.setResourceId(id);
    }

    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    
    public Set<String> getCollections() {
        return collections;
    }

    public void setCollections(Set<String> collections) {
        this.collections = collections;
    }

    
    public Set<String> getManagers() {
        return managers;
    }

    public void setManagers(Set<String> managers) {
        this.managers = managers;
    }
    
    private boolean equals(Community c) {
        return (this.getName() != null) 
        && (c.getName() != null)
        && (this.getName().equals(c.getName()));
    }
    
    
	public Set<String> getRecommendations() {
		return recommendations;
	}

	public void setRecommendations(Set<String> recommendations) {
		this.recommendations = recommendations;
	}

	public Set<String> getAuthoritativeDocuments() {
		return authoritativeDocuments;
	}
	
	public void setAuthoritativeDocuments(Set<String> authoritativeDocuments) {
		this.authoritativeDocuments = authoritativeDocuments;
	}
	
    @Override
    public boolean equals(Object o) {
    	if (!(o instanceof Community))
    		return false;
    	else
    		return this.equals((Community) o);
    }
    
    @Override
    public int hashCode() {
        if (this.getName() != null) {
            return this.getName().hashCode();
        } else {
            return 0;
        }
    }
    
    public String getShortDescription() {
    	String desc = null;
    	
    	if (this.description != null) {
    		if (description.length() > 100) {
    			desc = description.substring(0, 100);
    			
    			desc += "...";
    		} else {
    			desc = description;
    		}
    	}
    	
    	return desc;
    }
    
    public void setShortDescription(String value) {
    }
    
}
