package eu.dnetlib.domain.functionality;



import java.util.Comparator;

public class UserProfileComparator implements Comparator<UserProfile>{

	public int compare(UserProfile o1, UserProfile o2) {
		return o1.getEmail().compareTo(o2.getEmail());
	}
}
