package eu.dnetlib.domain.functionality;

import java.util.Locale;
import java.util.Map;

public class Searchable {
	
	public enum Type { PLAIN, DATE };
	
	private String name;
	private Type type;
	private String indexType;

	private boolean hidden;
	
	private Map<Locale,String> descriptionMap;
	private Map<Locale,String> shortDescriptionMap;
	
	private boolean inSearch;
	private boolean inRefine;
	private boolean inBrowse;
	
	private int searchRank;
	private int refineRank;
	private int browseRank;

	private String searchVocabulary;
	private String browseVocabulary;
	
	public Searchable(Type type) {
		this.name = null;
		this.type = type;
		this.indexType = null;
		
		this.descriptionMap = null;
		this.shortDescriptionMap = null;
		
		this.hidden = false;
		
		this.inSearch = false;
		this.inRefine = false;
		this.inBrowse = false;
		
		this.searchRank = -1;
		this.refineRank = -1;
		this.browseRank = -1;
		
		this.searchVocabulary = null;
		this.browseVocabulary = null;

	}
	
	@Override
	public String toString() {
		return "Searchable " + this.name + " [ \n" +
		" Type = " + this.type + "\n" +
		" Descr = " + this.descriptionMap + 
		" short description = " + this.shortDescriptionMap + " \n" +
		" Index = " + this.indexType + "\n " +
		" Search available " + this.inSearch + " and rank " + searchRank + "\n" +  
		" Refine available " + this.inRefine + " and rank " + refineRank + "\n" + 
		" Browse available " + this.inBrowse + " and rank " + browseRank + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getIndexType() {
		return indexType;
	}

	public void setIndexType(String indexType) {
		this.indexType = indexType;
	}

	public void setDescriptionMap(Map<Locale, String> descriptionMap) {
		this.descriptionMap = descriptionMap;
	}
	public Map<Locale, String> getDescriptionMap() {
		return descriptionMap;
	}

/*	public String getDescription(){
		String localeDescription = descriptionMap.get(LocaleHolder.getLocale());
		if (localeDescription != null) {
			return localeDescription;
		}
		return descriptionMap.get(new Locale("en", "GB"));
	}
*/
	
	public Map<Locale, String> getShortDescriptionMap() {
		return shortDescriptionMap;
	}

	public void setShortDescriptionMap(Map<Locale, String> shortDescriptionMap) {
		this.shortDescriptionMap = shortDescriptionMap;
	}

/*	public String getShortDescription() {
		String localeDescription = shortDescriptionMap.get(LocaleHolder.getLocale());
		if (localeDescription != null) {
			return localeDescription;
		}
		return shortDescriptionMap.get(new Locale("en", "GB"));
	}
*/

	public boolean isInSearch() {
		return inSearch;
	}

	public void setInSearch(boolean inSearch) {
		this.inSearch = inSearch;
	}

	public boolean isInRefine() {
		return inRefine;
	}

	public void setInRefine(boolean inRefine) {
		this.inRefine = inRefine;
	}

	public boolean isInBrowse() {
		return inBrowse;
	}

	public void setInBrowse(boolean inBrowse) {
		this.inBrowse = inBrowse;
	}

	public int getSearchRank() {
		return searchRank;
	}

	public void setSearchRank(int searchRank) {
		this.searchRank = searchRank;
	}

	public int getRefineRank() {
		return refineRank;
	}

	public void setRefineRank(int refineRank) {
		this.refineRank = refineRank;
	}

	public int getBrowseRank() {
		return browseRank;
	}

	public void setBrowseRank(int browseRank) {
		this.browseRank = browseRank;
	}

	public String getSearchVocabulary() {
		return searchVocabulary;
	}

	public void setSearchVocabulary(String searchVocabulary) {
		this.searchVocabulary = searchVocabulary;
	}

	public String getBrowseVocabulary() {
		return browseVocabulary;
	}

	public void setBrowseVocabulary(String browseVocabulary) {
		this.browseVocabulary = browseVocabulary;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public boolean isHidden() {
		return hidden;
	}
	
}
