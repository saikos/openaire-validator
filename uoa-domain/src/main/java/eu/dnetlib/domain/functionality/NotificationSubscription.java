package eu.dnetlib.domain.functionality;

import java.net.URL;

/**
 * This class represents a subscription of an alert service to a notification query. 
 * @author thanos@di.uoa.gr
 * @see eu.dnetlib.api.functionality.NotificationService
 * @see eu.dnetlib.functionality.notification.executor.NotificationQueryExecutor
 * @see NotificationQuery
 * @see NotificationSchedule
 * @see NotificationEvent
 * @see NotificationResult
 * 
 */
public class NotificationSubscription implements Comparable<NotificationSubscription> {
	private String queryId;
	private URL alertService;
	private boolean enabled;
	
	/**
	 * Construct a new notification subscription.
	 * @param queryId the unique identifier of the query of this subscription
	 * @param alertService the URL of the alert service of this subscription
	 * @param enabled true if this subscription is currently enabled, false if it currently disabled
	 */
	public NotificationSubscription(final String queryId, final URL alertService, final boolean enabled) {
		this.queryId = queryId;
		this.alertService = alertService;
		this.enabled = enabled;
	}
		
	/**
	 * Construct a new notification subscription with query identifier and alert service set to null and enabled set to false.
	 */
	public NotificationSubscription() {
		this(null, null, false);
	}
		
	/**
	 * Get the query of this subscription.
	 * @return the unique identifier of the query of this subscription
	 */
	public String getQueryId() {
		return queryId;
	}
	
	/**
	 * Set the query of this subscription.
	 * @param queryId the unique identifier of the query of this subscription
	 */
	public void setQueryId(final String queryId) {
		this.queryId = queryId;
	}
	
	/**
	 * Get the alert service of this subscription.
	 * @return the URL of the alert service of this subscription
	 */
	public URL getAlertService() {
		return alertService;
	}
	
	/**
	 * Set the alert service of this subscription.
	 * @param alertService the URL of the alert service of this subscription
	 */
	public void setAlertService(final URL alertService) {
		this.alertService = alertService;
	}
	
	/**
	 * Check if this subscription is enabled.
	 * @return true if this subscription is currently enabled, false otherwise
	 */
	public boolean isEnabled() {
		return enabled;
	}
	
	/**
	 * Set the enabled status of this subscription.
	 * @param enabled true to enable this subscription, false to disable it
	 */
	public void setEnabled(final boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public int compareTo(final NotificationSubscription subscription) {
		final int queryIdComparison = queryId.compareTo(subscription.queryId);
		final int alertServiceComparison = alertService.toString().compareTo(subscription.alertService.toString());
		return (queryIdComparison == 0) ? alertServiceComparison : queryIdComparison;
	}

	@Override
	public boolean equals(final Object object) {
		if (!(object instanceof NotificationSubscription))
			return false;
		final NotificationSubscription subscription = (NotificationSubscription) object;
		return ((queryId == null) ? (subscription.queryId == null) : queryId.equals(subscription.queryId)) && ((alertService == null) ? (subscription.alertService == null) : alertService.equals(subscription.alertService));
	}
	
	@Override
	public int hashCode() {
		return ((queryId == null) ? 0 : queryId.hashCode()) + ((alertService == null) ? 0 : alertService.hashCode());
	}
	
	@Override
	public String toString() {
		return "(query: " + queryId + ", alert service: " + alertService + ")";
	}
}
