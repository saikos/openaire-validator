package eu.dnetlib.domain.functionality;

import java.util.Locale;
import java.util.Map;

public abstract class DriverUrlDisplayType extends UrlDisplayType{

	private String action;
	private String parameter;
	
	public DriverUrlDisplayType(String field, String action, 
			String parameter) {
		super(field);
		setAction(action);
		setParameter(parameter);
	}
	
	public DriverUrlDisplayType(String field, Map<Locale, String> descriptionMap, String action, 
			String parameter) {
		super(field, descriptionMap);
		setAction(action);
		setParameter(parameter);
	}
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

}
