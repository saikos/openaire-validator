package eu.dnetlib.domain.functionality;

import eu.dnetlib.domain.SearchCriteria;
import eu.dnetlib.domain.SearchCriteriaImpl;


/**
 * The search criteria class for looking community profiles
 *
 */
public class CommunitySearchCriteria extends SearchCriteriaImpl implements
		SearchCriteria {

	public boolean matches(Object o) {
		if (!(o instanceof Community))
			return false;
		
		Community community = (Community) o;
		
		if (this.getContains() != null) {
			if (community.getName() == null || 
					!community.getName().toLowerCase().contains(this.getContains().toLowerCase()))
				return false;
		}
		
		if (this.getStartsWith() != null) {
			if (community.getName() == null || 
					!community.getName().toLowerCase().startsWith(this.getStartsWith().toLowerCase()))
				return false;
		}
		
		if (this.getEndsWith() != null) {
			if (community.getName() == null || 
					!community.getName().toLowerCase().endsWith(this.getEndsWith().toLowerCase()))
				return false;
		}
		
		return true;
	}
	
	public boolean equals(Object o) {
		return super.equals(o);
	}
	
	public int hashCode() {
		return super.hashCode();
	}
}

