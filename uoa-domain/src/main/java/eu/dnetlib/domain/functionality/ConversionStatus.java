package eu.dnetlib.domain.functionality;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ConversionStatus {
		
	public enum Status {
		ON_QUEUE, IN_PROGRESS, CANCELLED, COMPLETED;
	}
	
	private String conversionId = null;
	private String fileUrl = null;
	private String format = null;
	private String convertedFileUrl = null;
	private Status status = null;
	
	public ConversionStatus() { }

	/**
	 * Returns the conversion id
	 * @return
	 */
	public String getConversionId() {
		return conversionId;
	}
	public void setConversionId(String conversionId) {
		this.conversionId = conversionId;
	}
	
	/**
	 * Returns the url where the file can be found (the jumpoff url 
	 * or the url of the source)
	 * @return
	 */
	public String getFileUrl() {
		return fileUrl;
	}
	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}
	
	/**
	 * Returns the desired format
	 * @return
	 */
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	
	/**
	 * Returns the url where the converted file is.
	 * @return
	 */
	public String getConvertedFileUrl() {
		return convertedFileUrl;
	}
	public void setConvertedFileUrl(String convertedFileUrl) {
		this.convertedFileUrl = convertedFileUrl;
	}
	
	/**
	 * Returns the status of the conversion job. Can be ON_QUEUE,
	 * IN_PROCESS, CANCELLED, COMPLETED.
	 * @return
	 */
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
}
