package eu.dnetlib.domain.functionality;

/**
 * This class represents an execution schedule for a notification query. 
 * @author thanos
 * @see eu.dnetlib.api.functionality.NotificationService
 * @see eu.dnetlib.functionality.notification.executor.NotificationQueryExecutor
 * @see NotificationQuery
 * @see NotificationEvent
 * @see NotificationResult
 * @see NotificationSubscription
 *
 */
public class NotificationSchedule implements Comparable<NotificationSchedule> {
	private String queryId;
	private Float triggerThreshold;
	private boolean percentileThreshold;
	private long executionPeriod;
	private boolean enabled;
	
	/**
	 * Construct a new notification schedule.
	 * @param queryId the unique identifier of the query of this schedule
	 * @param triggerThreshold the trigger threshold of this schedule or null if no threshold applies
	 * @param percentileThreshold true if trigger threshold is percentile threshold, false otherwise
	 * @param executionPeriod the execution period of this schedule expressed in minutes
	 * @param enabled true if this topic is currently enabled, false if it currently disabled
	 */
	public NotificationSchedule(final String queryId, final Float triggerThreshold, final boolean percentileThreshold, final long executionPeriod, final boolean enabled) {
		this.queryId = queryId;
		this.triggerThreshold = triggerThreshold;
		this.percentileThreshold = percentileThreshold;
		this.executionPeriod = executionPeriod;
		this.enabled = enabled;
	}
	
	/**
	 * Construct a new notification query with query identifier and trigger threshold set to null, percentile threshold and enabled set to false and execution period set to zero.
	 */
	public NotificationSchedule() {
		this(null, null, false, 0L, false);
	}

	/**
	 * Get the query of this schedule.
	 * @return the unique identifier of the query of this schedule
	 */
	public String getQueryId() {
		return queryId;
	}

	/**
	 * Set the query of this schedule.
	 * @param queryId the unique identifier of the query of this schedule
	 */
	public void setQueryId(final String queryId) {
		this.queryId = queryId;
	}

	/**
	 * Get the trigger threshold of this schedule.
	 * @return the trigger threshold of this schedule
	 */
	public Float getTriggerThreshold() {
		return triggerThreshold;
	}

	/**
	 * Set the trigger threshold of this schedule.
	 * @param triggerThreshold the the trigger threshold of this schedule
	 */
	public void setTriggerThreshold(final Float triggerThreshold) {
		this.triggerThreshold = triggerThreshold;
	}

	/**
	 * Check if the trigger threshold of this schedule  is percentile.
	 * @return true if the trigger threshold of this topic is percentile, false otherwise
	 */
	public boolean isPercentileThreshold() {
		return percentileThreshold;
	}

	/**
	 * Set the percentile status of the trigger threshold of this schedule.
	 * @param percentileThreshold true to set a percentile trigger threshold to this schedule, false otherwise
	 */
	public void setPercentileThreshold(final boolean percentileThreshold) {
		this.percentileThreshold = percentileThreshold;
	}

	/**
	 * Get the execution period of this schedule.
	 * @return the the execution period of this schedule
	 */
	public long getExecutionPeriod() {
		return executionPeriod;
	}

	/**
	 * Set the execution period of this schedule.
	 * @param executionPeriod the execution period of this schedule
	 */
	public void setExecutionPeriod(final long executionPeriod) {
		this.executionPeriod = executionPeriod;
	}

	/**
	 * Check if this schedule is enabled.
	 * @return true if this schedule is currently enabled, false otherwise
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Set the enabled status of this schedule.
	 * @param enabled true to enable this schedule, false to disable it
	 */
	public void setEnabled(final boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public int compareTo(final NotificationSchedule schedule) {
		return queryId.compareTo(schedule.queryId);
	}
	@Override
	public boolean equals(final Object object) {
		if (!(object instanceof NotificationSchedule))
			return false;
		final NotificationSchedule schedule = (NotificationSchedule) object;
		return (queryId == null) ? (schedule.queryId == null) : queryId.equals(schedule.queryId);
	}
	
	@Override
	public int hashCode() {
		return (queryId == null) ? 0 : queryId.hashCode();
	}
	
	@Override
	public String toString() {
		return queryId;
	}
}
