package eu.dnetlib.domain.functionality;

import eu.dnetlib.domain.SearchCriteria;
import eu.dnetlib.domain.SearchCriteriaImpl;

public class WebInterfaceSearchCriteria extends SearchCriteriaImpl implements
		SearchCriteria {

	private String layoutName = null;

	public boolean equals(Object o) {
		return this.equals((WebInterfaceSearchCriteria) o);
	}

	public boolean equals(WebInterfaceSearchCriteria criteria) {
		boolean equal = super.equals(criteria);

		if (layoutName != null)
			equal |= this.layoutName.equals(criteria.layoutName);

		return equal;
	}

	@Override
	public boolean matches(Object o) {
		WebInterfaceLayout layout = (WebInterfaceLayout) o;

		if (this.getLayoutName() != null)
			return layout.getName().equals(this.getLayoutName());

		return true;
	}

	public String getLayoutName() {
		return layoutName;
	}

	public void setLayoutName(String layoutName) {
		this.layoutName = layoutName;
	}

}
