package eu.dnetlib.domain.functionality;

import eu.dnetlib.domain.SearchCriteria;
import eu.dnetlib.domain.SearchCriteriaImpl;

public class MDFormatSearchCriteria extends SearchCriteriaImpl implements 
	SearchCriteria {

	String MDFormatName = null;
	
	public String getMDFormatName() {
		return MDFormatName;
	}

	public void setMDFormatName(String formatName) {
		MDFormatName = formatName;
	}

	@Override
	public boolean matches(Object o) {
		throw new UnsupportedOperationException();
	}

}