package eu.dnetlib.domain.functionality;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class represents a forum thread.
 * @author thanos@di.uoa.gr
 *
 */
public class Thread {
	private long threadId;
	private String communityId;
	private String userId;
	private Date creationDate;
	private String topic;
	private List<Post> posts = new ArrayList<Post>();
	
	/**
	 * Default constructor.
	 *
	 */
	public Thread() {}
	
	/**
	 * Full constructor.
	 * @param threadId the id of this thread
	 * @param communityId the id of the community this thread belongs to
	 * @param userId the id of the user that created this thread
	 * @param creationDate the date that this thread was created
	 * @param topic the topic of this thread
	 */
	public Thread(long threadId, String communityId, String userId, Date creationDate, String topic) {
		this.threadId = threadId;
		this.communityId = communityId;
		this.userId = userId;
		this.creationDate = creationDate;
		this.topic = topic;
	}
	
	/**
	 * Constructor used for creating a new thread; sets thread id to null and creation date to current date.
	 * @param communityId the id of the community this thread belongs to
	 * @param userId the id of the user that created this thread
	 * @param topic the topic of this thread
	 */
	public Thread(String communityId, String userId, String topic) {
		this(0L, communityId, userId, new Date(), topic);
	}
	
	/**
	 * Get the thread id.
	 * @return the id of this thread
	 */
	public long getThreadId() {
		return threadId;
	}
	
	/**
	 * Set the thread id.
	 * @param threadId the new id of this thread
	 */
	public void setThreadId(long threadId) {
		this.threadId = threadId;
	}
	
	/**
	 * Get the community id.
	 * @return the id of the community this thread belongs to
	 */
	public String getCommunityId() {
		return communityId;
	}
	
	/**
	 * Set the community id.
	 * @param communityId the id of the new community this thread belongs to
	 */
	public void setCommunityId(String communityId) {
		this.communityId = communityId;
	}
	
	/**
	 * Get the user id.
	 * @return the id of the user that created this thread
	 */
	public String getUserId() {
		return userId;
	}
	
	/**
	 * Set the user id.
	 * @param userId the id of the new user that created this thread
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	/**
	 * Get the creation date.
	 * @return the date that this thread was created
	 */
	public Date getCreationDate() {
		return creationDate;
	}
	
	/**
	 * Set the creation date.
	 * @param creationDate the new date that this thread was created
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	/**
	 * Get the topic.
	 * @return the topic of this thread
	 */
	public String getTopic() {
		return topic;
	}
	
	/**
	 * Set the topic.
	 * @param topic the new topic of this thread
	 */
	public void setTopic(String topic) {
		this.topic = topic;
	}
	
	/**
	 * Get the posts.
	 * @return the posts of this thread
	 */
	public List<Post> getPosts() {
		return posts;
	}
	
	/**
	 * Set the posts.
	 * @param posts the new posts of this thread
	 */
	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}
}
