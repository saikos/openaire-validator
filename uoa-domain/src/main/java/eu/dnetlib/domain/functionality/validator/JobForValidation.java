package eu.dnetlib.domain.functionality.validator;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class JobForValidation{
	private String officialName, baseUrl, userEmail, validationSet, datasourceId,
			interfaceId, desiredCompatibilityLevel, activationId, repoType,
			interfaceIdOld, groupByXpath, metadataPrefix;
	private int records;
	
	private boolean registration,updateExisting;

	private boolean cris = false ;
	private boolean crisReferentialChecks;
	
	private Set<String> selectedCrisEntities;
	private Set<Integer> selectedContentRules;
	private Set<Integer> selectedUsageRules;
	
	private List<String> adminEmails = new ArrayList<String>();

	public JobForValidation() {
		super();
	}

	public JobForValidation(JobForValidation oldJob) {
		super();
		this.setBaseUrl(oldJob.getBaseUrl());
		this.setRecords(oldJob.getRecords());
		this.setValidationSet(oldJob.getValidationSet());
		this.setGroupByXpath(oldJob.getGroupByXpath());
		this.setActivationId(oldJob.getActivationId());
		this.setMetadataPrefix(oldJob.getMetadataPrefix());
		this.setUserEmail(oldJob.getUserEmail());
		this.setDesiredCompatibilityLevel(oldJob.getDesiredCompatibilityLevel());
	}
	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public String getMetadataPrefix() {
		return metadataPrefix;
	}

	public void setMetadataPrefix(String metadataPrefix) {
		this.metadataPrefix = metadataPrefix;
	}

	public String getGroupByXpath() {
		return groupByXpath;
	}
	
	public void setGroupByXpath(String groupByXpath) {
		this.groupByXpath = groupByXpath;
	}

	public boolean isCris() {
		return cris;
	}

	public void setCris(boolean cris) {
		this.cris = cris;
	}

	public boolean isCrisReferentialChecks() {
		return crisReferentialChecks;
	}

	public void setCrisReferentialChecks(boolean crisReferentialChecks) {
		this.crisReferentialChecks = crisReferentialChecks;
	}

	public Set<String> getSelectedCrisEntities() {
		return selectedCrisEntities;
	}

	public void setSelectedCrisEntities(Set<String> selectedCrisEntities) {
		this.selectedCrisEntities = selectedCrisEntities;
	}

	public String getInterfaceIdOld() {
		return interfaceIdOld;
	}

	public void setInterfaceIdOld(String interfaceIdOld) {
		this.interfaceIdOld = interfaceIdOld;
	}

	public boolean isRegistration() {
		return registration;
	}

	public void setRegistration(boolean registration) {
		this.registration = registration;
	}

	public boolean isUpdateExisting() {
		return updateExisting;
	}

	public void setUpdateExisting(boolean updateExisting) {
		this.updateExisting = updateExisting;
	}

	public String getOfficialName() {
		return officialName;
	}

	public void setOfficialName(String officialName) {
		this.officialName = officialName;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}


	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getValidationSet() {
		return validationSet;
	}

	public void setValidationSet(String validationSet) {
		this.validationSet = validationSet;
	}

	public String getDatasourceId() {
		return datasourceId;
	}

	public void setDatasourceId(String datasourceId) {
		this.datasourceId = datasourceId;
	}

	public String getInterfaceId() {
		return interfaceId;
	}

	public void setInterfaceId(String interfaceId) {
		this.interfaceId = interfaceId;
	}


	public String getDesiredCompatibilityLevel() {
		return desiredCompatibilityLevel;
	}

	public void setDesiredCompatibilityLevel(String desiredCompatibilityLevel) {
		this.desiredCompatibilityLevel = desiredCompatibilityLevel;
	}

	public String getActivationId() {
		return activationId;
	}

	public void setActivationId(String activationId) {
		this.activationId = activationId;
	}

	public String getRepoType() {
		return repoType;
	}

	public void setRepoType(String repoType) {
		this.repoType = repoType;
	}

	public List<String> getAdminEmails() {
		return adminEmails;
	}

	public void setAdminEmails(List<String> adminEmails) {
		this.adminEmails = adminEmails;
	}

	public Set<Integer> getSelectedUsageRules() {
		return selectedUsageRules;
	}

	public void setSelectedUsageRules(Set<Integer> selectedUsageRules) {
		this.selectedUsageRules = selectedUsageRules;
	}

	public Set<Integer> getSelectedContentRules() {
		return selectedContentRules;
	}

	public void setSelectedContentRules(Set<Integer> selectedContentRules) {
		this.selectedContentRules = selectedContentRules;
	}

	
}