package eu.dnetlib.domain.functionality;

import java.util.Date;

import eu.dnetlib.domain.DriverServiceProfile;

public class RecommendationServiceProfile extends DriverServiceProfile {

	private static final long serialVersionUID = 131769612060109646L;

	public RecommendationServiceProfile() {
		super("ServiceResources", "RecommendationServiceResourceType", new Date());
	}

}
