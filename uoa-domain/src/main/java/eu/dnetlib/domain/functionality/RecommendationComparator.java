package eu.dnetlib.domain.functionality;

import java.util.Comparator;

public class RecommendationComparator implements Comparator<Recommendation> {
    
    public int compare( Recommendation o1, Recommendation o2 ) {
        return o1.getDateOfCreation().compareTo( o2.getDateOfCreation() );
    }
    
}
