package eu.dnetlib.domain.functionality;

import java.util.Date;

import eu.dnetlib.domain.DriverServiceProfile;

public class UserProfileServiceProfile extends DriverServiceProfile {
	private static final long serialVersionUID = 1003330679801258669L;

	public UserProfileServiceProfile() {
		super("ServiceResources", "UserInterfaceServiceResourceType", new Date());
	}

}
