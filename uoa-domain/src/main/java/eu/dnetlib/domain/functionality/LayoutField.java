package eu.dnetlib.domain.functionality;

public class LayoutField {
	
	private String name = null;
	private String xpath = null;
	private String type = null;
	private boolean indexable = false;
	private boolean result = false;
	private boolean stat = false;
	private boolean tokenizable = false;
	
	public LayoutField() {
		super();
	}
	
	public boolean isIndexable() {
		return indexable;
	}
	public void setIndexable(boolean indexable) {
		this.indexable = indexable;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public boolean isStat() {
		return stat;
	}
	public void setStat(boolean stat) {
		this.stat = stat;
	}
	public boolean isTokenizable() {
		return tokenizable;
	}
	public void setTokenizable(boolean tokenizable) {
		this.tokenizable = tokenizable;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getXpath() {
		return xpath;
	}
	public void setXpath(String xpath) {
		this.xpath = xpath;
	}
}
