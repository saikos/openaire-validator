package eu.dnetlib.domain.functionality;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import eu.dnetlib.domain.DriverResource;

@SuppressWarnings("serial")
public class WebInterfaceLayout extends DriverResource {

	private String name;
	private String mdFormatName;
	private String indexLayoutName;
	private String cssFile;
	private Locale defaultLocale;
	
	private List<Locale> supportedLocales;
	
	private boolean viewCollections;
	private boolean viewCommunities;
	private boolean viewRepositories;
	private boolean viewAnnouncements;
	private boolean viewUserProfile;
	private boolean viewSimilarDocuments;
	private boolean viewEPublications;
	
	private List<Searchable> searchFields;
	
	private List<String> baseCollections;
	
	private Map<String, DocumentDescription> documentDescriptions;
	
	public WebInterfaceLayout(String name, 
			String mdFormatName, String indexLayoutName) {
		super();
		this.name = name;
		this.mdFormatName = mdFormatName;
		this.indexLayoutName = indexLayoutName;
		this.searchFields = new ArrayList<Searchable>();
		
		viewCollections = true;
		viewCommunities = true;
		viewRepositories = true;
		viewAnnouncements = true;
		viewSimilarDocuments = true;
		viewUserProfile = true;
		setViewEPublications(true);
		setDocumentDescriptions(new HashMap<String, DocumentDescription>());
		
		baseCollections = new ArrayList<String>();
		supportedLocales = new ArrayList<Locale>();
		
		this.setResourceKind("WebInterfaceLayoutResources");
		this.setResourceType("WebInterfaceLayoutResourceType");
		this.setDateOfCreation(new Date());
	}

	public String getIndexLayoutName() {
		return indexLayoutName;
	}

	public void setIndexLayoutName(String indexLayoutName) {
		this.indexLayoutName = indexLayoutName;
	}

	public String getMdFormatName() {
		return mdFormatName;
	}

	public void setMdFormatName(String mdFormatName) {
		this.mdFormatName = mdFormatName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<Searchable> getSearchFields() {
		return searchFields;
	}

	public void setSearchFields(List<Searchable> searchFields) {
		this.searchFields = searchFields;
	}

	public String getCssFile() {
		return cssFile;
	}

	public void setCssFile(String cssFile) {
		this.cssFile = cssFile;
	}

	public void setBaseCollections(List<String> baseCollections) {
		this.baseCollections = baseCollections;
	}

	public List<String> getBaseCollections() {
		return baseCollections;
	}

	public boolean isViewCollections() {
		return viewCollections;
	}

	public void setViewCollections(boolean viewCollections) {
		this.viewCollections = viewCollections;
	}

	public boolean isViewCommunities() {
		return viewCommunities;
	}

	public void setViewCommunities(boolean viewCommunities) {
		this.viewCommunities = viewCommunities;
	}

	public boolean isViewRepositories() {
		return viewRepositories;
	}

	public void setViewRepositories(boolean viewRepositories) {
		this.viewRepositories = viewRepositories;
	}

	public boolean isViewAnnouncements() {
		return viewAnnouncements;
	}

	public void setViewAnnouncements(boolean viewAnnouncements) {
		this.viewAnnouncements = viewAnnouncements;
	}

	public Map<String, DocumentDescription> getDocumentDescriptions() {
		return documentDescriptions;
	}

	public void setDocumentDescriptions(
			Map<String, DocumentDescription> documentDescriptions) {
		this.documentDescriptions = documentDescriptions;
	}

	public void setViewUserProfile(boolean viewUserProfile) {
		this.viewUserProfile = viewUserProfile;
	}

	public boolean isViewUserProfile() {
		return viewUserProfile;
	}

	public void setViewSimilarDocuments(boolean viewSimilarDocuments) {
		this.viewSimilarDocuments = viewSimilarDocuments;
	}

	public boolean isViewSimilarDocuments() {
		return viewSimilarDocuments;
	}

	public void setDefaultLocale(Locale defaultLocale) {
		this.defaultLocale = defaultLocale;
	}

	public Locale getDefaultLocale() {
		return defaultLocale;
	}

	public void setSupportedLocales(List<Locale> supportedLocales) {
		this.supportedLocales = supportedLocales;
	}

	public List<Locale> getSupportedLocales() {
		return supportedLocales;
	}

	public void setViewEPublications(boolean viewEPublications) {
		this.viewEPublications = viewEPublications;
	}

	public boolean isViewEPublications() {
		return viewEPublications;
	}
}
