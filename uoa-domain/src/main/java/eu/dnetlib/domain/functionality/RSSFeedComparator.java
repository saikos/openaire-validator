package eu.dnetlib.domain.functionality;

import java.util.Comparator;

public class RSSFeedComparator implements Comparator<RSSFeed> {

	public int compare( RSSFeed o1, RSSFeed o2 ) {
        return o1.getDateOfCreation().compareTo( o2.getDateOfCreation() );
    }
    	
}

