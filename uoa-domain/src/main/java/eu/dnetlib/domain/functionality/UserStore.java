package eu.dnetlib.domain.functionality;

public class UserStore {
	
	private String storeServiceURL;
    private String storeID;
    
	public String getStoreServiceURL() {
		return storeServiceURL;
	}
	public void setStoreServiceURL(String storeServiceURL) {
		this.storeServiceURL = storeServiceURL;
	}
	public String getStoreID() {
		return storeID;
	}
	public void setStoreID(String storeID) {
		this.storeID = storeID;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((storeID == null) ? 0 : storeID.hashCode());
		result = prime * result
				+ ((storeServiceURL == null) ? 0 : storeServiceURL.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserStore other = (UserStore) obj;
		if (storeID == null) {
			if (other.storeID != null)
				return false;
		} else if (!storeID.equals(other.storeID))
			return false;
		if (storeServiceURL == null) {
			if (other.storeServiceURL != null)
				return false;
		} else if (!storeServiceURL.equals(other.storeServiceURL))
			return false;
		return true;
	}
}
