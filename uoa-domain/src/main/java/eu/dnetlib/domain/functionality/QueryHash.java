package eu.dnetlib.domain.functionality;

import java.util.Date;
import java.util.Hashtable;

import eu.dnetlib.domain.DriverResource;

@SuppressWarnings("serial")
public class QueryHash extends DriverResource {
	private String userId;
	private int hashValue;
	private String cqlQuery;	
	private Hashtable<String, Boolean> queryResults;
	
	public QueryHash() {
		this.setResourceKind("QueryHashDSResources");
		this.setResourceType("QueryHashDSResourceType");
		this.setDateOfCreation( new Date() );
		userId = "";
		hashValue = -1;
		this.queryResults = new Hashtable<String,Boolean >();
	}
	
	@Deprecated
	public String getId() {		
		return this.getResourceId();
	}

	@Deprecated
	public void setId( String id ) {
		this.setResourceId(id);
	}
	
	public String getCqlQuery() {
		return this.cqlQuery;
	}
	
	public void setCqlQuery( String query ) {
		this.cqlQuery = query;
	}
	
	public String getUserId() {
		return this.userId;
	}

	public void setUserId( String id ) {
		this.userId = id;
	}

	public int getHashValue() {
		return this.hashValue;
	}

	public void setHashValue( int value ) {
		this.hashValue = value;
	}

	public Hashtable<String, Boolean> getQueryResults() {
		return queryResults;
	}

	public void setQueryResults(Hashtable<String, Boolean> queryResults) {
		this.queryResults = queryResults;
	}
}

