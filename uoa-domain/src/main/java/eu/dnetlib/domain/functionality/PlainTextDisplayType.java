package eu.dnetlib.domain.functionality;

import java.util.Locale;
import java.util.Map;

public class PlainTextDisplayType extends DisplayType {
	
	public PlainTextDisplayType(String field) {
		super(field);
	}
	
	public PlainTextDisplayType(String field, Map<Locale, String> descriptionMap) {
		super(field);
		setDescriptionMap(descriptionMap);
	}
	
/*	@Override
	public int getDisplayMessage(StringBuilder builder, Document document, 
			String fieldValue) {
		String finalValue = replaceFieldValueExpression(document, getDescription(), fieldValue);
		builder.append(finalValue);
		return finalValue.length();
	}

	@Override
	int getDisplayMessage(StringBuilder builder, Document document,
			String fieldValue, String secondaryLink) {
		String finalValue = replaceFieldValueExpression(document, secondaryLink, fieldValue);
		builder.append(finalValue);
		return finalValue.length();
	}
*/	
}
