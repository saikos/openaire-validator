package eu.dnetlib.domain.functionality;

import java.util.Date;
import java.util.HashMap;

import eu.dnetlib.domain.DriverResource;

public class RSSFeed extends DriverResource {
	private static final long serialVersionUID = -8055604767449235236L;
	   
	private String usedId;
	private String queryCql;
	private HashMap<String,Boolean> documentIds = new HashMap<String,Boolean>(); 
	private Date lastTimeUsed;
	
    /**
     * The default constructor sets the resourceKind to <b>RSSFeedDSResources</b>, 
     * resourceType to <b>RSSFeedDSResourceType</b> and dateOfCreation to the current system date.
     */
    public RSSFeed() {
        this.setResourceKind("RSSFeedDSResources");
        this.setResourceType("RSSFeedDSResourceType");
        this.setDateOfCreation(new Date());        
    }

	public HashMap<String, Boolean> getDocumentIds() {
		return documentIds;
	}

	public void setDocumentIds(HashMap<String, Boolean> documentIds) {
		this.documentIds = documentIds;
	}

	public Date getLastTimeUsed() {
		return lastTimeUsed;
	}

	public void setLastTimeUsed(Date lastTimeUsed) {
		this.lastTimeUsed = lastTimeUsed;
	}

	public String getQueryCql() {
		return queryCql;
	}

	public void setQueryCql(String queryCql) {
		this.queryCql = queryCql;
	}

	public String getUsedId() {
		return usedId;
	}

	public void setUsedId(String usedId) {
		this.usedId = usedId;
	}
        
    
    
}