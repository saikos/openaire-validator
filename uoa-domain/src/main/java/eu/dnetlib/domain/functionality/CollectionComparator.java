package eu.dnetlib.domain.functionality;

import java.util.Comparator;

public class CollectionComparator implements Comparator<Collection> {

	public int compare(Collection o1, Collection o2) {
		return o1.getName().compareToIgnoreCase(o2.getName());
	}
}
