package eu.dnetlib.domain.functionality;

import java.util.Locale;
import java.util.Map;

public abstract class UrlDisplayType extends DisplayType{

	public UrlDisplayType(String field) {
		super(field);
	}
	
	public UrlDisplayType(String field, Map<Locale, String> descriptionMap) {
		super(field, descriptionMap);
	}
	
}
