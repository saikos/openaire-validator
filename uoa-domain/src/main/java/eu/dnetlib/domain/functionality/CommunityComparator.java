package eu.dnetlib.domain.functionality;

import java.util.Comparator;

/**
 * This class implements a comparator for communities.
 * @author thanos@di.uoa.gr
 *
 */
public class CommunityComparator implements Comparator<Community> {

	/**
	 * Compare two communities by name ignoring case.
	 * @param community0 the first community to be compared
	 * @param community1 the second community to be compared
	 * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second
	 */
	@Override
	public int compare(Community community0, Community community1) {
		return community0.getName().compareToIgnoreCase(community1.getName());
	}
	
}
