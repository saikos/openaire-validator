package eu.dnetlib.domain.functionality;

import java.util.List;

public class UpdateCollectionType {

	protected Boolean cContainer;
	protected List<Record1> cDesc;
	protected Boolean cFrozen;
	protected String cImage;
	protected String cMemCond;
	protected String cName;
	protected String cOwner;
	protected Boolean cPrivate;
	protected String cSource;
	protected String cSubject;
	protected Boolean cVisible;

	public Boolean getCContainer() {
		return cContainer;
	}

	public void setCContainer(Boolean container) {
		cContainer = container;
	}

	public List<Record1> getCDesc() {
		return cDesc;
	}

	public void setCDesc(List<Record1> desc) {
		cDesc = desc;
	}

	public Boolean getCFrozen() {
		return cFrozen;
	}

	public void setCFrozen(Boolean frozen) {
		cFrozen = frozen;
	}

	public String getCImage() {
		return cImage;
	}

	public void setCImage(String image) {
		cImage = image;
	}

	public String getCMemCond() {
		return cMemCond;
	}

	public void setCMemCond(String memCond) {
		cMemCond = memCond;
	}

	public String getCName() {
		return cName;
	}

	public void setCName(String name) {
		cName = name;
	}

	public String getCOwner() {
		return cOwner;
	}

	public void setCOwner(String owner) {
		cOwner = owner;
	}

	public Boolean getCPrivate() {
		return cPrivate;
	}

	public void setCPrivate(Boolean private1) {
		cPrivate = private1;
	}

	public String getCSource() {
		return cSource;
	}

	public void setCSource(String source) {
		cSource = source;
	}

	public String getCSubject() {
		return cSubject;
	}

	public void setCSubject(String subject) {
		cSubject = subject;
	}

	public Boolean getCVisible() {
		return cVisible;
	}

	public void setCVisible(Boolean visible) {
		cVisible = visible;
	}
}
