package eu.dnetlib.domain.functionality;

import java.util.Locale;
import java.util.Map;

public class SearchDisplayType extends DriverUrlDisplayType {

	private String label;
	
	public SearchDisplayType(String field, String label) {
		super(field, "showResults", "query");
		this.setLabel(label);
	}
	
	public SearchDisplayType(String field, String label, Map<Locale, String> descriptionMap) {
		super(field, descriptionMap, "showResults", "query");
		this.setLabel(label);
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
	/*	private StringBuilder generateHref(String fieldValue, StringBuilder builder) {
		return builder.append("<a class=\"searchlink\" href='")
				.append(this.getAction()).append(".action?")
				.append(this.getParameter()).append("=(").append(getLabel())
				.append("=").append("\"").append(fieldValue).append("\"")
				.append(")'>");
	}

	@Override
	public int getDisplayMessage(StringBuilder builder, Document document,
			String fieldValue) {
		String finalValue = replaceFieldValueExpression(document, getDescription(), fieldValue);
		builder = generateHref(fieldValue, builder);
		builder.append(finalValue);
		builder.append("</a>");

		return finalValue.length();
	}

	@Override
	int getDisplayMessage(StringBuilder builder, Document document,
			String fieldValue, String secondaryLink) {
		String finalValue = replaceFieldValueExpression(document,
				secondaryLink, fieldValue);
		builder = generateHref(fieldValue, builder);
		builder.append(finalValue);
		builder.append("</a>");

		return finalValue.length();
	}
*/
}
