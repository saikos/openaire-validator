package eu.dnetlib.domain.functionality;

import java.util.List;

/**
 * This class represents a single page of results which is part of a multi-page sequence.
 * @author thanos
 * @see eu.dnetlib.api.functionality.NotificationService
 * @see eu.dnetlib.functionality.notification.executor.NotificationQueryExecutor
 *
 */
public class ResultPage {
	private int limit;
	private int offset;
	private String[] columns;
	private List<String[]> rows;
	
	/**
	 * Construct a new result page.
	 * @param limit the limit of this result page
	 * @param offset the offset of this result page
	 * @param columns the column names of this result page
	 * @param rows the rows of this result page
	 */
	public ResultPage(final int limit, final int offset, final String[] columns, final List<String[]> rows) {
		this.limit = limit;
		this.offset = offset;
		this.columns = columns;
		this.rows = rows;
	}
	
	/**
	 * Construct a new result page with limit and offset set to zero and columns and rows set to null.
	 */
	public ResultPage() {
		this(0, 0, null, null);
	}
	
	/**
	 * Get the limit.
	 * @return the limit of this result page
	 */
	public int getLimit() {
		return limit;
	}
	
	/**
	 * Set the limit.
	 * @param limit the limit of this result page
	 */
	public void setLimit(final int limit) {
		this.limit = limit;
	}
	
	/**
	 * Get the offset.
	 * @return the offset of this result page
	 */
	public int getOffset() {
		return offset;
	}
	
	/**
	 * Set the offset.
	 * @param offset the offset of this result page
	 */
	public void setOffset(final int offset) {
		this.offset = offset;
	}
	
	/**
	 * Get the columns.
	 * @return the column names of this result page
	 */
	public String[] getColumns() {
		return columns;
	}
	
	/**
	 * Set the columns.
	 * @param columns the column names of this result page 
	 */
	public void setColumns(final String[] columns) {
		this.columns = columns;
	}
	
	/**
	 * Get the rows.
	 * @return the list containing the rows of this result page
	 */
	public List<String[]> getRows() {
		return rows;
	}
	
	/**
	 * Set the rows.
	 * @param rows the list containing the rows of this result page
	 */
	public void setRows(final List<String[]> rows) {
		this.rows = rows;
	}
}
