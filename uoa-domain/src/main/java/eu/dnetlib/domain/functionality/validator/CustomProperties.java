package eu.dnetlib.domain.functionality.validator;


import java.util.HashMap;
import java.util.Map;

/**
 * Created by nikonas on 29/3/16.
 */
public class CustomProperties{

    private Map<String, String> properties;

    public CustomProperties() {
        this.properties = new HashMap<String, String>();
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }

    public void setProperty(String key, String value) {
        this.properties.put(key, value);
    }

    public String getProperty(String key) {
        return this.properties.get(key);
    }

    public Boolean containsProperty(String key) {
        return this.properties.containsKey(key);
    }

}
