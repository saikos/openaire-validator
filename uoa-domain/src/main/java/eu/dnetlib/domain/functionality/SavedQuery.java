package eu.dnetlib.domain.functionality;

import java.util.ArrayList;
import java.util.List;

public class SavedQuery {
	private int id = 0;
	private String cqlText = null;
	private String refineText = null;
	private List<String> collectionIds = new ArrayList<String>();
	private List<String> communityIds = new ArrayList<String>();
	private List<String> hashValues = new ArrayList<String>();
	private boolean alertOnChange = false;
	
	public boolean isAlertOnChange() {
		return alertOnChange;
	}
	public void setAlertOnChange(boolean alertOnChange) {
		this.alertOnChange = alertOnChange;
	}

	public List<String> getHashValues() {
		return hashValues;
	}
	public void setHashValues(List<String> hashValues) {
		this.hashValues = hashValues;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public List<String> getCollectionIds() {
		return collectionIds;
	}
	public void setCollectionIds(List<String> collectionIds) {
		this.collectionIds = collectionIds;
	}
	public List<String> getCommunityIds() {
		return communityIds;
	}
	public void setCommunityIds(List<String> communityIds) {
		this.communityIds = communityIds;
	}
	public String getCqlText() {
		return cqlText;
	}
	public void setCqlText(String cqlText) {
		this.cqlText = cqlText;
	}
	public String getRefineText() {
		return refineText;
	}
	public void setRefineText(String refineText) {
		this.refineText = refineText;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (alertOnChange ? 1231 : 1237);
		result = prime * result
				+ ((collectionIds == null) ? 0 : collectionIds.hashCode());
		result = prime * result
				+ ((communityIds == null) ? 0 : communityIds.hashCode());
		result = prime * result + ((cqlText == null) ? 0 : cqlText.hashCode());
		result = prime * result
				+ ((hashValues == null) ? 0 : hashValues.hashCode());
		result = prime * result + id;
		result = prime * result
				+ ((refineText == null) ? 0 : refineText.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SavedQuery other = (SavedQuery) obj;
		if (alertOnChange != other.alertOnChange)
			return false;
		if (collectionIds == null) {
			if (other.collectionIds != null)
				return false;
		} else if (!collectionIds.equals(other.collectionIds))
			return false;
		if (communityIds == null) {
			if (other.communityIds != null)
				return false;
		} else if (!communityIds.equals(other.communityIds))
			return false;
		if (cqlText == null) {
			if (other.cqlText != null)
				return false;
		} else if (!cqlText.equals(other.cqlText))
			return false;
		if (hashValues == null) {
			if (other.hashValues != null)
				return false;
		} else if (!hashValues.equals(other.hashValues))
			return false;
		if (id != other.id)
			return false;
		if (refineText == null) {
			if (other.refineText != null)
				return false;
		} else if (!refineText.equals(other.refineText))
			return false;
		return true;
	}
}
