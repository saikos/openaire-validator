package eu.dnetlib.domain.functionality;

import java.util.Date;

/**
 * This class represents a result of a notification event.
 * @author thanos@di.uoa.gr
 * @see eu.dnetlib.api.functionality.NotificationService
 * @see eu.dnetlib.functionality.notification.executor.NotificationQueryExecutor
 * @see NotificationQuery
 * @see NotificationSchedule
 * @see NotificationEvent
 * @see NotificationSubscription
 *
 */
public class NotificationResult implements Comparable<NotificationResult> {
	private String queryId;
	private Date date;
	private String resultId;
	private String name;
	private int value;
	
	/**
	 * Construct a new notification result.
	 * @param queryId the unique identifier of the query of the event of this result
	 * @param date the date of the event of this result
	 * @param resultId the unique identifier of this result
	 * @param name the name of this result
	 * @param value the value of this result
	 */
	public NotificationResult(final String queryId, final Date date, final String resultId, final String name, final int value) {
		this.queryId = queryId;
		this.date = date;
		this.resultId = resultId;
		this.name = name;
		this.value = value;
	}
	
	/**
	 * Construct a new notification result with query identifier, date, result identifier and result name set to null and value set to zero.
	 */
	public NotificationResult() {
		this(null, null, null, null, 0);
	}
	
	/**
	 * Get the query of the event of this result.
	 * @return the unique identifier of the query of the event of this result
	 */
	public String getQueryId() {
		return queryId;
	}
	
	/**
	 * Set the query of the event of this result.
	 * @param queryId the unique identifier of the query of the event of this result
	 */
	public void setQueryId(final String queryId) {
		this.queryId = queryId;
	}

	/**
	 * Get the date of the event of this result.
	 * @return the date of the event of this result
	 */
	public Date getDate() {
		return date;
	}
	
	/**
	 * Set the date of the event of this result.
	 * @param date the date of the event of this result
	 */
	public void setDate(final Date date) {
		this.date = date;
	}
	
	/**
	 * Get the result identifier.
	 * @return the unique identifier of this result
	 */
	public String getResultId() {
		return resultId;
	}
	
	/**
	 * Set the result identifier.
	 * @param resultId the unique identifier of this result
	 */
	public void setResultId(final String resultId) {
		this.resultId = resultId;
	}
	
	/**
	 * Get the name.
	 * @return the name of this result
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Set the name of this result.
	 * @param name the name of this result
	 */
	public void setName(final String name) {
		this.name = name;
	}
		
	/**
	 * Get the value of this result.
	 * @return the value of this result
	 */
	public int getValue() {
		return value;
	}
		
	/**
	 * Set the value of this result.
	 * @param value the value of this result
	 */
	public void setValue(final int value) {
		this.value = value;
	}
	
	@Override
	public int compareTo(final NotificationResult result) {
		final int queryIdComparison = queryId.compareTo(result.queryId);
		final int dateComparison = date.compareTo(result.date);
		final int resultIdComparison = resultId.compareTo(result.resultId);
		return (queryIdComparison == 0) ? ((dateComparison == 0) ? resultIdComparison : dateComparison) : queryIdComparison;
	}
	
	@Override
	public boolean equals(final Object object) {
		if (!(object instanceof NotificationResult))
			return false;
		final NotificationResult result = (NotificationResult) object;
		return ((queryId == null) ? (result.queryId == null) : queryId.equals(result.queryId)) && ((date == null) ? (result.date == null) : date.equals(result.date)) &&
				((resultId == null) ? (result.resultId == null) : resultId.equals(result.resultId));
	}
	
	@Override
	public int hashCode() {
		return ((queryId == null) ? 0 : queryId.hashCode()) + ((date == null) ? 0 : date.hashCode()) + ((resultId == null) ? 0 : resultId.hashCode());
	}
	
	@Override
	public String toString() {
		return "(query: " + queryId + ", date: " + date + ", result: " + resultId + ")";
	}
}
