package eu.dnetlib.domain.functionality;

import java.util.Date;

/**
 * This class represents a forum post.
 * @author thanos@di.uoa.gr
 *
 */
public class Post implements Comparable<Post> {
	private long postId;
	private String userId;
	private Date creationDate;
	private String content;

	/**
	 * Default constructor.
	 *
	 */
	public Post() {}
	
	/**
	 * Full constructor.
	 * @param postId the id of this post
	 * @param userId the id of the user that created this post
	 * @param creationDate the date that this post was created
	 * @param content the content of this post
	 */
	public Post(long postId, String userId, Date creationDate, String content) {
		this.postId = postId;
		this.userId = userId;
		this.creationDate = creationDate;
		this.content = content;
	}
	
	/**
	 * Constructor used for creating a new post; sets post id to null and creation date to current date.
	 * @param userId the id of the user that created this post
	 * @param content the content of this post
	 */
	public Post(String userId, String content) {
		this(0L, userId, new Date(), content);
	}
	
	/**
	 * Get the post id.
	 * @return the id of this post
	 */
	public long getPostId() {
		return postId;
	}

	/**
	 * Set the post id.
	 * @param postId the new id of this post
	 */
	public void setPostId(long postId) {
		this.postId = postId;
	}
	
	/**
	 * Get the user id.
	 * @return the id of the user that created this post
	 */
	public String getUserId() {
		return userId;
	}
	
	/**
	 * Set the user id.
	 * @param userId the id of the new user that created this post
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	/**
	 * Get the creation date.
	 * @return the date that this post was created
	 */
	public Date getCreationDate() {
		return creationDate;
	}
	
	/**
	 * Set the creation date.
	 * @param creationDate the new date that this post was created
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	/**
	 * Get the content.
	 * @return the content of this post
	 */
	public String getContent() {
		return content;
	}
	
	/**
	 * Set the content.
	 * @param content the new content of this post
	 */
	public void setContent(String content) {
		this.content = content;
	}

	public int compareTo(Post post) {
		return -post.getCreationDate().compareTo(creationDate);
	}
}
