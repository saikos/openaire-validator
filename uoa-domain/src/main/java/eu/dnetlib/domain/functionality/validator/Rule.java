package eu.dnetlib.domain.functionality.validator;


public class Rule{

	// for XML//
	public static String XPATH = "xpath";
	public static String SUCCESS = "success";

	// for cardinality rule
	public static String GREATER_THAN = "greater_than";
	public static String LESS_THAN = "less_than";

	// for regular expresion
	public static String REGEXP = "regexp";

	// for vocabulary
	public static String TERMS = "terms";

	private int id;
	private String name = null;
	private String description = null;
	private String type = null;
	private boolean mandatory = false;
	private int weight = 0;
	private String provider_information = null;
	private String job_type = null;
	private String entity_type = null;
	private boolean for_cris = false;

//	protected Properties pros;
//
//	public Rule() {
//	}
//
//	public Properties getConfiguration() {
//		return pros;
//	}
//
//	public void setConfiguration(Properties pros) {
//		this.pros = pros;
//	}

	protected CustomProperties pros;

	public Rule() {
	}

	public CustomProperties getConfiguration() {
		return pros;
	}

	public void setConfiguration(CustomProperties pros) {
		this.pros = pros;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isMandatory() {
		return mandatory;
	}

	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	public boolean isFor_cris() {
		return for_cris;
	}

	public void setFor_cris(boolean for_cris) {
		this.for_cris = for_cris;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getProvider_information() {
		return provider_information;
	}

	public void setProvider_information(String provider_information) {
		this.provider_information = provider_information;
	}

	public String getJob_type() {
		return job_type;
	}

	public void setJob_type(String job_type) {
		this.job_type = job_type;
	}

	public String getEntity_type() {
		return entity_type;
	}

	public void setEntity_type(String entity_type) {
		this.entity_type = entity_type;
	}

}
