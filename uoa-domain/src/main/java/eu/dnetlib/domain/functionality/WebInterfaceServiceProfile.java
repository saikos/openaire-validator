package eu.dnetlib.domain.functionality;

import java.util.Date;

import eu.dnetlib.domain.DriverServiceProfile;

public class WebInterfaceServiceProfile extends DriverServiceProfile {

	private static final long serialVersionUID = -6866028072976712196L;

	public WebInterfaceServiceProfile() {
		super("ServiceResources", "UserInterfaceServiceResourceType",
				new Date());
	}

}
