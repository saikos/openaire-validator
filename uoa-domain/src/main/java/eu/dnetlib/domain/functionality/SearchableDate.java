package eu.dnetlib.domain.functionality;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

public class SearchableDate extends Searchable{
	
	private TreeMap<Integer, Map<Locale,String>> periodBackMap = null; 
	private int fromYear = -1;
	private int toYear = -1;

	public SearchableDate() {
		super(Type.DATE);
		setPeriodBackMap(new TreeMap<Integer, Map<Locale, String>>());
	}
	
	public int getFromYear() {
		return fromYear;
	}

	public void setFromYear(int fromYear) {
		this.fromYear = fromYear;
	}

	public int getToYear() {
		return toYear;
	}

	public void setToYear(int toYear) {
		this.toYear = toYear;
	}
	
	public TreeMap<Integer, Map<Locale, String>> getPeriodBackMap() {
		return periodBackMap;
	}

	public void setPeriodBackMap(TreeMap<Integer, Map<Locale, String>> periodBackMap) {
		this.periodBackMap = periodBackMap;
	}

	/*	public TreeMap<Integer, String> getPeriodBack() {
		TreeMap<Integer, String> periodBack = new TreeMap<Integer, String>();
		for (Integer integer:periodBackMap.keySet()){
			String description = periodBackMap.get(integer).get(LocaleHolder.getLocale());
			if (description == null) {
				description = periodBackMap.get(integer).get(new Locale("en", "GB"));
			}
			periodBack.put(integer, description);
		}
		
		return periodBack;
	}

	public long[] getRanges(){
		if (getPeriodBack().values() == null || 
				getPeriodBack().values().isEmpty()) {
			return null;
		}
		
		long[] ranges = new long[getPeriodBack().keySet().size()];
		int i = 0;
		for (Integer range:getPeriodBack().keySet()){
			ranges[i] = range;
			i++;
		}
		
		return ranges;
	}
*/	
	public List<String> getYears() {
		List<String> years = new ArrayList<String>();
		
		for (int i=fromYear; i< toYear+1; i++){
			years.add(i+"");
		}	
		
		return years;
	}

/*	@Override
	public String toString() {
		return  super.toString() +
		", for years " +  getYears() + ", period_back: " +
		getPeriodBack();
	}
*/	
}
