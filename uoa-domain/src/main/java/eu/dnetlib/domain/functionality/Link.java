package eu.dnetlib.domain.functionality;

public class Link {
	
	public enum Type { SEARCH, EXTERNAL, DRIVER };
	
	private Type type = null;
	private String description = null;
	private String value = null;
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public Type getType() {
		return type;
	}
	
	public void setType(Type type) {
		this.type = type;
	}

}
