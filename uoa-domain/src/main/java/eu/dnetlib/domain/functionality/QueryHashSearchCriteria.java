package eu.dnetlib.domain.functionality;

import eu.dnetlib.domain.SearchCriteria;
import eu.dnetlib.domain.SearchCriteriaImpl;


public class QueryHashSearchCriteria extends SearchCriteriaImpl implements
		SearchCriteria {
	
	private String cqlQuery = null;
	private String userId = null;
	private int hashValue = 0;	

	public void setUserId( String id ){
		this.userId = id;
	}

	public String getUserId(){
		return userId;
	}
	
	public void setHashValue( int val ){
		this.hashValue = val;
	}
	
	public int getHashValue(){
		return this.hashValue;
	}
	
	public boolean matches(Object o) {
		QueryHash queryHash = (QueryHash) o;
		
		if ( this.getUserId() != null ) {
			if ( queryHash.getUserId() == null ){ 					
				return false;
			}else if ( this.getUserId().equals( queryHash.getUserId() ) == true ) {
				return true;
			} else {
				return false;
			}
		}	
		
		if ( this.getCqlQuery() != null ) {
			if ( queryHash.getCqlQuery() == null ){ 					
				return false;
			}else if ( this.getCqlQuery().equals( queryHash.getCqlQuery() ) == true ) {
				return true;
			} else {
				return false;
			}
		}
				
		return true;
	}

	public boolean equals(Object o) {
		return this.equals((QueryHashSearchCriteria) o);
	}
	
	public boolean equals(QueryHashSearchCriteria crit) {
		if (!super.equals(crit))
			return false;
		
		if( this.userId != null && crit.getUserId() != null ) {
			if( this.userId.endsWith( crit.getUserId() ) ){
				if( this.hashValue == crit.getHashValue() ){
					return true;
				}else{
					return false;
				}
			} else {
				return false;
			}
		} else if( this.userId == null && crit.getUserId() == null ){
			if( this.hashValue == crit.getHashValue() ){
				return true;
			}else{
				return false;
			}
		}else 			
			return false;
	}
	
	public int hashCode() {
		int code = super.hashCode();		
		
		if (userId != null)
			code |= userId.hashCode();
		
		code |= this.hashValue;
		
		return code;
	}

	public void setCqlQuery(String cqlQuery) {
		this.cqlQuery = cqlQuery;
	}

	public String getCqlQuery() {
		return cqlQuery;
	}
}

