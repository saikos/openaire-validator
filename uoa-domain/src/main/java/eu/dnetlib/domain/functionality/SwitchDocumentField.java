package eu.dnetlib.domain.functionality;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import eu.dnetlib.domain.data.Document;

public class SwitchDocumentField extends DocumentField {

	private String conditionField;
	private Map<String, DocumentField> documentFieldMap = new HashMap<String, DocumentField>();
	private Logger logger = Logger.getLogger(SwitchDocumentField.class);

	public SwitchDocumentField(String conditionField) {
		this.conditionField = conditionField;
	}

	public void addSwitchDisplayField(String conditionFieldValue, DocumentField documentField) {
		documentFieldMap.put(conditionFieldValue, documentField);
	}
	
	public DocumentField getDocumentField(String conditionFieldValue){
		return  documentFieldMap.get(conditionFieldValue);
	}
	
	@Override
	public String getCssClass(Document document) {
		List<String> fieldValues = document.getMap().get(conditionField);
		if (fieldValues != null) {
			String fieldValue = fieldValues.get(0);
			if (fieldValue != null) {
				DocumentField documentField = documentFieldMap.get(fieldValue);
				return documentField.getCssClass();
			}
			logger.warn("FieldValues were empty for document");
		}
		logger.warn("FieldValues were null for document");
		return null;
	}
	
/*	@Override
	public String getDescription(Document document) {
		List<String> fieldValues = document.getMap().get(conditionField);
		if (fieldValues != null) {
			String fieldValue = fieldValues.get(0);
			if (fieldValue != null) {
				DocumentField documentField = documentFieldMap.get(fieldValue);
				return documentField.getDescription(); 
			}
			logger.warn("FieldValues were empty for document");
		}
		logger.warn("FieldValues were null for document");
		return null;		
	}
	
	@Override
	public List<List<String>> getMessage(Document document) {	
		List<String> fieldValues = document.getMap().get(conditionField);
		if (fieldValues != null) {
			String fieldValue = fieldValues.get(0);
			if (fieldValue != null) {
				DocumentField documentField = documentFieldMap.get(fieldValue);
				return documentField.getMessage(document);
			}
			logger.warn("FieldValues were empty for document");
		}
		logger.warn("FieldValues were null for document");
		return new ArrayList<List<String>>();
	}
*/
	public void setConditionField(String conditionField) {
		this.conditionField = conditionField;
	}

	public String getConditionField() {
		return conditionField;
	}

	public Map<String, DocumentField> getDocumentFieldMap() {
		return documentFieldMap;
	}
	
}
