package eu.dnetlib.domain.functionality;

import java.util.Locale;
import java.util.Map;

public class InternalUrlDisplayType extends DriverUrlDisplayType{
	
	public InternalUrlDisplayType(String field, Map<Locale, String> descriptionMap, 
			String action, String parameter) {
		super(field, descriptionMap, action, parameter);
	}

/*	private StringBuilder generateHref(String fieldValue, StringBuilder builder) {
		return builder.append("<a href=\"").append(this.getAction())
						.append(".action?").append(this.getParameter())
						.append("=").append(fieldValue).append("\">");
	}

	@Override
	public int getDisplayMessage(StringBuilder builder, Document document, 
			String fieldValue) {
		String finalValue = replaceFieldValueExpression(document,
				getDescription(), fieldValue);
		builder = generateHref(fieldValue, builder);
		builder.append(finalValue);
		builder.append("</a>");

		return finalValue.length();
			
	}

	@Override
	int getDisplayMessage(StringBuilder builder, Document document,
			String fieldValue, String secondaryLink) {
		String finalValue = replaceFieldValueExpression(document,
				secondaryLink, fieldValue);
		builder = generateHref(fieldValue, builder);
		builder.append(finalValue);
		builder.append("</a>");

		return finalValue.length();
		
	}
*/
}
