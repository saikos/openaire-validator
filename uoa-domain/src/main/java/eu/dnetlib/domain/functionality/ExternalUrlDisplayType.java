package eu.dnetlib.domain.functionality;

import java.util.Locale;
import java.util.Map;

public class ExternalUrlDisplayType extends DriverUrlDisplayType {
	
	public ExternalUrlDisplayType(String field) {
		super(field, "redirect", "url");
	}
	
	public ExternalUrlDisplayType(String field, Map<Locale, String> descriptionMap) {
		super(field, descriptionMap, "redirect", "url");
	}

/*	private StringBuilder generateHref(String fieldValue, StringBuilder builder) {
		String encodedFieldValue = null;
		try {
			encodedFieldValue = URLEncoder.encode(fieldValue, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			encodedFieldValue = fieldValue;
		}
		return builder.append("<a class=\"externallink\" href=\"").append(this.getAction())
						.append(".action?").append(this.getParameter())
						.append("=").append(encodedFieldValue)
						.append("\" target=\"_blank\"")
						.append(">");
	}

	@Override
	public int getDisplayMessage(StringBuilder builder, Document document, 
			String fieldValue) {
		String finalValue = replaceFieldValueExpression(document,
				getDescription(), fieldValue);
		builder = generateHref(fieldValue, builder);
		builder.append(finalValue);
		builder.append("</a>");

		return finalValue.length();
		
	}

	@Override
	int getDisplayMessage(StringBuilder builder, Document document,
			String fieldValue, String secondaryLink) {
		String finalValue = replaceFieldValueExpression(document,
				secondaryLink, fieldValue);
		builder = generateHref(fieldValue, builder);
		builder.append(finalValue);
		builder.append("</a>");

		return finalValue.length();
	}
	
	*/
}
