package eu.dnetlib.domain;




public class DriverServiceProfileSearchCriteria extends SearchCriteriaImpl
	implements SearchCriteria {
	
	private String resourceType = null;
	private String resourceKind = null;
	private Boolean includePending = null;
	private String parentId = null;
	private String parentType = null;
	private String resourceUri = null;

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getParentType() {
		return parentType;
	}

	public void setParentType(String parentType) {
		this.parentType = parentType;
	}

	public String getResourceUri() {
		return resourceUri;
	}

	public void setResourceUri(String resourceUri) {
		this.resourceUri = resourceUri;
	}

	public Boolean getIncludePending() {
		return includePending;
	}

	public void setIncludePending(Boolean includePending) {
		this.includePending = includePending;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String serviceType) {
		this.resourceType = serviceType;
	}

	public boolean matches(Object o) {
		throw new UnsupportedOperationException();
	}

	public String getResourceKind() {
		return resourceKind;
	}

	public void setResourceKind(String resourceKind) {
		this.resourceKind = resourceKind;
	}
}
