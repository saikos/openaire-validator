package eu.dnetlib.domain.enabling;

import eu.dnetlib.domain.EPR;

/**
 * Represents a subscription used for communication with the IS_SN
 * 
 * @author christos
 * 
 */
public class Subscription {
	private String id = null;
	private String topic = null;
	private int timeToLive = 0;
	private EPR epr = null;

	public EPR getEpr() {
		return epr;
	}

	public void setEpr(EPR epr) {
		this.epr = epr;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public int getTimeToLive() {
		return timeToLive;
	}

	public void setTimeToLive(int timeToLive) {
		this.timeToLive = timeToLive;
	}

	public String toString() {
		return topic;
	}
}
