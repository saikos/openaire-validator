package eu.dnetlib.domain.enabling;

public class ListResourceProfilesType {

	protected String format;
	protected String resourceType;

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}
}
