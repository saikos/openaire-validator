package eu.dnetlib.domain.enabling;

import eu.dnetlib.domain.SearchCriteria;

public class VocabularySearchCriteria implements SearchCriteria {
	private String name = null;

	public VocabularySearchCriteria() {
	}
	
	public VocabularySearchCriteria(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean matches(Object o) {
		Vocabulary vocab = (Vocabulary) o;

		if (this.name != null) {
			return this.name.equals(vocab.getName());
		}
		
		return true;
	}
}