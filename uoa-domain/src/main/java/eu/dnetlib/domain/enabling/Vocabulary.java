package eu.dnetlib.domain.enabling;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import eu.dnetlib.domain.DriverResource;

/**
 * 
 * The domain object for the Vocabulary resource data structure
 * 
 */
@SuppressWarnings("serial")
public class Vocabulary extends DriverResource {
	private String name = null;
	private Map<String, String> nameMap = null;
	private Map<String, String> encodingMap = null;
	
	public Vocabulary(String name, Map<String, String> nameMap) {
		this.setResourceType("VocabularyDSResourceType");
		this.setResourceKind("VocabularyDSResources");
		
		this.name = name;
		initializeMaps(nameMap);
	}
	
	public Vocabulary(String name, Map<String, String> nameMap, Map<String, String> encodingMap) {
		this.setResourceType("VocabularyDSResourceType");
		this.setResourceKind("VocabularyDSResources");
		
		this.name = name;
		this.nameMap = nameMap;
		this.encodingMap = encodingMap;	
	}
	
	public void initializeMaps(Map<String, String> nameMap) {
		this.nameMap = nameMap;
		this.encodingMap = new TreeMap<String, String>();
		for (String name : nameMap.keySet()) {
			encodingMap.put(nameMap.get(name), name);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEncoding(String englishName) {
		return nameMap.get(englishName);
	}
	
	public String getEnglishName(String encoding) {
		return encodingMap.get(encoding);
	}
	
	public List<String> getEnglishNames() {
		return new ArrayList<String>(this.nameMap.keySet());
	}
	
	public List<String> getEncodings() {
		return new ArrayList<String>(this.encodingMap.keySet());
	}

	public Map<String, String> getAsMap() { return this.encodingMap; }
}
