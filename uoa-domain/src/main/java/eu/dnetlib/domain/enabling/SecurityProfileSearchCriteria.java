package eu.dnetlib.domain.enabling;

import java.util.ArrayList;
import java.util.List;

import eu.dnetlib.domain.SearchCriteria;
import eu.dnetlib.domain.SearchCriteriaImpl;

public class SecurityProfileSearchCriteria extends SearchCriteriaImpl implements
		SearchCriteria {

	private List<String> driverResourceIds = new ArrayList<String>();

	public boolean matches(Object o) {
		if (!(o instanceof SecurityProfile)) {
			return false;
		}

		SecurityProfile profile = (SecurityProfile) o;

		if (driverResourceIds != null) {
			for (String driverResourceId : driverResourceIds)
				if (driverResourceId != null) {
					if (profile.getDriverResourceId() == null
							|| !profile.getDriverResourceId().equals(
									driverResourceId))
						return false;
				}
		}

		return true;
	}

	public List<String> getDriverResourceIds() {
		return driverResourceIds;
	}

	public void setDriverResourceIds(List<String> driverResourceIds) {
		this.driverResourceIds = driverResourceIds;
	}
}