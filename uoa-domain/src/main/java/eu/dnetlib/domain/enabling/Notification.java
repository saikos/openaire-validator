package eu.dnetlib.domain.enabling;

import java.io.ByteArrayInputStream;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import eu.dnetlib.domain.ActionType;
import eu.dnetlib.domain.ResourceType;

public class Notification {
	private static Logger logger = Logger.getLogger(Notification.class);
	
	private String subscriptionId = null;
	private String message = null;
	private String topic = null;
	private String isId = null;
	private Date date = new Date();
	
	private ActionType actionType = null;
	private ResourceType resourceType = null;
	private String resource = null;
	private String resourceId = null;
	
	public Notification(String subscriptionId, String message, String topic,
			String isId) throws Exception  {
		super();
		
		this.subscriptionId = subscriptionId;
		this.message = message;
		this.topic = topic;
		this.isId = isId;
		
		this.resource = message;
		this.resourceId = this.getField(message, "RESOURCE_IDENTIFIER");
		this.resourceType = ResourceType.valueOf(ResourceType.class, this.getField(message, "RESOURCE_TYPE").toUpperCase());
		this.actionType = ActionType.valueOf(topic.split("\\.")[0]);
	}

	public Notification(String subscriptionId, String resource,
			String resourceId, ResourceType resourceType, ActionType actionType) {
		this.resource = resource;
		this.resourceId = resourceId;
		this.resourceType = resourceType;
		this.actionType = actionType;
		
		this.subscriptionId = subscriptionId;
		this.message = resource;
		this.topic = actionType.getValue() + "." + resourceType.getValue();
	}

	public String getSubscriptionId() {
		return subscriptionId;
	}

	public String getMessage() {
		return message;
	}

	public String getTopic() {
		return topic;
	}

	public String getIsId() {
		return isId;
	}

	public Date getDate() {
		return date;
	}

	public ActionType getActionType() {
		return actionType;
	}

	public ResourceType getResourceType() {
		return resourceType;
	}

	public String getResource() {
		return resource;
	}

	public String getResourceId() {
		return resourceId;
	}

	private String getField(String xml, String fieldName) throws Exception {
		ByteArrayInputStream bis = new ByteArrayInputStream(xml.getBytes());
		DocumentBuilderFactory domFactory = DocumentBuilderFactory
				.newInstance();

		domFactory.setNamespaceAware(true); // never forget this!

		try {
			DocumentBuilder builder = domFactory.newDocumentBuilder();
			Document doc = builder.parse(bis);
			XPathFactory factory = XPathFactory.newInstance();
			XPath xpath = factory.newXPath();

			XPathExpression expression = xpath.compile("//*[local-name()='"
					+ fieldName + "']");

			Node node = (Node) expression.evaluate(doc, XPathConstants.NODE);
			String value = node.getAttributes().getNamedItem("value").getTextContent();
			
			logger.debug("Field : " + fieldName + ":" + value);

			return value;
		} catch (ParserConfigurationException e) {
			throw new Exception("XML Parser configuration Error", e);
		}
	}
}