package eu.dnetlib.domain.enabling;

import eu.dnetlib.domain.DriverResource;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * The domain object for the SecurityProfile resource data structure
 * 
 */
public class SecurityProfile extends DriverResource{
	private static final long serialVersionUID = 6533097295469183688L;

	private String driverResourceId;
	private List<String> identities = new ArrayList<String>();
	private String password = null;

	public SecurityProfile() {
		this.setResourceKind("SecurityProfileDSResources");
		this.setResourceType("SecurityProfileDSResourceType");
	}

	@Deprecated
	public String getId() {
		return this.getResourceId();
	}

	@Deprecated
	public void setId(String id) {
		this.setResourceId(id);
	}

	public List<String> getIdentities() {
		return identities;
	}

	public void setIdentities(List<String> identities) {
		this.identities = identities;
	}

	public String getDriverResourceId() {
		return driverResourceId;
	}

	public void setDriverResourceId(String recourceId) {
		this.driverResourceId = recourceId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((driverResourceId == null) ? 0 : driverResourceId.hashCode());
		result = prime * result
				+ ((identities == null) ? 0 : identities.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SecurityProfile other = (SecurityProfile) obj;
		if (driverResourceId == null) {
			if (other.driverResourceId != null)
				return false;
		} else if (!driverResourceId.equals(other.driverResourceId))
			return false;
		if (identities == null) {
			if (other.identities != null)
				return false;
		} else if (!identities.equals(other.identities))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		return true;
	}
}