package eu.dnetlib.domain.enabling;

public class PreparePullRSType {

	protected Integer keepAliveTime;
	protected String styleSheet;
	protected Integer total;

	public Integer getKeepAliveTime() {
		return keepAliveTime;
	}

	public void setKeepAliveTime(Integer keepAliveTime) {
		this.keepAliveTime = keepAliveTime;
	}

	public String getStyleSheet() {
		return styleSheet;
	}

	public void setStyleSheet(String styleSheet) {
		this.styleSheet = styleSheet;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

}
